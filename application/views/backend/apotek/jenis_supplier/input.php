<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">INPUT DATA JENIS SUPPLIER</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/jenis_supplier/input" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div hidden="" class="form-group">
            <label class="control-label">
                ID Jenis Supplier <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text"  name="id_jenis_supplier" readonly placeholder="Masukan Id Jenis Supplier" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Jenis Supplier <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Nama Jenis Supplier" name="nama_jenis_supplier" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
               Ket <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan keterangan" name="keterangan" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>
