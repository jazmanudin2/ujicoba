<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT DATA JENIS PRODUK</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/jenis_produk/edit" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID Jenis Supplier <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" name="id_jenis_produk" value="<?php echo $jenis_produk['id_jenis_produk'] ?>" readonly placeholder="Masukan Id Jenis Produk" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Jenis Supplier <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" required="" placeholder="Masukan Nama Jenis" value="<?php echo $jenis_produk['nama_jenis_produk'] ?>" name="nama_jenis_produk" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
               Ket <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan keterangan" value="<?php echo $jenis_produk['keterangan'] ?>" name="keterangan" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>
