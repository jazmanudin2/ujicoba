<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">INPUT DATA GOL OBAT</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/gol_obat/input" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div hidden="" class="form-group">
            <label class="control-label">
                ID Gol Obat <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text"  name="id_gol_obat" readonly placeholder="Masukan Id Gol Obat" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Gol Obat <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Nama Jenis" name="nama_gol_obat" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
               Ket <span class="symbol "></span>
           </label>
           <input  autocomplete="off" type="text" placeholder="Masukan keterangan" name="keterangan" class="form-control" >
       </div>
       <div>
        <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
    </div>
</form>
</div>
