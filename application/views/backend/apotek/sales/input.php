<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">INPUT DATA SALES</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/sales/input" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID Sales <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" name="id_sales"  value="<?php echo $kodeunik;?>" readonly placeholder="Masukan Id Sales" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Sales <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Nama Sales" required name="nama_sales" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Alamat <span class="symbol "></span>
            </label>
            <textarea autocomplete="off" type="text" name="alamat" class="form-control" ></textarea>
        </div>
        <div class="form-group">
            <label class="control-label">
                Telp <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Telp1" name="telp_sales" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>



<script>
    $(document).ready(function(){

        $("#provinsi").change(function(){

            id = $("#provinsi").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>apotek/Sales/get_kota',
                data  : {id_provinsi:id},
                cache : false,
                success:function(respond){

                    $("#kabupaten").html(respond);
                }
                
            });
            
        });
        
        
        
        $("#kabupaten").change(function(){

            id = $("#kabupaten").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>apotek/Sales/get_kec',
                data  : {id_kota:id},
                cache : false,
                success:function(respond){

                    $("#kecamatan").html(respond);
                }
                
            });
            
        });
        
        
        $("#kecamatan").change(function(){

            id = $("#kecamatan").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>apotek/Sales/get_desa',
                data  : {id_kecamatan:id},
                cache : false,
                success:function(respond){

                    $("#desa").html(respond);
                }
                
            });
            
        });
        
        

        $("#form").validate({
            rules: {
                id_jenis_sales: {
                    required: true
                },
                nama_sales: {
                    required: true
                },
                owner_sales: {
                    required: true
                },
                penanggungjwb: {
                    required: true
                },
                telp_penanggungjwb: {
                    required: true,
                    number: true
                },
                alamat: {
                    required: true
                },
                provinsi: {
                    required: true
                },
                kota: {
                    required: true
                },
                kecamatan: {
                    required: true
                },
                kelurahan: {
                    required: true
                },
                telp1: {
                    required: true,
                    number: true
                },
                telp2: {
                    number: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                id_jenis_sales: {
                    required: "Opps, Jenis Apotek Tidak Boleh Kosong..!",
                },
                nama_sales: {
                    required: "Opps, Nama Apotek Tidak Boleh Kosong..!",
                },
                owner_sales: {
                    required: "Opps, Owner Apotek Tidak Boleh Kosong..!",
                },
                penanggungjwb: {
                    required: "Opps, Penanggung Jawab Tidak Boleh Kosong..!",
                },
                telp_penanggungjwb: {
                    required: "Opps, Telp Penanggung Jawab Tidak Boleh Kosong..!",
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                alamat: {
                    required: "Opps, Alamat Tidak Boleh Kosong..!",
                },
                provinsi: {
                    required: "Opps, Provinsi Tidak Boleh Kosong..!",
                },
                kota: {
                    required: "Opps, Kota Tidak Boleh Kosong..!",
                },
                kecamatan: {
                    required: "Opps, Kecamatan Tidak Boleh Kosong..!",
                },
                kelurahan: {
                    required: "Opps, Kelurahan Tidak Boleh Kosong..!",
                },
                telp1: {
                    required: "Opps, Telp1 Tidak Boleh Kosong..!",
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                telp2: {
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                email: {
                    required: "Opps, Email Tidak Boleh Kosong..!",
                    email: "Opps, Cek Kembali Email Anda..!! Example : infomedliz@medliz.com",
                }
            }
        });

    });
</script>