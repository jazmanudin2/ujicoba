<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT DATA SALES</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/sales/edit" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID Sales <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" name="id_sales" value="<?php echo $sales['id_sales'];?>" readonly placeholder="Masukan Id Sales" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Sales <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Nama Sales" value="<?php echo $sales['nama_sales'];?>" required name="nama_sales" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Alamat <span class="symbol "></span>
            </label>
            <textarea autocomplete="off" type="text" name="alamat" class="form-control" ><?php echo $sales['alamat'];?></textarea>
        </div>
        <div class="form-group">
            <label class="control-label">
                Telp <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Telp" value="<?php echo $sales['telp_sales'];?>" name="telp_sales" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>
