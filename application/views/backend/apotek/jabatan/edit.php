<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT DATA JABATAN</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/jabatan/edit" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID Jabatan <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text"  name="id_jabatan" readonly placeholder="Masukan Id Jabatan" value="<?php echo $jabatan['id_jabatan'];?>" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Jabatan <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Nama Jenis" value="<?php echo $jabatan['nama_jabatan'];?>"name="nama_jabatan" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
               Ket <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan keterangan" name="keterangan" value="<?php echo $jabatan['keterangan'];?>" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>
