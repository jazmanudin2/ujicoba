<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">INPUT DATA PELANGGAN</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/pelanggan/input" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID Pelanggan <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="id_pelanggan" value="<?php echo $pelanggan['id_pelanggan'];?>" readonly placeholder="Masukan Id Pelanggan" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Jenis Pelanggan <span class="symbol"></span>
            </label>
            <select name="id_jenis_pelanggan" id="id_jenis_pelanggan" class="form-control" required="">
                <option required value="<?php echo $pelanggan['nama_jenis_pelanggan'];?>"><?php echo $pelanggan['nama_jenis_pelanggan'];?></option>
                <?php foreach ($jenis_pelanggan->result() as $d){ ?>
                    <option required value="<?php echo $d->id_jenis_pelanggan;?>">
                        <?php echo $d->nama_jenis_pelanggan; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Jenis Harga <span class="symbol"></span>
            </label>
            <select name="id_jenis_harga" id="id_jenis_harga" class="form-control" required="">
                <option required value="<?php echo $pelanggan['nama_jenis_harga'];?>"><?php echo $pelanggan['nama_jenis_harga'];?> ( <?php echo $pelanggan['profit'];?> )</option>
                <?php foreach ($jenis_harga->result() as $d){ ?>
                    <option required value="<?php echo $d->id_jenis_harga;?>">
                        <?php echo $d->nama_jenis_harga; ?> ( <?php echo $d->profit; ?> )
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Pelanggan <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="nama_pelanggan" value="<?php echo $pelanggan['nama_pelanggan'];?>" placeholder="Masukan Nama Pelanggan" required="" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Perusahaan <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="nama_perusahaan" value="<?php echo $pelanggan['nama_perusahaan'];?>" placeholder="Masukan Nama Perusahaan" required="" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Alamat <span class="symbol"></span>
            </label>
            <textarea autocomplete="off" type="text" name="alamat" required class="form-control" ><?php echo $pelanggan['alamat'];?></textarea>
        </div>
        <div class="form-group">
            <label class="control-label">
                Telp Pelanggan <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="telp_pelanggan" value="<?php echo $pelanggan['telp_pelanggan'];?>" placeholder="Masukan Telp Pelanggan" required="" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Keterangan <span class="symbol"></span>
            </label>
            <textarea autocomplete="off" type="text" name="keterangan" class="form-control" ><?php echo $pelanggan['keterangan'];?></textarea>
        </div>
        <div class="form-group">
            <label class="control-label">
                Catatan <span class="symbol"></span>
            </label>
            <textarea autocomplete="off" type="text" name="catatan" class="form-control" ><?php echo $pelanggan['keterangan'];?></textarea>
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>



<script>
    $(document).ready(function(){

        $("#form").validate({
            rules: {
                id_pelanggan: {
                    required: true
                },
                nama_perusahaan: {
                    required: true
                },
                id_jenis_pelanggan: {
                    required: true
                },
                id_jenis_produk: {
                    required: true
                },
                alamat: {
                    required: true,
                }
            },
            messages: {
                id_pelanggan: {
                    required: "Opps, ID pelanggan Tidak Boleh Kosong..!",
                },
                nama_perusahaan: {
                    required: "Opps, Nama Perusahaan Tidak Boleh Kosong..!",
                },
                id_jenis_produk: {
                    required: "Opps, Jenis Produk Tidak Boleh Kosong..!",
                },
                id_jenis_pelanggan: {
                    required: "Opps, Jenis pelanggan Tidak Boleh Kosong..!",
                },
                alamat: {
                    required: "Opps, Alamat Tidak Boleh Kosong..!",
                }
            }
        });

    });
</script>