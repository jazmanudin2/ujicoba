<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Detail Barang</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Data Master</a>
            </li>
            <li>
                <a>Data Barang</a>
            </li>
            <li class="active">
                <strong>Detail Barang</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Table View apt -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <div class="col-lg-6">
                            <table class="table table-striped table-hover" style="font-size: 14px;font-variant: arial" >
                                <tbody>
                                    <tr>
                                        <th width="170px">ID SP</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['id_sp'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Tanggal</th>
                                        <th width="10">:</th>
                                        <td><?php echo DateToIndo2($detail['tanggal']);?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Suppliant</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['suppliant'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Keterangan</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['keterangan'];?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-striped table-hover" style="font-size: 14px;font-variant: arial" >
                                <tbody>
                                    <tr>
                                        <th width="170px">ID Supplier</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['id_supplier'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Nama Perusahaan</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['nama_perusahaan'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Alamat</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['alamat'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Telp Supplier</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['telp_supplier'];?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <div class="col-lg-12">
                            <table class="table table-striped table-bordered table-hover" style="font-size: 12px">
                                <thead>
                                    <tr>
                                        <th width="15">No.</th>
                                        <th>Kode Barang</th>
                                        <th>Merk Dagang</th>
                                        <th>Golongan Obat</th>
                                        <th>Jumlah Satuan</th>
                                        <th>Jumlah Box</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $sno  = 1;
                                    $jumlah_box = 0;
                                    $jumlah_satuan   = 0;
                                    foreach ($details->result() as $d) {
                                        $jumlah_box     = $jumlah_box + $d->jumlah_box;
                                        $jumlah_satuan  = $jumlah_satuan + $d->jumlah_satuan;
                                        ?>
                                        <tr id="">
                                            <td><?php echo $sno++;?></td>
                                            <td><?php echo $d->id_barang;?></td>
                                            <td><?php echo $d->nama_merk_dag;?></td>
                                            <td><?php echo $d->nama_gol_obat;?></td>
                                            <td><?php echo $d->jumlah_box;?></td>
                                            <td><?php echo $d->jumlah_satuan;?></td>
                                        </tr>
                                    <?php }?>
                                    <tr>
                                        <td colspan="4" align="center"><b>TOTAL</b></td>
                                        <td ><h4><?php echo $jumlah_box; ?></h4></td>
                                        <td ><h4><?php echo $jumlah_satuan; ?></h4></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
