<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Data Surat Pesanan</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Transaksi</a>
            </li>
            <li>
                <a>Data Transaksi</a>
            </li>
            <li class="active">
                <strong>Data Surat Pesanan</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Tabel View Barang -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Surat Pesanan</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive" >
                        <div class="col-md-12">
                            <form class="form-horizontal" method="post" action="" autocomplete="off">

                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" value="<?php echo $pencarian;?>" id="pencarian" name="pencarian" class="form-control" placeholder="Pencarian">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="submit" name="submit" class="btn btn-sm bg-blue m-2-15 waves-effect" value="Cari Data">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <table class="table table-striped table-bordered table-hover" style="font-size: 12px">
                            <thead >
                                <tr>
                                    <th>No</th>
                                    <th>ID SP</th>
                                    <th>Nama Perusahaan</th>
                                    <th>Telp</th>
                                    <th>Alamat</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>

                            <body>
                                <?php $no=1;?>
                                <?php foreach ($data as $d) {?>
                                    <tr id="">
                                        <td><?php echo $no++;?></td>
                                        <td><?php echo $d['id_sp'];?></td>
                                        <td><?php echo $d['nama_perusahaan'];?></td>
                                        <td><?php echo $d['telp_supplier'];?></td>
                                        <td><?php echo $d['alamat'];?></td>
                                        <td><?php echo $d['keterangan'];?></td>
                                        <td><a class="fa fa-address-card btn-info btn btn-sm" href="<?php echo base_url();?>apotek/surat_pesanan/detail/<?php echo $d['id_sp']; ?>"></a></td>
                                    </tr>
                                <?php }?>
                            </body>

                        </table>
                        <div style='margin-top: 10px;'>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive" >

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>