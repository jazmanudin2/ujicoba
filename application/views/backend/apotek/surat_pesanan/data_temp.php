<?php 
$no     = 1;
$total  = 0;
foreach($listmp->result() as $d){ 
	$total = $total + $d->jumlah_box;
	?>
	<tr>
        <td><?php echo $no++; ?></td>
        <td><?php echo $d->id_barang; ?></td>
        <td><?php echo $d->nama_merk_dag; ?></td>
        <td><?php echo $d->nama_gol_obat; ?></td>
        <td><?php echo $d->id_kandungan_obat; ?></td>
        <td><?php echo $d->jumlah_satuan; ?></td>
        <td><?php echo $d->jumlah_box; ?></td>
        <td><a data-kode="<?php echo $d->id_barang_m;?>" class="fa fa-trash btn-danger btn btn-sm hapus_tmp sum_total" ></a>
        </td>
    </tr>
<?php } ?>

<script type="text/javascript">

    function cektemp(){

        $.ajax({

            type    : 'POST',
            url     : '<?php echo base_url(); ?>apotek/surat_pesanan/cektemp',
            cache   : false,
            success :function(respond){

                $("#cektemp").val(respond);

            }

        });
    } 

    // Menampilkan Data Detail Penjumalan Temp
    function tampiltmp(){

        // Mengosongkan Inputan Text Ketika Sudah Di Klik Tambah
        $("#id_barang").val("");
        $("#id_barang_m").val("");
        $("#id_gol_obat").val("");
        $("#id_merk_dag").val("");
        $("#jumlah_box").val("");
        $("#jumlah_satuan").val("");

        $.ajax({
        	type    : 'GET',
        	url     : '<?php echo base_url();?>apotek/surat_pesanan/view_temp',
        	data    : '',
        	success : function (html) {
        		$("#tampiltmp").html(html);
        	}
        });

    }

    $('.hapus_tmp').click(function(e){
    	e.preventDefault();
    	var id_barang_m = $(this).attr("data-kode");
    	$.ajax({

    		type : 'POST',
    		url  : '<?php echo base_url();?>apotek/surat_pesanan/hapus_tmp',
    		data : {id_barang_m:id_barang_m},
    		cache:false,
    		success:function(respond){
                cektemp();
                tampiltmp();
            }
        });

    }); 

</script>