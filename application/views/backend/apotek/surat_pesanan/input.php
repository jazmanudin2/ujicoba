<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Transaksi Surat Pesanan</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Transaksi</a>
            </li>
            <li>
                <a>Surat Pesanan</a>
            </li>
            <li class="active">
                <strong>Input Surat Pesanan</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <form class="form-horizontal" method="POST" id="form" action="<?php echo base_url();?>apotek/surat_pesanan/input">
        <div class="row">

            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="form-group">
                            <label class="col-lg-3 control-label">No. SP</label>
                            <div class="col-lg-5">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                    <input type="hidden" name="cektemp" id="cektemp">
                                    <input type="hidden" name="cekbarang" id="cekbarang">
                                    <input  type="text" readonly="" placeholder="No. SP" value="<?php echo $kodeunik;?>" name="id_sp" class="form-control input-sm">
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="data_1">
                            <label class="col-lg-3 control-label">Tanggal</label>
                            <div class="col-lg-5">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input  type="text" autocomplete="off" class="input-sm form-control" value="<?php echo date('Y-m-d');?>" name="tanggal"/>
                                </div>  
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Supplier</label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon required"><i class="fa fa-users required"></i></span>
                                    <select  name="id_supplier" id="id_supplier" class="chosen-select form-control input-sm"  tabindex="2">
                                        <option value="">-- Pilih Supplier --</option>
                                        <?php foreach ($supplier->result() as $d){ ?>
                                            <option value="<?php echo $d->id_supplier;?>">
                                                <?php echo $d->nama_perusahaan; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Suppliant</label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input  type="text" placeholder="Suppliant" name="suppliant" class="form-control input-sm">
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-lg-3 control-label">Keterangan</label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                    <textarea type="text" placeholder="Keterangan" rows="3" value="-" name="keterangan" class="form-control input-sm"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div hidden="" class="form-group">
                            <label class="col-lg-4 control-label">ID Barang Medliz</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                    <input  type="text" name="id_barang_m" id="id_barang_m" readonly="" placeholder="Kode Barang" class="form-control input-sm">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">ID Barang</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span> 
                                    <input type="text" placeholder="Kode Barang" name="id_barang" id="id_barang" readonly="" class="form-control input-sm caribarang"> 
                                    <span class="input-group-addon"><a href="#" class="caribarang"><i class="fa fa-search"></i></a></span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Merk Dagang</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                    <input  class="form-control input-sm" readonly="" name="id_merk_dag" id="id_merk_dag" type="text" placeholder="Merk Dagang">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Golongan Obat</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                    <input  class="form-control input-sm" readonly="" name="id_gol_obat" id="id_gol_obat" type="text" placeholder="Golongan Obat">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Kandungan Obat</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                    <input  class="form-control input-sm" readonly="" name="kandungan_obat" id="kandungan_obat" type="text" placeholder="Kandungan Obat">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Jumlah Satuan</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                    <input  class="form-control input-sm jumlah_satuan" name="jumlah_satuan" id="jumlah_satuan" type="number" placeholder="Jumlah Satuan">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Jumlah Box</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                    <input  class="form-control input-sm" name="jumlah_box" id="jumlah_box" type="number" placeholder="Jumlah Box">
                                    <span class="input-group-addon"><a href="#" class="input_tmp"><i class="fa fa-plus"></i></a></span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-12"  style="font-size: 12px">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <table class="table-bordered table">
                            <thead>
                                <tr>
                                    <th width="30">NO</th>
                                    <th width="100px">ID Barang</th>
                                    <th>Merk Dagang</th>
                                    <th>Golongan Obat</th>
                                    <th>Kandungan Obat</th>
                                    <th width="120px">Jumlah Satuan</th>
                                    <th width="100px">@ Box</th>
                                    <th width="30px">#</th>
                                </tr>
                            </thead>
                            <tbody id="tampiltmp" style="font-size: 11px">

                            </tbody>
                        </table>
                        <!-- End Table -->
                        <br>
                        <hr>
                        <div class="form-group">
                            <div class="col-lg-7 ">
                                <div class="input-group">
                                    <button class="ladda-button btn btn-sm btn-success submit" name="submit" type="submit"><i class="fa fa-print "></i> Simpan</button> .
                                    <button type="reset" class="btn btn-sm btn-danger"><i class="fa  fa-mail-reply-all"></i>  Batal</button>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
</div>



<!------- Modal Barang -------->
<div class="modal fade bd-example-modal-lg" id="Modalbarang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="loadform">

        </div>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function(){

        tampiltmp();
        cektemp();

        function cektemp(){

            $.ajax({

                type    : 'POST',
                url     : '<?php echo base_url(); ?>apotek/surat_pesanan/cektemp',
                cache   : false,
                success :function(respond){

                    $("#cektemp").val(respond);

                }

            });
        } 

        function tampiltmp(){

            // Mengosongkan Inputan Text Ketika Sudah Di Klik Tambah
            $("#id_barang").val("");
            $("#id_barang_m").val("");
            $("#id_gol_obat").val("");
            $("#kandungan_obat").val("");
            $("#id_merk_dag").val("");
            $("#jumlah_box").val("");
            $("#jumlah_satuan").val("");

            $.ajax({
                type    : 'GET',
                url     : '<?php echo base_url();?>apotek/surat_pesanan/view_temp',
                data    : '',
                success : function (html) {
                    $("#tampiltmp").html(html);
                }
            });

        }

        // Tombol Modal Cari Barang
        $(".caribarang").click(function(e){
            e.preventDefault();
            var id_barang      = $(this).attr("data-kode");
            $('#Modalbarang').modal("show");

            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/surat_pesanan/view_barang",
                data    : {id_barang:id_barang},
                cache   : false,
                success : function(respond){
                    $("#loadform").html(respond);
                }
            });
        });
        
        // Inputan Detail Surat Pesanan Temp
        $(".input_tmp").click(function(e){
            e.preventDefault();
            var id_barang_m     = $("#id_barang_m").val();
            var jumlah_box      = $("#jumlah_box").val();
            var jumlah_satuan   = $("#jumlah_satuan").val();
            var cekbarang       = $("#cekbarang").val();

            if (id_barang_m == 0) {

                swal("Oops", "Data Barang Harus Diisi", "warning");

            }else if (jumlah_satuan == 0) {

                swal("Oops", "Jumlah Satuan Harus Diisi", "warning");

            } else if (jumlah_box == 0) {

                swal("Oops", "Jumlah Box Harus Diisi", "warning");

            } else if (cekbarang > 0){

                swal("Oops","Barang Sudah Ada","warning");

            } else {

                $.ajax({
                    type    : 'POST',
                    url     : '<?php echo base_url();?>apotek/surat_pesanan/input_tmp',
                    data    :
                    {
                        id_barang_m     : id_barang_m,
                        jumlah_box      : jumlah_box,
                        jumlah_satuan   : jumlah_satuan,
                    },
                    cache   : false,
                    success : function(respond){
                        //swal("Success", "Barang Sudah Di Tambahkan", "success");
                        cektemp();
                        tampiltmp();
                    }

                });
            }
        });


        // DataTable Barang
        $("#tabelbarang").DataTable();


        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });


        // Select Data Pelanggan
        $('.chosen-select').chosen({width: "100%"});


        // Form Validasi
        $("#form").submit(function(){

            var id_sales        = $("#id_sales").val();
            var id_supplier     = $("#id_supplier").val();
            var potongan        = $("#potongan").val();
            var no_sp           = $("#no_sp").val();
            var pembayaran      = $("#pembayaran").val();
            var cektemp         = $("#cektemp").val();

            if(no_sp == ""){

                swal("Oops","No SP Harus Diisi","warning");
                $("#kode_pelanggan").focus();
                return false;

            }else if(id_supplier==""){

                swal("Oops","Silahkan Pilih Dahulu Data Supplier","warning");
                $("#id_supplier").focus();
                return false;

            }else if(id_sales==""){

                swal("Oops","Silahkan Pilih Dahulu Data Sales","warning");
                $("#id_sales").focus();
                return false;

            }else if(cektemp < 1){

                swal("Oops","Barang Belum Ada Yang Dipilih","warning");
                $("#nama_barang").focus();
                return false;

            }else if(potongan==""){

                swal("Oops","Potongan Harus Diisi (0)","warning");
                $("#potongan").focus();
                return false;

            }else if(jatuhtempo==""){

                swal("Oops","Jatuh Tempo Harus Diisi","warning");
                $("#jatuhtempo").focus();
                return false;

            }else{

                swal("Berhasil","Berhasil Di Simpan","success");
                return true;
            }
        });
        
    });
</script>