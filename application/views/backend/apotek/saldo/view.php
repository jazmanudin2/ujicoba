<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Data Keuangan</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Data Keuangan</a>
            </li>
            <li class="active">
                <strong>Data Keuangan Apotek</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Table View saldo -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Data Keuangan Apotek</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content">
                    <div class="table-responsive" >
                        <div class="col-md-12">
                            <form class="form-horizontal" method="post" action="" autocomplete="off">

                                <?php
                                $id_user = $this->session->userdata('id_admin');
                                $query=$this->db->query("SELECT *
                                    FROM tbl_saldo_apt
                                    WHERE tbl_saldo_apt.id_admin = '$id_user' ORDER BY id_saldo DESC");
                                $d=$query->row_array();
                                ?>

                                <div class="row clearfix">
                                    <div class="col-lg-5 col-md-5 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <div class="bg-info p-xs b-r-sm" style="min-height:170px;">
                                                    <font size="5">
                                                        <b>SALDO</b>
                                                    </font>
                                                    <span class="info-box-icon" style="min-height:170px;">
                                                        <i class="fa fa-shopping-cart"></i>
                                                    </span>
                                                    <font size="10">
                                                        <p align="right">Rp.<?php echo uang($d['saldo']);?></p>
                                                    </font>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" value="<?php echo $pencarian;?>" id="pencarian" name="pencarian" class="form-control" placeholder="Pencarian">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="submit" name="submit" class="btn btn-sm bg-blue m-2-15 waves-effect" value="Cari Data">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>
                        <table class="table table-striped table-bordered table-hover" style="font-size: 12px" >
                            <thead >
                                <tr>
                                    <th width="15">No.</th>
                                    <th>Tanggal</th>
                                    <th>Nama</th>
                                    <th>Setor</th>
                                    <th>Tarik</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php 
                                $sno  = $row+1;
                                foreach ($data as $d) {?>
                                    <tr id="">
                                        <td><?php echo $sno++
                                        ;?></td>
                                        <td><?php echo $d['tanggal'];?></td>
                                        <td><?php echo $d['nama'];?></td>
                                        <td align="right"><?php echo uang($d['setor']);?></td>
                                        <td align="right"><?php echo uang($d['tarik']);?></td>
                                        <td><?php echo $d['keterangan'];?></td>
                                    </tr>
                                <?php }?>
                            </tbody>
                        </table>
                        <div style='margin-top: 10px;'>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div id="modal-saldo" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row" id="loadform">

                </div>
            </div>
        </div>
    </div>
</div>
