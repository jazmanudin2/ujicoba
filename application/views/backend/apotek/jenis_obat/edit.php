<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT DATA JENIS OBAT</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/jenis_obat/edit" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID Jenis Obat <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" name="id_jenis_obat" value="<?php echo $jenis_obat['id_jenis_obat'] ?>" readonly placeholder="Masukan Id Jenis Obat" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Jenis Obat <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" required="" placeholder="Masukan Nama Jenis" value="<?php echo $jenis_obat['nama_jenis_obat'] ?>" name="nama_jenis_obat" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
               Ket <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan keterangan" value="<?php echo $jenis_obat['keterangan'] ?>" name="keterangan" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>
