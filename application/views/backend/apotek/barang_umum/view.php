<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Data Barang Umum</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Data Master</a>
            </li>
            <li class="active">
                <strong>Data Barang Umum</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Table View apt -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Data Barang Umum</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content">
                    <div class="table-responsive" >
                        <div class="col-md-12">
                            <form class="form-horizontal" method="post" action="" autocomplete="off">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" value="<?php echo $pencarian;?>" id="pencarian" name="pencarian" class="form-control" placeholder="Pencarian">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="submit" name="submit" class="btn btn-sm bg-blue m-2-15 waves-effect" value="Cari Data">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </form>
                        </div>
                        <table class="table table-striped table-bordered table-hover" style="font-size: 12px" >
                            <thead >
                                <form action="" method="GET">
                                    <tr>
                                        <td colspan="13">
                                            <a class="input fa fa-plus-square btn-success btn"> Tambah</a>
                                            <a class="delete_all fa fa-trash btn-sm btn btn-danger"> Hapus Select</a>
                                            <a href="<?php echo base_url('apotek/barang_umum/form'); ?>"  class="fa fa-upload btn btn-primary btn-sm"> Import</a>
                                        </td>
                                    </tr>
                                </form>
                                <tr>
                                    <th width="10px"><input type="checkbox" id="master"></th>
                                    <th width="15">No.</th>
                                    <th width="100px">Kode Barang</th>
                                    <th>Nama Barang</th>
                                    <th>Gol Barang</th>
                                    <th>Min Stok</th>
                                    <th width="180px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $sno  = $row+1;
                                foreach ($data as $d) {?>
                                    <tr id="">
                                        <td>
                                            <input type="checkbox" class="sub_chk" data-id="<?php echo $d['id_barang_m']; ?>">
                                        </td>
                                        <td><?php echo $sno++;?></td>
                                        <td><?php echo $d['id_barang'];?></td>
                                        <td><?php echo $d['nama_barang'];?></td>
                                        <td><?php echo $d['gol_barang'];?></td>
                                        <td><?php echo $d['min_stok'];?></td>
                                        <td> 
                                            <div class="text-center">
                                                <a name="submit" class="fa fa fa-barcode btn-primary btn btn-sm" href="<?php echo base_url();?>apotek/barang_alkes/barcode_umum/<?php echo $d['id_barang_m']; ?>"></a>
                                                <a class="fa fa fa-address-card btn-info btn btn-sm" href="<?php echo base_url();?>apotek/barang_umum/detail/<?php echo $d['id_barang_m']; ?>"></a>
                                                <a class="hapus fa fa-trash btn-danger btn btn-sm" data-id="<?php echo $d['id_barang_m']; ?>"></a>
                                                <a class="edit fa fa-edit btn-warning btn btn-sm" data-id="<?php echo $d['id_barang_m']; ?>"></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php }?>
                            </tbody>
                        </table>
                        <div style='margin-top: 10px;'>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div id="modal-barang" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row" id="loadform">

                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $(function(){

        // Hapus Checkbox
        $('#master').on('click', function(e) {
            if($(this).is(':checked',true))  
            {
                $(".sub_chk").prop('checked', true);  
            } else {  
                $(".sub_chk").prop('checked',false);  
            }  

        });

        $('.delete_all').on('click', function(e) {

            var allVals = [];  
            $(".sub_chk:checked").each(function() {  
                allVals.push($(this).attr('data-id'));
            });  
            if(allVals.length <=0)  
            {  
                swal({
                    title:"Silahkan Select Checkbox Terlebih Dahulu",
                    type: "warning",
                });

            }  else {  

                var check = swal({
                    title:"Hapus Data",
                    text:"Yakin Akan Menghapus Data ini??",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Hapus",
                    closeOnConfirm: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                }).then((result) => {
                    if (result.value) {
                        var join_selected_values = allVals.join(","); 
                        $.ajax({
                            url: "<?php echo base_url(); ?>apotek/barang_umum/delete_checkbox",
                            type: 'POST',
                            data: 'id_barang_m='+join_selected_values,
                            success: function (data) {
                              console.log(data);
                              $(".sub_chk:checked").each(function() {  
                                  $(this).parents("tr").remove();
                              });
                              swal(
                                'Hapus',
                                'Data Berhasil Di Hapus',
                                'success'
                                );
                          },
                          error: function (data) {
                            alert(data.responseText);
                        }

                    });
                        $.each(allVals, function( index, value ) {
                          $('table tr').filter("[data-row-id='" + value + "']").remove();
                      });
                    }
                });  
            }  

        });

        //Hapus Data apt
        $('.hapus').click(function(){
            var kode = $(this).attr("data-id");

            swal({
                title:"Hapus Data",
                text:"Yakin Akan Menghapus Data ini??",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Hapus",
                closeOnConfirm: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
            }).then((result) => {
                if (result.value) {
                  swal(
                    'Hapus',
                    'Data Berhasil Di Hapus',
                    'success'
                    );
                  $(location).attr('href','<?php echo base_url()?>apotek/barang_umum/hapus/'+kode);
              }
          })

        }); 

        //Edit Data Modal

        $('.edit').click(function(){
            var id_barang_m = $(this).attr("data-id");
            $("#modal-barang").modal("show");
            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/barang_umum/edit",
                data    : {id_barang_m:id_barang_m},
                cache   : false,
                success : function(respond){

                    $("#loadform").html(respond);
                }

            });

        });

        //Input Data Modal

        $('.input').click(function(){
            var id_barang_m = $(this).attr("data-id");
            $("#modal-barang").modal("show");
            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/barang_umum/input",
                data    : {id_barang_m:id_barang_m},
                cache   : false,
                success : function(respond){

                    $("#loadform").html(respond);
                }

            });

        });

    });

</script>

