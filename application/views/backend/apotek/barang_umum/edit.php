<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT DATA BARANG UMUM</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/barang_umum/edit" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div hidden="" class="form-group">
            <label class="control-label">
                ID Barang Medliz <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="id_barang_m" value="<?php echo $barang_umum['id_barang_m'];?>" readonly placeholder="Masukan ID Barang Medliz" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                ID Barang <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="id_barang" value="<?php echo $barang_umum['id_barang'];?>" placeholder="Masukan ID Barang" required="" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Barang <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text"  placeholder="Nama Barang" value="<?php echo $barang_umum['nama_barang'];?>" name="nama_barang" id="nama_barang" class="form-control"required>
        </div>
        <div class="form-group">
            <label class="control-label">
                Gol Obat <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" name="gol_barang" id="gol_barang" value="<?php echo $barang_umum['gol_barang'];?>" class="form-control"required>
        </div>
        <div class="form-group">
            <label class="control-label">
                Minimal Stok <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" name="min_stok" id="min_stok" value="<?php echo $barang_umum['min_stok'];?>" class="form-control"required>
        </div>
        <div class="form-group">
            <label class="control-label">
                Keterangan <span class="symbol"></span>
            </label>
            <textarea autocomplete="off" type="text" name="keterangan" class="form-control" ><?php echo $barang_umum['keterangan'];?></textarea>
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>


<script>
    $(document).ready(function(){

        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "dd-mm-yyyy"
        });

        $("#form").validate({
            rules: {
                id_jenis_apt: {
                    required: true
                },
                nama_jenis_karyawan: {
                    required: true
                },
                owner_apt: {
                    required: true
                },
                penanggungjwb: {
                    required: true
                },
                telp_penanggungjwb: {
                    required: true,
                    number: true
                },
                alamat: {
                    required: true
                },
                merk_dag: {
                    required: true
                },
                kota: {
                    required: true
                },
                kecamatan: {
                    required: true
                },
                kelurahan: {
                    required: true
                },
                telp1: {
                    required: true,
                    number: true
                },
                telp2: {
                    number: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                id_jenis_apt: {
                    required: "Opps, Jenis Apotek Tidak Boleh Kosong..!",
                },
                nama_jenis_karyawan: {
                    required: "Opps, Nama Jenis Karyawan Tidak Boleh Kosong..!",
                },
                owner_apt: {
                    required: "Opps, Owner Apotek Tidak Boleh Kosong..!",
                },
                penanggungjwb: {
                    required: "Opps, Penanggung Jawab Tidak Boleh Kosong..!",
                },
                telp_penanggungjwb: {
                    required: "Opps, Telp Penanggung Jawab Tidak Boleh Kosong..!",
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                alamat: {
                    required: "Opps, Alamat Tidak Boleh Kosong..!",
                },
                merk_dag: {
                    required: "Opps, merk_dag Tidak Boleh Kosong..!",
                },
                kota: {
                    required: "Opps, Kota Tidak Boleh Kosong..!",
                },
                kecamatan: {
                    required: "Opps, Kecamatan Tidak Boleh Kosong..!",
                },
                kelurahan: {
                    required: "Opps, Kelurahan Tidak Boleh Kosong..!",
                },
                telp1: {
                    required: "Opps, Telp1 Tidak Boleh Kosong..!",
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                telp2: {
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                email: {
                    required: "Opps, Email Tidak Boleh Kosong..!",
                    email: "Opps, Cek Kembali Email Anda..!! Example : infomedliz@medliz.com",
                }
            }
        });

    });
</script>