<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">INPUT DATA PENGELUARAN</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/pengeluaran/input" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" name="id_pengeluaran" value="<?php echo $kodeunik;?>" readonly class="form-control">
        </div>
        <div class="form-group" id="data_1">
            <label class="control-label">
                Tanggal <span class="symbol "></span>
            </label>
            <div class="input-group date">
                <input  type="text" autocomplete="off" class="input-sm form-control" value="<?php echo date('Y-m-d');?>" name="tanggal"/>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>  
        </div>
        <div class="form-group">
            <label class="control-label">
                Jenis Pengeluaran<span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Jenis Pengeluaran" required name="jenis_pengeluaran" class="form-control">
        </div>

        <div class="form-group">
            <label class="control-label">
                Penerima<span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Penerima" id="penerima" name="penerima" class="form-control" >
        </div>

        <div class="form-group">
            <label class="control-label">
                Biaya <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Biaya" id="biaya" name="biaya" class="form-control saldo sisa_saldo" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Jumlah <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Jumlah" id="jumlah" name="jumlah" class="form-control saldo sisa_saldo" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Total <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" readonly="" placeholder="Masukan Total" id="total" name="total" class="form-control saldo sisa_saldo" >
        </div>
        <?php
        $id_user = $this->session->userdata('id_admin');
        $query=$this->db->query("SELECT *
            FROM tbl_saldo_apt
            WHERE tbl_saldo_apt.id_admin = '$id_user' ORDER BY id_saldo DESC");
        $d=$query->row_array();
        ?>

        <div class="form-group">
            <label class="control-label">
                Saldo Awal<span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Saldo" id="saldo" readonly="" value="<?php echo $d['saldo'];?>" name="saldo" class="form-control" >
        </div>

        <div hidden="" class="form-group">
            <label class="control-label">
                ID<span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Saldo" id="id_saldo" readonly="" value="<?php echo $kodeunik2;?>" name="id_saldo" class="form-control" >
        </div>

        <div class="form-group">
            <label class="control-label">
                Saldo Akhir <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Akhir" readonly="" id="sisa_saldo" name="sisa_saldo" class="form-control" >
        </div>

        <div class="form-group">
            <label class="control-label">
                Invoice <span class="symbol "></span>
            </label>
            <select  name="invoice" id="invoice" class="chosen-select form-control input-sm"  tabindex="2">
                <option value="Ada">Ada</option>
                <option value="Tidak">Tidak</option>
            </select>
        </div>

        <div class="form-group">
            <label class="control-label">
                Keterangan <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" value="-" placeholder="Masukan Keterangan" name="keterangan" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>



<script>
    $(document).ready(function(){

        $(".saldo").on("input",function(){
            var biaya           = $("#biaya").val();
            var jumlah          = $("#jumlah").val();
            var saldo           = $("#saldo").val();
            var tarik           = $("#tarik").val();
            var total           = $("#total").val();
            var setor           = $("#setor").val();
            $("#sisa_saldo").val((saldo*1)-(biaya*jumlah));
            $("#total").val(biaya*jumlah)
        });

        $('.chosen-select').chosen({width: "100%"});

        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

    });
</script>