<div class="modal-header">
    <h3 class="modal-title" id="exampleModalLabel">Data Barang Obat</h3>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <table class="table table-bordered" id="tabelbarang">
        <thead>
            <tr>
                <th>Nama Profit</th>
                <th>Profit</th>
                <th>Harga Jual</th>
                <th>Keterangan</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($jenis_harga->result() as $b){ ?> 
                <tr>
                    <td><?php echo $b->nama_profit; ?></td>
                    <td><?php echo $b->profit; ?></td>
                    <td><?php echo uang($b->harga_jual); ?></td>
                    <td><?php echo $b->nama_keterangan_harga; ?></td>
                    <td><a href="#" data-kode="<?php echo $b->id_jenis_harga;?>" data-harga="<?php echo $b->harga_jual;?>" " class="btn btn-sm btn-primary pilihbarang">Pilih</a></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>    
</div>

<script type="text/javascript">

    $(".pilihbarang").click(function(e){
        e.preventDefault();
        $('#Modalbarang').modal("hide");
        $("#harga_jual").val($(this).attr('data-harga'));
    });

    $("#tabelbarang").DataTable();

</script>