<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Detail Penjualan</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Transaksi</a>
            </li>
            <li>
                <a>Penjualan</a>
            </li>
            <li class="active">
                <strong>Detail Penjualan</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <form class="form-horizontal" method="POST" id="form" action="<?php echo base_url();?>apotek/penjualan/input">
        <div class="row">
            <div class="col-lg-7">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="form-group">
                            <label class="col-lg-3 control-label">No. Faktur</label>
                            <div class="col-lg-5">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                    <input  type="text" readonly="" placeholder="No. Faktur" value="<?php echo $penjualan['no_fak_penj'];?>" name="no_fak_penj" class="form-control input-sm">
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="data_1">
                            <label class="col-lg-3 control-label">Tanggal</label>
                            <div class="col-lg-5">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input readonly=""  type="text" autocomplete="off" class="input-sm form-control" value="<?php echo $penjualan['tanggal'];?>" name="tanggal"/>
                                </div>  
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Nama Pelanggan</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon required"><i class="fa fa-users"></i></span>
                                    <input  type="text" readonly="" autocomplete="off" placeholder="No. Faktur" value="<?php echo $penjualan['nama_pelanggan'];?>" name="no_fak_penj" class="form-control input-sm">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Nama Perusahaan</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon required"><i class="fa fa-users"></i></span>
                                    <input  type="text" readonly="" autocomplete="off" placeholder="No. Faktur" value="<?php echo $penjualan['nama_perusahaan'];?>" name="no_fak_penj" class="form-control input-sm">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Alamat</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon required"><i class="fa fa-users"></i></span>
                                    <input  type="text" readonly="" autocomplete="off" placeholder="No. Faktur" value="<?php echo $penjualan['alamat'];?>" name="no_fak_penj" class="form-control input-sm">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <div class="col-lg-5">
                <div class="bg-info p-xs b-r-sm" style="min-height:170px;">
                    <font size="5">
                        <b>TOTAL</b>
                    </font>
                    <span class="info-box-icon" style="min-height:170px;">
                        <i class="fa fa-shopping-cart"></i>
                    </span>
                    <br>
                    <br>
                    <?php
                    $id_user     = $this->session->userdata('id_admin');
                    $no_fak_penj = $penjualan['no_fak_penj'];
                    $query=$this->db->query("SELECT SUM(total) AS totals
                        FROM tbl_penjualan
                        WHERE id_admin = '$id_user' AND no_fak_penj = '$no_fak_penj' ORDER BY no_fak_penj DESC");
                    $d=$query->row_array();
                    ?>
                    <font size="10">
                        <b><p align="right" id="sisa_bayar2">Rp. <?php echo uang($d['totals']);?></p></b>
                    </font>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Obat</h3>
                        <table class="table-bordered table-striped table-hover table">
                            <thead>
                                <tr>
                                    <th>ID Barang</th>
                                    <th>Nama Merk Dagang</th>
                                    <th>Nama Golongan Obat</th>
                                    <th>Kandungan Obat</th>
                                    <th>Harga</th>
                                    <th>Jumlah</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $total = 0;
                                foreach($obat->result() as $d){ 
                                    $total = $total + $d->subtotal;
                                    ?>
                                    <tr>
                                        <td><?php echo $d->id_barang; ?></td>
                                        <td><?php echo $d->merk_dag; ?></td>
                                        <td><?php echo $d->merk_dag; ?></td>
                                        <td><?php echo $d->id_kandungan_obat; ?></td>
                                        <td align="right"><?php echo uang($d->harga_jual); ?></td>
                                        <td><?php echo $d->jumlah; ?></td>
                                        <td align="right"><?php echo uang($d->subtotal); ?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="6" align="center" style="font-size:12px">TOTAL</td>
                                    <td colspan="1" align="right" style="font-size:12px">Rp. <?php echo uang($total);?></td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- End Table -->
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Umum</h3>
                        <table class="table-bordered table-striped table-hover table">
                            <thead>
                                <tr>
                                    <th>ID Barang</th>
                                    <th>Nama Barang</th>
                                    <th>Golongan Barang</th>
                                    <th>Harga</th>
                                    <th>Jumlah</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $total = 0;
                                foreach($umum->result() as $d){ 
                                    $total = $total + $d->subtotal;
                                    ?>
                                    <tr>
                                        <td><?php echo $d->id_barang; ?></td>
                                        <td><?php echo $d->nama_barang; ?></td>
                                        <td><?php echo $d->gol_barang; ?></td>
                                        <td align="right"><?php echo uang($d->harga_jual); ?></td>
                                        <td><?php echo $d->jumlah; ?></td>
                                        <td align="right"><?php echo uang($d->subtotal); ?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="5" align="center" style="font-size:12px">TOTAL</td>
                                    <td colspan="1" align="right" style="font-size:12px">Rp. <?php echo uang($total);?></td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- End Table -->
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Alkes</h3>
                        <table class="table-bordered table-striped table-hover table">
                            <thead>
                                <tr>
                                    <th>ID Barang</th>
                                    <th>Jenis Barang</th>
                                    <th>Detail Alkes</th>
                                    <th>Harga</th>
                                    <th>Jumlah</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $total = 0;
                                foreach($alkes->result() as $d){ 
                                    $total = $total + $d->subtotal;
                                    ?>
                                    <tr>
                                        <td><?php echo $d->id_barang; ?></td>
                                        <td><?php echo $d->jenis_barang; ?></td>
                                        <td><?php echo $d->detail_alkes; ?></td>
                                        <td align="right"><?php echo uang($d->harga_jual); ?></td>
                                        <td><?php echo $d->jumlah; ?></td>
                                        <td align="right"><?php echo uang($d->subtotal); ?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="5" align="center" style="font-size:12px">TOTAL</td>
                                    <td colspan="1" align="right" style="font-size:12px">Rp. <?php echo uang($total);?></td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- End Table -->
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <h3>Posting Pembayaran</h3>
                <hr>
                <table class="table-striped table-hover table"  style="font-size:11px">
                    <thead>
                        <tr>
                            <th colspan="4" height="42px"></th>
                        </tr>
                        <tr>
                            <th bgcolor="#e3d878">Jatuh Tempo</th>
                            <th bgcolor="#e3d878">Total Bayar</th>
                            <th bgcolor="#e3d878">Sudah Bayar</th>
                            <th bgcolor="#e3d878">Sisa Bayar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tbody>
                            <?php $totwjibbayar = 0;?>
                            <?php $totrealisasi = 0;?>
                            <?php $tottun       = 0;?>
                            <?php 
                            foreach($postingbyr->result() as $d){ 
                                $totwjibbayar = $totwjibbayar + $d->wajibbayar;
                                $totrealisasi = $totrealisasi + $d->realisasi;
                                $tottun       = ($tottun + ($d->wajibbayar - $d->realisasi));
                                ?>
                                <tr>
                                    <?php if ($d->cicilanke == !0) {?>
                                    <?php }else{ ?>
                                        <td><?php echo $d->jatuhtempo; ?></td>
                                    <?php }?>
                                    <?php if ($d->cicilanke == !0) {?>
                                    <?php }else{ ?>
                                        <td align="right"><?php echo uang($totwjibbayar); ?></td>
                                    <?php }?>
                                    <?php if ($d->cicilanke == !0) {?>
                                    <?php }else{ ?>
                                        <td align="right"><?php echo uang($totrealisasi); ?></td>
                                    <?php }?>
                                    <?php if ($d->cicilanke == !0) {?>
                                    <?php }else{ ?>
                                        <td align="right"><?php echo uang($tottun); ?></td>
                                    <?php }?>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td bgcolor="#b0b0b0" colspan="1" align="center" style="font-size:12px"><b>TOTAL</b></td>
                                <td bgcolor="#b0b0b0" align="right" style="font-size:12px"><b><?php echo uang($totwjibbayar); ?></b></td>
                                <td bgcolor="#b0b0b0" align="right" style="font-size:12px"><b><?php echo uang($totrealisasi); ?></b></td>
                                <td bgcolor="#b0b0b0" align="right" style="font-size:12px"><b><?php echo uang($tottun); ?></b></td>
                            </tr>
                        </tbody>
                    </tbody>
                </table>
            </div>

            <div class="col-sm-6">
                <h3 >Histori Pembayaran</h3>
                <hr>
                <table class="table-striped table-hover table"  style="font-size:11px">
                    <thead>
                        <tr>
                            <?php if ($tottun == !0) {?>
                                <td colspan="4"><a class="btn fa fa-list btn-success pull-right" href="#modal-form" data-toggle="modal"> Bayar Cicilan</a></td>
                            <?php }else{?>
                                <th colspan="4" height="42px"></th>
                            <?php }?>
                        </tr>
                        <tr>
                            <th bgcolor="#07dea9">No.</th>
                            <th bgcolor="#07dea9">Tanggal</th>
                            <th bgcolor="#07dea9">Jumlah Bayar</th>
                            <th bgcolor="#07dea9">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $totbayar = 0;?>
                        <?php 
                        foreach($hbayar->result() as $d){ 
                            $totbayar      = $totbayar + $d->bayar;
                            ?>
                            <tr>
                                <td width="40px"><?php echo $d->nobukti; ?></td>
                                <td width="90px"><?php echo $d->tglbayar; ?></td>
                                <td align="right"><?php echo uang($d->bayar); ?></td>
                                <td width="180px" align="right">
                                    <a class="fa fa-trash btn-danger btn btn-sm" href="<?php echo base_url();?>apotek/penjualan/hapus_pembayaran/<?php echo $d->nobukti;?>/<?php echo $d->no_fak_penj;?>"></a>
                                    <a class="fa fa-pencil btn-warning btn btn-sm edit" data-id="<?php echo $d->nobukti;?>" ></a>
                                    <a class="fa fa-print btn-info btn btn-sm" href="<?php echo base_url();?>apotek/penjualan/print_nota/<?php echo $d->no_fak_penj;?>/<?php echo $d->nobukti;?>" ></a>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td bgcolor="#b0b0b0" colspan="1" align="center" style="font-size:12px"><b>TOTAL</b></td>
                            <td bgcolor="#b0b0b0" colspan="2" align="right" style="font-size:12px"><b><?php echo uang($totbayar); ?></b></td>
                            <td bgcolor="#b0b0b0"></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </form>
</div>


<div id="modal-form" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 align="center" class="m-t-none m-b">INPUT PEMBAYARAN CICILAN</h3>
                        <form class="needs-validation form" role="form" id="form2" action="<?php echo base_url();?>apotek/penjualan/detailpenjualan" novalidate method="POST" enctype="multipart/form-data">

                            <div hidden="" class="form-group">
                                <label class="control-label">
                                    No. Fak <span class="symbol "></span>
                                </label>
                                <div class="input-group">
                                    <input  type="text" readonly="" placeholder="No. Faktur" value="<?php echo $penjualan['no_fak_penj'];?>" name="no_fak_penj" class="form-control input-sm">
                                </div>
                            </div>

                            <div hidden="" class="form-group">
                                <label class="control-label">
                                    No. Bukti <span class="symbol "></span>
                                </label>  
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                    <input  type="text" readonly="" placeholder="No. Bukti" value="<?php echo $nobukti;?>" name="nobukti" class="form-control input-sm">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">
                                    Tanggal Bayar<span class="symbol "></span>
                                </label>  
                                <input type="text" name="tglbayar" value="<?php echo date('Y-m-d');?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="control-label">
                                    Jumlah Bayar <span class="symbol "></span>
                                </label>
                                <input type="text" autocomplete="off" autofocus placeholder="Masukan Jumlah Bayar" name="bayar" id="bayar" class="form-control" >
                            </div>

                            <div>
                                <button class="btn btn-primary fa fa-save pull-left"  name="submit" data-style="zoom-in"> Bayar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal Input -->
<div id="modal-bayar" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row" id="loadform">

                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){

        // Form Validasi
        $("#form2").submit(function(){

            var bayar     = $("#bayar").val();

            if(bayar == ""){

                swal("Oops","Jumlah Bayar Harus Diisi","warning");
                $("#bayar").focus();
                return false;

            }else{
                swal("Berhasil","Berhasil Di Simpan","success");
                return true;
            }

        });

        //Edit Data Modal
        $('.edit').click(function(){
            var nobukti = $(this).attr("data-id");
            $("#modal-bayar").modal("show");
            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>apotek/penjualan/edit_histori_piutang",
                data    : {nobukti:nobukti},
                cache   : false,
                success : function(respond){

                    $("#loadform").html(respond);
                }

            });

        });


    });
</script>