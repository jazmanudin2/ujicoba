<?php 
$total = 0;
foreach($listmpobat->result() as $d){ 
    $total = $total + $d->subtotal;
    ?>
    <tr>
        <td><?php echo $d->id_barang; ?></td>
        <td><?php echo $d->nama_gol_obat; ?></td>
        <td><?php echo $d->nama_kandungan_obat; ?></td>
        <td><?php echo $d->merk_dag; ?></td>
        <td align="right"><?php echo uang($d->harga_jual); ?></td>
        <td><?php echo $d->jumlah; ?></td>
        <td align="right"><?php echo uang($d->subtotal); ?></td>
        <td><a data-kode="<?php echo $d->id_barang_m;?>" data-jml="<?php echo $d->jumlah;?>"  data-dtl="<?php echo $d->id_dtlbarang_temp;?>" class="fa fa-trash btn-danger btn btn-sm hapus_tmp sum_total" ></a>
        </td>
    </tr>
<?php } ?>
<tr>
    <td colspan="6" align="center"><b>TOTAL</b></td>
    <td align="right"><h3><?php echo uang($total); ?></h3></td>
    <td></td>
    <td hidden="">
        <input  class="form-control input-sm" value="<?php echo $total;?>" readonly="" name="subtotalobat" id="subtotalobat" type="text" placeholder="Subtotal">
    </td>
</tr>   
<script type="text/javascript">

    function sum_tmp(){

        $.ajax({

            type    : 'POST',
            url     : '<?php echo base_url(); ?>apotek/penjualan/sum_tmp',
            cache   : false,
            success :function(respond){

                $("#sum_tmp").html(respond);
            }

        });
    } 


    function cektemp(){

        $.ajax({

            type    : 'POST',
            url     : '<?php echo base_url(); ?>apotek/penjualan/cektemp',
            cache   : false,
            success :function(respond){

                $("#cektemp").val(respond);

            }

        });
    } 

    // Menampilkan Data Detail Penjumalan Temp
    function tampiltmpobat(){

        // Mengosongkan Inputan Text Ketika Sudah Di Klik Tambah
        $("#id_barang_m").val("");
        $("#id_barang").val("");
        $("#jumlah_perbox").val("");
        $("#no_batch").val("");
        $("#exp_date").val("");
        $("#harga_satuan").val("");
        $("#jumlah_stok").val("");
        $("#box").val("");
        $("#jumlah_satuan").val("");
        $("#subtotal").val("");

        $.ajax({
            type    : 'GET',
            url     : '<?php echo base_url();?>apotek/penjualan/view_temp_barangobat',
            data    : '',
            success : function (html) {
                $("#tampiltmpobat").html(html);
            }
        });

    }

    $('.hapus_tmp').click(function(e){
        e.preventDefault();
        var id_dtlbarang_temp = $(this).attr("data-dtl");
        $.ajax({

            type : 'POST',
            url  : '<?php echo base_url();?>apotek/penjualan/hapus_tmp',
            data : {id_dtlbarang_temp:id_dtlbarang_temp},
            cache:false,
            success:function(respond){
                cektemp();
                sum_tmp();
                tampiltmpobat();
            }
        });

    }); 

</script>