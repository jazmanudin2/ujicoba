<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Transaksi Penjualan</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Transaksi</a>
            </li>
            <li>
                <a>Penjualan</a>
            </li>
            <li class="active">
                <strong>Input Penjualan</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <form class="form-horizontal" method="POST" id="form" action="<?php echo base_url();?>apotek/penjualan/input">
        <div class="row">
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="form-group">
                            <label class="col-lg-3 control-label">No. Struk</label>
                            <div class="col-lg-5">
                                <div class="input-group">
                                    <input  type="hidden" value="<?php echo $nobukti;?>" name="nobukti" class="form-control">
                                    <input type="hidden" name="cektemp" id="cektemp">
                                    <input type="hidden" name="cekbarang" id="cekbarang">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                    <input  type="text" readonly="" placeholder="No. Faktur" value="<?php echo $kodeunik;?>" name="no_fak_penj" class="form-control input-sm">
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="data_1">
                            <label class="col-lg-3 control-label">Tanggal</label>
                            <div class="col-lg-5">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input  type="text" autocomplete="off" class="input-sm form-control" value="<?php echo date('Y-m-d H:i:s');?>" name="tanggal"/>
                                </div>  
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Jenis Pelanggan</label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon required"><i class="fa fa-users required"></i></span>
                                    <select  name="id_jenis_pelanggan" id="id_jenis_pelanggan" class="chosen-select form-control input-sm"  tabindex="1">
                                        <option value="">Pilih Jenis Pelanggan</option>
                                        <?php foreach ($jenis_pelanggan->result() as $d){ ?>
                                            <option value="<?php echo $d->id_jenis_pelanggan;?>">
                                                <?php echo $d->nama_jenis_pelanggan; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Pelanggan</label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon required"><i class="fa fa-users required"></i></span>
                                    <select  name="id_pelanggan" id="id_pelanggan" class="form-control input-sm"  tabindex="2">
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="bg-info p-xs b-r-sm" style="min-height:170px;">
                    <font size="5">
                        <b>TOTAL</b>
                    </font>
                    <span class="info-box-icon" style="min-height:170px;">
                        <i class="fa fa-shopping-cart"></i>
                    </span>
                    <br>
                    <br>
                    <font size="10">
                        <b><p align="right" id="sum_tmp"></p></b>
                    </font>
                </div>
            </div>



            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="row">

                                    <div hidden="" class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                            <input  type="text" name="id_barang_m" id="id_barang_m" readonly="" placeholder="Kode Barang" class="form-control input-sm">
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon "><a href="#" class="caribarangobat"><i class="fa fa-search "></i> Obat</a></span>
                                            <span class="input-group-addon"><a href="#" class="caribarangumum"><i class="fa fa-search"></i> Umum</a></span>
                                            <span class="input-group-addon"><a href="#" class="caribarangalkes"><i class="fa fa-search"></i> Alkes</a></span>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                            <input type="text" placeholder="Kode Barang" name="id_barang" id="id_barang" readonly="" class="form-control input-sm"> 
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                            <input  class="form-control input-sm" readonly="" name="jumlah_stok" id="jumlah_stok" type="number" placeholder="Stok">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm" readonly="" name="jumlah_perbox" id="jumlah_perbox" type="number" placeholder="Jumlah/Box">
                                        </div>
                                    </div>

                                    <div hidden="" class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm" readonly="" name="harga_beli" id="harga_beli" type="number" placeholder="Harga Beli">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <hr>
                            <div class="col-sm-12">
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm carijenisharga" readonly="" name="harga_jual" id="harga_jual" type="number" placeholder="Harga Jual">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm subtotal subtotal3 jml_stok" name="jumlah" id="jumlah" type="number" placeholder="Jumlah">
                                        </div>
                                    </div>

                                    <div hidden="" class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm subtotal subtotal3 jml_stok" name="jml_stok" id="jml_stok" type="text" placeholder="Hasil Jumlah Stok">
                                        </div>
                                    </div>

                                    <div hidden="" class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm subtotal subtotal3" name="exp_date" id="exp_date" type="text" placeholder="Exp Date">
                                        </div>
                                    </div>

                                    <div hidden="" class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm subtotal subtotal3" name="id_dtlbarang_temp" id="id_dtlbarang_temp" type="text" >
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm" readonly="" name="subtotal" id="subtotal" type="number" placeholder="Subtotal">
                                            <span class="input-group-addon"><a href="#" class="input_tmp subtotal3"><i class="fa fa-plus"></i></a></span>
                                        </div>
                                    </div>

                                    <div hidden="" class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm" readonly="" name="subtotal3" id="subtotal3" type="text" placeholder="Subtotal">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12"  style="font-size: 12px">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Obat</h3>
                        <table class="table-bordered table">
                            <thead>
                                <tr>
                                    <th width="80px">ID Barang</th>
                                    <th width="120px">Gol Obat</th>
                                    <th width="150px">Kandungan Obat</th>
                                    <th width="150px">Merk Dag</th>
                                    <th width="90px">Harga Jual</th>
                                    <th width="110px">Jumlah</th>
                                    <th width="100px" align="right">Subtotal</th>
                                    <th width="30px">#</th>
                                </tr>
                            </thead>
                            <tbody id="tampiltmpobat" style="font-size: 11px">

                            </tbody>
                        </table>
                        <!-- End Table -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12"  style="font-size: 12px">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Umum</h3>
                        <table class="table-bordered table">
                            <thead>
                                <tr>
                                    <th width="120px">ID Barang</th>
                                    <th>Nama Barang</th>
                                    <th>Golongan Barang</th>
                                    <th>Harga Jual</th>
                                    <th>Jumlah</th>
                                    <th>Subtotal</th>
                                    <th width="30px">#</th>
                                </tr>
                            </thead>
                            <tbody id="tampiltmpumum" style="font-size: 11px">

                            </tbody>
                        </table>
                        <!-- End Table -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12"  style="font-size: 12px">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Alkes</h3>
                        <table class="table-bordered table">
                            <thead>
                                <tr>
                                    <th width="120px">ID Barang</th>
                                    <th>Jenis Barang</th>
                                    <th>Deatil Alkes</th>
                                    <th>Harga Jual</th>
                                    <th>Jumlah</th>
                                    <th>Subtotal</th>
                                    <th width="30px">#</th>
                                </tr>
                            </thead>
                            <tbody id="tampiltmpalkes" style="font-size: 11px">

                            </tbody>
                        </table>
                        <!-- End Table -->
                    </div>
                </div>
            </div>

            <div class="col-sm-6 ">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">


                        <div class="form-group">
                            <label class="col-lg-4 control-label">Jenis Pembayaran</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <select class="form-control subtotal3" name="jenisbayar" id="jenisbayar">
                                        <option value="">--- Pilih Jenis Pembayaran ---</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Debit">Debit</option>
                                        <option value="Kredit">Kredit</option>
                                        <option value="Giro">Giro</option>
                                        <option value="Ditanggung">Ditanggung</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Ket </label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <textarea style="text-align:right" type="text" autocomplete="off" placeholder="Ket" id="keterangan" name="keterangan" class="form-control sisa_bayar subtotal3">-</textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-sm-6 ">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="form-group">
                            <label class="col-lg-5 control-label">PPN (%)</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input style="text-align:right" value="0" type="text" autocomplete="off" placeholder="PPN (%)" id="ppn" name="ppn" class="form-control sisa_bayar total ppn subtotal3">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Potongan</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input style="text-align:right" type="text" autocomplete="off" placeholder="Potongan (Rp.)" id="potongan" value="0" name="potongan" class="form-control sisa_bayar diskon total subtotal3">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Diskon (%)</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input style="text-align:right" type="text" autocomplete="off" placeholder="Diskon (%)" id="diskon" name="diskon" readonly="" value="0" class="form-control sisa_bayar diskon total subtotal3">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Total Bayar</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input style="text-align:right" readonly="" autocomplete="off" type="text" placeholder="Total" id="total2" name="total2" class="form-control input-sm total">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Pembayaran</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input style="text-align:right" type="text"  autocomplete="off" placeholder="Pembayaran" name="pembayaran" id="pembayaran" class="form-control sisa_bayar total kembalian subtotal3">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Sisa Bayar</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input style="text-align:right" readonly="" autocomplete="off" type="text" placeholder="Sisa Bayar" id="sisa_bayar" name="sisa_bayar" class="form-control input-sm ">
                                </div>
                            </div>
                        </div>

                        <div hidden="" class="form-group">
                            <label class="col-lg-5 control-label">Jumlah Cicilan</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input style="text-align:right" readonly="" value="1" autocomplete="off" type="text" placeholder="Total" id="jumlah_cicilan" name="jumlah_cicilan" class="form-control input-sm total">
                                </div>
                            </div>
                        </div>

                        <?php 
                        $dt = mktime(0,0,0,date('n'),date('j')+30,date('y'));
                        $k  = date('Y-m-d',$dt);?>

                        <div class="form-group" id="data_1">
                            <label class="col-lg-5 control-label">Jatuh Tempo</label>
                            <div class="col-lg-4">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" placeholder="Tanggal" value="<?php echo $k;?>" autocomplete="off" id="jatuhtempo" class="form-control input-sm datepicker" name="jatuhtempo" >
                                </div>  
                            </div>
                        </div>

                        <br>
                        <div class="form-group">
                            <div class="col-lg-12 ">
                                <div class="input-group">
                                    <button class="ladda-button btn btn-w-m btn-success submit" name="submit" type="submit"><i class="fa fa-print "></i> Simpan</button> .
                                    <button type="reset" class="btn btn-fw btn-danger"><i class="fa  fa-mail-reply-all"></i>  Batal</button>  
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </form>
</div>

<!------- Modal Barang -------->
<div class="modal fade bd-example-modal-lg" id="Modalbarang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="loadform">

        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){

        tampiltmpobat();
        tampiltmpumum();
        tampiltmpalkes();
        cektemp();
        sum_tmp();

        $("#id_jenis_pelanggan").change(function(){

            var id_jenis_pelanggan = $("#id_jenis_pelanggan").val();
            $.ajax({

                type    : 'POST',
                url     : '<?php echo base_url();?>apotek/penjualan/get_pelanggan',
                data    : {id_jenis_pelanggan:id_jenis_pelanggan},
                cache   : false,
                success : function(respond){

                    $("#id_pelanggan").html(respond);

                }

            });
        });

        // Cek Tmp
        function cektemp(){

            $.ajax({

                type    : 'POST',
                url     : '<?php echo base_url(); ?>apotek/penjualan/cektemp',
                cache   : false,
                success :function(respond){

                    $("#cektemp").val(respond);
                }

            });
        } 

        function sum_tmp(){

            $.ajax({

                type    : 'POST',
                url     : '<?php echo base_url(); ?>apotek/penjualan/sum_tmp',
                cache   : false,
                success :function(respond){

                    $("#sum_tmp").html(respond);
                }

            });
        } 

        // Menampilkan Data Detail Penjumalan Temp
        function tampiltmpobat(){

            // Mengosongkan Inputan Text Ketika Sudah Di Klik Tambah
            $("#id_barang_m").val("");
            $("#id_barang").val("");
            $("#jumlah_stok").val("");
            $("#jumlah").val("");
            $("#subtotal").val("");
            $("#harga_jual").val("");
            $("#harga_beli").val("");
            $("#jumlah_perbox").val("");
            $("#jenis_harga").val("");

            $.ajax({
                type    : 'GET',
                url     : '<?php echo base_url();?>apotek/penjualan/view_temp_barangobat',
                data    : '',
                success : function (html) {
                    $("#tampiltmpobat").html(html);
                }
            });

        }

        // Menampilkan Data Detail Penjumalan Temp
        function tampiltmpumum(){

            // Mengosongkan Inputan Text Ketika Sudah Di Klik Tambah
            $("#id_barang_m").val("");
            $("#id_barang").val("");
            $("#jumlah_stok").val("");
            $("#jumlah").val("");
            $("#subtotal").val("");
            $("#harga_jual").val("");
            $("#harga_beli").val("");
            $("#jumlah_perbox").val("");
            $("#jenis_harga").val("");

            $.ajax({
                type    : 'GET',
                url     : '<?php echo base_url();?>apotek/penjualan/view_temp_barangumum',
                data    : '',
                success : function (html) {
                    $("#tampiltmpumum").html(html);
                }
            });

        }

        // Menampilkan Data Detail Penjumalan Temp
        function tampiltmpalkes(){

            // Mengosongkan Inputan Text Ketika Sudah Di Klik Tambah
            $("#id_barang_m").val("");
            $("#id_barang").val("");
            $("#jumlah_stok").val("");
            $("#jumlah").val("");
            $("#subtotal").val("");
            $("#harga_jual").val("");
            $("#harga_beli").val("");
            $("#jumlah_perbox").val("");
            $("#jenis_harga").val("");

            $.ajax({
                type    : 'GET',
                url     : '<?php echo base_url();?>apotek/penjualan/view_temp_barangalkes',
                data    : '',
                success : function (html) {
                    $("#tampiltmpalkes").html(html);
                }
            });

        }

        // Tombol Modal Cari Barang
        $(".caribarangobat").click(function(e){
            e.preventDefault();
            var kodebarang      = $(this).attr("data-kode");
            $('#Modalbarang').modal("show");

            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/penjualan/view_barang_obat",
                data    : {kodebarang:kodebarang},
                cache   : false,
                success : function(respond){
                    $("#loadform").html(respond);
                }
            });
        });

        // Tombol Modal Cari Barang
        $(".caribarangumum").click(function(e){
            e.preventDefault();
            var kodebarang      = $(this).attr("data-kode");
            $('#Modalbarang').modal("show");

            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/penjualan/view_barang_umum",
                data    : {kodebarang:kodebarang},
                cache   : false,
                success : function(respond){
                    $("#loadform").html(respond);
                }
            });
        });

        // Tombol Modal Cari Barang
        $(".caribarangalkes").click(function(e){
            e.preventDefault();
            var kodebarang      = $(this).attr("data-kode");
            $('#Modalbarang').modal("show");

            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/penjualan/view_barang_alkes",
                data    : {kodebarang:kodebarang},
                cache   : false,
                success : function(respond){
                    $("#loadform").html(respond);
                }
            });
        });


        // Tombol Modal Cari Barang
        $(".carijenisharga").click(function(e){
            e.preventDefault();
            var kodebarang      = $(this).attr("data-kode");
            var id_barang_m     = $("#id_barang_m").val();
            $('#Modalbarang').modal("show");

            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/penjualan/view_jenis_harga",
                data    : {kodebarang:kodebarang,id_barang_m:id_barang_m},
                cache   : false,
                success : function(respond){
                    $("#loadform").html(respond);
                }
            });
        });

        // Penjumlahan Sisa Bayar
        $(".harga_jual").on("input",function(){
            var jenis_harga     = $("#jenis_harga").val();
            var harga_beli      = $("#harga_beli").val();
            $("#harga_jual").val((harga_beli*jenis_harga)+(harga_beli*1));
        });

        // Penjumlahan Sisa Bayar
        $(".subtotal").on("input",function(){
            var subtotal        = $("#subtotal").val();
            var jumlah          = $("#jumlah").val();
            var harga_jual      = $("#harga_jual").val();
            $("#subtotal").val(harga_jual*jumlah);
        });

        // Penjumlahan Sisa Bayar
        $(".jml_stok").on("input",function(){
            var jumlah_stok     = $("#jumlah_stok").val();
            var jumlah          = $("#jumlah").val();
            $("#jml_stok").val((jumlah_stok*1)-(jumlah*1));
        });

        // Penjumlahan Sisa Bayar
        $(".subtotal3").on("input",function(){
            var subtotalalkes        = $("#subtotalalkes").val();
            var subtotalumum         = $("#subtotalumum").val();
            var subtotalobat         = $("#subtotalobat").val();
            $("#subtotal3").val((subtotalalkes*1)+(subtotalumum*1)+(subtotalobat*1));
        });

        $(".sisa_bayar").on("input",function(){
            var pembayaran      = $("#pembayaran").val();
            var potongan        = $("#potongan").val();
            var subtotal3       = $("#subtotal3").val();
            var diskon          = $("#diskon").val();
            var ppn             = $("#ppn").val();
            var biaya_lain      = $("#biaya_lain").val();
            $("#sisa_bayar").val(subtotal3-pembayaran-potongan+(ppn/100*subtotal3));
        });

        $(".diskon").on("input",function(){
            var diskon          = $("#diskon").val();
            var potongan        = $("#potongan").val();
            var subtotal3       = $("#subtotal3").val();
            $("#diskon").val((potongan/subtotal3*100));
        });

        // Subtotal Pembelian
        $(".total").on("input",function(){
            var subtotal3    = $("#subtotal3").val();
            var potongan     = $("#potongan").val();
            var biaya_lain   = $("#biaya_lain").val();
            var ppn          = $("#ppn").val();
            var pembayaran   = $("#pembayaran").val();
            $("#total2").val(subtotal3-potongan+(ppn/100*subtotal3));
        });


        // Inputan Detail Penjualan Temp
        $(".input_tmp").click(function(e){
            e.preventDefault();
            var id_dtlbarang_temp= $("#id_dtlbarang_temp").val();
            var id_barang_m     = $("#id_barang_m").val();
            var id_barang       = $("#id_barang").val();
            var cekbarang       = $("#cekbarang").val();
            var id_jenis_harga  = $("#id_jenis_harga").val();
            var jenis_harga     = $("#jenis_harga").val();
            var harga_beli      = $("#harga_beli").val();
            var exp_date        = $("#exp_date").val();
            var harga_jual      = $("#harga_jual").val();
            var jumlah          = $("#jumlah").val();
            var jml_stok        = $("#jml_stok").val();
            var subtotal        = $("#subtotal").val();
            var jumlah_stok     = $("#jumlah_stok").val();

            if (id_barang_m == 0) {

                swal("Oops", "Data Barang Harus Diisi", "warning");

            } else if (jumlah == 0) {

                swal("Oops", "Jumlah Barang Harus Diisi Dan Tidak Boleh 0", "warning");
                $("#jumlah").focus();

            } else if (jml_stok < 0) {

                swal("Oops", "Jumlah Stok Tidak Cukup", "warning");
                $("#jumlah").focus();

            } else if (cekbarang > 0) {

                swal("Oops", "Barang Sudah Ada, Silahkan Cari ID Barang dan Exp Date Yang Berbeda", "warning");
                $("#jumlah").focus();

            } else {

                $.ajax({
                    type    : 'POST',
                    url     : '<?php echo base_url();?>apotek/penjualan/input_tmp',
                    data    :
                    {
                        id_barang_m : id_barang_m,
                        id_barang   : id_barang,
                        harga_jual  : harga_jual,
                        harga_beli  : harga_beli,
                        jumlah      : jumlah,
                        exp_date    : exp_date,
                        subtotal    : subtotal,
                        id_dtlbarang_temp : id_dtlbarang_temp,
                    },
                    cache   : false,
                    success : function(respond){
                        //swal("Success", "Barang Sudah Di Tambahkan", "success");
                        tampiltmpumum();
                        tampiltmpobat();
                        tampiltmpalkes();
                        cektemp();
                        sum_tmp();
                    }

                });
            }
        });

        // DataTable Barang
        $("#tabelbarang").DataTable();

        // Select Data Pelanggan
        $('.chosen-select').chosen({width: "100%"});

        // Form Validasi
        $("#form").submit(function(){

            var pelanggan       = $("#id_pelanggan").val();
            var dp              = $("#dp").val();
            var potongan        = $("#potongan").val();
            var mulai_cicilan   = $("#mulai_cicilan").val();
            var jumlah_cicilan  = $("#jumlah_cicilan").val();
            var cektemp         = $("#cektemp").val();
            var kode_barang     = $("#kode_barang").val();
            var nama_barang     = $("#nama_barang").val();

            if(pelanggan == ""){

                swal("Oops","Data Pelanggan Harus Diisi","warning");
                $("#id_pelanggan").focus();
                return false;

            }else if(cektemp < 1){

                swal("Oops","Barang Belum Ada Yang Dipilih","warning");
                $("#nama_barang").focus();
                return false;

            }else if(dp == ""){

                swal("Oops","DP Harus Diisi","warning");
                $("#dp").focus();
                return false;

            }else if(potongan == ""){

                swal("Oops","Potongan Harus Diisi","warning");
                $("#potongan").focus();
                return false;

            }else if(jumlah_cicilan == ""){

                swal("Oops","Jumlah Cicilan Harus Diisi","warning");
                $("#jumlah_cicilan").focus();
                return false;

            }else if(mulai_cicilan == ""){

                swal("Oops","Mulai Cicilan Harus Diisi","warning");
                $("#mulai_cicilan").focus();
                return false;

            }else if(jenisbayar == ""){

                swal("Oops","Jenis Bayar Harus Diisi","warning");
                $("#jenisbayar").focus();
                return false;

            }else{

                swal("Berhasil","Berhasil Di Simpan","success");
                return true;
            }
        });
        
    });
</script>