<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Data Piutang Pelanggan</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Piutang</a>
            </li>
            <li>
                <a>Data Piutang</a>
            </li>
            <li class="active">
                <strong>Data Piutang Pelanggan</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Tabel View Barang -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Piutang Pelanggan</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive" >
                        <div class="col-md-12">
                            <form class="form-horizontal" method="post" action="" autocomplete="off">

                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" value="<?php echo $nama_pelanggan;?>" id="nama_pelanggan" name="nama_pelanggan" class="form-control" placeholder="Pencarian">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="submit" name="submit" class="btn btn-sm bg-blue m-2-15 waves-effect" value="Cari Data">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </form>
                        </div>
                        <table class="table table-striped table-bordered table-hover" ">
                            <thead >
                                <tr>
                                    <th>NO</th>
                                    <th>KODE</th>
                                    <th>NAMA PELANGGAN</th>
                                    <th>NO HP</th>
                                    <th>JUMLAH PIUTANG</th>
                                    <th>JUMLAH BAYAR</th>
                                    <th>SISA BAYAR</th>
                                    <th>KETERANGAN</th>
                                    <th>AKSI</th>
                                </tr>
                            </thead>

                            <body>
                                <?php $no=1;?>
                                <?php foreach ($data as $d) {?>
                                    <tr>
                                        <td><?php echo $no++;?></td>
                                        <td><?php echo $d['id_pelanggan'];?></td>
                                        <td><?php echo $d['nama_pelanggan'];?></td>
                                        <td><?php echo $d['telp_pelanggan'];?></td>
                                        <td align="right"><?php echo uang($d['totalpiutang']);?></td>
                                        <td align="right"><?php echo uang($d['totalbayar']);?></td>
                                        <td align="right"><?php echo uang($d['sisabayar']);?></td>
                                        <?php if ($d['sisabayar'] > 0) {?>
                                            <td bgcolor="#ff5c5c" align="center"><font size="3">Belum Lunas</font></td>
                                        <?php }else{?>
                                            <td bgcolor="#84efef" align="center"><font size="3">Sudah Lunas</font></td>
                                        <?php }?>
                                        <td><a class="fa fa-address-card btn-info btn btn-sm" href="<?php echo base_url();?>apotek/Penjualan/view_historipiutang/<?php echo $d['id_pelanggan']; ?>"></a></td>
                                    </tr>
                                <?php }?>
                            </body>

                        </table>
                        <div style='margin-top: 10px;'>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive" >

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="loadform">

</div>

<script type="text/javascript">

    $(function(){

        // Detail Data Modal

        $('.detail').click(function(){
            var kodebarang = $(this).attr("data-id");
            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/barang/detai_historipelanggan",
                data    : {kodebarang:kodebarang},
                cache   : false,
                success : function(respond){

                    $("#loadform").html(respond);
                }
            });
        });


    });

</script>

