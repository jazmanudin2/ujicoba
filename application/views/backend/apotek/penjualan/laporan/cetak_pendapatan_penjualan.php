<title>Laporan Pendapatan Penjualan</title>
LAPORAN PENDAPATAN PENJUALAN<br>
PERIODE <?php echo DateToIndo2($dari)." s/d ".DateToIndo2($sampai); ?>
<table class="datatable3" style="width:50%" border="1">
	<thead bgcolor="#024a75" style="color:white; font-size:20;">
		<tr bgcolor="#024a75" style="color:white; font-size:20;">
			<td rowspan="2">No</td>
			<td rowspan="2">Tanggal</td>
			<td rowspan="2" style="background-color:#024a75">Pendapatan Bruto</td>
			<td rowspan="2" style="background-color:#024a75">Pendapatan Netto</td>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no 		= 1;
		$totals		= 0;
		$netto		= 0;
		foreach ($penjualan as $p ){ 
			$totals		= $totals + $p->totals;
			$netto		= $netto + $p->harga_juals-$p->harga_belis;
			?>
			<tr>
				<td><?php echo $no++;?></td>
				<td><?php echo $p->tanggal;?></td>
				<td><?php echo uang($p->totals);?></td>
				<td><?php echo uang($p->harga_juals-$p->harga_belis);?></td>
			</tr>
		<?php }?>
		<tr>
			<td align="center" colspan="2">Total</td>
			<td><?php echo uang($totals);?></td>
			<td ><?php echo uang($netto);?></td>
		</tr>
	</tbody>
</table>
