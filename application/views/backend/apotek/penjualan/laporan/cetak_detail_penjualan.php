LAPORAN PENJUALAN BARANG OBAT<br>
PERIODE <?php echo DateToIndo2($dari)." s/d ".DateToIndo2($sampai); ?><br>

<?php 
if($pelanggan['nama_pelanggan'] != ""){
	echo "NAMA PELANGGAN : ". strtoupper($pelanggan['nama_pelanggan']);
}
?>

<table class="datatable3" style="width:150%" border="1">
	<thead bgcolor="#024a75" style="color:white; font-size:12;">
		<tr bgcolor="#024a75" style="color:white; font-size:12;">
			<td rowspan="2">No</td>
			<td rowspan="2">Tanggal</td>
			<td rowspan="2">No Faktur</td>
			<td rowspan="2">Kode Pel.</td>
			<td rowspan="2">Nama Pelanggan</td>
			<td rowspan="2">Kode Barang</td>
			<td rowspan="2">Merk Dagang</td>
			<td rowspan="2">Gololang Obat</td>
			<td rowspan="2">Dosis</td>
			<td rowspan="2">Kandungan Obat</td>
			<td colspan="3" align="center">QTY</td>
			<td rowspan="2">Total</td>
		</tr>
		<tr bgcolor="#024a75" style="color:white; font-size:12;">
			<td>Jumlah</td>
			<td>Harga</td>
			<td>Subtotal</td>
		</tr>
	</thead>
	<tbody>
		<?php 
		$no 		= 1;
		foreach ($penjualan as $p ){ 
			?>
			<tr>
				<td><?php echo $no++;?></td>
				<td><?php echo $p->tanggal;?></td>
				<td><?php echo $p->no_fak_penj;?></td>
				<td><?php echo $p->id_pelanggan;?></td>
				<td><?php echo $p->nama_pelanggan;?></td>
				<td><?php echo $p->id_barang;?></td>
				<td><?php echo $p->nama_merk_dag;?></td>
				<td><?php echo $p->nama_gol_obat;?></td>
				<td><?php echo $p->dosis;?></td>
				<td><?php echo $p->id_kandungan_obat;?></td>
				<td><?php echo $p->jumlah;?></td>
				<td><?php echo $p->harga_jual;?></td>
				<td><?php echo $p->subtotal;?></td>
				<td><?php echo $p->total;?></td>
			</tr>
		<?php }?>
	</tbody>
</table>
