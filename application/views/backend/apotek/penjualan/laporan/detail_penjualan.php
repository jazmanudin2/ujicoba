<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Data Pelanggan</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Data Master</a>
            </li>
            <li class="active">
                <strong>Data Pelanggan</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Table View apt -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Data Pelanggan</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content">
                    <div class="table-responsive" >
                        <div class="col-md-12">
                            <form class="FormRetur " id="formValidate" method="POST" action="<?php echo base_url(); ?>apotek/laporan_penjualan/cetak_detail_penjualan" target="_blank">


                                <div class="form-group">
                                    <label class="font-normal">Pelanggan</label>
                                    <select  name="pelanggan" id="pelanggan" class="chosen-select form-control input-sm"  tabindex="2">
                                        <option value="">-- Semua Pelanggan --</option>
                                        <?php foreach ($pelanggan->result() as $d){ ?>
                                            <option value="<?php echo $d->id_pelanggan;?>">
                                                <?php echo $d->nama_pelanggan; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                                

                                <div class="form-group">
                                    <label class="font-normal">Jenis Transaksi</label>
                                    <select class="form-control show-tick" id="jenistransaksi" name="jenistransaksi" data-error=".errorTxt1" data-live-search="true">
                                        <option value="">Semua Jenis Transaksi</option>
                                        <option value="cash">Cash</option>
                                        <option value="kredit">Kredit</option>
                                    </select>
                                </div>

                                <div class="form-group" id="data_5">
                                    <label class="font-normal">Periode</label>
                                    <div class="input-daterange input-group" id="datepicker">
                                        <input type="text"  autocomplete="off" class="input-sm form-control" name="dari" id="dari"/>
                                        <span class="input-group-addon">to</span>
                                        <input type="text" autocomplete="off" class="input-sm form-control" name="sampai" id="sampai" />
                                    </div>
                                </div>

                                <div class="form-group" >
                                    <button type="submit"  name="submit" class="btn btn-sm btn-success waves-effect">
                                        <span>CETAK</span>
                                    </button>
                                    <button type="submit"  name="export" class="btn btn-sm btn-primary waves-effect">
                                        <span>EXPORT EXCEL</span>
                                    </button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $(function(){

        $('#data_5 .input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $(".FormRetur").submit(function(){

            var dari    = $("#dari").val();
            var sampai  = $("#sampai").val();

            if(dari == 0 ){
                swal("Oops!", "Silahkan Masukan Periode Dahulu  !", "warning");
                return false;
            }else if(sampai == 0 ){
                swal("Oops!", "Silahkan Masukan Periode Dahulu  !", "warning");
                return false;
            }else{
                return true;
            }


        });


        $("#pelanggan").change(function(){

            var pelanggan = $("#pelanggan").val();
            $.ajax({

                type    : 'POST',
                url     : '<?php echo base_url();?>apotek/laporan_penjualan/get_pelanggan',
                data    : {pelanggan:pelanggan},
                cache   : false,
                success : function(respond){

                    $("#pelanggan").html(respond);
                    $("#pelanggan").selectpicker("refresh");

                }


            });

        });


    });

</script>