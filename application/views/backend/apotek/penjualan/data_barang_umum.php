<div class="modal-header">
    <h3 class="modal-title" id="exampleModalLabel">Data Barang Umum</h3>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <table class="table table-bordered" id="tabelbarang">
        <thead>
            <tr>
                <th>ID Barang</th>
                <th>Nama Barang</th>
                <th>Golongan Barang</th>
                <th>Exp Date</th>
                <th>Stok</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($barang_obat->result() as $b){ ?> 
                <tr>
                    <td width="70"><?php echo $b->id_barang; ?></td>
                    <td><?php echo $b->nama_barang; ?></td>
                    <td><?php echo $b->gol_barang; ?></td>
                    <td><?php echo $b->exp_date; ?></td>
                    <td><?php echo $b->totalstok; ?></td>
                    <td><a href="#" data-kode="<?php echo $b->id_barang_m;?>" data-id="<?php echo $b->id_barang;?>" data-merk="<?php echo $b->nama_barang;?>" data-harga="<?php echo $b->harga_beli;?>" data-stok="<?php echo $b->totalstok;?>" data-perbox="<?php echo $b->jumlah_perbox;?>" data-dtl="<?php echo $b->id_dtlbarang;?>" class="btn btn-sm btn-primary pilihbarang">Pilih</a></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>    
</div>

<script type="text/javascript">

    function cekbarang(){

        var id_dtlbarang_temp = $("#id_dtlbarang_temp").val();
        $.ajax({
            type    : 'POST',
            url     : '<?php echo base_url(); ?>apotek/penjualan/cekbarang',
            cache   : false,
            data    : {id_dtlbarang_temp:id_dtlbarang_temp},
            success :function(respond){

                $("#cekbarang").val(respond);

            }

        });
    } 

    $(".pilihbarang").click(function(e){
        e.preventDefault();
        $('#Modalbarang').modal("hide");
        $("#id_barang_m").val($(this).attr('data-kode'));
        $("#id_barang").val($(this).attr('data-id'));
        $("#nama_merk_dag").val($(this).attr('data-merk'));
        $("#nama_gol_obat").val($(this).attr('data-gol'));
        $("#kandungan_obat").val($(this).attr('data-kand'));
        $("#jumlah_stok").val($(this).attr('data-stok'));
        $("#id_dtlbarang_temp").val($(this).attr('data-dtl'));
        $("#harga_beli").val($(this).attr('data-harga'));
        $("#jumlah_perbox").val($(this).attr('data-perbox'));
        cekbarang();
    });

    $("#tabelbarang").DataTable();

</script>