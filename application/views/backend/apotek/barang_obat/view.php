<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Data Stok Obat</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Data Master</a>
            </li>
            <li class="active">
                <strong>Data Stok Obat</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Table View apt -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Data Stok Obat</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content">
                    <div class="table-responsive">

                        <div class="col-md-12">
                            <form class="form-horizontal" method="post" action="" autocomplete="off">
                                <div class="row clearfix">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">
                                                ID Barang <span class="symbol "></span>
                                            </label>
                                            <div class="form-line">
                                                <input type="text" value="<?php echo $id_barang;?>" id="id_barang" name="id_barang" class="form-control" placeholder="ID Barang">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Golongan Obat <span class="symbol "></span>
                                            </label>
                                            <select name="id_gol_obat" id="id_gol_obat" class="form-control">
                                                <option value="">-- Semua Data --</option>  
                                                <?php foreach ($gol_obat->result() as $d){ ?>
                                                    <!-- Looping Option Untuk Data sediaan -->
                                                    <option value="<?php echo $d->id_gol_obat; ?>"><?php echo $d->nama_gol_obat; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-1">
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Kandungan Obat <span class="symbo"></span>
                                            </label>
                                            <select name="id_kandungan_obat" id="id_kandungan_obat" class="form-control"> </select>
                                        </div>
                                    </div>

                                    <div class="col-lg-1">
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Merk Dag<span class="symbol"></span>
                                            </label>
                                            <div class="form-line">
                                                <input type="text" value="<?php echo $merk_dag;?>" id="merk_dag" name="merk_dag" class="form-control" placeholder="Merk Dagang">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row clearfix">
                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="submit" name="submit" class="btn btn-sm btn-success" value="Cari Data">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="submit" name="submit" class="btn btn-sm btn-ifo" value="Reset Data">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>

                        <table class="table table-striped table-bordered table-hover" style="font-size: 11px" >
                            <thead >
                                <form action="" method="GET">
                                    <tr>
                                        <td colspan="13">
                                            <a class="input fa fa-plus-square btn-success btn"> Tambah</a>
                                            <a class="delete_all fa fa-trash btn-sm btn btn-danger"> Hapus Select</a>
                                            <a href="<?php echo base_url('apotek/barang_obat/form'); ?>"  class="fa fa-upload btn btn-primary btn-sm"> Import</a>
                                        </td>
                                    </tr>
                                </form>

                                <tr>
                                    <th width="10px"><input type="checkbox" id="master"></th>
                                    <th width="90px">ID Barang</th>
                                    <th width="110px">Gol Obat</th>
                                    <th width="110px">Kandungan Obat</th>
                                    <th width="120px">Merek Dagang</th>
                                    <th width="50px">Dosis</th>
                                    <th width="90px">Sediaan</th>
                                    <th width="100px">Jenis Obat</th>
                                    <th width="40px">Stok</th>
                                    <th width="90px">Exp Date</th>
                                    <th width="200px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sno            = $row+1;
                                foreach ($data as $d) {
                                    $id_user     = $this->session->userdata('id_admin');
                                    $id_barang_m = $d['id_barang_m'];
                                    $query       = $this->db->query("
                                        SELECT exp_date,SUM(stok) AS totalstok FROM 
                                        tbl_detailbarang
                                        WHERE tbl_detailbarang.id_admin = '$id_user' AND id_barang_m = '$id_barang_m'
                                        ORDER BY 
                                        exp_date ASC
                                        ");
                                    $dtl     = $query->row_array();
                                    $exp_date       = $dtl['exp_date']; 
                                    $sekarang       = date("d-m-Y"); 
                                    $masaberlaku    = strtotime($exp_date) - strtotime($sekarang); 
                                    ?>
                                    <tr id="">
                                        <td>
                                            <input type="checkbox" class="sub_chk" data-id="<?php echo $d['id_barang_m']; ?>">
                                        </td>
                                        <td><?php echo $d['id_barang'];?></td>
                                        <td><?php echo $d['nama_gol_obat'];?></td>
                                        <td><?php echo $d['nama_kandungan_obat'];?></td>
                                        <td><?php echo $d['merk_dag'];?></td>
                                        <td><?php echo $d['dosis'];?></td>
                                        <td><?php echo $d['sediaan'];?></td>
                                        <td><?php echo $d['nama_jenis_obat'];?></td>

                                        <?php if ($dtl['totalstok'] <= $d['min_stok'] ) {?>
                                            <td bgcolor="yellow"><font color='black' size='2'><?php echo $dtl['totalstok'];?></font></td>
                                        <?php }  else if ($dtl['totalstok'] == 0 ) {?>
                                            <td bgcolor="red"><font color='white' size='2'><?php echo $dtl['totalstok'];?></font></td>
                                        <?php } else {?>
                                            <td><?php echo $dtl['totalstok'];?></td>
                                        <?php }?>


                                        <?php if($masaberlaku/(24*60*60)<1) {?> 
                                            <td bgcolor="red"><font color='white' size='2'><?php echo $dtl['exp_date'];?></font></td>
                                        <?php } else if($masaberlaku/(24*60*60)<10) {?> 
                                            <td bgcolor="#FA8072"><font color='white' size='2'><?php echo $dtl['exp_date'];?> </font></td>
                                        <?php } else if($masaberlaku/(24*60*60)<30) {?> 
                                            <td bgcolor="#FF6347"><font color='white' size='2'><?php echo $dtl['exp_date'];?> </font></td>
                                        <?php } else if($masaberlaku/(24*60*60)<90) {?> 
                                            <td bgcolor="green"><font color='white' size='2'><?php echo $dtl['exp_date'];?> </font></td>
                                        <?php } else if($masaberlaku/(24*60*60)>91) {?> 
                                            <td><font size='2'><?php echo $dtl['exp_date'];?> </font></td>
                                        <?php }?>

                                        <td> 
                                            <div class="text-center">
                                                <a class="jenis_harga fa fa-money btn-success btn btn-sm" data-harga="<?php echo $d['id_barang_m'];?>"></a>
                                                <a class="fa fa fa-address-card btn-info btn btn-sm" href="<?php echo base_url();?>apotek/barang_obat/detail/<?php echo $d['id_barang_m']; ?>"></a>
                                                <a class="edit fa fa-edit btn-warning btn btn-sm" data-id="<?php echo $d['id_barang_m']; ?>"></a>
                                                <a name="submit" class="fa fa fa-barcode btn-primary btn btn-sm" href="<?php echo base_url();?>apotek/barang_alkes/barcode_obat/<?php echo $d['id_barang_m']; ?>"></a>
                                                <a class="hapus fa fa-trash btn-danger btn btn-sm" data-id="<?php echo $d['id_barang_m']; ?>"></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php }?>
                            </tbody>
                        </table>
                        <div style='margin-top: 10px;'>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div id="modal-barang" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row" id="loadform">

                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

    $(function(){


        $("#id_gol_obat").change(function(){

            var id_gol_obat = $("#id_gol_obat").val();
            $.ajax({

                type    : 'POST',
                url     : '<?php echo base_url();?>apotek/barang_obat/get_kandungan_obat2',
                data    : {id_gol_obat:id_gol_obat},
                cache   : false,
                success : function(respond){

                    $("#id_kandungan_obat").html(respond);

                }

            });
        });

        // Hapus Checkbox
        $('#master').on('click', function(e) {
            if($(this).is(':checked',true))  
            {
                $(".sub_chk").prop('checked', true);  
            } else {  
                $(".sub_chk").prop('checked',false);  
            }  

        });

        $('.delete_all').on('click', function(e) {

            var allVals = [];  
            $(".sub_chk:checked").each(function() {  
                allVals.push($(this).attr('data-id'));
            });  
            if(allVals.length <=0)  
            {  
                swal({
                    title:"Silahkan Select Checkbox Terlebih Dahulu",
                    type: "warning",
                });

            }  else {  

                var check = swal({
                    title:"Hapus Data",
                    text:"Yakin Akan Menghapus Data ini??",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Hapus",
                    closeOnConfirm: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                }).then((result) => {
                    if (result.value) {
                        var join_selected_values = allVals.join(","); 
                        $.ajax({
                            url: "<?php echo base_url(); ?>apotek/barang_obat/delete_checkbox",
                            type: 'POST',
                            data: 'id_barang_m='+join_selected_values,
                            success: function (data) {
                              console.log(data);
                              $(".sub_chk:checked").each(function() {  
                                  $(this).parents("tr").remove();
                              });
                              swal(
                                'Hapus',
                                'Data Berhasil Di Hapus',
                                'success'
                                );
                          },
                          error: function (data) {
                            alert(data.responseText);
                        }

                    });
                        $.each(allVals, function( index, value ) {
                          $('table tr').filter("[data-row-id='" + value + "']").remove();
                      });
                    }
                });  
            }  

        });

        //Hapus Data apt
        $('.hapus').click(function(){
            var kode = $(this).attr("data-id");

            swal({
                title:"Hapus Data",
                text:"Yakin Akan Menghapus Data ini??",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Hapus",
                closeOnConfirm: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
            }).then((result) => {
                if (result.value) {
                  swal(
                    'Hapus',
                    'Data Berhasil Di Hapus',
                    'success'
                    );
                  $(location).attr('href','<?php echo base_url()?>apotek/barang_obat/hapus/'+kode);
              }
          })

        }); 

        //Edit Data Modal

        $('.edit').click(function(){
            var id_barang_m = $(this).attr("data-id");
            $("#modal-barang").modal("show");
            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/barang_obat/edit",
                data    : {id_barang_m:id_barang_m},
                cache   : false,
                success : function(respond){

                    $("#loadform").html(respond);
                }

            });

        });

        //Edit Data Modal

        $('.jenis_harga').click(function(){
            var id_barang_m = $(this).attr("data-harga");
            $("#modal-barang").modal("show");
            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/barang_obat/jenis_harga",
                data    : {id_barang_m:id_barang_m},
                cache   : false,
                success : function(respond){

                    $("#loadform").html(respond);
                }

            });

        });

        //Input Data Modal

        $('.input').click(function(){
            var id_barang_m = $(this).attr("data-id");
            $("#modal-barang").modal("show");
            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/barang_obat/input",
                data    : {id_barang_m:id_barang_m},
                cache   : false,
                success : function(respond){

                    $("#loadform").html(respond);
                }

            });

        });


        


    });

</script>

