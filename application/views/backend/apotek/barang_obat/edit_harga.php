<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT DATA JENIS HARGA STOK OBAT</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/barang_obat/edit_harga" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div hidden="" class="form-group">
            <label class="control-label">
                ID Barang Medliz <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="id_barang_m" value="<?php echo $harga['id_barang_m'];?>" readonly placeholder="Masukan ID Barang Medliz" class="form-control">
        </div>
        <div hidden="" class="form-group">
            <label class="control-label">
                ID Jenis Harga <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="id_jenis_harga" value="<?php echo $harga['id_jenis_harga'];?>" readonly placeholder="Masukan ID Barang Medliz" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Profit <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" name="nama_profit" id="nama_profit" value="<?php echo $harga['nama_profit']; ?>" class="form-control harga_jual" required>
        </div>
        <div class="form-group">
            <label class="control-label">
                Profit <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" name="profit" id="profit" value="<?php echo $harga['profit']; ?>" class="form-control harga_jual" required>
        </div>
        <div class="form-group">
            <label class="control-label">
                HNA <span class="symbol "></span>
            </label>
            <input  autocomplete="off" readonly="" type="text" name="hna" id="hna" value="<?php echo $harga['hna']; ?>" class="form-control"required>
        </div>
        <div class="form-group">
            <label class="control-label">
                Harga Jual <span class="symbol "></span>
            </label>
            <input  autocomplete="off" readonly="" type="text" name="harga_jual" id="harga_jual" value="<?php echo $harga['harga_jual']; ?>" class="form-control"required>
        </div>
        <div class="form-group">
            <label class="control-label">
                Keterangan <span class="symbol "></span>
            </label>
            <select name="keterangan" id="keterangan" class="form-control" required>
                <?php foreach ($keterangan->result() as $d){ ?>
                    <!-- Looping Option Untuk Data merk_dag -->
                    <option value="<?php echo $d->id_keterangan_harga; ?>"><?php echo $d->nama_keterangan_harga; ?></option>
                    
                <?php } ?>
            </select>
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>


<script>
    $(document).ready(function(){

        $(".harga_jual").on("input",function(){
            var hna             = $("#hna").val();
            var profit          = $("#profit").val();
            $("#harga_jual").val((profit/100*hna)+(hna*1));
        });

        $("#form").validate({
            rules: {
                keterangan: {
                    required: true
                },
                id_barang: {
                    required: true
                },
                id_merk_dag: {
                    required: true
                },
                id_gol_obat: {
                    required: true
                },
                id_kandungan_obat: {
                    required: true
                },
                dosis: {
                    required: true
                },
                sediaan: {
                    required: true
                },
                min_stok: {
                    required: true
                }
            },
            messages: {
                keterangan: {
                    required: "Opps, Silahkan Pilih Jenis Obat..!",
                },
                id_barang: {
                    required: "Opps, ID Barang Tidak Boleh Kosong..!",
                },
                id_merk_dag: {
                    required: "Opps, Owner Apotek Tidak Boleh Kosong..!",
                },
                id_gol_obat: {
                    required: "Opps, Penanggung Jawab Tidak Boleh Kosong..!",
                },
                id_kandungan_obat: {
                    required: "Opps, Telp Penanggung Jawab Tidak Boleh Kosong..!"
                },
                dosis: {
                    required: "Opps, dosis Tidak Boleh Kosong..!",
                },
                sediaan: {
                    required: "Opps, sediaan Tidak Boleh Kosong..!",
                },
                min_stok: {
                    required: "Opps, min_stok Tidak Boleh Kosong..!",
                }
            }
        });

    });
</script>