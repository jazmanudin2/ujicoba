<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT DATA STOK OBAT</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/barang_obat/edit" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div hidden="" class="form-group">
            <label class="control-label">
                ID Barang Medliz <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="id_barang_m" value="<?php echo $barang_obat['id_barang_m'];?>" readonly placeholder="Masukan ID Barang Medliz" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                ID Barang <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="id_barang" value="<?php echo $barang_obat['id_barang'];?>" placeholder="Masukan ID Barang" required="" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Jenis Obat <span class="symbol "></span>
            </label>
            <select name="id_jenis_obat" id="id_jenis_obat" class="form-control" required>
                <option value="<?php echo $barang_obat['id_jenis_obat'];?>" ><?php echo $barang_obat['nama_jenis_obat'];?></option>  
                <?php foreach ($jenis_obat->result() as $d){ ?>
                    <!-- Looping Option Untuk Data merk_dag -->
                    <option value="<?php echo $d->id_jenis_obat; ?>"><?php echo $d->nama_jenis_obat; ?></option>
                    
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Merk Dagang <span class="symbol "></span>
            </label>
            <select name="id_merk_dag" id="id_merk_dag" class="form-control" required>
                <option value="<?php echo $barang_obat['id_merk_dag'];?>"><?php echo $barang_obat['nama_merk_dag'];?></option>  
                <?php foreach ($merk_dag->result() as $d){ ?>
                    <!-- Looping Option Untuk Data merk_dag -->
                    <option value="<?php echo $d->id_merk_dag; ?>"><?php echo $d->nama_merk_dag; ?></option>
                    
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Golongan Obat <span class="symbol "></span>
            </label>
            <select name="id_gol_obat" id="id_gol_obat" class="form-control" required>
                <option value="<?php echo $barang_obat['id_gol_obat']; ?>"><?php echo $barang_obat['nama_gol_obat']; ?></option>  
                <?php foreach ($gol_obat->result() as $d){ ?>
                    <!-- Looping Option Untuk Data merk_dag -->
                    <option value="<?php echo $d->id_gol_obat; ?>"><?php echo $d->nama_gol_obat; ?></option>
                    
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Dosis <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" name="dosis" id="dosis" value="<?php echo $barang_obat['dosis']; ?>" class="form-control"required>
        </div>
        <div class="form-group">
            <label class="control-label">
                Sediaan <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" name="sediaan" id="sediaan" value="<?php echo $barang_obat['sediaan']; ?>" class="form-control"required>
        </div>
        <div class="form-group">
            <label class="control-label">
                HNA <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" name="hna" id="hna" value="<?php echo $barang_obat['hna']; ?>" class="form-control" required>
        </div>
        <div class="form-group">
            <label class="control-label">
                HET <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" name="het" id="het" value="<?php echo $barang_obat['het']; ?>" class="form-control" required>
        </div>
        <div class="form-group">
            <label class="control-label">
                Minimal Stok <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" name="min_stok" value="<?php echo $barang_obat['min_stok']; ?>" id="min_stok" class="form-control"required>
        </div>
        <div class="form-group">
            <label class="control-label">
                Keterangan <span class="symbol"></span>
            </label>
            <textarea autocomplete="off" type="text" name="keterangan" class="form-control" ><?php echo $barang_obat['keterangan']; ?></textarea>
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>

