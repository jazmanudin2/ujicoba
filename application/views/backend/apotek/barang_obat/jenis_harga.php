<div class="col-lg-12">
    <table class="table table-striped table-bordered table-hover" style="font-size: 14px">
       <thead>
        <tr>
            <th width="150px">ID Barang</th>
            <th width="5px">:</th>
            <td><?php echo $barang['id_barang'];?></td>
        </tr>
        <tr>
            <th>Merk Dagang</th>
            <th>:</th>
            <td><?php echo $barang['merk_dag'];?></td>
        </tr>
        <tr>
            <th>HNA</th>
            <th>:</th>
            <td><?php echo $barang['hna'];?></td>
        </tr>
        <tr>
            <th>HET</th>
            <th>:</th>
            <td><?php echo $barang['het'];?></td>
        </tr>
        <tr>
            <th>Ket</th>
            <th>:</th>
            <td><h5><?php echo $barang['keterangan'];?></h5></td>
        </tr>
    </thead>
    <table class="table table-striped table-bordered table-hover" style="font-size: 12px">
        <thead>
            <tr>
                <th width="95px">Nama Profit</th>
                <th width="40px">Profit</th>
                <th width="100px">Harga Jual</th>
                <th>Keterangan</th>
            </tr>
        </thead>

        <tbody>
            <?php 
            $sno  = 1;
            foreach ($harga->result() as $d) { ?> 
                <tr id="">
                    <td><?php echo $d->nama_profit;?></td>
                    <td><?php echo $d->profit;?> %</td>
                    <td>Rp. <?php echo uang($d->harga_jual);?></td>
                    <td><?php echo $d->nama_keterangan_harga;?></td>
                </tr>
            <?php }?>
        </tbody>
    </table>
</table>
</div>