<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Import Data Barang Obat</h2>
		<ol class="breadcrumb">
			<li>
				<a href="<?php echo base_url();?>dashboard">Home</a>
			</li>
			<li>
				<a>Data Master</a>
			</li>
			<li>
				<a>Barang</a>
			</li>
			<li class="active">
				<strong>Import Data Barang Obat</strong>
			</li>
		</ol>
	</div>
</div>

<!-- Tabel View Barang Obat -->
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Import Data Barang Obat</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
						<a class="dropdown-toggle" data-toggle="dropdown" href="#">
							<i class="fa fa-wrench"></i>
						</a>
						<ul class="dropdown-menu dropdown-user">
							<li><a href="#">Config option 1</a>
							</li>
							<li><a href="#">Config option 2</a>
							</li>
						</ul>
						<a class="close-link">
							<i class="fa fa-times"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content">
					<div class="table-responsive" >
						<div class="col-md-12">
							<h3 align="center">Form Import Data Barang Obat</h3>
							<hr>
							<div class="row clearfix">
								<div class="col-sm-1">
									<div class="form-group">
										<a class="btn-sm btn btn-primary fa fa-download" href="<?php echo base_url("assets/backend/apotek/csv/barang_obat/import_data.csv"); ?>"> Download Contoh</a>
									</div>
								</div>
							</div>
							<div class="row clearfix">
								<div class="col-sm-12">
									<div class="form-group">
										<h5>Catatan :</h5>
										<p>1. Format Excel Harus Seperti Contoh (Format .CSV)</p>
										<p>2. ID Barang Obat Tidak Boleh Sama , Yang di Exel Maupun Yang Sudah Ada</p>
									</div>
								</div>
							</div>
							<hr>
							<form method="post" id="form" action="<?php echo base_url("apotek/barang_obat/import"); ?>" enctype="multipart/form-data">
								<div class="row clearfix">
									<div class="col-sm-3 offset-1">
										<div class="form-group">
											<input type="file" class="form-control" id="file" name="file">
										</div>
									</div>
									<label>
										<input type="submit" class="btn btn-sm btn-info" name="submit" value="Submit">
									</label>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script>
	$(document).ready(function(){
		$("#kosong").hide();

		$("#form").submit(function(){

			var file     = $("#file").val();

			if(file == ""){

				swal("Oops","Silahkan Masukan File Excel Berdasarkan Format Yang di Sudah di Tentukan","warning");
				$("#file").focus();
				return false;

			}
		});
	});
</script>

