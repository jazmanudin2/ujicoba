<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Data Stok Obat</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Data Master</a>
            </li>
            <li class="active">
                <strong>Detail Stok Obat</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Table View apt -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Detail Stok Obat</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content">
                    <div class="table-responsive" >
                        <div class="col-md-12">
                            <form class="FormRetur " id="formValidate" method="POST" action="<?php echo base_url(); ?>apotek/laporan_barang/cetak_barang_obat" target="_blank">

                                <div class="form-group">
                                    <label class="font-normal">Gol Obat</label>
                                    <select  name="gol_obat" id="gol_obat" class="chosen-select form-control input-sm"  tabindex="2">
                                        <option value="">-- Semua Golongan Obat --</option>
                                        <?php foreach ($gol_obat->result() as $d){ ?>
                                            <option value="<?php echo $d->id_gol_obat;?>">
                                                <?php echo $d->nama_gol_obat; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <div class="form-group" >
                                    <button type="submit"  name="submit" class="btn btn-sm btn-success waves-effect">
                                        <span>CETAK</span>
                                    </button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $(function(){

        $('#data_5 .input-daterange').datepicker({
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });

        $(".FormRetur").submit(function(){

            var dari    = $("#dari").val();
            var sampai  = $("#sampai").val();

            if(dari == 0 ){
                swal("Oops!", "Silahkan Masukan Periode Dahulu  !", "warning");
                return false;
            }else if(sampai == 0 ){
                swal("Oops!", "Silahkan Masukan Periode Dahulu  !", "warning");
                return false;
            }else{
                return true;
            }


        });



    });

</script>