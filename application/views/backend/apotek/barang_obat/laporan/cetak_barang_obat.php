<title>LAPORAN STOK OBAT</title>
LAPORAN STOK OBAT <br>
<!-- 
<?php 
if($gol_obat['id_gol_obat'] != ""){

	echo "GOLONGAN OBAT : ". strtoupper($gol_obat['nama_gol_obat']);
}
?> -->

<table class="datatable3" style="width:130%" border="1">
	<thead bgcolor="#024a75" style="color:white; font-size:14;">
		<tr bgcolor="#024a75" >
			<td width="20px" rowspan="2">No</td>
			<td width="70px" rowspan="2">ID Barang</td>
			<td width="130px" rowspan="2">Gol Obat</td>
			<td width="130px" rowspan="2">Kandungan Obat</td>
			<td width="130px" rowspan="2">Merk Dagang</td>
			<td width="120px" rowspan="2">Jenis Obat</td>
			<td width="30px" rowspan="2">Dosis</td>
			<td width="70px" rowspan="2">Sediaan</td>
			<td width="55px" rowspan="2">Min Stok</td>
			<td width="100px" rowspan="2">HNA</td>
			<td width="100px" rowspan="2">HET</td>
			<td width="130px" rowspan="2">Supplier</td>
			<td width="90px" rowspan="2">Jumlah Perbox</td>
			<td width="60px" rowspan="2">No Batch</td>
			<td width="40px" rowspan="2">Stok</td>
			<td width="80px" rowspan="2">Harga Beli</td>
			<td width="70px" rowspan="2">Exp Date</td>
			<td width="100px" rowspan="2">Keterangan</td>
		</tr>
	</thead>
	<tbody style="color:black; font-size:12;">
		<?php 
		$no 		= 1;
		foreach ($data as $p ){ 
			?>
			<tr >
				<td><?php echo $no++;?></td>
				<td><?php echo $p->id_barang;?></td>
				<td><?php echo $p->nama_gol_obat;?></td>
				<td><?php echo $p->nama_kandungan_obat;?></td>
				<td><?php echo $p->merk_dag;?></td>
				<td><?php echo $p->nama_jenis_obat;?></td>
				<td><?php echo $p->dosis;?></td>
				<td><?php echo $p->sediaan;?></td>
				<td><?php echo $p->min_stok;?></td>
				<td><?php echo $p->hna;?></td>
				<td><?php echo $p->het;?></td>
				<td><?php echo $p->nama_perusahaan;?></td>
				<td><?php echo $p->jumlah_perbox;?></td>
				<td><?php echo $p->no_batch;?></td>
				<td><?php echo $p->stok;?></td>
				<td><?php echo $p->harga_beli;?></td>
				<td><?php echo $p->exp_date;?></td>
				<td><?php echo $p->keterangan;?></td>
			</tr>
		<?php }?>
	</tbody>
</table>
