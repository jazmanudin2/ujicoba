<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Detail Stok Obat</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Data Master</a>
            </li>
            <li>
                <a>Data Stok Obat</a>
            </li>
            <li class="active">
                <strong>Detail Stok Obat</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Table View apt -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <div class="col-lg-6">
                            <table class="table table-striped table-hover" style="font-size: 14px;font-variant: arial" >
                                <tbody>
                                    <tr>
                                        <th width="170px">ID Barang</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['id_barang'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Golongan Obat</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['nama_gol_obat'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Kandungan</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['nama_kandungan_obat'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Merk Dagang</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['merk_dag'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Dosis</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['dosis'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Sediaan</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['sediaan'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">HNA</th>
                                        <th width="10">:</th>
                                        <td>Rp. <?php echo uang($detail['hna']);?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">HET</th>
                                        <th width="10">:</th>
                                        <td>Rp. <?php echo uang($detail['het']);?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        
                        <div class="col-lg-6">
                            <table class="table table-striped table-hover" style="font-size: 14px;" >
                                <tbody>
                                    <tr>
                                        <th width="170px">Jenis Obat</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['nama_jenis_obat'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Minimal Stok</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['min_stok'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Jumlah Per Box</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['jumlah_perbox'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Main Supplier</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['nama_perusahaan'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="150px">Keterangan</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['keterangan'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="150px">Created At</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['created_at'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="150px">Updated At</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['updated_at'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Barcode</th>
                                        <th width="10">:</th>
                                        <td><img width="120px" height="50" src="<?php echo base_url();?>assets/backend/apotek/barcode/barang_obat/<?php echo $detail['barcode'];?>"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>


                        <div class="col-lg-12">
                            <table class="table table-striped table-hover" style="font-size: 14px;" >
                                <table class="table table-striped table-bordered table-hover" style="font-size: 12px">
                                    <thead>
                                        <tr>
                                            <th width="15">No.</th>
                                            <th width="100px">Nama Profit</th>
                                            <th width="50px">Profit</th>
                                            <th width="120px">Harga Jual</th>
                                            <th>Keterangan</th>
                                            <th width="50px">Aksi</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php 
                                        $sno  = 1;
                                        foreach ($harga->result() as $d) {
                                            ?> 
                                            <tr id="">
                                                <td><?php echo $sno++;?></td>
                                                <td><?php echo $d->nama_profit;?></td>
                                                <td><?php echo $d->profit;?> %</td>
                                                <td>Rp. <?php echo uang($d->harga_jual);?></td>
                                                <td><?php echo $d->nama_keterangan_harga;?></td>
                                                <td><a class="edit_harga fa fa-edit btn-warning btn btn-sm" data-id="<?php echo $d->id_jenis_harga; ?>"></a></td>
                                            </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <div class="col-lg-12">
                            <h3>Data Barang Masuk</h3>
                            <table class="table table-striped table-hover" style="font-size: 14px;" >
                                <table class="table table-striped table-bordered table-hover" style="font-size: 12px">
                                    <thead>
                                        <tr>
                                            <th width="85px">Tanggal</th>
                                            <th width="80">No. Batch</th>
                                            <th width="80">Exp Date</th>
                                            <th>Supplier</th>
                                            <th width="100px">No Faktur</th>
                                            <th width="65px">Per-Box</th>
                                            <th width="100px">Harga Satuan</th>
                                            <th width="80px">Subtotal</th>
                                            <th width="80px">Jml Pembelian</th>
                                            <th width="20px">Sisa</th>
                                            <th width="90px">Warning</th>
                                            <th width="50px">Ket</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php 
                                        $sno  = 1;
                                        $jumlah_perbox  = 0;
                                        $jumlah_stok    = 0;
                                        $harga_beli     = 0;
                                        foreach ($details->result() as $d) {
                                            $jumlah_stok    = $jumlah_stok + $d->stok;
                                            $jumlah_perbox  = $jumlah_perbox + $d->jumlah_perbox;
                                            $harga_beli     = $harga_beli + $d->harga_beli;
                                            $exp_date       = $d->exp_date; 
                                            $sekarang       = date("d-m-Y"); 
                                            $masaberlaku    = strtotime($exp_date) - strtotime($sekarang);
                                            ?> 
                                            <?php if ($d->stok == !0) { ?>
                                                <tr id="">
                                                    <td><?php echo $d->tanggal;?></td>
                                                    <td><?php echo $d->no_batch;?></td>
                                                    <td><?php echo $d->exp_date;?></td>
                                                    <td><?php echo $d->nama_perusahaan;?></td>
                                                    <td><?php echo $d->no_fak_pemb;?></td>
                                                    <td><?php echo $d->jumlah_perbox;?></td>
                                                    <td align="right"><?php echo uang($d->harga_beli);?></td>
                                                    <td align="right"><?php echo uang($d->subtotal);?></td>
                                                    <td><?php echo $d->jumlah_satuan;?></td>
                                                    <td><?php echo $d->stok;?></td>

                                                    <?php if($masaberlaku/(24*60*60)<1) {?> 
                                                        <td><font color='red'  size='2'><?php echo "Sudah Habis";?></font></td>
                                                    <?php } else if($masaberlaku/(24*60*60)<30) {?> 
                                                        <td><font color='orange' size='2'><?php echo floor($masaberlaku/(24*60*60))." hari lagi";?>  </font></td>
                                                    <?php } else if($masaberlaku/(24*60*60)<60) {?> 
                                                        <td><font color='blue' size='2'><?php echo floor($masaberlaku/(24*60*60))." hari lagi";?> </font></td>
                                                    <?php } else if($masaberlaku/(24*60*60)>61) {?> 
                                                        <td><font color='blak' size='2'><?php echo floor($masaberlaku/(24*60*60))." hari lagi";?>  </font></td>
                                                    <?php }?>
                                                    <td><?php echo $d->jenis_mutasi;?></td>
                                                </tr>
                                            <?php }else{ ?>

                                            <?php } ?>
                                        <?php }?>
                                        <tr>
                                            <td colspan="8" align="center"><b>TOTAL</b></td>
                                            <td align="right"></td>
                                            <td><h3><?php echo $jumlah_stok; ?></h3></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Modal Edit -->
<div id="modal-harga" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row" id="loadform2">

                </div>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

    $(function(){

        //Edit Data Modal

        $('.edit_harga').click(function(){
            var id_jenis_harga = $(this).attr("data-id");
            $("#modal-harga").modal("show");
            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>apotek/barang_obat/edit_harga",
                data    : {id_jenis_harga:id_jenis_harga},
                cache   : false,
                success : function(respond){

                    $("#loadform2").html(respond);
                }

            });

        });

        //Input Data Modal

    });

</script>

