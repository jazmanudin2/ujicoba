<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">INPUT DATA KANDUNGAN OBAT</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/kandungan_obat/input" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div hidden="" class="form-group">
            <label class="control-label">
                ID Kandungan Obat <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text"  name="id_kandungan_obat" readonly placeholder="Masukan Id Kandungan Obat" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Golongan Obat <span class="symbol "></span>
            </label>
            <select name="id_gol_obat" id="id_gol_obat" class="form-control" required>
                <option value="">-- Pilih Golongan Obat --</option>  
                <?php foreach ($gol_obat->result() as $d){ ?>
                    <!-- Looping Option Untuk Data sediaan -->
                    <option value="<?php echo $d->id_gol_obat; ?>"><?php echo $d->nama_gol_obat; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Kandungan Obat <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Nama Jenis" name="nama_kandungan_obat" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
               Ket <span class="symbol "></span>
           </label>
           <input  autocomplete="off" type="text" placeholder="Masukan keterangan" name="keterangan" class="form-control" >
       </div>
       <div>
        <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
    </div>
</form>
</div>
