<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">INPUT DATA KARYAWAN</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/karyawan/input" novalidate method="POST" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                NIK <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" required="" name="nik" placeholder="Masukan NIK" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Depan <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="text" placeholder="Masukan Nama Depan" required name="nama_depan" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Belakang <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="text" placeholder="Masukan Nama Belakang" required name="nama_belakang" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Jabatan <span class="symbol "></span>
            </label>
            <select  name="id_jabatan" id="id_jabatan" class="chosen-select form-control input-sm"  tabindex="2" required="">
                <option required value="">--- Pilih Jabatan ---</option>
                <?php foreach ($jabatan->result() as $d){ ?>
                    <option required value="<?php echo $d->id_jabatan;?>">
                        <?php echo $d->nama_jabatan; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Jenis Kelamin <span class="symbol "></span>
            </label>
            <select  name="jk" class="chosen-select form-control input-sm"  tabindex="2" required="">
                <option required value="">--- Pilih Jenis Kelamin ---</option>
                <option value="Laki-Laki">Laki-Laki</option>
                <option value="Perempuan">Perempuan</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Pendidikan Terakhir <span class="symbol "></span>
            </label>
            <select  name="pendidikan_terakhir" class="chosen-select form-control input-sm"  tabindex="2"  required="">
                <option required value="">--- Pilih Pendidikan Terakhir ---</option>
                <option value="SD">SD/MI</option>
                <option value="SMP">SMP/MTs</option>
                <option value="SMA">MA/SMA/SMK</option>
                <option value="D1">D1</option>
                <option value="D2">D2</option>
                <option value="D3">D3</option>
                <option value="S1">S1</option>
                <option value="S2">S2</option>
                <option value="S3">S3</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Agama <span class="symbol "></span>
            </label>
            <select  name="agama" class="chosen-select form-control input-sm"  tabindex="2"  required="">
                <option required value="">--- Pilih Agama ---</option>
                <option value="Islam">Islam</option>
                <option value="Kristen">Kristen</option>
                <option value="Katolik">Katolik</option>
                <option value="Hindu">Hindu</option>
                <option value="Buddha">Buddha</option>
                <option value="Kong Hu Chu">Kong Hu Chu</option>
                <option value="Agama Lainnya">Agama Lainnya</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Tempat Lahir <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="text" placeholder="Masukan Tempat Lahir" required name="tempat_lahir" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Tanggal Lahir <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="text" placeholder="Masukan Tgl Lahir (27-Maret-1998)" required name="tgl_lahir" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Alamat <span class="symbol "></span>
            </label>
            <textarea autocomplete="off" type="text" required name="alamat" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <label class="control-label">
                Provinsi <span class="symbol "></span>
            </label>
            <select name="provinsi" id="provinsi" class="chosen-select form-control input-sm"  required>
                <option value="">--- Pilih Provinsi ---</option>  
                <?php foreach ($listprovinsi->result() as $d){ ?>
                    <!-- Looping Option Untuk Data Provinsi -->
                    <option value="<?php echo $d->id_provinsi; ?>"><?php echo $d->nama_provinsi; ?></option>
                    
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Kota <span class="symbol "></span>
            </label>
            <select  autocomplete="off" type="text" placeholder="Masukan Kota" name="kabupaten" id="kabupaten" class="form-control" required>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Kecamatan <span class="symbol"></span>
            </label>
            <select  autocomplete="off" type="text" placeholder="Masukan Kecamatan" name="kecamatan" id="kecamatan" class="form-control" required>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Kelurahan <span class="symbol"></span>
            </label>
            <select  autocomplete="off" type="text" placeholder="Masukan Kecamatan" name="desa" id="desa" class="form-control" required>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Telp <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="text" placeholder="Masukan Telp" required name="telp" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                No HP <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="text" placeholder="Masukan No HP" required name="no_hp" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Email <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="email" placeholder="Masukan Email" name="email" class="form-control" >
        </div>
        <div class="form-group" id="data_1">
            <label class="control-label">Mulai Kerja</label>
            <div class="input-group date">
                <input autocomplete="off" type="text" autocomplete="off" placeholder="Masukan Mulai Kerja" class="input-sm form-control" name="mulai_kerja"/>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>  
        </div>
        <div hidden="" class="form-group">
            <label class="control-label">
                Status <span class="symbol "></span>
            </label>
            <select  name="status" class="form-control" required="">
                <option value="Aktif">Aktif</option>
                <option value="Tidak Aktif">Tidak Aktif</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Keterangan <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="text" placeholder="Masukan Keterangan" value="-" required name="keterangan" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Foto <span class="symbol "></span>
            </label>
            <input type="file" name="foto" class="form-control">
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>


<script>
    $(document).ready(function(){

        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $("#provinsi").change(function(){

            id = $("#provinsi").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>apotek/karyawan/get_kota',
                data  : {id_provinsi:id},
                cache : false,
                success:function(respond){

                    $("#kabupaten").html(respond);
                }

            });

        });

        // Select Data Pelanggan
        $('.chosen-select').chosen({width: "100%"});

        $("#kabupaten").change(function(){

            id = $("#kabupaten").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>apotek/karyawan/get_kec',
                data  : {id_kota:id},
                cache : false,
                success:function(respond){

                    $("#kecamatan").html(respond);
                }

            });

        });


        $("#kecamatan").change(function(){

            id = $("#kecamatan").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>apotek/karyawan/get_desa',
                data  : {id_kecamatan:id},
                cache : false,
                success:function(respond){

                    $("#desa").html(respond);
                }

            });

        });



        $("#form").validate({
            rules: {
                id_jenis_apt: {
                    required: true
                },
                nama_jenis_karyawan: {
                    required: true
                },
                owner_apt: {
                    required: true
                },
                penanggungjwb: {
                    required: true
                },
                telp_penanggungjwb: {
                    required: true,
                    number: true
                },
                alamat: {
                    required: true
                },
                provinsi: {
                    required: true
                },
                kota: {
                    required: true
                },
                kecamatan: {
                    required: true
                },
                kelurahan: {
                    required: true
                },
                telp1: {
                    required: true,
                    number: true
                },
                telp2: {
                    number: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                id_jenis_apt: {
                    required: "Opps, Jenis Apotek Tidak Boleh Kosong..!",
                },
                nama_jenis_karyawan: {
                    required: "Opps, Nama Jenis Karyawan Tidak Boleh Kosong..!",
                },
                owner_apt: {
                    required: "Opps, Owner Apotek Tidak Boleh Kosong..!",
                },
                penanggungjwb: {
                    required: "Opps, Penanggung Jawab Tidak Boleh Kosong..!",
                },
                telp_penanggungjwb: {
                    required: "Opps, Telp Penanggung Jawab Tidak Boleh Kosong..!",
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                alamat: {
                    required: "Opps, Alamat Tidak Boleh Kosong..!",
                },
                provinsi: {
                    required: "Opps, Provinsi Tidak Boleh Kosong..!",
                },
                kota: {
                    required: "Opps, Kota Tidak Boleh Kosong..!",
                },
                kecamatan: {
                    required: "Opps, Kecamatan Tidak Boleh Kosong..!",
                },
                kelurahan: {
                    required: "Opps, Kelurahan Tidak Boleh Kosong..!",
                },
                no_hp: {
                    required: "Opps, NO HP Tidak Boleh Kosong..!",
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                telp: {
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                email: {
                    required: "Opps, Email Tidak Boleh Kosong..!",
                    email: "Opps, Cek Kembali Email Anda..!! Example : infomedliz@medliz.com",
                }
            }
        });

    });
</script>