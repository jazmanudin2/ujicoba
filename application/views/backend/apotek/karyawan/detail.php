<div class="row wrapper border-bottom white-bg page-heading"> <div class="col-lg-10">
    <h2>Detail Karyawan</h2>
    <ol class="breadcrumb">
        <li>
            <a href="<?php echo base_url();?>dashboard">Home</a>
        </li>
        <li>
            <a>Data Master</a>
        </li>
        <li>
            <a>Data Karyawan</a>
        </li>
        <li class="active">
            <strong>Detail Karyawan</strong>
        </li>
    </ol>
</div>
</div>

<!-- Table View apt -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <div class="col-lg-5">
                            <table class="table table-striped table-hover" style="font-size: 12px;font-variant: arial" >
                                <tbody>
                                    <tr>
                                        <th width="170px">Nama Depan</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['nama_depan'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Nama Belakang</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['nama_belakang'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">NIK</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['nik'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Jabatan</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['nama_jabatan'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Tanggal Lahir</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['tgl_lahir'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Tempat Lahir</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['tempat_lahir'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Jenis Kelamin</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['jk'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Pendidikan Terakhir</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['pendidikan_terakhir'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Agama</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['agama'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Keterangan</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['keterangan'];?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-4">
                            <table class="table table-striped table-hover" style="font-size: 12px;" >
                                <tbody>
                                    <tr>
                                        <th width="170px">Email</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['email'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">NO HP</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['no_hp'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Telp</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['telp'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="130px">Alamat</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['alamat'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="130px">Provinsi</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['nama_provinsi'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="130px">Kota/Kabupaten</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['nama_kota'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="130px">Kecamatan</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['nama_kecamatan'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="130px">Kelurahan/Desa</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['nama_desa'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Created At</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['created_at'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Updated At</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['updated_at'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="130px">Status</th>
                                        <th width="10">:</th>
                                        <td><?php echo $karyawan['status'];?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-2">
                            <table class="table table-striped table-hover" style="font-size: 12px;" >
                                <tbody>
                                    <tr>
                                        <td colspan="3">
                                            <img width="200px" height="190px" src="<?php echo base_url();?>assets/apotek/Karyawan/<?php echo $karyawan['foto'];?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <form action="<?php echo base_url().'apotek/karyawan/upload_image'?>" method="post" enctype="multipart/form-data">
                                                <input type="text" name="nik" value="<?php echo $karyawan['nik'];?>">
                                                <input type="file" name="filefoto">
                                                <button type="submit">Upload</button>
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
