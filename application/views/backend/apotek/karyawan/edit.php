<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT DATA KARYAWAN</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/karyawan/edit" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                NIK <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" name="nik" placeholder="Masukan NIK" value="<?php echo $karyawan['nik'];?>" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Depan <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="text" placeholder="Masukan Nama Depan" value="<?php echo $karyawan['nama_depan'];?>" required name="nama_depan" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Belakang <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="text" placeholder="Masukan Nama Belakang" value="<?php echo $karyawan['nama_belakang'];?>" required name="nama_belakang" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Jabatan <span class="symbol "></span>
            </label>
            <select  name="id_jabatan" id="id_jabatan" class="form-control" required="">
                <option required value="<?php echo $karyawan['id_jabatan'];?>"><?php echo $karyawan['nama_jabatan'];?></option>
                <?php foreach ($jabatan->result() as $d){ ?>
                    <option required value="<?php echo $d->id_jabatan;?>">
                        <?php echo $d->nama_jabatan; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Jenis Kelamin <span class="symbol "></span>
            </label>
            <select  name="jk" class="form-control" required="">
                <option required value="<?php echo $karyawan['jk'];?>"><?php echo $karyawan['jk'];?></option>
                <option value="Laki-Laki">Laki-Laki</option>
                <option value="Perempuan">Perempuan</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Pendidikan Terakhir <span class="symbol "></span>
            </label>
            <select  name="pendidikan_terakhir" class="form-control" required="">
                <option required value="<?php echo $karyawan['pendidikan_terakhir'];?>"><?php echo $karyawan['pendidikan_terakhir'];?></option>
                <option value="SD">SD/MI</option>
                <option value="SMP">SMP/MTs</option>
                <option value="SMA">MA/SMA/SMK</option>
                <option value="D1">D1</option>
                <option value="D3">D2</option>
                <option value="D4">D3</option>
                <option value="S1">S1</option>
                <option value="S2">S2</option>
                <option value="S3">S3</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Agama <span class="symbol "></span>
            </label>
            <select  name="agama" class="form-control" required="">
                <option required value="<?php echo $karyawan['agama'];?>"><?php echo $karyawan['agama'];?></option>
                <option value="Islam">Islam</option>
                <option value="Kristen">Kristen</option>
                <option value="Katolik">Katolik</option>
                <option value="Hindu">Hindu</option>
                <option value="Buddha">Buddha</option>
                <option value="Kong Hu Chu">Kong Hu Chu</option>
                <option value="Agama Lainnya">Agama Lainnya</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Tempat Lahir <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="text" placeholder="Masukan Tempat Lahir" value="<?php echo $karyawan['tempat_lahir'];?>" required name="tempat_lahir" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Tanggal Lahir <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="text" value="<?php echo $karyawan['tgl_lahir'];?>" placeholder="Masukan Tgl Lahir (27-Maret-1998)" required name="tgl_lahir" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Alamat <span class="symbol "></span>
            </label>
            <textarea autocomplete="off" type="text"  required name="alamat" class="form-control"><?php echo $karyawan['alamat'];?></textarea>
        </div>
        <div class="form-group">
            <label class="control-label">
                Provinsi <span class="symbol "></span>
            </label>
            <select name="id_provinsi" id="provinsi" class="form-control" required>
                <option value="<?php echo $karyawan['id_provinsi'];?>"><?php echo $karyawan['nama_provinsi'];?></option>  
                <?php foreach ($listprovinsi->result() as $d){ ?>
                    <!-- Looping Option Untuk Data Provinsi -->
                    <option value="<?php echo $d->id_provinsi; ?>"><?php echo $d->nama_provinsi; ?></option>

                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Kota <span class="symbol "></span>
            </label>
            <select  autocomplete="off" type="text" placeholder="Masukan Kota" value="<?php echo $karyawan['id_kota'];?>" name="id_kota" id="kabupaten" class="form-control" required>
                <option value="<?php echo $karyawan['id_kota'];?>"><?php echo $karyawan['nama_kota'];?></option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Kecamatan <span class="symbol"></span>
            </label>
            <select  autocomplete="off" type="text" placeholder="Masukan Kecamatan" value="<?php echo $karyawan['id_kecamatan'];?>" name="id_kecamatan" id="kecamatan" class="form-control" required>
                <option value="<?php echo $karyawan['id_kecamatan'];?>"><?php echo $karyawan['nama_kecamatan'];?></option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Kelurahan <span class="symbol"></span>
            </label>
            <select  autocomplete="off" type="text" placeholder="Masukan Kecamatan" value="<?php echo $karyawan['id_desa'];?>"  name="id_desa" id="desa" class="form-control" required>
                <option value="<?php echo $karyawan['id_desa'];?>"><?php echo $karyawan['nama_desa'];?></option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Telp <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="text" placeholder="Masukan Telp" value="<?php echo $karyawan['telp'];?>" required name="telp" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                No HP <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="text" placeholder="Masukan No HP" value="<?php echo $karyawan['no_hp'];?>" required name="no_hp" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Email <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="email" placeholder="Masukan Email" value="<?php echo $karyawan['email'];?>" name="email" class="form-control" >
        </div>
        <div class="form-group" id="data_1">
            <label class="control-label">Mulai Kerja</label>
            <div class="input-group date">
                <input autocomplete="off" value="<?php echo $karyawan['mulai_kerja'];?>" type="text" autocomplete="off" placeholder="Masukan Mulai Kerja" class="input-sm form-control" name="mulai_kerja"/>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>  
        </div>
        <div hidden="" class="form-group">
            <label class="control-label">
                Status <span class="symbol "></span>
            </label>
            <select  name="status" class="form-control" required="">
                <option value="<?php echo $karyawan['status'];?>">Status <?php echo $karyawan['status'];?></option>
                <option value="Aktif">Aktif</option>
                <option value="Tidak Aktif">Tidak Aktif</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Keterangan <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="text" placeholder="Masukan Keterangan" value="<?php echo $karyawan['keterangan'];?>" required name="keterangan" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Foto <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="file" placeholder="Masukan Keterangan" required name="foto" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>


<script>
    $(document).ready(function(){

        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        $("#provinsi").change(function(){

            id = $("#provinsi").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>apotek/karyawan/get_kota',
                data  : {id_provinsi:id},
                cache : false,
                success:function(respond){

                    $("#kabupaten").html(respond);
                }

            });

        });



        $("#kabupaten").change(function(){

            id = $("#kabupaten").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>apotek/karyawan/get_kec',
                data  : {id_kota:id},
                cache : false,
                success:function(respond){

                    $("#kecamatan").html(respond);
                }

            });

        });


        // Select Data Pelanggan
        $('.chosen-select').chosen({width: "100%"});

        
        
        $("#kecamatan").change(function(){

            id = $("#kecamatan").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>apotek/karyawan/get_desa',
                data  : {id_kecamatan:id},
                cache : false,
                success:function(respond){

                    $("#desa").html(respond);
                }
                
            });
            
        });
        

        $("#form").validate({
            rules: {
                id_jenis_apt: {
                    required: true
                },
                nama_jenis_karyawan: {
                    required: true
                },
                owner_apt: {
                    required: true
                },
                penanggungjwb: {
                    required: true
                },
                telp_penanggungjwb: {
                    required: true,
                    number: true
                },
                alamat: {
                    required: true
                },
                provinsi: {
                    required: true
                },
                kota: {
                    required: true
                },
                kecamatan: {
                    required: true
                },
                kelurahan: {
                    required: true
                },
                telp1: {
                    required: true,
                    number: true
                },
                telp2: {
                    number: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                id_jenis_apt: {
                    required: "Opps, Jenis karyawan Tidak Boleh Kosong..!",
                },
                nama_jenis_karyawan: {
                    required: "Opps, Nama Jenis Karyawan Tidak Boleh Kosong..!",
                },
                owner_apt: {
                    required: "Opps, Owner karyawan Tidak Boleh Kosong..!",
                },
                penanggungjwb: {
                    required: "Opps, Penanggung Jawab Tidak Boleh Kosong..!",
                },
                telp_penanggungjwb: {
                    required: "Opps, Telp Penanggung Jawab Tidak Boleh Kosong..!",
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                alamat: {
                    required: "Opps, Alamat Tidak Boleh Kosong..!",
                },
                provinsi: {
                    required: "Opps, Provinsi Tidak Boleh Kosong..!",
                },
                kota: {
                    required: "Opps, Kota Tidak Boleh Kosong..!",
                },
                kecamatan: {
                    required: "Opps, Kecamatan Tidak Boleh Kosong..!",
                },
                kelurahan: {
                    required: "Opps, Kelurahan Tidak Boleh Kosong..!",
                },
                telp1: {
                    required: "Opps, Telp1 Tidak Boleh Kosong..!",
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                telp2: {
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                email: {
                    required: "Opps, Email Tidak Boleh Kosong..!",
                    email: "Opps, Cek Kembali Email Anda..!! Example : infomedliz@medliz.com",
                }
            }
        });

    });
</script>