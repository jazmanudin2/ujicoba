<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Data Supplier</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Data Master</a>
            </li>
            <li class="active">
                <strong>Data Supplier</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Table View apt -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Data Supplier</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>

                <div class="ibox-content">
                    <div class="table-responsive" >
                        <div class="col-md-12">
                            <form class="form-horizontal" method="post" action="" autocomplete="off">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" value="<?php echo $pencarian;?>" id="pencarian" name="pencarian" class="form-control" placeholder="Pencarian">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="submit" name="submit" class="btn btn-sm bg-blue m-2-15 waves-effect" value="Cari Data">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                            </form>
                        </div>
                        <table class="table table-striped table-bordered table-hover" style="font-size: 12px" >
                            <thead >
                                <form action="" method="GET">
                                    <tr>
                                        <td colspan="13">
                                            <a class="input fa fa-plus-square btn-success btn"> Tambah</a>

                                            <a class="delete_all fa fa-trash btn-sm btn btn-danger"> Hapus Select</a>

                                        </td>
                                    </tr>
                                </form>

                                <tr>
                                    <th width="10px"><input type="checkbox" id="master"></th>
                                    <th width="15">No.</th>
                                    <th width="70px">ID</th>
                                    <th>Nama Perusahaan</th>
                                    <th>Jenis Supplier</th>
                                    <th>Jenis Produk</th>
                                    <th>Alamat</th>
                                    <th>No. Telp</th>
                                    <th width="130px">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $sno  = $row+1;
                                foreach ($data as $d) {?>
                                    <tr id="">
                                        <td>
                                            <input type="checkbox" class="sub_chk" data-id="<?php echo $d['id_supplier']; ?>">
                                        </td>
                                        <td><?php echo $sno++;?></td>
                                        <td><?php echo $d['id_supplier'];?></td>
                                        <td><?php echo $d['nama_perusahaan'];?></td>
                                        <td><?php echo $d['nama_jenis_supplier'];?></td>
                                        <td><?php echo $d['nama_jenis_produk'];?></td>
                                        <td><?php echo $d['alamat'];?></td>
                                        <td><?php echo $d['telp_supplier'];?></td>
                                        <td> 
                                            <div class="text-center">
                                                <a class="fa fa fa-address-card btn-info btn btn-sm" href="<?php echo base_url();?>apotek/supplier/detail/<?php echo $d['id_supplier']; ?>"></a>
                                                <a class="hapus fa fa-trash btn-danger btn btn-sm" data-id="<?php echo $d['id_supplier']; ?>"></a>
                                                <a class="edit fa fa-edit btn-warning btn btn-sm" data-id="<?php echo $d['id_supplier']; ?>"></a>
                                            </div>
                                        </td>
                                    </tr>
                                <?php }?>
                            </tbody>
                        </table>
                        <div style='margin-top: 10px;'>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div id="modal-supplier" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row" id="loadform">

                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $(function(){

        // Hapus Checkbox
        $('#master').on('click', function(e) {
            if($(this).is(':checked',true))  
            {
                $(".sub_chk").prop('checked', true);  
            } else {  
                $(".sub_chk").prop('checked',false);  
            }  

        });

        $('.delete_all').on('click', function(e) {

            var allVals = [];  
            $(".sub_chk:checked").each(function() {  
                allVals.push($(this).attr('data-id'));
            });  
            if(allVals.length <=0)  
            {  
                swal({
                    title:"Silahkan Select Checkbox Terlebih Dahulu",
                    type: "warning",
                });

            }  else {  

                var check = swal({
                    title:"Hapus Data",
                    text:"Yakin Akan Menghapus Data ini??",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Hapus",
                    closeOnConfirm: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                }).then((result) => {
                    if (result.value) {
                        var join_selected_values = allVals.join(","); 
                        $.ajax({
                            url: "<?php echo base_url(); ?>apotek/supplier/delete_checkbox",
                            type: 'POST',
                            data: 'id_supplier='+join_selected_values,
                            success: function (data) {
                              console.log(data);
                              $(".sub_chk:checked").each(function() {  
                                  $(this).parents("tr").remove();
                              });
                              swal(
                                'Hapus',
                                'Data Berhasil Di Hapus',
                                'success'
                                );
                          },
                          error: function (data) {
                            alert(data.responseText);
                        }

                    });
                        $.each(allVals, function( index, value ) {
                          $('table tr').filter("[data-row-id='" + value + "']").remove();
                      });
                    }
                });  
            }  

        });

        //Hapus Data apt
        $('.hapus').click(function(){
            var kode = $(this).attr("data-id");

            swal({
                title:"Hapus Data",
                text:"Yakin Akan Menghapus Data ini??",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Hapus",
                closeOnConfirm: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
            }).then((result) => {
                if (result.value) {
                  swal(
                    'Hapus',
                    'Data Berhasil Di Hapus',
                    'success'
                    );
                  $(location).attr('href','<?php echo base_url()?>apotek/supplier/hapus/'+kode);
              }
          })

        }); 

        //Edit Data Modal

        $('.edit').click(function(){
            var id_supplier = $(this).attr("data-id");
            $("#modal-supplier").modal("show");
            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/supplier/edit",
                data    : {id_supplier:id_supplier},
                cache   : false,
                success : function(respond){

                    $("#loadform").html(respond);
                }

            });

        });

        //Input Data Modal

        $('.input').click(function(){
            var id_supplier = $(this).attr("data-id");
            $("#modal-supplier").modal("show");
            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/supplier/input",
                data    : {id_supplier:id_supplier},
                cache   : false,
                success : function(respond){

                    $("#loadform").html(respond);
                }

            });

        });

    });

</script>

