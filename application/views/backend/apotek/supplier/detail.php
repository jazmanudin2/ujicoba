<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Detail Supplier</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Data Master</a>
            </li>
            <li>
                <a>Data Supplier</a>
            </li>
            <li class="active">
                <strong>Detail Supplier</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Table View apt -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <div class="col-lg-6">
                            <table class="table table-striped table-hover" style="font-size: 12px;font-variant: arial" >
                                <tbody>
                                    <tr>
                                        <th width="170px">ID Supplier</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['id_supplier'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Jenis Supplier</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['nama_jenis_supplier'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Jenis Produk</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['nama_jenis_produk'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Nama Perusahaan</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['nama_perusahaan'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Telp Supplier</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['telp_supplier'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">ID Sales</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['id_sales'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Nama Sales</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['nama_sales'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Telp Sales</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['telp_sales'];?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-striped table-hover" style="font-size: 12px;" >
                                <tbody>
                                    <tr>
                                        <th width="150px">Alamat</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['alamat'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="150px">Keterangan Supplier</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['keterangan'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="150px">Catatan Supplier</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['catatan'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="150px">Created At</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['created_at'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="150px">Updated At</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['updated_at'];?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
