<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">INPUT DATA SUPPLIER</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/supplier/input" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID Supplier <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="id_supplier" value="<?php echo $kodeunik;?>" readonly placeholder="Masukan Id Supplier" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Jenis Supplier <span class="symbol"></span>
            </label>
            <select name="id_jenis_supplier" id="id_jenis_supplier" class="form-control" required="">
                <option required value="">--- Pilih Jenis Supplier --</option>
                <?php foreach ($jenis_sup->result() as $d){ ?>
                    <option required value="<?php echo $d->id_jenis_supplier;?>">
                        <?php echo $d->nama_jenis_supplier; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Sales <span class="symbol"></span>
            </label>
            <select name="id_sales" id="id_sales" class="form-control" required="">
                <option required value="">--- Pilih Sales --</option>
                <?php foreach ($sales->result() as $d){ ?>
                    <option required value="<?php echo $d->id_sales;?>">
                        <?php echo $d->nama_sales; ?> 
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Jenis Produk <span class="symbol"></span>
            </label>
            <select name="id_jenis_produk" id="id_jenis_produk" class="form-control" required="">
                <option required value="">--- Pilih Jenis Produk --</option>
                <?php foreach ($jenis_produk->result() as $d){ ?>
                    <option required value="<?php echo $d->id_jenis_produk;?>">
                        <?php echo $d->nama_jenis_produk; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Perusahaan <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="nama_perusahaan" placeholder="Masukan Nama Perusahaan" required="" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Alamat <span class="symbol"></span>
            </label>
            <textarea autocomplete="off" type="text" name="alamat" required class="form-control" ></textarea>
        </div>
        <div class="form-group">
            <label class="control-label">
                Telp Supplier <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="telp_supplier" placeholder="Masukan Telp Supplier" required="" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Keterangan <span class="symbol"></span>
            </label>
            <textarea autocomplete="off" type="text" name="keterangan" class="form-control" ></textarea>
        </div>
        <div class="form-group">
            <label class="control-label">
                Catatan <span class="symbol"></span>
            </label>
            <textarea autocomplete="off" type="text" name="catatan" class="form-control" ></textarea>
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>



<script>
    $(document).ready(function(){

        $("#form").validate({
            rules: {
                id_supplier: {
                    required: true
                },
                nama_perusahaan: {
                    required: true
                },
                id_jenis_supplier: {
                    required: true
                },
                id_jenis_produk: {
                    required: true
                },
                alamat: {
                    required: true,
                },
                telp_supplier: {
                    required: true,
                    number: true
                }
            },
            messages: {
                id_supplier: {
                    required: "Opps, ID Supplier Tidak Boleh Kosong..!",
                },
                nama_perusahaan: {
                    required: "Opps, Nama Perusahaan Tidak Boleh Kosong..!",
                },
                id_jenis_produk: {
                    required: "Opps, Jenis Produk Tidak Boleh Kosong..!",
                },
                id_jenis_supplier: {
                    required: "Opps, Jenis Supplier Tidak Boleh Kosong..!",
                },
                alamat: {
                    required: "Opps, Alamat Tidak Boleh Kosong..!",
                },
                telp_supplier: {
                    required: "Opps, Telp Supplier Tidak Boleh Kosong..!",
                    number: "Opps, Tidak Boleh Karakter, Harus Number..!",
                }
            }
        });

    });
</script>