<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Data Histori Hutang Supplier</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Detail</a>
            </li>
            <li>
                <a>Detail Histori Hutang</a>
            </li>
            <li class="active">
                <strong>Data Histori Hutang Supplier</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content form-horizontal">

    <div class="row">
        <div class="col-lg-7">
            <div class="ibox-content">
                <table class="table table-striped table-bordered table-hover" >
                    <tr>
                        <td width="150px">Kode Supplier</td>
                        <th><?php echo $histori['id_supplier'];?></th>
                    </tr>
                    <tr>
                        <td width="150px">Nama Supplier</td>
                        <th><?php echo $histori['nama_perusahaan'];?></th>
                    </tr>
                    <tr>
                        <td width="150px">Alamat </td>
                        <th><?php echo $histori['alamat'];?></th>
                    </tr>
                    <tr>
                        <td width="150px">No. HP</td>
                        <th><?php echo $histori['telp_supplier'];?></th>
                    </tr>
                    <tr>
                        <td width="150px">ID Sales</td>
                        <th><?php echo $histori['id_sales'];?></th>
                    </tr>
                    <tr>
                        <td width="150px">Nama Sales</td>
                        <th><?php echo $histori['nama_sales'];?></th>
                    </tr>
                </table>
            </div>
        </div>

        <div class="col-lg-5">
            <div class="bg-info p-xs b-r-sm" style="min-height:195px;">
                <font size="5">
                    <b>TOTAL HUTANG</b>
                </font>
                <span class="info-box-icon" style="min-height:195px;">
                    <i class="fa fa-shopping-cart"></i>
                </span>
                <br>
                <br>
                <font size="10">
                    <b><p align="right">Rp. <?php echo uang($sum['totalhutangs']);?></p></b>
                </font>
            </div>
        </div>

        <div class="col-lg-12">
            <br>
        </div>

        <div class="col-lg-12">
            <div class="ibox-content">
                <div class="table-responsive" >
                    <table class="table table-striped table-bordered table-hover" >

                        <thead >

                            <tr>
                                <th width="30">NO</th>
                                <th width="100">NO FAKTUR</th>
                                <th>TANGGAL</th>
                                <th>TOTAL HUTANG</th>
                                <th>JUMLAH BAYAR</th>
                                <th>SISA BAYAR</th>
                                <th>KETERANGAN</th>
                                <th>AKSI</th>
                            </tr>
                        </thead>

                        <body>
                            <?php 
                            $no         = 1;
                            $sumtotal   = 0;
                            $sumjmlbyr  = 0;
                            $sumsisabyr = 0;
                            ?>

                            <?php foreach ($data->result() as $d) { 
                                $sumtotal       = $sumtotal + $d->totalhutang;
                                $sumjmlbyr      = $sumjmlbyr + $d->totalbayar;
                                $sumsisabyr     = $sumsisabyr + $d->sisabayar;
                                ?>
                                <tr>
                                    <td><?php echo $no++;?></td>
                                    <td><a href="<?php echo base_url();?>apotek/pembelian/detailpembelian/<?php echo $d->no_fak_pemb;?>"><?php echo $d->no_fak_pemb;?></a></td>
                                    <td align="right"><?php echo $d->tanggal;?></td>
                                    <td align="right"><?php echo uang($d->totalhutang);?></td>
                                    <td align="right"><?php echo uang($d->totalbayar);?></td>
                                    <td align="right"><?php echo uang($d->sisabayar);?></td>

                                    <?php if ($d->sisabayar > 0) {?>
                                        <td bgcolor="#ff5c5c" align="center"><font size="3">Belum Lunas</font></td>
                                    <?php }else{?>
                                        <td bgcolor="#84efef" align="center"><font size="3">Sudah Lunas</font></td>
                                    <?php }?>

                                    <td align="center"><a class="fa fa-trash btn-danger btn btn-sm" href="<?php echo base_url();?>apotek/pembelian/hapus_histori_penj/<?php echo $d->no_fak_pemb;?>/<?php echo $d->id_supplier;?>"></a></td>
                                </tr>
                            <?php }?>
                            <tr>
                                <td colspan="3" align="center"><b><font size="3">Total</font></b></td>
                                <td align="right"><b><font size="3">Rp. <?php echo uang($sumtotal);?></font></b></td>
                                <td align="right"><b><font size="3">Rp. <?php echo uang($sumjmlbyr);?></font></b></td>
                                <td align="right"><b><font size="3">Rp. <?php echo uang($sumsisabyr);?></font></b></td>
                                <td colspan="2"></td>
                            </tr>
                        </body>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>