<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Detail Faktur</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Transaksi</a>
            </li>
            <li>
                <a>Faktur</a>
            </li>
            <li class="active">
                <strong>Detail Faktur</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <form class="form-horizontal" method="POST" id="form" action="<?php echo base_url();?>pembelian/input">
        <div class="row">

            <div class="col-lg-6">
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover" >
                        <tr>
                            <td width="150px">No. Faktur</td>
                            <th><?php echo $pembelian['no_fak_pemb'];?></th>
                        </tr>
                        <tr>
                            <td width="150px">Tanggal</td>
                            <th><?php echo $pembelian['tanggal'];?></th>
                        </tr>
                        <tr>
                            <td width="150px">ID Sales </td>
                            <th><?php echo $pembelian['id_supplier'];?></th>
                        </tr>
                        <tr>
                            <td width="150px">Perusahaan</td>
                            <th><?php echo $pembelian['nama_perusahaan'];?></th>
                        </tr>
                        <tr>
                            <td width="150px">ID Supplier </td>
                            <th><?php echo $pembelian['id_sales'];?></th>
                        </tr>
                        <tr>
                            <td width="150px">Nama Sales</td>
                            <th><?php echo $pembelian['nama_sales'];?></th>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover" >
                        <tr>
                            <td width="150px">Subtotal</td>
                            <th><?php echo uang($pembelian['subtotal']);?></th>
                        </tr>
                        <tr>
                            <td width="150px">Potongan</td>
                            <th><?php echo uang($pembelian['potongan']);?> </th>
                        </tr>
                        <tr>
                            <td width="150px">PPN</td>
                            <?php $hasil = $pembelian['ppn']/100*$pembelian['subtotal'];?>
                            <th><?php echo $pembelian['ppn'];?>% ( <?php echo uang($hasil);?> )
                            </th>
                        </tr>
                        <tr>
                            <td width="150px">Biaya Lain </td>
                            <th><?php echo uang($pembelian['biaya_lain']);?></th>
                        </tr>
                        <tr>
                            <td width="150px">Total </td>
                            <th><?php echo uang($pembelian['total']);?></th>
                        </tr>
                        <tr>
                            <td width="150px">Ket</td>
                            <th><?php echo $pembelian['keterangan'];?></th>
                        </tr>
                    </table>
                </div>
            </div>
            <p>`</p>
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Obat</h3>
                        <table class="table-bordered table-striped table-hover table">
                            <thead>
                                <tr>
                                    <th width="150px">ID Barang Medliz</th>
                                    <th>ID Barang</th>
                                    <th>Jumlah Per-Box</th>
                                    <th>Exp Date</th>
                                    <th>Harga Satuan</th>
                                    <th>Box</th>
                                    <th>Jumlah Satuan</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $total = 0;
                                foreach($obat->result() as $d){ 
                                    $total = $total + $d->subtotal;
                                    ?>
                                    <tr>
                                        <td><?php echo $d->id_barang_m; ?></td>
                                        <td><?php echo $d->id_barang; ?></td>
                                        <td><?php echo $d->jumlah_perbox; ?></td>
                                        <td><?php echo $d->exp_date; ?></td>
                                        <td align="right"><?php echo uang($d->harga_satuan); ?></td>
                                        <td><?php echo $d->box; ?></td>
                                        <td><?php echo $d->jumlah_satuan; ?></td>
                                        <td align="right"><?php echo uang($d->subtotal); ?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="7" align="center" style="font-size:12px">TOTAL</td>
                                    <td colspan="1" align="right" style="font-size:12px">Rp. <?php echo uang($total);?></td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- End Table -->
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Umum</h3>
                        <table class="table-bordered table-striped table-hover table">
                            <thead>
                                <tr>
                                    <th width="150px">ID Barang Medliz</th>
                                    <th>ID Barang</th>
                                    <th>Jumlah Per-Box</th>
                                    <th>Exp Date</th>
                                    <th>Harga Satuan</th>
                                    <th>Box</th>
                                    <th>Jumlah Satuan</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $total = 0;
                                foreach($umum->result() as $d){ 
                                    $total = $total + $d->subtotal;
                                    ?>
                                    <tr>
                                        <td><?php echo $d->id_barang_m; ?></td>
                                        <td><?php echo $d->id_barang; ?></td>
                                        <td><?php echo $d->jumlah_perbox; ?></td>
                                        <td><?php echo $d->exp_date; ?></td>
                                        <td align="right"><?php echo uang($d->harga_satuan); ?></td>
                                        <td><?php echo $d->box; ?></td>
                                        <td><?php echo $d->jumlah_satuan; ?></td>
                                        <td align="right"><?php echo uang($d->subtotal); ?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="7" align="center" style="font-size:12px">TOTAL</td>
                                    <td colspan="1" align="right" style="font-size:12px">Rp. <?php echo uang($total);?></td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- End Table -->
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Alkes</h3>
                        <table class="table-bordered table-striped table-hover table">
                            <thead>
                                <tr>
                                    <th width="150px">ID Barang Medliz</th>
                                    <th>ID Barang</th>
                                    <th>Jumlah Per-Box</th>
                                    <th>Exp Date</th>
                                    <th>Harga Satuan</th>
                                    <th>Box</th>
                                    <th>Jumlah Satuan</th>
                                    <th>Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $total = 0;
                                foreach($alkes->result() as $d){ 
                                    $total = $total + $d->subtotal;
                                    ?>
                                    <tr>
                                        <td><?php echo $d->id_barang_m; ?></td>
                                        <td><?php echo $d->id_barang; ?></td>
                                        <td><?php echo $d->jumlah_perbox; ?></td>
                                        <td><?php echo $d->exp_date; ?></td>
                                        <td align="right"><?php echo uang($d->harga_satuan); ?></td>
                                        <td><?php echo $d->box; ?></td>
                                        <td><?php echo $d->jumlah_satuan; ?></td>
                                        <td align="right"><?php echo uang($d->subtotal); ?></td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="7" align="center" style="font-size:12px">TOTAL</td>
                                    <td colspan="1" align="right" style="font-size:12px">Rp. <?php echo uang($total);?></td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- End Table -->
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <h3>Posting Pembayaran</h3>
                <hr>
                <table class="table-striped table-hover table"  style="font-size:12px">
                    <thead>
                        <tr>
                            <th colspan="4" height="42px"></th>
                        </tr>
                        <tr>
                            <th bgcolor="#e3d878">Jatuh Tempo</th>
                            <th bgcolor="#e3d878">Total Bayar</th>
                            <th bgcolor="#e3d878">Sudah Bayar</th>
                            <th bgcolor="#e3d878">Sisa Bayar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tbody>
                            <?php $totwjibbayar = 0;?>
                            <?php $totrealisasi = 0;?>
                            <?php $tottun       = 0;?>
                            <?php 
                            foreach($postingbyr->result() as $d){ 
                                $totwjibbayar = $totwjibbayar + $d->totwajibbayar;
                                $totrealisasi = $totrealisasi + $d->totalrealisasi;
                                $tottun       = ($d->totwajibbayar - $d->totalrealisasi)*1;
                                ?>
                                <tr>
                                    <td><?php echo $d->jatuhtempo; ?></td>
                                    <td align="right"><?php echo uang($d->totwajibbayar); ?></td>
                                    <td align="right"><?php echo uang($d->totalrealisasi); ?></td>
                                    <td align="right"><?php echo uang($d->totwajibbayar-$d->totalrealisasi); ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td bgcolor="#b0b0b0" align="right" style="font-size:12px"></td>
                                <td bgcolor="#b0b0b0" align="right" style="font-size:12px"><b><?php echo uang($totwjibbayar); ?></b></td>
                                <td bgcolor="#b0b0b0" align="right" style="font-size:12px"><b><?php echo uang($totrealisasi); ?></b></td>
                                <td bgcolor="#b0b0b0" align="right" style="font-size:12px"><b><?php echo uang($tottun); ?></b></td>
                            </tr>
                        </tbody>
                    </tbody>
                </table>
            </div>

            <div class="col-sm-6">
                <h3 >Histori Pembayaran</h3>
                <hr>
                <table class="table-striped table-hover table"  style="font-size:12px">
                    <thead>
                        <tr>
                            <?php if ($tottun == !0) {?>
                                <td colspan="4"><a class="btn fa fa-list btn-success pull-right" href="#modal-form" data-toggle="modal"> Bayar Cicilan</a></td>
                            <?php }else{?>
                                <th colspan="4" height="42px"></th>
                            <?php }?>
                        </tr>
                        <tr>
                            <th bgcolor="#07dea9">No.</th>
                            <th bgcolor="#07dea9">Tanggal</th>
                            <th bgcolor="#07dea9" align="right">Jumlah Bayar</th>
                            <th bgcolor="#07dea9">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $totbayar = 0;?>
                        <?php 
                        foreach($hbayar->result() as $d){ 
                            $totbayar      = $totbayar + $d->bayar;
                            ?>
                            <tr>
                                <td width="50px"><?php echo $d->nobukti; ?></td>
                                <td width="140px"><?php echo $d->tglbayar; ?></td>
                                <td width="140px" align="right"><?php echo uang($d->bayar); ?></td>
                                <td width="200px" align="right">
                                    <a class="fa fa-trash btn-danger btn btn-sm" href="<?php echo base_url();?>apotek/pembelian/hapus_pembayaran/<?php echo $d->nobukti;?>/<?php echo $d->no_fak_pemb;?>"></a>
                                    <a class="fa fa-pencil btn-warning btn btn-sm edit" data-id="<?php echo $d->nobukti;?>" ></a>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td bgcolor="#b0b0b0" colspan="1" align="center" style="font-size:12px"><b>TOTAL</b></td>
                            <td bgcolor="#b0b0b0" colspan="2" align="right" style="font-size:12px"><b><?php echo uang($totbayar); ?></b></td>
                            <td bgcolor="#b0b0b0"></td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </form>
</div>


<div id="modal-form" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 align="center" class="m-t-none m-b">INPUT PEMBAYARAN HUTANG</h3>
                        <form class="needs-validation form" role="form" id="form2" action="<?php echo base_url();?>apotek/pembelian/detailpembelian" novalidate method="POST" enctype="multipart/form-data">

                            <div hidden="" class="form-group">
                                <label class="control-label">
                                    No. Fak <span class="symbol "></span>
                                </label>
                                <div class="input-group">
                                    <input  type="text" readonly="" placeholder="No. Faktur" value="<?php echo $pembelian['no_fak_pemb'];?>" name="no_fak_pemb" class="form-control input-sm">
                                </div>
                            </div>

                            <div hidden="" class="form-group">
                                <label class="control-label">
                                    No. Bukti <span class="symbol "></span>
                                </label>  
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                    <input  type="text" readonly="" placeholder="No. Bukti" value="<?php echo $nobukti;?>" name="nobukti" class="form-control input-sm">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">
                                    Tanggal Bayar<span class="symbol "></span>
                                </label>  
                                <input type="text" name="tglbayar" value="<?php echo date('Y-m-d');?>" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="control-label">
                                    Jumlah Bayar <span class="symbol "></span>
                                </label>
                                <input type="text" autocomplete="off" autofocus placeholder="Masukan Jumlah Bayar" name="bayar" id="bayar" class="form-control" >
                            </div>

                            <div>
                                <button class="btn btn-primary fa fa-save pull-left"  name="submit" data-style="zoom-in"> Bayar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal Input -->
<div id="modal-bayar" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row" id="loadform">

                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){

        // Form Validasi
        $("#form2").submit(function(){

            var bayar     = $("#bayar").val();

            if(bayar == ""){

                swal("Oops","Jumlah Bayar Harus Diisi","warning");
                $("#bayar").focus();
                return false;

            }else{
                swal("Berhasil","Berhasil Di Simpan","success");
                return true;
            }

        });

        //Edit Data Modal
        $('.edit').click(function(){
            var nobukti = $(this).attr("data-id");
            $("#modal-bayar").modal("show");
            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/pembelian/edit_histori_hutang",
                data    : {nobukti:nobukti},
                cache   : false,
                success : function(respond){

                    $("#loadform").html(respond);
                }

            });

        });

    });
</script>