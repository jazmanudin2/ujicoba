 <div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT PEMBAYARAN HUTANG</h3>
    <form class="needs-validation form" role="form" id="form3" action="<?php echo base_url();?>apotek/pembelian/edit_histori_hutang" novalidate method="POST" enctype="multipart/form-data">

        <div class="form-group">
            <label class="control-label">
                No. Bukti <span class="symbol"></span>
            </label>  
            <div class="input-group">
                <input  type="text" readonly="" placeholder="No. Bukti" value="<?php echo $histori['nobukti'];?>" name="nobukti" class="form-control input-sm">
            </div>
        </div>

        <div hidden="" class="form-group">
            <label class="control-label">
                No. Faktur Pemb <span class="symbol"></span>
            </label>  
            <div class="input-group">
                <input  type="text" readonly="" placeholder="No. Faktur Pemb" value="<?php echo $histori['no_fak_pemb'];?>" name="no_fak_pemb" class="form-control input-sm">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label">
                Tanggal Bayar<span class="symbol"></span>
            </label>  
            <input type="text" name="tglbayar" value="<?php echo $histori['tglbayar'];?>" class="form-control">
        </div>

        <div class="form-group">
            <label class="control-label">
                Jumlah Bayar <span class="symbol"></span>
            </label>
            <input type="text" autocomplete="off" autofocus placeholder="Masukan Jumlah Bayar" name="bayar" value="<?php echo $histori['bayar'];?>" id="bayar" class="form-control" >
        </div>

        <div>
            <button class="btn btn-primary fa fa-save pull-left"  name="submit" data-style="zoom-in"> Ubah</button>
        </div>
    </form>
</div>