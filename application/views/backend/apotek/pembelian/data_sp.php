<div class="modal-header">
    <h3 class="modal-title" id="exampleModalLabel">Data Surat Pesanan</h3>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <table class="table table-bordered" id="tabelbarang">
        <thead>
            <tr>
                <th>ID SP</th>
                <th>Tanggal</th>
                <th>Suppliant</th>
                <th>Nama Perusahaan</th>
                <th>Jenis Supplier</th>
                <th>Jenis Prouk</th>
                <th>Telp</th>
                <th>Aksi</th>
            </tr>
        </thead>
        </thead>
        <tbody>
            <?php foreach ($surat_pesanan->result() as $b){ ?> 
                <tr>
                    <td width="70"><?php echo $b->id_sp; ?></td>
                    <td><?php echo $b->tanggal; ?></td>
                    <td><?php echo $b->suppliant; ?></td>
                    <td><?php echo $b->nama_perusahaan; ?></td>
                    <td><?php echo $b->nama_jenis_supplier; ?></td>
                    <td><?php echo $b->nama_jenis_produk; ?></td>
                    <td><?php echo $b->telp_supplier; ?></td>
                    <td><a href="#" data-kode="<?php echo $b->id_sp;?>" class="btn btn-sm btn-primary pilihbarang">Pilih</a></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>    
</div>

<script type="text/javascript">

    $(".pilihbarang").click(function(e){
        e.preventDefault();
        $('#Modalbarang').modal("hide");
        $("#id_sp").val($(this).attr('data-kode'));
        cekbarang();
    });

    $("#tabelbarang").DataTable();

</script>