<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Transaksi Faktur Pembelian</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Transaksi</a>
            </li>
            <li>
                <a>Faktur</a>
            </li>
            <li class="active">
                <strong>Input Faktur Pembelian</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <form class="form-horizontal" method="POST" id="form" action="<?php echo base_url();?>apotek/Pembelian/input">
        <div class="row">
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="form-group">
                            <label class="col-lg-3 control-label">No. Faktur</label>
                            <div class="col-lg-5">
                                <div class="input-group">
                                    <input  type="hidden" value="<?php echo $nobukti;?>" name="nobukti" class="form-control">
                                    <input type="hidden" name="cektemp" id="cektemp">
                                    <input type="hidden" name="cekbarang" id="cekbarang">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                    <input  type="text" readonly="" placeholder="No. Faktur" value="<?php echo $kodeunik;?>" name="no_fak_pemb" class="form-control input-sm">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">No. SP</label>
                            <div class="col-lg-5">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                    <input type="text" placeholder="No. SP" name="id_sp" id="id_sp" class="form-control input-sm">
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="input-group">
                                    <i class="fa fa-search btn btn-sm btn-success carisuratsp"> Cari</i>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="data_1">
                            <label class="col-lg-3 control-label">Tanggal</label>
                            <div class="col-lg-5">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input  type="text" autocomplete="off" class="input-sm form-control" value="<?php echo date('Y-m-d');?>" name="tanggal"/>
                                </div>  
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Jenis Obat</label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon required"><i class="fa fa-users required"></i></span>
                                    <select  name="jenis_obat" id="jenis_obat" class="form-control input-sm jenis_obat"  tabindex="2">
                                        <option value="">-- Pilih Jenis Obat --</option>
                                        <option value="obat">Obat</option>
                                        <option value="umum">Umum</option>
                                        <option value="alkes">Alkes</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-lg-3 control-label">Supplier</label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon required"><i class="fa fa-users required"></i></span>
                                    <select  name="id_supplier" id="id_supplier" class="chosen-select form-control input-sm"  tabindex="2">
                                        <option value="">-- Pilih Supplier --</option>
                                        <?php foreach ($supplier->result() as $d){ ?>
                                            <option value="<?php echo $d->id_supplier;?>">
                                                <?php echo $d->nama_perusahaan; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Sales</label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon required"><i class="fa fa-user required"></i></span>
                                    <select  name="id_sales" id="id_sales" class="chosen-select form-control input-sm"  tabindex="2">
                                        <option value="">-- Pilih Sales --</option>
                                        <?php foreach ($sales->result() as $d){ ?>
                                            <option value="<?php echo $d->id_sales;?>">
                                                <?php echo $d->nama_sales; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="bg-info p-xs b-r-sm" style="min-height:170px;">
                    <font size="5">
                        <b>TOTAL</b>
                    </font>
                    <span class="info-box-icon" style="min-height:170px;">
                        <i class="fa fa-shopping-cart"></i>
                    </span>
                    <br>
                    <br>
                    <font size="10">
                        <b><p align="right" id="sum_temp"></p></b>
                    </font>
                </div>
            </div>

            <div class="col-lg-12 ">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="form-group inputpembelian">
                            <div class="col-sm-12">
                                <div class="row">

                                    <div hidden="" class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                            <input  type="text" name="id_barang_m" id="id_barang_m" readonly="" placeholder="Kode Barang" class="form-control input-sm">
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon obat"><a href="#" class="caribarangobat"><i class="fa fa-search"></i> Obat</a></span>
                                            <span class="input-group-addon umum"><a href="#" class="caribarangumum"><i class="fa fa-search"></i> Umum</a></span>
                                            <span class="input-group-addon alkes"><a href="#" class="caribarangalkes"><i class="fa fa-search"></i> Alkes</a></span>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                            <input type="text" placeholder="Kode Barang" name="id_barang" id="id_barang" readonly="" class="form-control input-sm"> 
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                            <input  class="form-control input-sm " name="no_batch" id="no_batch" type="number" placeholder="No Batch">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                            <input  class="form-control input-sm jumlah_satuan" name="harga_satuan" id="harga_satuan" type="number" placeholder="Harga Satuan">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <hr>
                            <div class="col-sm-12">
                                <div class="row">

                                    <div class="col-md-3">
                                        <div id="data_1">
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input placeholder="Exp Date" readonly="" type="text" autocomplete="off" class="input-sm form-control" id="exp_date" name="exp_date"/>
                                            </div>  
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm jumlah_satuan" name="jumlah_perbox" id="jumlah_perbox" type="number" placeholder="Jumlah/Box">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm jumlah_satuan" name="box" id="box" type="number" placeholder="@ Box">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm" readonly="" name="jumlah_satuan" id="jumlah_satuan" type="number" placeholder="Jumlah Satuan">
                                            <span class="input-group-addon"><a href="#" class="input_tmp"><i class="fa fa-plus"></i></a></span>
                                        </div>
                                    </div>
                                    
                                    <div hidden="" class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm subtotal" readonly="" name="subtotal" id="subtotal" type="text" placeholder="Subtotal">
                                        </div>
                                    </div>

                                    <?php
                                    $id_user = $this->session->userdata('id_admin');
                                    $query=$this->db->query("SELECT SUM(tbl_detailpembelian_temp.subtotal) AS subtotals
                                        FROM tbl_detailpembelian_temp
                                        WHERE tbl_detailpembelian_temp.id_admin = '$id_user'");
                                    $d=$query->row_array();
                                    ?>

                                    <div hidden="" class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm" value="<?php echo $d['subtotals'];?>" readonly="" name="subtotal4" id="subtotal4" type="text" placeholder="Subtotal">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 obat"  style="font-size: 12px" id="">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Obat</h3>
                        <table class="table-bordered table">
                            <thead>
                                <tr>
                                    <th width="80px">ID Barang</th>
                                    <th width="150px">Nama Merk Dag</th>
                                    <th width="120px">Gol Obat</th>
                                    <th width="90px">Jumlah/Box</th>
                                    <th width="80px">No Batch</th>
                                    <th width="80px">Exp Date</th>
                                    <th width="100px">Harga Satuan</th>
                                    <th width="60px">@ Box</th>
                                    <th width="110px">Jumlah Satuan</th>
                                    <th width="100px" align="right">Subtotal</th>
                                    <th width="30px">#</th>
                                </tr>
                            </thead>
                            <tbody id="tampiltmpobat" style="font-size: 11px">

                            </tbody>
                        </table>
                        <!-- End Table -->
                    </div>
                </div>
            </div>


            <div class="col-lg-12 umum"  style="font-size: 12px" >
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Umum</h3>
                        <table class="table-bordered table">
                            <thead>
                                <tr>
                                    <th width="120px">ID Barang</th>
                                    <th>Nama Barang</th>
                                    <th>Gol Barang</th>
                                    <th>Jumlah/Box</th>
                                    <th>No Batch</th>
                                    <th>Exp Date</th>
                                    <th>Harga Satuan</th>
                                    <th>@ Box</th>
                                    <th>Jumlah Satuan</th>
                                    <th>Subtotal</th>
                                    <th width="30px">#</th>
                                </tr>
                            </thead>
                            <tbody id="tampiltmpumum" style="font-size: 11px">

                            </tbody>
                        </table>
                        <!-- End Table -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12 alkes"  style="font-size: 12px" id="">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Alkes</h3>
                        <table class="table-bordered table">
                            <thead>
                                <tr>
                                    <th width="120px">ID Barang</th>
                                    <th>Jenis Barang</th>
                                    <th>Detail Alkes</th>
                                    <th>Jumlah/Box</th>
                                    <th>No Batch</th>
                                    <th>Exp Date</th>
                                    <th>Harga Satuan</th>
                                    <th>@ Box</th>
                                    <th>Jumlah Satuan</th>
                                    <th>Subtotal</th>
                                    <th width="30px">#</th>
                                </tr>
                            </thead>
                            <tbody id="tampiltmpalkes" style="font-size: 11px">

                            </tbody>
                        </table>
                        <!-- End Table -->
                    </div>
                </div>
            </div>

            <div class="col-sm-6 ">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">


                        <div class="form-group">
                            <label class="col-lg-5 control-label">Biaya Lain</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input style="text-align:right" value="0" type="text" autocomplete="off" placeholder="Biaya Lain" id="biaya_lain" name="biaya_lain" class="form-control sisa_bayar total">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Ket </label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <textarea style="text-align:left" type="text" autocomplete="off" placeholder="Ket" id="keterangan" name="keterangan" class="form-control sisa_bayar">-</textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-sm-6 ">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="form-group">
                            <label class="col-lg-5 control-label">PPN (%)</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input style="text-align:right" value="0" type="text" autocomplete="off" placeholder="PPN (%)" id="ppn" name="ppn" class="form-control sisa_bayar total ppn">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Potongan</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input style="text-align:right" type="text" autocomplete="off" placeholder="Potongan (Rp.)" id="potongan" value="0" name="potongan" class="form-control sisa_bayar diskon total">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Diskon (%)</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input style="text-align:right" type="text" autocomplete="off" placeholder="Diskon (%)" id="diskon" name="diskon" readonly="" value="0" class="form-control sisa_bayar diskon total">
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-lg-5 control-label">Total</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input style="text-align:right" readonly="" autocomplete="off" type="text" placeholder="Total" id="total2" name="total2" class="form-control input-sm total">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Pembayaran</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input style="text-align:right" type="text"  autocomplete="off" placeholder="Pembayaran" name="pembayaran" id="pembayaran" class="form-control sisa_bayar input-sm total">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-5 control-label">Sisa Bayar</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input type="hidden" name="jenisbayar" value="kredit">
                                    <input style="text-align:right" readonly="" autocomplete="off" type="text" placeholder="Sisa Bayar" id="sisa_bayar" name="sisa_bayar" class="form-control input-sm ">
                                </div>
                            </div>
                        </div>

                        <div hidden="" class="form-group">
                            <label class="col-lg-5 control-label">Jumlah Cicilan</label>
                            <div class="col-lg-7">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input style="text-align:right" readonly="" value="1" autocomplete="off" type="text" placeholder="Total" id="jumlah_cicilan" name="jumlah_cicilan" class="form-control input-sm total">
                                </div>
                            </div>
                        </div>

                        <?php 
                        $dt = mktime(0,0,0,date('n'),date('j')+30,date('y'));
                        $k  = date('Y-m-d',$dt);?>

                        <div class="form-group" id="data_1">
                            <label class="col-lg-5 control-label">Jatuh Tempo</label>
                            <div class="col-lg-4">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" placeholder="Tanggal" value="<?php echo $k;?>" autocomplete="off" id="jatuhtempo" class="form-control input-sm datepicker" name="jatuhtempo" >
                                </div>  
                            </div>
                        </div>

                        <br>
                        <div class="form-group">
                            <div class="col-lg-12 ">
                                <div class="input-group">
                                    <button class="ladda-button btn btn-w-m btn-success submit" name="submit" type="submit"><i class="fa fa-print "></i> Simpan</button> .
                                    <button type="reset" class="btn btn-fw btn-danger"><i class="fa  fa-mail-reply-all"></i>  Batal</button>  
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </form>
</div>

<!------- Modal Barang -------->
<div class="modal fade bd-example-modal-lg" id="Modalbarang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="loadform">

        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){

        tampiltmpobat();
        tampiltmpalkes();
        tampiltmpumum();
        cektemp();
        sum_temp();
        jenis_obat();

        function sum_temp(){

            $.ajax({

                type    : 'POST',
                url     : '<?php echo base_url(); ?>apotek/pembelian/sum_temp',
                cache   : false,
                success :function(respond){

                    $("#sum_temp").html(respond);
                }

            });
        } 

        function cektemp(){

            $.ajax({

                type    : 'POST',
                url     : '<?php echo base_url(); ?>apotek/pembelian/cektemp',
                cache   : false,
                success :function(respond){

                    $("#cektemp").val(respond);

                }

            });
        } 

        function tampiltmpobat(){

            // Mengosongkan inputpembelian Text Ketika Sudah Di Klik Tambah
            $("#id_barang_m").val("");
            $("#id_barang").val("");
            $("#jumlah_perbox").val("");
            $("#no_batch").val("");
            $("#exp_date").val("");
            $("#harga_satuan").val("");
            $("#box").val("");
            $("#jumlah_satuan").val("");
            $("#subtotal").val("");

            $.ajax({
                type    : 'GET',
                url     : '<?php echo base_url();?>apotek/pembelian/view_temp_barangobat',
                data    : '',
                success : function (html) {
                    $("#tampiltmpobat").html(html);
                }
            });

        }

        function tampiltmpumum(){

            // Mengosongkan inputpembelian Text Ketika Sudah Di Klik Tambah
            $("#id_barang_m").val("");
            $("#id_barang").val("");
            $("#jumlah_perbox").val("");
            $("#no_batch").val("");
            $("#exp_date").val("");
            $("#harga_satuan").val("");
            $("#box").val("");
            $("#jumlah_satuan").val("");
            $("#subtotal").val("");

            $.ajax({
                type    : 'GET',
                url     : '<?php echo base_url();?>apotek/pembelian/view_temp_barangumum',
                data    : '',
                success : function (html) {
                    $("#tampiltmpumum").html(html);
                }
            });

        }

        function tampiltmpalkes(){

            // Mengosongkan inputpembelian Text Ketika Sudah Di Klik Tambah
            $("#id_barang_m").val("");
            $("#id_barang").val("");
            $("#jumlah_perbox").val("");
            $("#no_batch").val("");
            $("#exp_date").val("");
            $("#harga_satuan").val("");
            $("#box").val("");
            $("#jumlah_satuan").val("");
            $("#subtotal").val("");

            $.ajax({
                type    : 'GET',
                url     : '<?php echo base_url();?>apotek/pembelian/view_temp_barangalkes',
                data    : '',
                success : function (html) {
                    $("#tampiltmpalkes").html(html);
                }
            });

        }

        // Tombol Modal Cari Barang
        $(".caribarangobat").click(function(e){
            e.preventDefault();
            var id_barang      = $(this).attr("data-kode");
            $('#Modalbarang').modal("show");

            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/Pembelian/view_barang_obat",
                data    : {id_barang:id_barang},
                cache   : false,
                success : function(respond){
                    $("#loadform").html(respond);
                }
            });
        });

        // Tombol Modal Surat SP
        $(".carisuratsp").click(function(e){
            e.preventDefault();
            var id_sp      = $(this).attr("data-kode");
            $('#Modalbarang').modal("show");

            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/Pembelian/view_sp",
                data    : {id_sp:id_sp},
                cache   : false,
                success : function(respond){
                    $("#loadform").html(respond);
                }
            });
        });

        // Tombol Modal Cari Barang
        $(".caribarangumum").click(function(e){
            e.preventDefault();
            var id_barang      = $(this).attr("data-kode");
            $('#Modalbarang').modal("show");

            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/Pembelian/view_barang_umum",
                data    : {id_barang:id_barang},
                cache   : false,
                success : function(respond){
                    $("#loadform").html(respond);
                }
            });
        });


        // Tombol Modal Cari Barang
        $(".caribarangalkes").click(function(e){
            e.preventDefault();
            var id_barang      = $(this).attr("data-kode");
            $('#Modalbarang').modal("show");

            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/Pembelian/view_barang_alkes",
                data    : {id_barang:id_barang},
                cache   : false,
                success : function(respond){
                    $("#loadform").html(respond);
                }
            });
        });

        $(".jumlah_satuan").on("input",function(){
            var jumlah_perbox   = $("#jumlah_perbox").val();
            var harga_satuan    = $("#harga_satuan").val();
            var box             = $("#box").val();
            $("#jumlah_satuan").val(jumlah_perbox*box);
            $("#subtotal").val(jumlah_perbox*box*harga_satuan);
        });

        $(".sisa_bayar").on("input",function(){
            var pembayaran      = $("#pembayaran").val();
            var potongan        = $("#potongan").val();
            var subtotal1       = $("#subtotal1").val();
            var subtotal2       = $("#subtotal2").val();
            var subtotal3       = $("#subtotal3").val();
            var subtotal4       = $("#subtotal4").val();
            var diskon          = $("#diskon").val();
            var ppn             = $("#ppn").val();
            var biaya_lain      = $("#biaya_lain").val();
            $("#subtotal4").val((subtotal1*1)+(subtotal2*1)+(subtotal3*1));
            $("#sisa_bayar").val(subtotal4-pembayaran-potongan+(biaya_lain*1)+(ppn/100*subtotal4));
        });

        $(".diskon").on("input",function(){
            var diskon          = $("#diskon").val();
            var potongan        = $("#potongan").val();
            var subtotal4       = $("#subtotal4").val();
            $("#diskon").val((potongan/subtotal4*100));
        });

        // Subtotal Pembelian
        $(".total").on("input",function(){
            var subtotal4    = $("#subtotal4").val();
            var potongan     = $("#potongan").val();
            var biaya_lain   = $("#biaya_lain").val();
            var ppn          = $("#ppn").val();
            var pembayaran   = $("#pembayaran").val();
            $("#total2").val((subtotal4*1)-(potongan*1)+(biaya_lain*1)+(ppn/100*subtotal4));
        });

        
        // inputpembelian Detail Pembelian Temp
        $(".input_tmp").click(function(e){
            e.preventDefault();
            var id_barang_m     = $("#id_barang_m").val();
            var jumlah_perbox   = $("#jumlah_perbox").val();
            var no_batch        = $("#no_batch").val();
            var id_barang       = $("#id_barang").val();
            var exp_date        = $("#exp_date").val();
            var harga_satuan    = $("#harga_satuan").val();
            var box             = $("#box").val();
            var jumlah_satuan   = $("#jumlah_satuan").val();
            var subtotal        = $("#subtotal").val();
            var cekbarang       = $("#cekbarang").val();

            if (id_barang_m == 0) {

                swal("Oops", "Data Barang Harus Diisi", "warning");

            } else if (no_batch == 0) {

                swal("Oops", "No Batch Harus Diisi", "warning");

            } else if (harga_satuan == 0) {

                swal("Oops", "Harga Satuan Harus Diisi", "warning");

            } else if (exp_date == 0) {

                swal("Oops", "Exp Date Harus Diisi", "warning");

            } else if (jumlah_perbox == 0) {

                swal("Oops", "Jumlah Per-Box Harus Diisi", "warning");

            }  else if (box == 0) {

                swal("Oops", "Box Harus Diisi", "warning");

            } else if (cekbarang > 0){

                swal("Oops","Barang Sudah Ada","warning");

            } else {

                $.ajax({
                    type    : 'POST',
                    url     : '<?php echo base_url();?>apotek/Pembelian/input_tmp',
                    data    :
                    {
                        id_barang_m     : id_barang_m,
                        id_barang       : id_barang,
                        jumlah_perbox   : jumlah_perbox,
                        no_batch        : no_batch,
                        exp_date        : exp_date,
                        harga_satuan    : harga_satuan,
                        box             : box,
                        jumlah_satuan   : jumlah_satuan,
                        subtotal        : subtotal,
                    },
                    cache   : false,
                    success : function(respond){
                        //swal("Success", "Barang Sudah Di Tambahkan", "success");
                        tampiltmpobat();
                        tampiltmpumum();
                        tampiltmpalkes();
                        cektemp();
                        sum_temp();
                    }

                });
            }
        });


        $('.obat').hide();
        $('.alkes').hide();
        $('.umum').hide();
        $('.inputpembelian').hide();

        function jenis_obat(){


            $(".jenis_obat").on("input",function(){

                var jenis_obat     = $("#jenis_obat").val();

                if (jenis_obat == 'obat') {

                    $('.obat').show();
                    $('.inputpembelian').show();
                    $('.alkes').hide();
                    $('.umum').hide();

                } else if (jenis_obat == 'umum') {

                    $('.umum').show();
                    $('.inputpembelian').show();
                    $('.alkes').hide();
                    $('.obat').hide();

                } else if (jenis_obat == 'alkes') {

                    $('.alkes').show();
                    $('.inputpembelian').show();
                    $('.obat').hide();
                    $('.umum').hide();

                } else {

                    $('.inputpembelian').hidden();
                    $('.obat').hide();
                    $('.alkes').hide();
                    $('.umum').hide();
                }
            });
        };


        // DataTable Barang
        $("#tabelbarang").DataTable();


        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });


        // Select Data Pelanggan
        $('.chosen-select').chosen({width: "100%"});


        // Form Validasi
        $("#form").submit(function(){

            var id_sales        = $("#id_sales").val();
            var id_supplier     = $("#id_supplier").val();
            var potongan        = $("#potongan").val();
            var id_sp           = $("#id_sp").val();
            var pembayaran      = $("#pembayaran").val();
            var cektemp         = $("#cektemp").val();

            if(id_sp == ""){

                swal("Oops","No SP Harus Diisi","warning");
                $("#kode_pelanggan").focus();
                return false;

            }else if(id_supplier==""){

                swal("Oops","Silahkan Pilih Dahulu Data Supplier","warning");
                $("#id_supplier").focus();
                return false;

            }else if(id_sales==""){

                swal("Oops","Silahkan Pilih Dahulu Data Sales","warning");
                $("#id_sales").focus();
                return false;

            }else if(cektemp < 1){

                swal("Oops","Barang Belum Ada Yang Dipilih","warning");
                $("#nama_barang").focus();
                return false;

            }else if(potongan==""){

                swal("Oops","Potongan Harus Diisi (0)","warning");
                $("#potongan").focus();
                return false;

            }else if(jatuhtempo==""){

                swal("Oops","Jatuh Tempo Harus Diisi","warning");
                $("#jatuhtempo").focus();
                return false;

            }else{

                swal("Berhasil","Berhasil Di Simpan","success");
                return true;
            }
        });
        
    });
</script>