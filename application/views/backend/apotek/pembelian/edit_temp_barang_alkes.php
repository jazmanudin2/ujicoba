<div class="col-lg-12">
    <div class="ibox2 float-e-margins">
        <div class="ibox2-content">
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="row">

                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <input  type="text" name="id_barang_m2" value="<?php echo $listmpalkes['id_barang_m'];?>" id="id_barang_m2" readonly="" placeholder="Kode Barang" class="form-control input-sm">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                <input type="text" placeholder="Kode Barang" value="<?php echo $listmpalkes['id_barang'];?>" name="id_barang2" id="id_barang2" readonly="" class="form-control input-sm"> 
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                <input  class="form-control input-sm " name="no_batch2" id="no_batch2" type="number" placeholder="No Batch">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                <input  class="form-control input-sm jumlah_satuan2" name="harga_satuan2" id="harga_satuan2" type="number" placeholder="Harga Satuan">
                            </div>
                        </div>

                    </div>
                </div>
                <br>
                <br>
                <br>
                <div class="col-sm-12">
                    <div class="row">

                        <div class="col-md-6">
                            <div id="data_1">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input placeholder="Exp Date" type="text" autocomplete="off" class="input-sm form-control" id="exp_date2" name="exp_date2"/>
                                </div>  
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                <input  class="form-control input-sm jumlah_satuan2" name="jumlah_perbox2" id="jumlah_perbox2" type="number" placeholder="Jumlah/box2">
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <br>
                <br>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                <input  class="form-control input-sm jumlah_satuan2" name="box2" id="box2" type="number" placeholder="@ box2">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                <input  class="form-control input-sm" readonly="" name="jumlah_satuan2" id="jumlah_satuan2" type="number" placeholder="Jumlah Satuan">
                                <span class="input-group-addon"><a href="#" class="edit_tmp"><i class="fa fa-plus"></i></a></span>
                            </div>
                        </div>

                        <div hidden="" class="col-md-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                <input  class="form-control input-sm subtotal2" readonly="" name="subtotal2" id="subtotal2" type="text" placeholder="subtotal2">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(".jumlah_satuan2").on("input",function(){
        var jumlah_perbox2   = $("#jumlah_perbox2").val();
        var harga_satuan2    = $("#harga_satuan2").val();
        var box2             = $("#box2").val();
        $("#jumlah_satuan2").val(jumlah_perbox2*box2);
        $("#subtotal2").val(jumlah_perbox2*box2*harga_satuan2);
    });


    $('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true,
        format: "yyyy-mm-dd"
    });

</script>