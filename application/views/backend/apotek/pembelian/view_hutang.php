<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Data Hutang Supplier</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Hutang</a>
            </li>
            <li>
                <a>Data Hutang</a>
            </li>
            <li class="active">
                <strong>Data Hutang Supplier</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Tabel View Barang -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Hutang Supplier</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive" >
                        <div class="col-md-12">
                            <form class="form-horizontal" method="post" action="" autocomplete="off">

                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" value="<?php echo $pencarian;?>" id="pencarian" name="pencarian" class="form-control" placeholder="Pencarian">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="submit" name="submit" class="btn btn-sm bg-blue m-2-15 waves-effect" value="CARI DATA">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <table class="table table-striped table-bordered table-hover" style="font-size: 12px">
                            <thead >
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>ID Supplier</th>
                                    <th>Nama Perusahaan</th>
                                    <th>ID Sales</th>
                                    <th>Nama Sales</th>
                                    <th>Telp</th>
                                    <th>Jumlah Hutang</th>
                                    <th>Jumlah Bayar</th>
                                    <th>Sisa Bayar</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>

                            <body>
                                <?php $no=1;?>
                                <?php foreach ($data as $d) {?>
                                    <tr style="font-size: 11px">
                                        <td><?php echo $no++;?></td>
                                        <td><?php echo $d['tanggal'];?></td>
                                        <td><?php echo $d['id_supplier'];?></td>
                                        <td><?php echo $d['nama_perusahaan'];?></td>
                                        <td><?php echo $d['id_sales'];?></td>
                                        <td><?php echo $d['nama_sales'];?></td>
                                        <td><?php echo $d['telp_supplier'];?></td>
                                        <td align="right"><?php echo uang($d['totalhutang']);?></td>
                                        <td align="right"><?php echo uang($d['totalbayar']);?></td>
                                        <td align="right"><?php echo uang($d['sisabayar']);?></td>
                                        <?php if ($d['sisabayar'] > 0) {?>
                                            <td bgcolor="#ff5c5c" align="center"><font size="2">Belum Lunas</font></td>
                                        <?php }else{?>
                                            <td bgcolor="#84efef" align="center"><font size="2">Sudah Lunas</font></td>
                                        <?php }?>
                                        <td><a class="fa fa-address-card btn-info btn btn-sm" href="<?php echo base_url();?>apotek/pembelian/view_historihutang/<?php echo $d['id_supplier']; ?>"></a></td>
                                    </tr>
                                <?php }?>
                            </body>

                        </table>
                        <div style='margin-top: 10px;'>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive" >

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>