<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Detail Barang Alkes</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Data Master</a>
            </li>
            <li>
                <a>Data Barang Alkes</a>
            </li>
            <li class="active">
                <strong>Detail Barang Alkes</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Table View apt -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <div class="col-lg-6">
                            <table class="table table-striped table-hover" style="font-size: 14px;font-variant: arial" >
                                <tbody>
                                    <tr>
                                        <th width="170px">ID Barang</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['id_barang'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Jenis Barang</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['jenis_barang'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Detail Alkes</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['detail_alkes'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Min Stok</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['min_stok'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Keterangan</th>
                                        <th width="10">:</th>
                                        <td><?php echo $detail['keterangan'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Barcode</th>
                                        <th width="10">:</th>
                                        <td><img width="120px" height="50" src="<?php echo base_url();?>assets/backend/apotek/barcode/barang_alkes/<?php echo $detail['barcode'];?>"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <div class="col-lg-12">
                            <table class="table table-striped table-hover" style="font-size: 14px;" >
                                <table class="table table-striped table-bordered table-hover" style="font-size: 12px">
                                    <thead>
                                        <tr>
                                            <th width="15">No.</th>
                                            <th>Kode Barang</th>
                                            <th>Exp Date</th>
                                            <th>Warning</th>
                                            <th>Stok Obat</th>
                                            <th>Jumlah Per-Box</th>
                                            <th>Harga Beli</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $sno            = 1;
                                        $jumlah_perbox  = 0;
                                        $jumlah_stok    = 0;
                                        $harga_beli     = 0;
                                        foreach ($details->result() as $d) {
                                            $jumlah_stok    = $jumlah_stok + $d->stok;
                                            $jumlah_perbox  = $jumlah_perbox + $d->jumlah_perbox;
                                            $harga_beli     = $harga_beli + $d->harga_beli;
                                            $exp_date       = $d->exp_date; 
                                            $sekarang       = date("d-m-Y"); 
                                            $masaberlaku    = strtotime($exp_date) - strtotime($sekarang); 
                                            ?>
                                            <tr id="">
                                                <td><?php echo $sno++;?></td>
                                                <td><?php echo $d->id_barang;?></td>
                                                <td><?php echo $d->exp_date;?></td>
                                                <?php if($masaberlaku/(24*60*60)<1) {?> 
                                                    <td><font color='red'  size='2'><?php echo "Sudah Habis";?></font></td>
                                                <?php } else if($masaberlaku/(24*60*60)<30) {?> 
                                                    <td><font color='orange' size='2'><?php echo $masaberlaku/(24*60*60)." hari lagi";?> <?php echo "Masa Berlaku akan Habis";?> </font></td>
                                                <?php } else if($masaberlaku/(24*60*60)<60) {?> 
                                                    <td><font color='blue' size='2'><?php echo $masaberlaku/(24*60*60)." hari lagi";?> <?php echo "Masa Berlaku akan Habis";?> </font></td>
                                                <?php } else if($masaberlaku/(24*60*60)>61) {?> 
                                                    <td><font color='green' size='2'><?php echo $masaberlaku/(24*60*60)." hari lagi";?> <?php echo "Masa Berlaku akan Habis";?> </font></td>
                                                <?php }?>
                                                <td><?php echo $d->stok;?></td>
                                                <td><?php echo $d->jumlah_perbox;?></td>
                                                <td><?php echo $d->harga_beli;?></td>
                                            </tr>
                                        <?php }?>
                                        <tr>
                                            <td colspan="4" align="center"><b>TOTAL</b></td>
                                            <td align="right"><h3><?php echo $jumlah_stok; ?></h3></td>
                                            <td align="right"><h3><?php echo $jumlah_perbox; ?></h3></td>
                                            <td align="right"><h3><?php echo uang($harga_beli); ?></h3></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
