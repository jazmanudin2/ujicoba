<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">INPUT DATA BARANG ALKES</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/barang_alkes/input" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div hidden="" class="form-group">
            <label class="control-label">
                ID Barang Medliz <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="id_barang_m" value="<?php echo $barang_alkes['id_barang_m'];?>" readonly placeholder="Masukan ID Barang Medliz" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                ID Barang <span class="symbol"></span>
            </label> 
            <input autocomplete="off" type="text" name="id_barang" placeholder="Masukan ID Barang" value="<?php echo $barang_alkes['id_barang'];?>" required="" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Jenis Barang <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text"  placeholder="Jenis Barang" value="<?php echo $barang_alkes['jenis_barang'];?>" name="jenis_barang" id="jenis_barang" class="form-control"required>
        </div>
        <div class="form-group">
            <label class="control-label">
                Detail Alkes <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" name="detail_alkes" value="<?php echo $barang_alkes['detail_alkes'];?>" id="detail_alkes" class="form-control"required>
        </div>
        <div class="form-group">
            <label class="control-label">
                Minimal Stok <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" name="min_stok" id="min_stok" value="<?php echo $barang_alkes['min_stok'];?>" class="form-control"required>
        </div>
        <div class="form-group">
            <label class="control-label">
                Keterangan <span class="symbol"></span>
            </label>
            <textarea autocomplete="off" type="text" name="keterangan" class="form-control"><?php echo $barang_alkes['keterangan'];?></textarea>
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>


<script>
    $(document).ready(function(){

        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "dd-mm-yyyy"
        });

        $("#form").validate({
            rules: {
                id_barang: {
                    required: true
                },
                jenis_barang: {
                    required: true
                },
                detail_alkes: {
                    required: true
                },
                min_stok: {
                    required: true,
                    number: true
                }
            },
            messages: {
                id_barang: {
                    required: "Opps, ID Barang Tidak Boleh Kosong..!",
                },
                jenis_barang: {
                    required: "Opps, Jenis Barang Tidak Boleh Kosong..!",
                },
                detail_alkes: {
                    required: "Opps,Detail Alkes Tidak Boleh Kosong..!",
                },
                min_stok: {
                    required: "Opps, Minimal Stok Tidak Boleh Kosong..!",
                }
            }
        });

    });
</script>