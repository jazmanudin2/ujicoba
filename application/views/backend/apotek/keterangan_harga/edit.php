<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT DATA KETERANGAN HARGA</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/keterangan_harga/edit" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID keterangan Harga <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" name="id_keterangan_harga" value="<?php echo $keterangan_harga['id_keterangan_harga'] ?>" readonly placeholder="Masukan Id keterangan Harga" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama keterangan Harga <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" required="" placeholder="Masukan Nama keterangan" value="<?php echo $keterangan_harga['nama_keterangan_harga'] ?>" name="nama_keterangan_harga" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
               Ket <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan keterangan" value="<?php echo $keterangan_harga['keterangan'] ?>" name="keterangan" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>
