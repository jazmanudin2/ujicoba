<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Data Mutasi Gudang Barang Masuk</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Data Master</a>
            </li>
            <li>
                <a>Data Mutasi Barang</a>
            </li>
            <li class="active">
                <strong>Detail Mutasi Gudang Barang Masuk</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Table View apt -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <div class="col-lg-6">
                            <table class="table table-striped table-hover" style="font-size: 14px;font-variant: arial" >
                                <tbody>
                                    <tr>
                                        <th width="170px">ID Mutasi Gudang</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtl['id_mutasi_gudang'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Tanggal</th>
                                        <th width="10">:</th>
                                        <td><?php echo DateToIndo2($dtl['tanggal']);?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">PIC Mutasi Gudang</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtl['pic_mg'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Asal Barang Masuk</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtl['asal_barang_masuk'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Nama Pengirim</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtl['nama_pengirim'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Nama Penerima</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtl['nama_penerima'];?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="table table-striped table-hover" style="font-size: 14px;font-variant: arial" >
                                <tbody>
                                    <tr>
                                        <th width="170px">Biaya Mutasi</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtl['biaya_mutasi'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Subtotal</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtl['subtotal'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Total</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtl['total'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Jenis Mutasi</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtl['jenis_mutasi'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Keterangan</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtl['keterangan'];?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <div class="col-lg-12">
                            <table class="table table-striped table-bordered table-hover" style="font-size: 12px">
                                <thead>
                                    <tr>
                                        <th width="15">No.</th>
                                        <th>Kode Barang</th>
                                        <th>Merk Dagang</th>
                                        <th>Golongan Obat</th>
                                        <th>Kandungan Obat</th>
                                        <th>Harga Satuan</th>
                                        <th>Jumlah Per-Box</th>
                                        <th>Jumlah Box</th>
                                        <th>Jumlah Satuan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $sno  = 1;
                                    $jumlah_perbox      = 0;
                                    $harga_satuan       = 0;
                                    $jumlah_box         = 0;
                                    $jumlah_satuan      = 0;
                                    foreach ($dtls->result() as $d) {
                                        $harga_satuan   = $harga_satuan + $d->harga_satuan;
                                        $jumlah_perbox  = $jumlah_perbox + $d->jumlah_perbox;
                                        $jumlah_box     = $jumlah_box + $d->jumlah_box;
                                        $jumlah_satuan  = $jumlah_satuan + $d->jumlah_satuan;
                                        ?>
                                        <tr id="">
                                            <td><?php echo $sno++;?></td>
                                            <td><?php echo $d->id_barang;?></td>
                                            <td><?php echo $d->nama_merk_dag;?></td>
                                            <td><?php echo $d->nama_gol_obat;?></td>
                                            <td><?php echo $d->id_kandungan_obat;?></td>
                                            <td align="right"><?php echo uang($d->harga_satuan);?></td>
                                            <td><?php echo $d->jumlah_perbox;?></td>
                                            <td><?php echo $d->jumlah_box;?></td>
                                            <td><?php echo $d->jumlah_satuan;?></td>
                                        </tr>
                                    <?php }?>
                                    <tr>
                                        <td colspan="5" align="center"><b>TOTAL</b></td>
                                        <td align="right"><h4><?php echo uang($harga_satuan); ?></h4></td>
                                        <td ><h4><?php echo $jumlah_perbox; ?></h4></td>
                                        <td ><h4><?php echo $jumlah_box; ?></h4></td>
                                        <td ><h4><?php echo $jumlah_satuan; ?></h4></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
