<div class="modal-header">
    <h3 class="modal-title" id="exampleModalLabel">Data Barang Alkes</h3>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <table class="table table-bordered" id="tabelbarang">
        <thead>
            <tr>
                <th>ID Barang</th>
                <th>Jenis Barang</th>
                <th>Detail Alkes</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($barang_alkes->result() as $b){ ?> 
                <tr>
                    <td width="70"><?php echo $b->id_barang; ?></td>
                    <td><?php echo $b->jenis_barang; ?></td>
                    <td><?php echo $b->detail_alkes; ?></td>
                    <td><a href="#" data-kode="<?php echo $b->id_barang_m;?>" data-id="<?php echo $b->id_barang;?>" data-detail="<?php echo $b->detail_alkes;?>"  class="btn btn-sm btn-primary pilihbarangalkes">Pilih</a></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>    
</div>

<script type="text/javascript">

    function cekbarang(){

        var id_barang_m = $("#id_barang_m").val();
        $.ajax({
            type    : 'POST',
            url     : '<?php echo base_url(); ?>apotek/mutasi_barang_masuk/cekbarang',
            cache   : false,
            data    : {id_barang_m:id_barang_m},
            success :function(respond){

                $("#cekbarang").val(respond);

            }

        });
    } 

    $(".pilihbarangalkes").click(function(e){
        e.preventDefault();
        $('#Modalbarang').modal("hide");
        $("#id_barang_m").val($(this).attr('data-kode'));
        $("#id_barang").val($(this).attr('data-id'));
        $("#nama_barang").val($(this).attr('data-detail'));
        cekbarang();
    });

    $("#tabelbarang").DataTable();

</script>