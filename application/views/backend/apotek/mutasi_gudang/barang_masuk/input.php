<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Transaksi Mutasi Barang Masuk</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Transaksi</a>
            </li>
            <li>
                <a>Mutasi Gudang</a>
            </li>
            <li class="active">
                <strong>Input Mutasi Barang Masuk</strong>
            </li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content">
    <form class="form-horizontal" method="POST" id="form" action="<?php echo base_url();?>apotek/mutasi_barang_masuk/input">
        <div class="row">

            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="form-group">
                            <label class="col-lg-3 control-label">No. Mutasi</label>
                            <div class="col-lg-5">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                    <input type="hidden" name="cektemp" id="cektemp">
                                    <input type="hidden" name="cekbarang" id="cekbarang">
                                    <input  type="text" readonly="" placeholder="No Mutasi" value="<?php echo $kodeunik;?>" name="id_mutasi_gudang" class="form-control input-sm">
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="data_1">
                            <label class="col-lg-3 control-label">Tanggal</label>
                            <div class="col-lg-5">
                                <div class="input-group date">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input  type="text" autocomplete="off" class="input-sm form-control" value="<?php echo date('Y-m-d');?>" name="tanggal"/>
                                </div>  
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">PIC Mutasi</label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input  type="text" placeholder="PIC Mutasi Gudang" name="pic" class="form-control input-sm">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-3 control-label">Keterangan</label>
                            <div class="col-lg-9">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                    <textarea type="text" placeholder="Keterangan" rows="3" value="-" name="keterangan" class="form-control input-sm"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="bg-info p-xs b-r-sm" style="min-height:170px;">
                    <font size="5">
                        <b>TOTAL</b>
                    </font>
                    <span class="info-box-icon" style="min-height:170px;">
                        <i class="fa fa-shopping-cart"></i>
                    </span>
                    <br>
                    <br>
                    <font size="10">
                        <b><p align="right" id="sum_temp"></p></b>
                    </font>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="row">

                                    <div hidden="" class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                            <input  type="text" name="id_barang_m" id="id_barang_m" readonly="" placeholder="Kode Barang" class="form-control input-sm">
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="input-group">
                                            <span class="input-group-addon "><a href="#" class="caribarangobat"><i class="fa fa-search "></i> Obat</a></span>
                                            <span class="input-group-addon"><a href="#" class="caribarangumum"><i class="fa fa-search"></i> Umum</a></span>
                                            <span class="input-group-addon"><a href="#" class="caribarangalkes"><i class="fa fa-search"></i> Alkes</a></span>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-book"></i></span>
                                            <input type="text" placeholder="Kode Barang" name="id_barang" id="id_barang" readonly="" class="form-control input-sm"> 
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div id="data_1">
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input placeholder="Exp Date" type="text" autocomplete="off" class="input-sm form-control" readonly="" id="exp_date" name="exp_date"/>
                                            </div>  
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-barcode"></i></span>
                                            <input  class="form-control input-sm jumlah_satuan" name="harga_satuan" id="harga_satuan" type="number" placeholder="Harga Satuan">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <hr>
                            <div class="col-sm-12">
                                <div class="row">

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm jumlah_satuan" name="jumlah_perbox" id="jumlah_perbox" type="number" placeholder="Jumlah/Box">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm jumlah_satuan" name="box" id="box" type="number" placeholder="@ Box">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm" readonly="" name="jumlah_satuan" id="jumlah_satuan" type="number" placeholder="Jumlah Satuan">
                                        </div>
                                    </div>

                                    <div hidden="" class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm" value="Barang Masuk" readonly="" name="jenis_mutasi" id="jenis_mutasi" type="text">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                                            <input  class="form-control input-sm subtotal" readonly="" name="subtotal" id="subtotal" type="text" placeholder="Subtotal">
                                            <span class="input-group-addon"><a href="#" class="input_tmp"><i class="fa fa-plus"></i></a></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12"  style="font-size: 12px">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Obat</h3>
                        <table class="table-bordered table">
                            <thead>
                                <tr>
                                    <th width="80px">ID Barang</th>
                                    <th width="150px">Nama Merk Dag</th>
                                    <th width="120px">Gol Obat</th>
                                    <th width="90px">Jumlah/Box</th>
                                    <th width="80px">Exp Date</th>
                                    <th width="100px">Harga Satuan</th>
                                    <th width="60px">@ Box</th>
                                    <th width="110px">Jumlah Satuan</th>
                                    <th width="100px" align="right">Subtotal</th>
                                    <th width="30px">#</th>
                                </tr>
                            </thead>
                            <tbody id="tampiltmpobat" style="font-size: 11px">

                            </tbody>
                        </table>
                        <!-- End Table -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12"  style="font-size: 12px">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Umum</h3>
                        <table class="table-bordered table">
                            <thead>
                                <tr>
                                    <th width="120px">ID Barang</th>
                                    <th>Nama Barang</th>
                                    <th>Gol Barang</th>
                                    <th>Jumlah/Box</th>
                                    <th>Exp Date</th>
                                    <th>Harga Satuan</th>
                                    <th>@ Box</th>
                                    <th>Jumlah Satuan</th>
                                    <th>Subtotal</th>
                                    <th width="30px">#</th>
                                </tr>
                            </thead>
                            <tbody id="tampiltmpumum" style="font-size: 11px">

                            </tbody>
                        </table>
                        <!-- End Table -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12"  style="font-size: 12px">

                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <!-- Table -->
                        <h3>Data Barang Alkes</h3>
                        <table class="table-bordered table">
                            <thead>
                                <tr>
                                    <th width="120px">ID Barang</th>
                                    <th>Jenis Barang</th>
                                    <th>Detail Alkes</th>
                                    <th>Jumlah/Box</th>
                                    <th>Exp Date</th>
                                    <th>Harga Satuan</th>
                                    <th>@ Box</th>
                                    <th>Jumlah Satuan</th>
                                    <th>Subtotal</th>
                                    <th width="30px">#</th>
                                </tr>
                            </thead>
                            <tbody id="tampiltmpalkes" style="font-size: 11px">

                            </tbody>
                        </table>
                        <!-- End Table -->
                        
                    </div>
                </div>
            </div>

            <div class="col-lg-6">

            </div>

            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Asal Barang Masuk</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-copy"></i></span>
                                    <input  class="form-control input-sm" id="asal_barang_masuk" name="asal_barang_masuk" type="text" placeholder="Asal Barang Masuk">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Nama Pengirim</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input  class="form-control input-sm" name="nama_pengirim" id="nama_pengirim" type="text" placeholder="Nama Pengirim">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Nama Penerima</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input  class="form-control input-sm" name="nama_penerima" id="nama_penerima" type="text" placeholder="Nama Penerima">
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Biaya Mutasi</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input  class="form-control input-sm total" name="biaya_mutasi" id="biaya_mutasi" type="text" placeholder="Biaya Mutasi">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Subtotal</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input readonly="" class="form-control input-sm" name="subtotal5" id="subtotal5" type="text" placeholder="Total">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-lg-4 control-label">Total</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input readonly="" class="form-control input-sm" name="total" id="total" type="text" placeholder="Total">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="col-lg-7 ">
                                <div class="input-group">
                                    <button class="ladda-button btn btn-sm btn-success submit" name="submit" type="submit"><i class="fa fa-print "></i> Simpan</button> .
                                    <button type="reset" class="btn btn-sm btn-danger"><i class="fa  fa-mail-reply-all"></i>  Batal</button>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
</div>

<!------- Modal Barang -------->
<div class="modal fade bd-example-modal-lg" id="Modalbarang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="loadform">

        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){

        tampiltmpobat();
        tampiltmpalkes();
        tampiltmpumum();
        cektemp();
        sum_temp();

        function cektemp(){

            $.ajax({

                type    : 'POST',
                url     : '<?php echo base_url(); ?>apotek/mutasi_barang_masuk/cektemp',
                cache   : false,
                success :function(respond){

                    $("#cektemp").val(respond);

                }

            });
        } 


        function sum_temp(){

            $.ajax({

                type    : 'POST',
                url     : '<?php echo base_url(); ?>apotek/mutasi_barang_masuk/sum_temp',
                cache   : false,
                success :function(respond){

                    $("#sum_temp").html(respond);
                }

            });
        } 


        function tampiltmpobat(){

            // Mengosongkan Inputan Text Ketika Sudah Di Klik Tambah
            $("#id_barang_m").val("");
            $("#id_barang").val("");
            $("#jumlah_perbox").val("");
            $("#no_batch").val("");
            $("#exp_date").val("");
            $("#harga_satuan").val("");
            $("#box").val("");
            $("#jumlah_satuan").val("");
            $("#subtotal").val("");

            $.ajax({
                type    : 'GET',
                url     : '<?php echo base_url();?>apotek/mutasi_barang_masuk/view_temp_barang_obat',
                data    : '',
                success : function (html) {
                    $("#tampiltmpobat").html(html);
                }
            });

        }

        function tampiltmpumum(){

            // Mengosongkan Inputan Text Ketika Sudah Di Klik Tambah
            $("#id_barang_m").val("");
            $("#id_barang").val("");
            $("#jumlah_perbox").val("");
            $("#no_batch").val("");
            $("#exp_date").val("");
            $("#harga_satuan").val("");
            $("#box").val("");
            $("#jumlah_satuan").val("");
            $("#subtotal").val("");

            $.ajax({
                type    : 'GET',
                url     : '<?php echo base_url();?>apotek/mutasi_barang_masuk/view_temp_barangumum',
                data    : '',
                success : function (html) {
                    $("#tampiltmpumum").html(html);
                }
            });

        }

        function tampiltmpalkes(){

            // Mengosongkan Inputan Text Ketika Sudah Di Klik Tambah
            $("#id_barang_m").val("");
            $("#id_barang").val("");
            $("#jumlah_perbox").val("");
            $("#no_batch").val("");
            $("#exp_date").val("");
            $("#harga_satuan").val("");
            $("#box").val("");
            $("#jumlah_satuan").val("");
            $("#subtotal").val("");

            $.ajax({
                type    : 'GET',
                url     : '<?php echo base_url();?>apotek/mutasi_barang_masuk/view_temp_barangalkes',
                data    : '',
                success : function (html) {
                    $("#tampiltmpalkes").html(html);
                }
            });

        }

        // Tombol Modal Cari Barang
        $(".caribarangobat").click(function(e){
            e.preventDefault();
            var id_barang      = $(this).attr("data-kode");
            $('#Modalbarang').modal("show");

            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/mutasi_barang_masuk/view_barang_obat",
                data    : {id_barang:id_barang},
                cache   : false,
                success : function(respond){
                    $("#loadform").html(respond);
                }
            });
        });

        // Tombol Modal Cari Barang
        $(".caribarangumum").click(function(e){
            e.preventDefault();
            var id_barang      = $(this).attr("data-kode");
            $('#Modalbarang').modal("show");

            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/mutasi_barang_masuk/view_barang_umum",
                data    : {id_barang:id_barang},
                cache   : false,
                success : function(respond){
                    $("#loadform").html(respond);
                }
            });
        });


        // Tombol Modal Cari Barang
        $(".caribarangalkes").click(function(e){
            e.preventDefault();
            var id_barang      = $(this).attr("data-kode");
            $('#Modalbarang').modal("show");

            $.ajax({
                type    : "POST",
                url     : "<?php echo base_url(); ?>/apotek/mutasi_barang_masuk/view_barang_alkes",
                data    : {id_barang:id_barang},
                cache   : false,
                success : function(respond){
                    $("#loadform").html(respond);
                }
            });
        });

        $(".jumlah_satuan").on("input",function(){
            var jumlah_perbox   = $("#jumlah_perbox").val();
            var harga_satuan    = $("#harga_satuan").val();
            var box             = $("#box").val();
            $("#jumlah_satuan").val(jumlah_perbox*box);
            $("#subtotal").val(jumlah_perbox*box*harga_satuan);
        });

        $(".sisa_bayar").on("input",function(){
            var pembayaran      = $("#pembayaran").val();
            var potongan        = $("#potongan").val();
            var subtotal4       = $("#subtotal4").val();
            var diskon          = $("#diskon").val();
            var ppn             = $("#ppn").val();
            var biaya_lain      = $("#biaya_lain").val();
            $("#sisa_bayar").val(subtotal4-pembayaran-potongan+(biaya_lain*1)+(ppn/100*subtotal4));
        });

        $(".diskon").on("input",function(){
            var diskon          = $("#diskon").val();
            var potongan        = $("#potongan").val();
            var subtotal4       = $("#subtotal4").val();
            $("#diskon").val((potongan/subtotal4*100));
        });

        // Subtotal mutasi_barang_masuk
        $(".total").on("input",function(){
            var subtotal1    = $("#subtotal1").val();
            var subtotal2    = $("#subtotal2").val();
            var subtotal3    = $("#subtotal3").val();
            var subtotal4    = $("#subtotal4").val();
            var biaya_mutasi = $("#biaya_mutasi").val();
            $("#total").val((biaya_mutasi*1)+(subtotal3*1)+(subtotal2*1)+(subtotal1*1));
            $("#subtotal5").val((subtotal3*1)+(subtotal2*1)+(subtotal1*1));
        });
        
        // Inputan Detail mutasi_barang_masuk Temp
        $(".input_tmp").click(function(e){
            e.preventDefault();
            var id_barang_m     = $("#id_barang_m").val();
            var jumlah_perbox   = $("#jumlah_perbox").val();
            var no_batch        = $("#no_batch").val();
            var id_barang       = $("#id_barang").val();
            var exp_date        = $("#exp_date").val();
            var harga_satuan    = $("#harga_satuan").val();
            var box             = $("#box").val();
            var jumlah_satuan   = $("#jumlah_satuan").val();
            var subtotal        = $("#subtotal").val();
            var cekbarang       = $("#cekbarang").val();
            var jenis_mutasi    = $("#jenis_mutasi").val();

            if (id_barang_m == 0) {

                swal("Oops", "Data Barang Harus Diisi", "warning");

            } else if (no_batch == 0) {

                swal("Oops", "No Batch Harus Diisi", "warning");

            } else if (harga_satuan == 0) {

                swal("Oops", "Harga Satuan Harus Diisi", "warning");

            } else if (exp_date == 0) {

                swal("Oops", "Exp Date Harus Diisi", "warning");

            } else if (jumlah_perbox == 0) {

                swal("Oops", "Jumlah Per-Box Harus Diisi", "warning");

            }  else if (box == 0) {

                swal("Oops", "Box Harus Diisi", "warning");

            } else if (cekbarang > 0){

                swal("Oops","Barang Sudah Ada","warning");

            } else {

                $.ajax({
                    type    : 'POST',
                    url     : '<?php echo base_url();?>apotek/mutasi_barang_masuk/input_tmp',
                    data    :
                    {
                        id_barang_m     : id_barang_m,
                        id_barang       : id_barang,
                        jumlah_perbox   : jumlah_perbox,
                        exp_date        : exp_date,
                        harga_satuan    : harga_satuan,
                        box             : box,
                        jumlah_satuan   : jumlah_satuan,
                        subtotal        : subtotal,
                        jenis_mutasi    : jenis_mutasi,
                    },
                    cache   : false,
                    success : function(respond){
                        //swal("Success", "Barang Sudah Di Tambahkan", "success");
                        tampiltmpobat();
                        tampiltmpumum();
                        tampiltmpalkes();
                        sum_temp();
                        cektemp();
                    }

                });
            }
        });

        // DataTable Barang
        $("#tabelbarang").DataTable();

        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

        // Select Data Pelanggan
        $('.chosen-select').chosen({width: "100%"});


        // Form Validasi
        $("#form").submit(function(){

            var asal_barang_masuk   = $("#asal_barang_masuk").val();
            var id_supplier         = $("#id_supplier").val();
            var potongan            = $("#potongan").val();
            var nama_pengirim       = $("#nama_pengirim").val();
            var nama_penerima       = $("#nama_penerima").val();
            var pic                 = $("#pic").val();
            var biaya_mutasi        = $("#biaya_mutasi").val();
            var pembayaran          = $("#pembayaran").val();
            var cektemp             = $("#cektemp").val();

            if(pic == ""){

                swal("Oops","PIC Mutasi Gudang Hasrus Di Isi","warning");
                $("#pic").focus();
                return false;

            }else if(asal_barang_masuk==""){

                swal("Oops","Asal Barang Hasrus Diisi","warning");
                $("#asal_barang_masuk").focus();
                return false;

            }else if(nama_pengirim==""){

                swal("Oops","Nama Pengirim Belum Di Isi","warning");
                $("#nama_pengirim").focus();
                return false;

            }else if(nama_penerima==""){

                swal("Oops","Nama Penerima Harus Di Isi","warning");
                $("#nama_penerima").focus();
                return false;

            }else if(biaya_mutasi==""){

                swal("Oops","Jika Tidak Ada Biaya Mutasi Silahkan Isi Dengan Angka 0","warning");
                $("#biaya_mutasi").focus();
                return false;

            }else if(cektemp < 1){

                swal("Oops","Barang Belum Ada Yang Dipilih","warning");
                $("#nama_penerima").focus();
                return false;

            }else{

                swal("Berhasil","Berhasil Di Simpan","success");
                return true;
            }
        });
        
    });
</script>