<div class="modal-header">
    <h3 class="modal-title" id="exampleModalLabel">Data Barang Obat</h3>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <table class="table table-bordered" id="tabelbarang">
        <thead>
            <tr>
                <th>Barang</th>
                <th>Merk Dagang</th>
                <th>Gol Obat</th>
                <th>Kandungan Obat</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($barang_obat->result() as $b){ ?> 
                <tr>
                    <td width="70"><?php echo $b->id_barang; ?></td>
                    <td><?php echo $b->merk_dag; ?></td>
                    <td><?php echo $b->nama_gol_obat; ?></td>
                    <td><?php echo $b->id_kandungan_obat; ?></td>
                    <td><a href="#" data-kode="<?php echo $b->id_barang_m;?>" data-id="<?php echo $b->id_barang;?>" data-merk="<?php echo $b->merk_dag;?>" data-gol="<?php echo $b->nama_gol_obat;?>" data-kand="<?php echo $b->id_kandungan_obat;?>" class="btn btn-sm btn-primary pilihbarang">Pilih</a></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>    
</div>

<script type="text/javascript">

    function cekbarang(){

        var id_barang = $("#id_barang").val();
        $.ajax({
            type    : 'POST',
            url     : '<?php echo base_url(); ?>apotek/mutasi_barang_masuk/cekbarang',
            cache   : false,
            data    : {id_barang:id_barang},
            success :function(respond){

                $("#cekbarang").val(respond);

            }

        });
    } 

    $(".pilihbarang").click(function(e){
        e.preventDefault();
        $('#Modalbarang').modal("hide");
        $("#id_barang_m").val($(this).attr('data-kode'));
        $("#id_barang").val($(this).attr('data-id'));
        $("#merk_dag").val($(this).attr('data-merk'));
        $("#nama_gol_obat").val($(this).attr('data-gol'));
        $("#kandungan_obat").val($(this).attr('data-kand'));
        cekbarang();
    });

    $("#tabelbarang").DataTable();

</script>