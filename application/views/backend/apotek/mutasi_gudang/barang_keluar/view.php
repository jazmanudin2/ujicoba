<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Data Mutasi Gudang Keluar</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Transaksi</a>
            </li>
            <li>
                <a>Data Mutasi Gudang</a>
            </li>
            <li class="active">
                <strong>Data Mutasi Gudang Barang Keluar</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Tabel View Barang -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Surat Pesanan</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive" >
                        <div class="col-md-12">
                            <form class="form-horizontal" method="post" action="" autocomplete="off">

                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" value="<?php echo $pencarian;?>" id="pencarian" name="pencarian" class="form-control" placeholder="Pencarian">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-7">
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="submit" name="submit" class="btn btn-sm bg-blue m-2-15 waves-effect" value="Cari Data">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <table class="table table-striped table-bordered table-hover" >
                            <thead >
                                <tr>
                                    <th>No</th>
                                    <th>ID Mutasi Gudang</th>
                                    <th>Tanggal</th>
                                    <th>Jenis Mutasi</th>
                                    <th>PIC Mutasi Gudang</th>
                                    <th>Asal Barang Masuk</th>
                                    <th>Nama Pengirim</th>
                                    <th>Nama Penerima</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>

                            <body>
                                <?php $no=1;?>
                                <?php foreach ($data as $d) {?>
                                    <tr id="">
                                        <td><?php echo $no++;?></td>
                                        <td><?php echo $d['id_mutasi_gudang'];?></td>
                                        <td><?php echo $d['tanggal'];?></td>
                                        <td><?php echo $d['jenis_mutasi'];?></td>
                                        <td><?php echo $d['pic_mg'];?></td>
                                        <td><?php echo $d['barang_dikirim_ke'];?></td>
                                        <td><?php echo $d['nama_pengirim'];?></td>
                                        <td><?php echo $d['nama_penerima'];?></td>
                                        <td><a class="fa fa-address-card btn-info btn btn-sm" href="<?php echo base_url();?>apotek/mutasi_barang_keluar/detail/<?php echo $d['id_mutasi_gudang']; ?>"></a></td>
                                    </tr>
                                <?php }?>
                            </body>

                        </table>
                        <div style='margin-top: 10px;'>
                            <?php echo $pagination; ?>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive" >

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>