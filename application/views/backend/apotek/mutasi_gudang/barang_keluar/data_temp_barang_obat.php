<?php 
$total = 0;
foreach($listmpobat->result() as $d){ 
    $total = $total + $d->subtotal;
    ?>
    <tr>
        <td><?php echo $d->id_barang; ?></td>
        <td><?php echo $d->merk_dag; ?></td>
        <td><?php echo $d->nama_gol_obat; ?></td>
        <td><?php echo $d->jumlah_perbox; ?></td>
        <td><?php echo $d->exp_date; ?></td>
        <td align="right"><?php echo uang($d->harga_satuan); ?></td>
        <td><?php echo $d->box; ?></td>
        <td><?php echo $d->jumlah_satuan; ?></td>
        <td align="right"><?php echo uang($d->subtotal); ?></td>
        <td><a data-kode="<?php echo $d->id_dtlbarang_temp;?>" data-exp="<?php echo $d->exp_date;?>" class="fa fa-trash btn-danger btn btn-sm hapus_tmp sum_total"></a>
        </td>
    </tr>
<?php } ?>
<tr>
    <td colspan="8" align="center"><b>TOTAL</b></td>
    <td align="right"><h3><?php echo uang($total); ?></h3></td>
    <td hidden=""><input type="text" id="subtotal2" value="<?php echo $total;?>" name="subtotal2"></td>
    <td></td>
</tr>   
<script type="text/javascript">


    function cektemp(){

        $.ajax({

            type    : 'POST',
            url     : '<?php echo base_url(); ?>apotek/mutasi_barang_keluar/cektemp',
            cache   : false,
            success :function(respond){

                $("#cektemp").val(respond);

            }

        });
    } 

    function sum_temp(){

        $.ajax({

            type    : 'POST',
            url     : '<?php echo base_url(); ?>apotek/mutasi_barang_keluar/sum_temp',
            cache   : false,
            success : function(respond){

                $("#sum_temp").html(respond);
            }

        });
    } 
    
    // Menampilkan Data Detail Penjumalan Temp
    function tampiltmpobat(){

        // Mengosongkan Inputan Text Ketika Sudah Di Klik Tambah
        $("#id_barang_m").val("");
        $("#stok").val("");
        $("#id_barang").val("");
        $("#jumlah_perbox").val("");
        $("#no_batch").val("");
        $("#exp_date").val("");
        $("#harga_satuan").val("");
        $("#box").val("");
        $("#jumlah_satuan").val("");
        $("#subtotal").val("");

        $.ajax({
            type    : 'GET',
            url     : '<?php echo base_url();?>apotek/mutasi_barang_keluar/view_temp_barang_obat',
            data    : '',
            success : function (html) {
                $("#tampiltmpobat").html(html);
            }
        });

    }

    $('.hapus_tmp').click(function(e){
        e.preventDefault();
        var id_dtlbarang_temp = $(this).attr("data-kode");
        $.ajax({

            type : 'POST',
            url  : '<?php echo base_url();?>apotek/mutasi_barang_keluar/hapus_tmp',
            data : {
                id_dtlbarang_temp     : id_dtlbarang_temp,
            },
            cache:false,
            success:function(respond){
                tampiltmpobat();
                sum_temp();
                cektemp();
            }
        });

    });

</script>