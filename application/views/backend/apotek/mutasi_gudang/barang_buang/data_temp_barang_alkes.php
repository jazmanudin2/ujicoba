<?php 
$total = 0;
foreach($listmpalkes->result() as $d){ 
	$total = $total + $d->subtotal;
	?>
	<tr>
        <td><?php echo $d->id_barang; ?></td>
        <td><?php echo $d->jenis_barang; ?></td>
        <td><?php echo $d->detail_alkes; ?></td>
        <td><?php echo $d->jumlah_perbox; ?></td>
        <td><?php echo $d->exp_date; ?></td>
        <td><?php echo uang($d->harga_satuan); ?></td>
        <td><?php echo $d->box; ?></td>
        <td><?php echo $d->jumlah_satuan; ?></td>
        <td><?php echo uang($d->subtotal); ?></td>
        <td><a data-kode="<?php echo $d->id_dtlbarang_temp;?>" class="fa fa-trash btn-danger btn btn-sm hapus_tmp sum_total"></a>
        </td>
    </tr>
<?php } ?>
<tr>
	<td colspan="8" align="center"><b>TOTAL</b></td>
	<td align="right"><h3><?php echo uang($total); ?></h3></td>
	<td hidden=""><input type="text" id="subtotal1" value="<?php echo $total;?>" name="subtotal1"></td>
	<td></td>
</tr>	
<script type="text/javascript">

    function sum_temp(){

        $.ajax({

            type    : 'POST',
            url     : '<?php echo base_url(); ?>apotek/mutasi_barang_buang/sum_temp',
            cache   : false,
            success :function(respond){

                $("#sum_temp").html(respond);
            }

        });
    } 


    function cektemp(){

        $.ajax({

            type    : 'POST',
            url     : '<?php echo base_url(); ?>apotek/mutasi_barang_buang/cektemp',
            cache   : false,
            success :function(respond){

                $("#cektemp").val(respond);

            }

        });
    } 

    // Menampilkan Data Detail Penjumalan Temp
    function tampiltmpalkes(){

        // Mengosongkan Inputan Text Ketika Sudah Di Klik Tambah
        $("#id_dtlbarang_temp").val("");
        $("#id_barang").val("");
        $("#jumlah_perbox").val("");
        $("#no_batch").val("");
        $("#exp_date").val("");
        $("#harga_satuan").val("");
        $("#box").val("");
        $("#jumlah_satuan").val("");
        $("#subtotal").val("");

        $.ajax({
        	type    : 'GET',
        	url     : '<?php echo base_url();?>apotek/mutasi_barang_buang/view_temp_barang_alkes',
        	data    : '',
        	success : function (html) {
        		$("#tampiltmpalkes").html(html);
        	}
        });

    }

    $('.hapus_tmp').click(function(e){
    	e.preventDefault();
    	var id_dtlbarang_temp = $(this).attr("data-kode");
    	$.ajax({

    		type : 'POST',
    		url  : '<?php echo base_url();?>apotek/mutasi_barang_buang/hapus_tmp',
    		data : {id_dtlbarang_temp:id_dtlbarang_temp},
    		cache:false,
    		success:function(respond){
                cektemp();
                sum_temp();
                tampiltmpalkes();
            }
        });

    }); 

</script>