<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">INPUT DATA PEMASUKAN</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/pemasukan/input" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" name="id_pemasukan" value="<?php echo $pemasukan['id_pemasukan'];?>" readonly class="form-control">
        </div>
        <div class="form-group" id="data_1">
            <label class="control-label">
                Tanggal <span class="symbol "></span>
            </label>
            <div class="input-group date">
                <input  type="text" autocomplete="off" class="input-sm form-control" value="<?php echo $pemasukan['id_pemasukan'];?>" name="tanggal"/>
                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
            </div>  
        </div>
        <div class="form-group">
            <label class="control-label">
                Jenis Pemasukan<span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Jenis Pemasukan" required name="jenis_pemasukan" value="<?php echo $pemasukan['jenis_pemasukan'];?>" class="form-control">
        </div>
        
        <div class="form-group">
            <label class="control-label">
                Penerima<span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Penerima" value="<?php echo $pemasukan['penerima'];?>" id="penerima" name="penerima" class="form-control" >
        </div>

        <div class="form-group">
            <label class="control-label">
                Biaya <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" value="<?php echo $pemasukan['biaya'];?>" placeholder="Masukan Biaya" id="biaya" name="biaya" class="form-control saldo sisa_saldo" >
        </div>

        <div class="form-group">
            <label class="control-label">
                Jumlah <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" value="<?php echo $pemasukan['jumlah'];?>" placeholder="Masukan Jumlah" id="jumlah" name="jumlah" class="form-control saldo sisa_saldo" >
        </div>

        <div class="form-group">
            <label class="control-label">
                Total <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" readonly="" placeholder="Masukan Total" value="<?php echo $pemasukan['setor'];?>" id="total" name="total" class="form-control saldo sisa_saldo" >
        </div>

        <div class="form-group">
            <label class="control-label">
                Saldo Awal<span class="symbol "></span>
            </label>
            <input  value="<?php echo $pemasukan['saldo'];?>" autocomplete="off" type="text" placeholder="Masukan Saldo" id="saldo" readonly="" value="<?php echo $pemasukan['saldo'];?>"  name="saldo" class="form-control" >
        </div>

        <div hidden="" class="form-group">
            <label class="control-label">
                ID<span class="symbol "></span>
            </label>
            <input value="<?php echo $pemasukan['id_saldo'];?>"  autocomplete="off" type="text" placeholder="Masukan Saldo" id="id_saldo" readonly="" name="id_saldo" class="form-control" >
        </div>

        <div class="form-group">
            <label class="control-label">
                Saldo Akhir <span class="symbol "></span>
            </label>
            <input value="<?php echo $pemasukan['saldo'];?>" autocomplete="off" type="text" placeholder="Masukan Akhir" readonly="" id="sisa_saldo" name="sisa_saldo" class="form-control" >
        </div>

        <div class="form-group">
            <label class="control-label">
                Invoice <span class="symbol "></span>
            </label>
            <select  name="invoice" id="invoice" class="chosen-select form-control input-sm"  tabindex="2">
                <?php if ($pemasukan['invoice'] == 'Ada') { ?>
                    <option value="<?php echo $pemasukan['invoice'];?>"><?php echo $pemasukan['invoice'];?></option>
                    <option value="Tidak">Tidak</option>
                <?php }else{ ?>
                    <option value="<?php echo $pemasukan['invoice'];?>"><?php echo $pemasukan['invoice'];?></option>
                    <option value="Ada">Ada</option>
                <?php } ?>

            </select>
        </div>

        <div class="form-group">
            <label class="control-label">
                Keterangan <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" value="<?php echo $pemasukan['keterangan'];?>" placeholder="Masukan Keterangan" name="keterangan" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>



<script>
    $(document).ready(function(){

        $(".saldo").on("input",function(){
            var biaya           = $("#biaya").val();
            var jumlah          = $("#jumlah").val();
            var saldo           = $("#saldo").val();
            var tarik           = $("#tarik").val();
            var total           = $("#total").val();
            var setor           = $("#setor").val();
            $("#sisa_saldo").val((saldo*1)+((biaya*jumlah)*1));
            $("#total").val(biaya*jumlah)
        });

        $('.chosen-select').chosen({width: "100%"});

        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: "yyyy-mm-dd"
        });

    });
</script>