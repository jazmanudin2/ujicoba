<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">INPUT DATA JENIS HARGA PELANGGAN</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/jenis_harga_pel/input" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div hidden="" class="form-group">
            <label class="control-label">
                ID Jenis Harga <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text"  name="id_jenis_harga_pel" readonly placeholder="Masukan Id Jenis Harga" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Jenis Harga <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Nama Jenis" name="nama_jenis_harga_pel" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Keterangan <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Keterangan" name="keterangan" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>
