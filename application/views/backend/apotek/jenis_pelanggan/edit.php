<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT DATA JENIS PELANGGAN</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/jenis_pelanggan/edit" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID Jenis Pelanggan <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" name="id_jenis_pelanggan" value="<?php echo $jenis_pelanggan['id_jenis_pelanggan'] ?>" readonly placeholder="Masukan Id Jenis Pelanggan" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Jenis Pelanggan <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" required="" placeholder="Masukan Nama Jenis" value="<?php echo $jenis_pelanggan['nama_jenis_pelanggan'] ?>" name="nama_jenis_pelanggan" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
               Ket <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan keterangan" value="<?php echo $jenis_pelanggan['keterangan'] ?>" name="keterangan" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>
