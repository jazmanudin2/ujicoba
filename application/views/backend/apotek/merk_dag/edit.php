<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT DATA MERK</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>apotek/merk_dag/edit" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID Merk <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" name="id_merk_dag" value="<?php echo $merk_dag['id_merk_dag'] ?>" readonly placeholder="Masukan Id Merk" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Merk <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" required="" placeholder="Masukan Nama Jenis" value="<?php echo $merk_dag['nama_merk_dag'] ?>" name="nama_merk_dag" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
               Ket <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan keterangan" value="<?php echo $merk_dag['keterangan'] ?>" name="keterangan" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>
