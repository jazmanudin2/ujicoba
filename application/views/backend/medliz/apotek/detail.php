<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Detail Apotek</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url();?>dashboard">Home</a>
            </li>
            <li>
                <a>Data Master</a>
            </li>
            <li>
                <a>Data Apotek</a>
            </li>
            <li class="active">
                <strong>Detail Apotek</strong>
            </li>
        </ol>
    </div>
</div>

<!-- Table View apt -->
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="table-responsive">
                        <div class="col-lg-5">
                            <table class="table table-striped table-hover" style="font-size: 12px;font-variant: arial" >
                                <tbody>
                                    <tr>
                                        <th width="170px">ID Apotek</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['id_apotek'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Jenis Apotek</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['nama_jenis_apt'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Nama Apotek</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['nama_apt'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Owner Apotek</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['owner_apt'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Penanggung Jawab</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['penanggungjwb'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="170px">Telp. Penanggung Jawab</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['telp_penanggungjwb'];?></td>
                                    </tr>

                                    <tr>
                                        <th width="170px">Email</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['email'];?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-4">
                            <table class="table table-striped table-hover" style="font-size: 12px;" >
                                <tbody>
                                    <tr>
                                        <th width="130px">Alamat</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['alamat'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="130px">Provinsi</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['nama_provinsi'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="130px">Kota/Kabupaten</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['nama_kota'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="130px">Kecamatan</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['nama_kecamatan'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="130px">Kelurahan/Desa</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['nama_desa'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="130px">Telpon 1</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['telp1'];?></td>
                                    </tr>
                                    <tr>
                                        <th width="130px">Telpon 2</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['telp2'];?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-2">
                            <table class="table table-striped table-hover" style="font-size: 12px;" >
                                <tbody>
                                    <tr>
                                        <td colspan="3"><img width="200px" height="190px" src="<?php echo base_url();?>assets/backend/apotek/<?php echo $dtlapt['foto'];?>"></td>
                                    </tr>
                                    <tr>
                                        <th width="120px">Gps Lokasi</th>
                                        <th width="10">:</th>
                                        <td><?php echo $dtlapt['gps_lok'];?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
