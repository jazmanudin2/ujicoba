<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">INPUT DATA APOTEK</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>medliz/apotek/input" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID Apotek <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" name="id_apotek"  value="<?php echo $kodeunik;?>" readonly placeholder="Masukan Id Apotek" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Jenis Apotek <span class="symbol "></span>
            </label>
            <select  name="id_jenis_apt" id="id_jenis_apt" class="form-control" required="">
                <option required value="">--- Pilih Jenis Apotek ---</option>
                <?php foreach ($jenis_apt->result() as $d){ ?>
                    <option required value="<?php echo $d->id_jenis_apt;?>">
                        <?php echo $d->nama_jenis_apt; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Apotek <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Nama Apotek" required name="nama_apt" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Owner Apotek <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Owner Apotek" name="owner_apt" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Penanggung Jawab <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Penanggung Jawab" name="penanggungjwb" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Telp Penanggung Jawab <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Telp Penanggung Jawab" name="telp_penanggungjwb" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Alamat <span class="symbol "></span>
            </label>
            <textarea autocomplete="off" type="text" name="alamat" class="form-control" ></textarea>
        </div>
        <div class="form-group">
            <label class="control-label">
                Provinsi <span class="symbol "></span>
            </label>
            <select name="provinsi" id="provinsi" class="form-control" required>
                <option value="">--- Pilih Provinsi ---</option>  
                <?php foreach ($listprovinsi->result() as $d){ ?>
                    <!-- Looping Option Untuk Data Provinsi -->
                    <option value="<?php echo $d->id_provinsi; ?>"><?php echo $d->nama_provinsi; ?></option>
                    
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Kota <span class="symbol "></span>
            </label>
            <select  autocomplete="off" type="text" placeholder="Masukan Kota" name="kota" id="kota" class="form-control"required>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Kecamatan <span class="symbol"></span>
            </label>
            <select  autocomplete="off" type="text" placeholder="Masukan Kecamatan" name="kecamatan" id="kecamatan" class="form-control" required>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Desa/Kel <span class="symbol"></span>
            </label>
            <select  autocomplete="off" type="text" placeholder="Masukan Desa/Kel" name="desa" id="desa" class="form-control" required>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Telp 1 <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Telp1" name="telp1" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Telp 2 <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Telp2" name="telp2" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Email <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="email" placeholder="Masukan Email" name="email" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>



<script>
    $(document).ready(function(){

        $("#provinsi").change(function(){

            id = $("#provinsi").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>medliz/apotek/get_kota',
                data  : {id_provinsi:id},
                cache : false,
                success:function(respond){

                    $("#kota").html(respond);
                }
                
            });
            
        });
        
        
        
        $("#kota").change(function(){

            id = $("#kota").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>medliz/apotek/get_kec',
                data  : {id_kota:id},
                cache : false,
                success:function(respond){

                    $("#kecamatan").html(respond);
                }
                
            });
            
        });
        
        
        $("#kecamatan").change(function(){

            id = $("#kecamatan").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>medliz/apotek/get_desa',
                data  : {id_kecamatan:id},
                cache : false,
                success:function(respond){

                    $("#desa").html(respond);
                }
                
            });
            
        });
        
        

        $("#form").validate({
            rules: {
                id_jenis_apt: {
                    required: true
                },
                nama_apt: {
                    required: true
                },
                owner_apt: {
                    required: true
                },
                penanggungjwb: {
                    required: true
                },
                telp_penanggungjwb: {
                    required: true,
                    number: true
                },
                alamat: {
                    required: true
                },
                provinsi: {
                    required: true
                },
                kota: {
                    required: true
                },
                kecamatan: {
                    required: true
                },
                desa: {
                    required: true
                },
                telp1: {
                    required: true,
                    number: true
                },
                telp2: {
                    number: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                id_jenis_apt: {
                    required: "Opps, Jenis Apotek Tidak Boleh Kosong..!",
                },
                nama_apt: {
                    required: "Opps, Nama Apotek Tidak Boleh Kosong..!",
                },
                owner_apt: {
                    required: "Opps, Owner Apotek Tidak Boleh Kosong..!",
                },
                penanggungjwb: {
                    required: "Opps, Penanggung Jawab Tidak Boleh Kosong..!",
                },
                telp_penanggungjwb: {
                    required: "Opps, Telp Penanggung Jawab Tidak Boleh Kosong..!",
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                alamat: {
                    required: "Opps, Alamat Tidak Boleh Kosong..!",
                },
                provinsi: {
                    required: "Opps, Provinsi Tidak Boleh Kosong..!",
                },
                kota: {
                    required: "Opps, Kota Tidak Boleh Kosong..!",
                },
                kecamatan: {
                    required: "Opps, Kecamatan Tidak Boleh Kosong..!",
                },
                desa: {
                    required: "Opps, desa Tidak Boleh Kosong..!",
                },
                telp1: {
                    required: "Opps, Telp1 Tidak Boleh Kosong..!",
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                telp2: {
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                email: {
                    required: "Opps, Email Tidak Boleh Kosong..!",
                    email: "Opps, Cek Kembali Email Anda..!! Example : infomedliz@medliz.com",
                }
            }
        });

    });
</script>