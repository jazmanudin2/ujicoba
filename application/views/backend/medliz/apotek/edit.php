<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT DATA APOTEK</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>medliz/apotek/edit" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID Apotek <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" name="id_apotek" value="<?php echo $apotek['id_apotek'];?>" readonly placeholder="Masukan Id Apotek" class="form-control" required>
        </div>
        <div class="form-group">
            <label class="control-label">
                Jenis Apotek <span class="symbol "></span>
            </label>
            <select  name="id_jenis_apt" id="id_jenis_apt" class="form-control" required required="">
                <option required value="<?php echo $apotek['id_jenis_apt'];?>">-- <?php echo $apotek['nama_jenis_apt'];?> --</option>
                <?php foreach ($jenis_apt->result() as $d){ ?>
                    <option required value="<?php echo $d->id_jenis_apt;?>">
                        <?php echo $d->nama_jenis_apt; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Apotek <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Nama Apotek" value="<?php echo $apotek['nama_apt'];?>"  required name="nama_apt" class="form-control" required >
        </div>
        <div class="form-group">
            <label class="control-label">
                Owner Apotek <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Owner Apotek" name="owner_apt" value="<?php echo $apotek['owner_apt'];?>" class="form-control" required >
        </div>
        <div class="form-group">
            <label class="control-label">
                Penanggung Jawab <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Penanggung Jawab" value="<?php echo $apotek['penanggungjwb'];?>"  name="penanggungjwb" class="form-control" required >
        </div>
        <div class="form-group">
            <label class="control-label">
                Telp Penanggung Jawab <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Telp Penanggung Jawab" value="<?php echo $apotek['telp_penanggungjwb'];?>"  name="telp_penanggungjwb" class="form-control" required >
        </div>
        <div class="form-group">
            <label class="control-label">
                Alamat <span class="symbol "></span>
            </label>
            <textarea autocomplete="off" type="text" name="alamat" class="form-control" required ><?php echo $apotek['alamat'];?> </textarea>
        </div>
        <div class="form-group">
            <label class="control-label">
                Provinsi <span class="symbol "></span>
            </label>
            <select name="id_provinsi" id="provinsi" class="form-control" required>
                <option value="<?php echo $apotek['id_provinsi'];?>"><?php echo $apotek['nama_provinsi'];?></option>  
                <?php foreach ($listprovinsi->result() as $d){ ?>
                    <!-- Looping Option Untuk Data Provinsi -->
                    <option value="<?php echo $d->id_provinsi; ?>"><?php echo $d->nama_provinsi; ?></option>

                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Kota <span class="symbol "></span>
            </label>
            <select  autocomplete="off" type="text" placeholder="Masukan Kota" value="<?php echo $apotek['id_kota'];?>" name="id_kota" id="kabupaten" class="form-control" required>
                <option value="<?php echo $apotek['id_kota'];?>"><?php echo $apotek['nama_kota'];?></option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Kecamatan <span class="symbol"></span>
            </label>
            <select  autocomplete="off" type="text" placeholder="Masukan Kecamatan" value="<?php echo $apotek['id_kecamatan'];?>" name="id_kecamatan" id="kecamatan" class="form-control" required>
                <option value="<?php echo $apotek['id_kecamatan'];?>"><?php echo $apotek['nama_kecamatan'];?></option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Kelurahan <span class="symbol"></span>
            </label>
            <select  autocomplete="off" type="text" placeholder="Masukan Kecamatan" value="<?php echo $apotek['id_desa'];?>"  name="id_desa" id="desa" class="form-control" required>
                <option value="<?php echo $apotek['id_desa'];?>"><?php echo $apotek['nama_desa'];?></option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Telp 1 <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Telp1" name="telp1" value="<?php echo $apotek['telp1'];?>"  class="form-control" required >
        </div>
        <div class="form-group">
            <label class="control-label">
                Telp 2 <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Telp2" value="<?php echo $apotek['telp2'];?>" name="telp2" class="form-control" required >
        </div>
        <div class="form-group">
            <label class="control-label">
                Email <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="email" placeholder="Masukan Email" value="<?php echo $apotek['email'];?>" name="email" class="form-control" required >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>



<script>
    $(document).ready(function(){

        $("#provinsi").change(function(){

            id = $("#provinsi").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>medliz/apotek/get_kota',
                data  : {id_provinsi:id},
                cache : false,
                success:function(respond){

                    $("#kabupaten").html(respond);
                }
                
            });
            
        });
        
        
        
        $("#kabupaten").change(function(){

            id = $("#kabupaten").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>medliz/apotek/get_kec',
                data  : {id_kota:id},
                cache : false,
                success:function(respond){

                    $("#kecamatan").html(respond);
                }
                
            });
            
        });
        
        
        $("#kecamatan").change(function(){

            id = $("#kecamatan").val();
            $.ajax({
                type  : 'POST',
                url   : '<?php echo base_url();?>medliz/apotek/get_desa',
                data  : {id_kecamatan:id},
                cache : false,
                success:function(respond){

                    $("#desa").html(respond);
                }
                
            });
            
        });
        
        

        $("#form").validate({
            rules: {
                id_jenis_apt: {
                    required: true
                },
                nama_apt: {
                    required: true
                },
                owner_apt: {
                    required: true
                },
                penanggungjwb: {
                    required: true
                },
                telp_penanggungjwb: {
                    required: true,
                    number: true
                },
                alamat: {
                    required: true
                },
                telp1: {
                    required: true,
                    number: true
                },
                id_provinsi: {
                    required: true,
                },
                id_kota: {
                    required: true,
                },
                id_kecamatan: {
                    required: true,
                },
                id_desa: {
                    required: true,
                },
                telp2: {
                    number: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                id_jenis_apt: {
                    required: "Opps, Jenis Apotek Tidak Boleh Kosong..!",
                },
                nama_apt: {
                    required: "Opps, Nama Apotek Tidak Boleh Kosong..!",
                },
                owner_apt: {
                    required: "Opps, Owner Apotek Tidak Boleh Kosong..!",
                },
                penanggungjwb: {
                    required: "Opps, Penanggung Jawab Tidak Boleh Kosong..!",
                },
                telp_penanggungjwb: {
                    required: "Opps, Telp Penanggung Jawab Tidak Boleh Kosong..!",
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                alamat: {
                    required: "Opps, Alamat Tidak Boleh Kosong..!",
                },
                id_provinsi: {
                    required: "Opps, Provinsi Tidak Boleh Kosong..!",
                },
                id_kota: {
                    required: "Opps, Kota Tidak Boleh Kosong..!",
                },
                id_kecamatan: {
                    required: "Opps, Kecamatan Tidak Boleh Kosong..!",
                },
                id_desa: {
                    required: "Opps, Kelurahan Tidak Boleh Kosong..!",
                },
                telp1: {
                    required: "Opps, Telp1 Tidak Boleh Kosong..!",
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                telp2: {
                    number: "Opps, Tidak Boleh Karakter, Harus Angka..!",
                },
                email: {
                    required: "Opps, Email Tidak Boleh Kosong..!",
                    email: "Opps, Cek Kembali Email Anda..!! Example : infomedliz@medliz.com",
                }
            }
        });

    });
</script>