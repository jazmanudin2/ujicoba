<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">INPUT DATA USERS</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>medliz/users/input" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div hidden="" class="form-group">
            <label class="control-label">
                ID <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text"  name="id_users" readonly placeholder="Masukan Id Jenis Karyawan" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Username <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Username" name="username" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
               Password <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="password" placeholder="Masukan Password" name="password" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
               Email <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="email" placeholder="Masukan Email" name="email" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Level <span class="symbol"></span>
            </label>
            <select name="id_level" id="id_level" class="form-control" required="">
                <option required value="">--- Pilih Level --</option>
                <?php foreach ($level->result() as $d){ ?>
                    <option required value="<?php echo $d->id_level;?>">
                        <?php echo $d->nama_level; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Apotek <span class="symbol"></span>
            </label>
            <select name="id_apotek" id="id_apotek" class="form-control" required="">
                <option required value="">--- Pilih Apotek --</option>
                <?php foreach ($apotek->result() as $d){ ?>
                    <option required value="<?php echo $d->id_apotek;?>">
                        <?php echo $d->nama_apt; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>
