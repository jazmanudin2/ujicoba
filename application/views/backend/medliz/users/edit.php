<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT DATA USERS</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>medliz/users/edit" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div hidden="" class="form-group">
            <label class="control-label">
                ID <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" value="<?php echo $users['id_users'] ?>" name="id_users" readonly placeholder="Masukan Id Jenis Karyawan" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Username <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" value="<?php echo $users['username'] ?>" placeholder="Masukan Username" name="username" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Password <span class="symbol "></span>
            </label>
            <input autocomplete="off" type="password" value="<?php echo $users['password'] ?>" placeholder="Masukan Password" name="password" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Email <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="email" value="<?php echo $users['email'] ?>" placeholder="Masukan Email" name="email" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
                Level <span class="symbol"></span>
            </label>
            <select name="id_level" id="id_level" class="form-control" required="">
                <option required value="<?php echo $users['id_level'] ?>"><?php echo $users['nama_level'] ?></option>
                <?php foreach ($level->result() as $d){ ?>
                    <option required value="<?php echo $d->id_level;?>">
                        <?php echo $d->nama_level; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Level <span class="symbol"></span>
            </label>
            <select name="id_apotek" id="id_apotek" class="form-control" required="">
                <option required value="<?php echo $users['id_apotek'] ?>"><?php echo $users['nama_apt'] ?></option>
                <?php foreach ($apotek->result() as $d){ ?>
                    <option required value="<?php echo $d->id_apotek;?>">
                        <?php echo $d->nama_apt; ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label">
                Status <span class="symbol"></span>
            </label>
            <select name="status" id="status" class="form-control" required="">
                <option value="<?php echo $users['status'] ?>">Status <?php echo $users['status'] ?></option>
                <option value="Tidak Aktif">Tidak Aktif</option>
                <option value="Aktif">Aktif</option>
            </select>
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>


