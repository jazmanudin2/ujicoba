<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">EDIT DATA JENIS APOTEK</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>medliz/jenis_apt/edit" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div class="form-group">
            <label class="control-label">
                ID Jenis Apotek <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text" name="id_jenis_apt" value="<?php echo $jenis_apt['id_jenis_apt'] ?>" readonly placeholder="Masukan Id Jenis Apotek" class="form-control">
        </div>
        <div class="form-group">
            <label class="control-label">
                Nama Jenis Apotek <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" required="" placeholder="Masukan Nama Jenis" value="<?php echo $jenis_apt['nama_jenis_apt'] ?>" name="nama_jenis_apt" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
               Ket <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan keterangan" value="<?php echo $jenis_apt['keterangan'] ?>" name="keterangan" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>
