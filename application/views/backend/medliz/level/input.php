<div class="col-sm-12">
    <h3 align="center" class="m-t-none m-b">INPUT DATA LEVEL</h3>
    <form class="needs-validation" id="form" action="<?php echo base_url();?>medliz/level/input" novalidate method="POST" encautocomplete="off" type="multipart/form-data">
        <div hidden="" class="form-group">
            <label class="control-label">
                ID Level <span class="symbol "></span>
            </label> 
            <input autocomplete="off" type="text"  name="id_level" readonly placeholder="Masukan Id Level" class="form-control">
        </div>  
        <div class="form-group">
            <label class="control-label">
                Nama Level <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan Nama Jenis" name="nama_level" class="form-control" >
        </div>
        <div class="form-group">
            <label class="control-label">
               Ket <span class="symbol "></span>
            </label>
            <input  autocomplete="off" type="text" placeholder="Masukan keterangan" name="keterangan" class="form-control" >
        </div>
        <div>
            <button class="ladda-button btn btn-primary btn-block"  name="submit" data-style="zoom-in">Simpan</button>
        </div>
    </form>
</div>
