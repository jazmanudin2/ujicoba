<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PD. AM MELA</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">

</head>

<body>
    <table>
        <tr><td height="100px"></td></tr>
        <tr align="center">
            <td width="500px"></td>
            <td width="370px">
                <div class="loginColumns animated fadeInDown">
                    <div class="row">
                        <div class="ibox-content">
                            <form role="form" method="POST" action="<?php echo base_url();?>medliz/auth/login">
                                <fieldset>
                                    <h1 align="center"><b>PT. MEDLIZ</b></h1>
                                    <br>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Username" name="username" autocomplete="off" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Password" name="password" autocomplete="off" type="password" value="">
                                    </div>
                                    <button  name="submit" class="btn btn-lg btn-success btn-block">Login</button>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </td>
            <td width="500px"></td>
        </tr>
    </table>
</body>

</html>
