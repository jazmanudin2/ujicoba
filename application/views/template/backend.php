<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PT. MEDLIZ</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- DataTables -->
    <link href="<?php echo base_url();?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">

    <!-- DatePicker -->
    <link href="<?php echo base_url();?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">

    <!-- Chosen -->
    <link href="<?php echo base_url();?>assets/css/plugins/chosen/bootstrap-chosen.css" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="<?php echo base_url();?>assets/sweetalert/sweetalert2.min.css" rel="stylesheet">

    <!-- Ladda style -->
    <link href="<?php echo base_url();?>assets/css/plugins/ladda/ladda-themeless.min.css" rel="stylesheet">

    <!-- FooTable -->
    <link href="<?php echo base_url();?>assets/css/plugins/footable/footable.core.css" rel="stylesheet">

    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    
    <style>
    #notifikasi {
      cursor: pointer;
      position: fixed;
      right: 0px;
      z-index: 9999;
      bottom: 0px;
      margin-bottom: 22px;
      margin-right: 15px;
      min-width: 300px; 
      max-width: 800px;
  }

</style>

<!-- Jquery -->

<!-- Mainly scripts -->
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url();?>assets/js/plugins/dataTables/datatables.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="<?php echo base_url();?>assets/js/inspinia.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/pace/pace.min.js"></script>

<!-- Data picker -->
<script src="<?php echo base_url();?>assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>

<!-- Sweet Alert -->
<script src="<?php echo base_url();?>assets/sweetalert/sweetalert2.min.js"></script>

<!-- Chosen -->
<script src="<?php echo base_url();?>assets/js/plugins/chosen/chosen.jquery.js"></script>

<!-- Jquery Validate -->
<script src="<?php echo base_url();?>assets/js/plugins/validate/jquery.validate.min.js"></script>

<!-- Ladda -->
<script src="<?php echo base_url();?>assets/js/plugins/ladda/spin.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/ladda/ladda.min.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/ladda/ladda.jquery.min.js"></script>

<!-- FooTable -->
<script src="<?php echo base_url();?>assets/js/plugins/footable/footable.all.min.js"></script>

</head>
<body>

    <div id="wrapper">

        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav metismenu" id="side-menu">
                    <li class="nav-header">
                        <div class="dropdown profile-element"> 
                            <img alt="image" class="img-circle" width="50px" src="<?php echo base_url();?>assets/email_templates/img/header.jpg">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear"> 
                                    <span class="block m-t-xs"> 
                                        <strong class="font-bold">Jazmanudin</strong>
                                    </span> 
                                    <span class="text-muted text-xs block">Administrator<b class="caret"></b>
                                    </span>
                                </span>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                <li><a href="#">Profile</a></li>
                                <li><a href="#">Contacts</a></li>
                                <li><a href="#">Mailbox</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo base_url();?>medliz/auth/logout">Logout</a></li>
                            </ul>
                        </div>
                    </li>

                    <?php
                    $level = $this->session->userdata('id_level');

                    $menu = $this->db->query("SELECT * FROM tbl_menu WHERE is_parent='0' AND is_active='1' AND role='$level'  ORDER BY id ASC");
                    foreach ($menu->result() as $m) {
                            //chek ada sub menu
                        $this->db->order_by('id','ASC');
                        $submenu = $this->db->get_where('tbl_menu',array('is_parent'=>$m->id,'is_active'=>1,'role'=>$this->session->userdata('id_level')));
                        if($submenu->num_rows()>0){
                            ?>
                            <li>
                                <a href="#"><i class="<?php echo $m->icon; ?>"></i> <span class="nav-label"><?php echo ucwords($m->name); ?> </span><span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level collapse">
                                    <?php 
                                    foreach ($submenu->result() as $s){

                                        $submenusub = $this->db->get_where('tbl_menu',array('is_parent'=>$s->id,'is_active'=>1,'role'=>$this->session->userdata('id_level')));
                                        if($submenusub->num_rows() > 0){
                                            ?> 
                                            <li>
                                                <a href="#"><?php echo ucwords($s->name); ?> <span class="fa arrow"></span></a>
                                                <ul class="nav nav-third-level">
                                                    <?php foreach ($submenusub->result() as $ss){ ?>
                                                        <li>
                                                            <a href="<?php echo base_url().$ss->link; ?>"><?php echo ucwords($ss->name); ?></a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                        <?php }else{ ?>
                                            <li>
                                                <a href="<?php echo base_url().$s->link; ?>"><?php echo ucwords($s->name); ?> 
                                            </a>
                                        </li>
                                    <?php } ?>
                                <?php } ?>  
                            </ul>
                        </li>
                        <?php
                    }else{ 
                        ?>
                        <li>
                            <a href="<?php echo base_url().$m->link; ?>"><i class="<?php echo $m->icon; ?>"></i>  <?php echo ucwords($m->name); ?></a>
                        </li>
                    <?php } ?>
                    <?php
                }
                ?>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" action="search_results.html">
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="<?php echo base_url();?>medliz/auth/logout">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- <div id="notifikasi">
            <?php echo $this->session->flashdata('msg'); ?>
        </div> -->
        <?php echo $contents;?>
        <br>
        <div class="footer">
            <div>
                <strong>Copyright</strong> Example Company &copy; 2014-2017
            </div>
        </div>
    </div>
</div>


</body>
</html>
