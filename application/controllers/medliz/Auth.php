<?php
class Auth extends CI_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('Model_auth');
    }

    function index(){
        $this->load->view('backend/medliz/auth/login');
    }

    function login(){

        $username=htmlspecialchars($this->input->post('username',TRUE),ENT_QUOTES);
        $password=htmlspecialchars($this->input->post('password',TRUE),ENT_QUOTES);

        $cek_apt=$this->Model_auth->auth_users($username,$password);

        if($cek_apt->num_rows() > 0){ 
            $data=$cek_apt->row_array();
            $this->session->set_userdata('masuk',TRUE);

            if($data['id_level']=='Administrator'){
                $this->session->set_userdata('akses','Administrator');
                $this->session->set_userdata('id_level',$data['id_level']);
                $this->session->set_userdata('id_admin',$data['id_admin']);
                $this->session->set_userdata('username',$data['username']);
                $this->session->set_userdata('email',$data['email']);
                redirect('medliz/apotek');

            }else if($data['id_level']=='Admin Apotek'){
                $this->session->set_userdata('akses','Admin Apotek');
                $this->session->set_userdata('id_level',$data['id_level']);
                $this->session->set_userdata('id_admin',$data['id_admin']);
                $this->session->set_userdata('username',$data['username']);
                $this->session->set_userdata('email',$data['email']);
                redirect('apotek/pembelian/input');

            }else{ 
                $this->session->set_userdata('akses','4');
                $this->session->set_userdata('username',$data['username']);
                $this->session->set_userdata('nama_mapel',$data['nama_mapel']);
                $this->session->set_userdata('nama_lengkap',$data['nama_guru']);
                $this->session->set_userdata('nip',$data['nip']);
                $this->session->set_userdata('foto_guru',$data['foto']);
                redirect('dashboard');
            }

        }else{ 
            $cek_siswa=$this->Model_auth->auth_users($username,$password);
            if($cek_siswa->num_rows() > 0){
                $data=$cek_siswa->row_array();
                $this->session->set_userdata('masuk',TRUE);
                if($data['status_siswa']=='1'){
                    $this->session->set_userdata('akses','5');
                    $this->session->set_userdata('username',$data['username']);
                    $this->session->set_userdata('nama_lengkap',$data['nama_siswa']);
                    $this->session->set_userdata('nama_rombel',$data['nama_rombel']);
                    $this->session->set_userdata('wali_kelas',$data['nama_guru']);
                    $this->session->set_userdata('nisn',$data['nisn']);
                    redirect('jadwal/jadwal');

                }else{ 
                    $this->session->set_userdata('akses','6');
                    $this->session->set_userdata('username',$data['username']);
                    $this->session->set_userdata('nama_lengkap',$data['nama_siswa']);
                    $this->session->set_userdata('nama_rombel',$data['nama_rombel']);
                    $this->session->set_userdata('wali_kelas',$data['nama_guru']);
                    $this->session->set_userdata('nisn',$data['nisn']);
                    redirect('peminjaman/input_peminjaman');
                }
            }else{  
                $url=base_url('medliz/auth');
                redirect($url);
            }
        }

    }

    function logout(){
        $this->session->sess_destroy();
        $url=base_url('medliz/auth');
        redirect($url);
    }

}
