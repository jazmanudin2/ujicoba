<?php

class Menu extends CI_Controller{

	function __construct(){
		parent::__construct();
		ceklogin();
		$this->load->model('Model_menu');
	}

	function index(){
		
		$id 			 = $this->uri->segment(4);
		$data['getmenu'] = $this->Model_menu->get_menu($id)->row_array();
		$data['parent']  = $this->Model_menu->get_Menuparent()->result();
		$data['menu'] 	 = $this->Model_menu->view_menu()->result();
		
		$this->template->load('template/backend','backend/medliz/menu/view_menu',$data);

	}

	function hapus(){

		$id 	= $this->uri->segment(4);
		$hapus  = $this->Model_menu->hapus($id);
       redirect('medliz/menu');
	}


	function insert_menu(){

		$this->Model_menu->insert_menu();
       redirect('medliz/menu');

	}


}