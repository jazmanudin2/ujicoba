<?php
class Apotek extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_apotek','Model_jenis_apt','Model_provinsi'));
		ceklogin();
		$this->load->library('upload');
	}

	function index($rowno=0){
		// Search text
		$id_apotek 		= "";
		$pencarian  	= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 	= $this->input->post('pencarian');
			$id_apotek 	= $this->input->post('id_apotek');

			$data 	= array(

				'pencarian'		=> $pencarian,
				'id_apotek'	 	=> $id_apotek,

			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_apotek') != NULL){
				$id_apotek = $this->session->userdata('id_apotek');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}

	    // All records count
		$allcount 	  = $this->Model_apotek->getRecordCount($id_apotek,$pencarian);

	    // Get records
		$users_record = $this->Model_apotek->getData($rowno,$rowperpage,$id_apotek,$pencarian);

	    // Pagination Configuration
		$config['base_url'] 		= base_url().'medliz/apotek/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);

		$data['pagination'] 	= $this->pagination->create_links();
		$data['data'] 			= $users_record;
		$data['row'] 			= $rowno;
		$data['id_apotek'] 		= $id_apotek;
		$data['pencarian']		= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/medliz/apotek/view',$data);
	}


	function get_kota(){


		$id_provinsi = $this->input->post('id_provinsi');
		$listkota = $this->Model_provinsi->listkota($id_provinsi);
		echo "<option value=''>-- Pilih Kabupaten / Kota -- </option>";
		foreach($listkota->result() as $k){

			echo "<option value='$k->id_kota'>$k->nama_kota</option>";
		}
	}


	function get_kec(){


		$id_kota = $this->input->post('id_kota');
		$listkec = $this->Model_provinsi->listkec($id_kota);
		echo "<option value=''>-- Pilih Kecamatan -- </option>";
		foreach($listkec->result() as $k){

			echo "<option value='$k->id_kecamatan'>$k->nama_kecamatan</option>";
		}
	}


	function get_desa(){

		$id_kecamatan = $this->input->post('id_kecamatan');
		$listdesa = $this->Model_provinsi->listdesa($id_kecamatan);
		echo "<option value=''>-- Pilih Kelurahan -- </option>";
		foreach($listdesa->result() as $k){

			echo "<option value='$k->id_desa'>$k->nama_desa</option>";
		}
	}


	public function delete_checkbox(){

		$id_apotek = $this->input->post('id_apotek');

		$this->db->where_in('id_apotek', explode(",", $id_apotek));
		$this->db->delete('tbl_data_apt');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_apotek->delete();
		redirect('backend/medliz/apotek');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data
			$this->Model_apotek->insert();
			redirect('backend/medliz/apotek');
		}else{

			$data['listprovinsi'] 	= $this->Model_provinsi->listprovinsi();
			$data['kodeunik'] 		= $this->Model_apotek->code_otomatis();
			$data['jenis_apt'] 		= $this->Model_jenis_apt->view_jenis_apt();
			$this->load->view('backend/medliz/apotek/input',$data);

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_apotek->update();

		}else{

			$data['listprovinsi'] 	= $this->Model_provinsi->listprovinsi();
			$data['jenis_apt'] 		= $this->Model_jenis_apt->view_jenis_apt();
			$data['apotek'] 		= $this->Model_apotek->get_apotek()->row_array();
			$this->load->view('backend/medliz/apotek/edit',$data);

		}
	}

	public function detail(){

		$data['dtlapt'] 		= $this->Model_apotek->detail_apotek()->row_array();
		$this->template->load('template/backend','backend/medliz/apotek/detail',$data);

	}


}