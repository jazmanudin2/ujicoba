<?php
class Pembelian extends CI_Controller {

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_pembelian','Model_supplier','Model_sales','Model_barang_obat','Model_barang_umum','Model_barang_alkes'));
		ceklogin();
	}

	function input_tmp(){
		
		$this->Model_pembelian->input_tmp();
		
	}


	function view_hutang($rowno=0){
		
		// Search text
		$id_supplier 		= "";
		$pencarian  		= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 			= $this->input->post('pencarian');
			$id_supplier 		= $this->input->post('id_supplier');
			
			$data 	= array(

				'pencarian'		 		 => $pencarian,
				'id_supplier'	 	 	 => $id_supplier,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_supplier') != NULL){
				$id_supplier = $this->session->userdata('id_supplier');
			}
			
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_pembelian->getRecordCount($id_supplier,$pencarian);

	    // Get records
		$users_record = $this->Model_pembelian->getData($rowno,$rowperpage,$id_supplier,$pencarian);

	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/pembelian/view_hutang';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';

	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 	= $this->pagination->create_links();
		$data['data'] 			= $users_record;
		$data['row'] 			= $rowno;
		$data['id_supplier'] 	= $id_supplier;
		$data['pencarian']		= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/pembelian/view_hutang',$data);
	}

	function view_historihutang(){

		$data['histori'] = $this->Model_pembelian->get_histori_supplier()->row_array();
		$data['sum'] 	 = $this->Model_pembelian->sum_hhutang()->row_array();
		$data['data'] 	 = $this->Model_pembelian->view_hhutang();
		$this->template->load('template/backend','backend/apotek/pembelian/view_historihutang',$data);

	}

	function detailpembelian(){
		if(isset($_POST['submit'])){

			$this->Model_pembelian->insert_bayar();
		}else{
			$data['nobukti'] 	= $this->Model_pembelian->nobukti();
			$data['pembelian'] 	= $this->Model_pembelian->view_pembelian()->row_array();
			$data['obat'] 		= $this->Model_pembelian->view_dpembelian_barangobat();
			$data['umum'] 		= $this->Model_pembelian->view_dpembelian_barangumum();
			$data['alkes'] 		= $this->Model_pembelian->view_dpembelian_barangalkes();
			$data['editrbayar'] = $this->Model_pembelian->get_rbayar_pembayaran();
			$data['rbayar'] 	= $this->Model_pembelian->view_rpembayaran();
			$data['hbayar'] 	= $this->Model_pembelian->view_hbayar();
			$data['postingbyr'] = $this->Model_pembelian->view_posting_pembayaran();
			$data['ebayar'] 	= $this->Model_pembelian->get_rbayar_pembayaran()->row_array();
		}
		$this->template->load('template/backend','backend/apotek/pembelian/view_detailpembelian',$data);
	}
	
	function cektemp(){

		$id_user 	= $this->session->userdata('id_admin');
		$cek 		= $this->db->get_where('tbl_detailpembelian_temp',array('id_admin'=>$id_user))->num_rows();
		echo $cek;
		
	}

	function hapus_tmp(){

		$id_barang_m	= $this->input->post('id_barang_m');
		$id_admin		= $this->session->userdata('id_admin');
		$data 			= $this->db->delete('tbl_detailpembelian_temp',array('id_barang_m' => $id_barang_m,'id_admin' => $id_admin));
		echo json_encode($data);
		
	}

	function sum_temp(){

		$data['sum_temp'] = $this->Model_pembelian->sum_temp()->row_array();
		$this->load->view('backend/apotek/pembelian/sum_temp',$data);

	}

	function view_sp(){

		$data['surat_pesanan'] 	= $this->Model_pembelian->view_sp();
		$this->load->view('backend/apotek/pembelian/data_sp',$data);

	}

	function view_barang_obat(){

		$data['barang_obat'] 	= $this->Model_pembelian->view_barang_obat();
		$this->load->view('backend/apotek/pembelian/data_barang_obat',$data);

	}

	function view_barang_umum(){

		$data['barang_umum'] 	= $this->Model_pembelian->view_barang_umum();
		$this->load->view('backend/apotek/pembelian/data_barang_umum',$data);

	}

	function view_barang_alkes(){

		$data['barang_alkes'] 	= $this->Model_pembelian->view_barang_alkes();
		$this->load->view('backend/apotek/pembelian/data_barang_alkes',$data);

	}
	
	function view_temp_barangobat(){

		$data['listmpobat'] = $this->Model_pembelian->view_temp_barangobat();
		$this->load->view('backend/apotek/pembelian/data_temp_barangobat',$data);
		
	}

	function view_temp_barangumum(){

		$data['listmpumum'] = $this->Model_pembelian->view_temp_barangumum();
		$this->load->view('backend/apotek/pembelian/data_temp_barangumum',$data);
		
	}

	function view_temp_barangalkes(){

		$data['listmpalkes'] = $this->Model_pembelian->view_temp_barangalkes();
		$this->load->view('backend/apotek/pembelian/data_temp_barangalkes',$data);
		
	}

	function cekbarang(){

		$id_barang_m 	= $this->input->post('id_barang_m');
		$id_user 	= $this->session->userdata('id_admin');
		$cek 			= $this->db->query("SELECT * FROM tbl_detailpembelian_temp WHERE id_barang='$id_barang_m' AND id_admin = '$id_user' ")->num_rows();
		echo $cek;

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data
			$this->Model_pembelian->insert();
			redirect('backend/apotek/pembelian');
		}else{

			$data['kodeunik'] 		= $this->Model_pembelian->code_otomatis();
			$data['supplier'] 		= $this->Model_supplier->view_supplier();
			$data['nobukti'] 		= $this->Model_pembelian->nobukti();
			$data['sales'] 			= $this->Model_sales->view_sales();
			$this->template->load('template/backend','backend/apotek/pembelian/input',$data);

		}
	}


	function edit_histori_hutang(){
		if(isset($_POST['submit'])){

			$this->Model_pembelian->update_histori_hutang();
		}else{
			$data['histori'] 	= $this->Model_pembelian->get_histori_pembayaran()->row_array();
			$this->load->view('backend/apotek/pembelian/edit_histori_hutang',$data);
		}
	}


	function hapus_histori_penj(){

		$this->Model_pembelian->delete_histori_pemb();
		
	}	

	
	function hapus_pembayaran(){

		$this->Model_pembelian->delete_bayar();
		
	}

}