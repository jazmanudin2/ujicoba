<?php
class Surat_pesanan extends CI_Controller {

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_surat_pesanan','Model_supplier','Model_barang_obat'));
		ceklogin();
	}

	function input_tmp(){
		
		$this->Model_surat_pesanan->input_tmp();
		
	}

	function index($rowno=0){

		// Search text
		$id_sp 			= "";
		$pencarian  	= "";

		if($this->input->post('submit') != NULL ){
			$pencarian 		= $this->input->post('pencarian');
			$id_sp 	= $this->input->post('id_sp');
			
			$data 	= array(

				'pencarian'		=> $pencarian,
				'id_sp'			=> $id_sp,
				
			);
			$this->session->set_userdata($data);

		}else{

			if($this->session->userdata('pencarian') != NULL){
				$pencarian 	= $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_sp') != NULL){
				$id_sp = $this->session->userdata('id_sp');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_surat_pesanan->getRecordCount($id_sp,$pencarian);

	    // Get records
		$users_record = $this->Model_surat_pesanan->getData($rowno,$rowperpage,$id_sp,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/supplier/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
		$this->pagination->initialize($config);
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_sp'] 				= $id_sp;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/surat_pesanan/view',$data);
	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data
			$this->Model_surat_pesanan->insert();
			redirect('backend/apotek/surat_pesanan');
		}else{

			$data['kodeunik'] 		= $this->Model_surat_pesanan->code_otomatis();
			$data['supplier'] 		= $this->Model_supplier->view_supplier();
			$this->template->load('template/backend','backend/apotek/surat_pesanan/input',$data);

		}
	}

	function cektemp(){

		$id_user 	= $this->session->userdata('id_admin');
		$cek 		= $this->db->get_where('tbl_detailsuratpesanan_temp',array('id_admin'=>$id_user))->num_rows();
		echo $cek;
		
	}

	

	function hapus_tmp(){

		$id_barang_m	= $this->input->post('id_barang_m');
		$id_admin		= $this->session->userdata('id_admin');
		$data 			= $this->db->delete('tbl_detailsuratpesanan_temp',array('id_barang_m' => $id_barang_m,'id_admin' => $id_admin));
		echo json_encode($data);
		
	}

	function view_barang(){

		$data['barang'] 	= $this->Model_surat_pesanan->view_barang_obat();
		$this->load->view('backend/apotek/surat_pesanan/data_barang',$data);

	}

	function view_temp(){

		$data['listmp'] = $this->Model_surat_pesanan->view_tmp();
		$this->load->view('backend/apotek/surat_pesanan/data_temp',$data);
		
	}

	function cekbarang(){

		$id_barang_m 	= $this->input->post('id_barang_m');
		$cek 			= $this->db->query("SELECT * FROM tbl_detailsuratpesanan_temp WHERE id_barang_m='$id_barang_m'")->num_rows();
		echo $cek;

	}

	public function detail(){

		$data['detail'] 		= $this->Model_surat_pesanan->detail_sp()->row_array();
		$data['details'] 		= $this->Model_surat_pesanan->detail_tbl_detailsp();
		$this->template->load('template/backend','backend/apotek/surat_pesanan/detail',$data);

	}

}