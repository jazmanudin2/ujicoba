<?php
class Merk_dag extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_merk_dag'));
		// ceklogin();
	}

	function index($rowno=0){
		// Search text
		$id_merk_dag 		= "";
		$pencarian  		= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 		= $this->input->post('pencarian');
			$id_merk_dag 	= $this->input->post('id_merk_dag');
			
			$data 	= array(

				'pencarian'		=> $pencarian,
				'id_merk_dag'	=> $id_merk_dag,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_merk_dag') != NULL){
				$id_merk_dag = $this->session->userdata('id_merk_dag');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_merk_dag->getRecordCount($id_merk_dag,$pencarian);

	    // Get records
		$users_record = $this->Model_merk_dag->getData($rowno,$rowperpage,$id_merk_dag,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'apotek/merk_dag/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_merk_dag'] 		= $id_merk_dag;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/merk_dag/view',$data);
	}

	public function delete_checkbox(){

		$id_merk_dag = $this->input->post('id_merk_dag');
		$this->db->where_in('id_merk_dag', explode(",", $id_merk_dag));
		$this->db->delete('tbl_merk_dag');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_merk_dag->delete();
		redirect('backend/apotek/merk_dag');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data

			$this->Model_merk_dag->insert();
			redirect('backend/apotek/merk_dag');
		}else{

			$this->load->view('backend/apotek/merk_dag/input');

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_merk_dag->update();

		}else{

			$data['merk_dag'] 	= $this->Model_merk_dag->get_merk_dag()->row_array();
			$this->load->view('backend/apotek/merk_dag/edit',$data);

		}
	}

}