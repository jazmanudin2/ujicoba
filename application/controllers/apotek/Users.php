<?php
class Users extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_users'));
		ceklogin();
	}

	function index($rowno=0){
		// Search text
		$id_users 				= "";
		$pencarian  			= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 			= $this->input->post('pencarian');
			$id_users 			= $this->input->post('id_users');
			
			$data 	= array(

				'pencarian'			=> $pencarian,
				'id_users'			=> $id_users,
				
			);
			$this->session->set_userdata($data);
		}else{

			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_users') != NULL){
				$id_users = $this->session->userdata('id_users');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_users->getRecordCount($id_users,$pencarian);

	    // Get records
		$users_record = $this->Model_users->getData($rowno,$rowperpage,$id_users,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/users/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_users'] 			= $id_users;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/users/view',$data);
	}

	public function delete_checkbox(){

		$id_users = $this->input->post('id_users');
		$this->db->where_in('id_users', explode(",", $id_users));
		$this->db->delete('tbl_users');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_users->delete();
		redirect('backend/apotek/users');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data

			$this->Model_users->insert();
			redirect('backend/apotek/users');
		}else{

			$data['level'] 		= $this->Model_users->get_level();
			$data['apotek'] 	= $this->Model_users->get_apotek();
			$this->load->view('backend/apotek/users/input',$data);

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_users->update();

		}else{

			$data['users'] 	= $this->Model_users->get_users()->row_array();
			$data['level'] 	= $this->Model_users->get_level();
			$data['apotek'] = $this->Model_users->get_apotek();
			$this->load->view('backend/apotek/users/edit',$data);

		}
	}

}
