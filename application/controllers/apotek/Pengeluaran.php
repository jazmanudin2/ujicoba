<?php
class Pengeluaran extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_pengeluaran','Model_saldo'));
		ceklogin();
		$this->load->library('upload');
	}

	function index($rowno=0){
		// Search text
		$id_pengeluaran = "";
		$pencarian  	= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 			= $this->input->post('pencarian');
			$id_pengeluaran 	= $this->input->post('id_pengeluaran');
			
			$data 	= array(

				'pencarian'			=> $pencarian,
				'id_pengeluaran'	=> $id_pengeluaran,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_pengeluaran') != NULL){
				$id_pengeluaran = $this->session->userdata('id_pengeluaran');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_pengeluaran->getRecordCount($id_pengeluaran,$pencarian);

	    // Get records
		$users_record = $this->Model_pengeluaran->getData($rowno,$rowperpage,$id_pengeluaran,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/pengeluaran/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 	= $this->pagination->create_links();
		$data['data'] 			= $users_record;
		$data['row'] 			= $rowno;
		$data['id_pengeluaran'] = $id_pengeluaran;
		$data['pencarian']		= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/pengeluaran/view',$data);
	}

	public function delete_checkbox(){

		$id_pengeluaran = $this->input->post('id_pengeluaran');

		$this->db->where_in('id_pengeluaran', explode(",", $id_pengeluaran));
		$this->db->delete('tbl_pengeluaran_apt');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_pengeluaran->delete();
		redirect('apotek/pengeluaran');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data

			$this->Model_pengeluaran->insert();
			//Notifikasi
			echo $this->session->set_flashdata('msg',
				'<div class="alert alert-success">
				<h4>Successfully</h4>
				<p>Data Berhasil Di Simpan</p>
				</div>');

			redirect('apotek/pengeluaran');
		}else{

			$data['karyawan'] 		= $this->Model_pengeluaran->get_karyawan();
			$data['pengeluaran'] 	= $this->Model_pengeluaran->get_pengeluaran()->row_array();
			$data['kodeunik'] 		= $this->Model_pengeluaran->code_otomatis();
			$data['kodeunik2'] 		= $this->Model_saldo->code_otomatis();
			$this->load->view('backend/apotek/pengeluaran/input',$data);

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_pengeluaran->update();

		}else{

			$data['pengeluaran'] 		= $this->Model_pengeluaran->get_pengeluaran()->row_array();
			$this->load->view('backend/apotek/pengeluaran/edit',$data);

		}
	}

}