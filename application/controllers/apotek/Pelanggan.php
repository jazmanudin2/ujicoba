<?php
class Pelanggan extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_pelanggan','Model_jenis_pelanggan','Model_sales'));
		ceklogin();
		$this->load->library('upload');

	}

	function index($rowno=0){
		// Search text
		$id_pelanggan 	= "";
		$pencarian  	= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 		= $this->input->post('pencarian');
			$id_pelanggan 	= $this->input->post('id_pelanggan');
			
			$data 	= array(

				'pencarian'		=> $pencarian,
				'id_pelanggan'	=> $id_pelanggan,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_pelanggan') != NULL){
				$id_pelanggan = $this->session->userdata('id_pelanggan');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_pelanggan->getRecordCount($id_pelanggan,$pencarian);

	    // Get records
		$users_record = $this->Model_pelanggan->getData($rowno,$rowperpage,$id_pelanggan,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'apotek/pelanggan/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_pelanggan'] 		= $id_pelanggan;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/pelanggan/view',$data);
	}

	public function delete_checkbox(){

		$id_pelanggan = $this->input->post('id_pelanggan');

		$this->db->where_in('id_pelanggan', explode(",", $id_pelanggan));
		$this->db->delete('tbl_pelanggan');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_pelanggan->delete();
		redirect('apotek/pelanggan');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data

			$this->Model_pelanggan->insert();
			redirect('apotek/pelanggan');
		}else{

			$data['kodeunik'] 			= $this->Model_pelanggan->code_otomatis();
			$data['jenis_pelanggan'] 	= $this->Model_jenis_pelanggan->view_jenis_pelanggan();
			// $data['jenis_harga']		= $this->Model_jenis_harga->view_jenis_harga();
			$this->load->view('backend/apotek/pelanggan/input',$data);

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_pelanggan->update();

		}else{

			// $data['jenis_harga']		= $this->Model_jenis_harga->view_jenis_harga();
			$data['jenis_pelanggan'] 	= $this->Model_jenis_pelanggan->view_jenis_pelanggan();
			$data['pelanggan'] 			= $this->Model_pelanggan->get_pelanggan()->row_array();
			$this->load->view('backend/apotek/pelanggan/edit',$data);

		}
	}

	public function detail(){

		$data['detail'] 		= $this->Model_pelanggan->detail_pelanggan()->row_array();
		$this->template->load('template/backend','backend/apotek/pelanggan/detail',$data);

	}

}