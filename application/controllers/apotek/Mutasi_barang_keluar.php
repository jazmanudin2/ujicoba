<?php
class Mutasi_barang_keluar extends CI_Controller {

	function __construct(){

		parent:: __construct();
		$this->load->Model(array(
			'Model_m_barang_keluar',
			'Model_pelanggan',
		));
		ceklogin();
		
	}

	function index($rowno=0){

		// Search text
		$id_mutasi_gudang 	= "";
		$pencarian  		= "";

		if($this->input->post('submit') != NULL ){
			$pencarian 			= $this->input->post('pencarian');
			$id_mutasi_gudang 	= $this->input->post('id_mutasi_gudang');
			
			$data 	= array(

				'pencarian'			=> $pencarian,
				'id_mutasi_gudang'	=> $id_mutasi_gudang,
				
			);
			$this->session->set_userdata($data);

		}else{

			if($this->session->userdata('pencarian') != NULL){
				$pencarian 	= $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_mutasi_gudang') != NULL){
				$id_mutasi_gudang = $this->session->userdata('id_mutasi_gudang');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_m_barang_keluar->getRecordCount($id_mutasi_gudang,$pencarian);

	    // Get records
		$users_record = $this->Model_m_barang_keluar->getData($rowno,$rowperpage,$id_mutasi_gudang,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'apotek/mutasi_barang_keluar/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
		$this->pagination->initialize($config);
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_mutasi_gudang'] 	= $id_mutasi_gudang;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/mutasi_gudang/barang_keluar/view',$data);
	}

	function input(){

		if(isset($_POST['submit'])){

			$this->Model_m_barang_keluar->insert();

		}else{
			$data['kodeunik'] 		= $this->Model_m_barang_keluar->code_otomatis();
			$this->template->load('template/backend','backend/apotek/mutasi_gudang/barang_keluar/input',$data);

		}
	}

	function cekbarang(){

		$id_dtlbarang_temp 		= $this->input->post('id_dtlbarang_temp');
		$id_user 		= $this->session->userdata('id_admin');
		$cek 			= $this->db->query("SELECT * FROM tbl_detailmutasi_gudang_temp WHERE id_dtlbarang_temp='$id_dtlbarang_temp' AND id_admin = '$id_user' AND jenis_mutasi = 'Barang Keluar'")->num_rows();
		echo $cek;

	}
	
	function cektemp(){

		$id_user 	= $this->session->userdata('id_admin');
		$cek 		= $this->db->get_where('tbl_detailmutasi_gudang_temp',array('id_admin'=>$id_user,'jenis_mutasi' =>'Barang Keluar'))->num_rows();
		echo $cek;
		
	}

	function hapus_tmp(){

		$id_dtlbarang_temp	= $this->input->post('id_dtlbarang_temp');
		$id_admin			= $this->session->userdata('id_admin');
		$data 				= $this->db->delete('tbl_detailmutasi_gudang_temp',array('id_dtlbarang_temp' => $id_dtlbarang_temp,'id_admin' => $id_admin));
		echo json_encode($data);
		
	}

	function sum_temp(){

		$data['sum_tmp'] = $this->Model_m_barang_keluar->sum_temp()->row_array();
		$this->load->view('backend/apotek/mutasi_gudang/barang_keluar/sum_temp',$data);

	}

	function input_tmp(){
		
		$this->Model_m_barang_keluar->input_tmp();
		
	}

	function view_temp_barang_obat(){

		$data['listmpobat'] = $this->Model_m_barang_keluar->view_temp_barang_obat();
		$this->load->view('backend/apotek/mutasi_gudang/barang_keluar/data_temp_barang_obat',$data);
		
	}

	function view_temp_barang_umum(){

		$data['listmpumum'] = $this->Model_m_barang_keluar->view_temp_barang_umum();
		$this->load->view('backend/apotek/mutasi_gudang/barang_keluar/data_temp_barang_umum',$data);
		
	}

	function view_temp_barang_alkes(){

		$data['listmpalkes'] = $this->Model_m_barang_keluar->view_temp_barang_alkes();
		$this->load->view('backend/apotek/mutasi_gudang/barang_keluar/data_temp_barang_alkes',$data);
		
	}

	function view_mutasi_barang_obat(){

		$data['barang_obat'] 		= $this->Model_m_barang_keluar->view_mutasi_barang_obat();
		$this->load->view('backend/apotek/mutasi_gudang/barang_keluar/data_barang_obat',$data);

	}

	function view_mutasi_barang_umum(){

		$data['barang_obat'] 		= $this->Model_m_barang_keluar->view_mutasi_barang_umum();
		$this->load->view('backend/apotek/mutasi_gudang/barang_keluar/data_barang_umum',$data);

	}

	function view_mutasi_barang_alkes(){

		$data['barang_alkes'] 		= $this->Model_m_barang_keluar->view_mutasi_barang_alkes();
		$this->load->view('backend/apotek/mutasi_gudang/barang_keluar/data_barang_alkes',$data);

	}

	
	function detail(){

		$data['dtl'] 	= $this->Model_m_barang_keluar->detail()->row_array();
		$data['dtls'] 	= $this->Model_m_barang_keluar->details();
		$this->template->load('template/backend','backend/apotek/mutasi_gudang/barang_keluar/detail',$data);

	}

}