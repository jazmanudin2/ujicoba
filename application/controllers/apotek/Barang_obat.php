<?php
class Barang_obat extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array(
			'Model_barang_obat',
			'Model_merk_dag',
			'Model_jenis_obat',
			'Model_gol_obat',
			'Model_kandungan_obat',
		));
		$this->load->library('upload');
		ceklogin();

	}
	

	function index($rowno=0){
		// Search text
		$id_barang_m 			= "";
		$id_kandungan_obat 		= "";
		$id_gol_obat 			= "";
		$id_barang  			= "";
		$merk_dag  				= "";

		if($this->input->post('submit') != NULL ){

			$id_barang 			= $this->input->post('id_barang');
			$id_kandungan_obat	= $this->input->post('id_kandungan_obat');
			$id_gol_obat 		= $this->input->post('id_gol_obat');
			$id_barang_m 		= $this->input->post('id_barang_m');
			$merk_dag 			= $this->input->post('merk_dag');
			
			$data 	= array(

				'id_barang'			=> $id_barang,
				'id_kandungan_obat'	=> $id_kandungan_obat,
				'id_gol_obat'		=> $id_gol_obat,
				'id_barang_m'		=> $id_barang_m,
				'merk_dag'			=> $merk_dag,
				
			);

			$this->session->set_userdata($data);
		}else{

			if($this->session->userdata('id_gol_obat') != NULL){
				$id_gol_obat = $this->session->userdata('id_gol_obat');
			}

			if($this->session->userdata('id_kandungan_obat') != NULL){
				$id_kandungan_obat = $this->session->userdata('id_kandungan_obat');
			}

			if($this->session->userdata('id_barang') != NULL){
				$id_barang = $this->session->userdata('id_barang');
			}

			if($this->session->userdata('merk_dag') != NULL){
				$merk_dag = $this->session->userdata('merk_dag');
			}

			if($this->session->userdata('id_barang_m') != NULL){
				$id_barang_m = $this->session->userdata('id_barang_m');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_barang_obat->getRecordCount($id_barang_m,$id_barang,$id_kandungan_obat,$id_gol_obat,$merk_dag);

	    // Get records
		$users_record = $this->Model_barang_obat->getData($rowno,$rowperpage,$id_barang_m,$id_barang,$id_kandungan_obat,$id_gol_obat,$merk_dag);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/barang_obat/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_barang_m'] 		= $id_barang_m;
		$data['merk_dag']			= $merk_dag;	
		$data['id_barang']			= $id_barang;	
		$data['id_kandungan_obat'] 	= $id_kandungan_obat;	
		$data['id_gol_obat']		= $id_gol_obat;	
		$data['gol_obat']		    = $this->Model_gol_obat->view_gol_obat();

	    // Load view
		$this->template->load('template/backend','backend/apotek/barang_obat/view',$data);
	}

	function get_kandungan_obat2(){
		
		
		$id_gol_obat 	 = $this->input->post('id_gol_obat');
		$listkandungan 	 = $this->Model_barang_obat->get_kandungan_obat($id_gol_obat);
		echo "<option value=''>-- Semua Data -- </option>";
		foreach($listkandungan->result() as $k){
			
			echo "<option value='$k->id_kandungan_obat'>$k->nama_kandungan_obat</option>";
		}
	}

	function get_kandungan_obat(){
		
		
		$id_gol_obat 	 = $this->input->post('id_gol_obat');
		$listkandungan 	 = $this->Model_barang_obat->get_kandungan_obat($id_gol_obat);
		echo "<option value=''>-- Pilih Kandungan -- </option>";
		foreach($listkandungan->result() as $k){
			
			echo "<option value='$k->id_kandungan_obat'>$k->nama_kandungan_obat</option>";
		}
	}


	function delete_checkbox(){

		$id_barang_m = $this->input->post('id_barang_m');

		$this->db->where_in('id_barang_m', explode(",", $id_barang_m));
		$this->db->delete('tbl_barang_obat');
		echo json_encode();

	}

	function hapus(){

		$this->Model_barang_obat->delete();
		redirect('apotek/barang_obat');

	}

	function input(){

		if(isset($_POST['submit'])){
			//Insert Data
			$this->Model_barang_obat->insert();

		}else{

			$data['merk_dag'] 		= $this->Model_gol_obat->listmerkdag();
			$data['supplier'] 		= $this->Model_barang_obat->get_supplier();
			$data['kodeunik'] 		= $this->Model_barang_obat->code_otomatis();
			$data['gol_obat']		= $this->Model_gol_obat->view_gol_obat();
			$data['jenis_obat'] 	= $this->Model_jenis_obat->view_jenis_obat();
			$data['kandungan_obat']	= $this->Model_kandungan_obat->view_kandungan_obat();
			$this->load->view('backend/apotek/barang_obat/input',$data);

		}
	}

	function jenis_harga(){

		$data['barang']				= $this->Model_barang_obat->jenis_harga()->row_array();
		$data['harga']				= $this->Model_barang_obat->jenis_harga();
		$this->load->view('backend/apotek/barang_obat/jenis_harga',$data);

	}

	function edit(){

		if(isset($_POST['submit'])){

			$this->Model_barang_obat->update();

		}else{

			$data['merk_dag'] 		= $this->Model_gol_obat->listmerkdag();
			$data['gol_obat']		= $this->Model_gol_obat->view_gol_obat();
			$data['jenis_obat'] 	= $this->Model_jenis_obat->view_jenis_obat();
			$data['barang_obat'] 	= $this->Model_barang_obat->get_barang()->row_array();
			$data['kandungan_obat']	= $this->Model_kandungan_obat->view_kandungan_obat();
			$this->load->view('backend/apotek/barang_obat/edit',$data);

		}
	}


	function barcode_obat(){

		$id_barang_m		= $this->uri->segment(4);

		$this->load->library('zend');
		$this->zend->load('Zend/Barcode'); 
		$barcode			= $this->uri->segment(4);
		$imageResource 		= Zend_Barcode::factory('code128', 'image', array('text'=>$barcode), array())->draw();
		$imageName 			= $barcode.'.jpg';
		$imagePath 			= 'assets/backend/apotek/barcode/barang_obat/';
		imagejpeg($imageResource, $imagePath.$imageName); 
		$pathBarcode	 	= $imageName; 

		$data = array(

			'barcode'		=> $pathBarcode,

		);

		$this->db->where('id_barang_m',$id_barang_m);
		$this->db->update('tbl_barang_obat',$data);

		redirect('apotek/barang_obat');

	}

	function detail(){

		$data['detail'] 		= $this->Model_barang_obat->detail_barang()->row_array();
		$data['details'] 		= $this->Model_barang_obat->detail_tbl_detailbarang();
		$data['harga'] 			= $this->Model_barang_obat->detail_harga();
		$data['jenis_harga'] 	= $this->Model_barang_obat->get_jenis_harga();
		$this->template->load('template/backend','backend/apotek/barang_obat/detail',$data);

	}

	function edit_harga(){

		if(isset($_POST['submit'])){

			$this->Model_barang_obat->update_harga();

		}else{
			$data['harga']		= $this->Model_barang_obat->get_jenis_harga()->row_array();
			$data['keterangan']	= $this->Model_barang_obat->get_keterangan_harga();
			$this->load->view('backend/apotek/barang_obat/edit_harga',$data);

		}
	}

	function form(){

		$this->template->load('template/backend','backend/apotek/barang_obat/import');

	}


	function import(){

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';

		$csvreader = PHPExcel_IOFactory::createReader('CSV');
		$loadcsv = $csvreader->load('assets/backend/apotek/csv/barang_obat/'.$this->filename.'.csv'); 
		$sheet = $loadcsv->getActiveSheet()->getRowIterator();

		$data = array();

		$numrow = 1;
		foreach($sheet as $row){
			if($numrow > 1){
				$cellIterator = $row->getCellIterator();
				$cellIterator->setIterateOnlyExistingCells(false); 
				$get = array(); 
				foreach ($cellIterator as $cell) {
					array_push($get, $cell->getValue()); 
				}

				$id_barang_m 		= $get[0];
				$id_barang 			= $get[1];
				$id_merk_dag 		= $get[2];
				$id_gol_obat 		= $get[3];
				$id_kandungan_obat 	= $get[4];
				$dosis 				= $get[5]; 
				$sediaan 			= $get[6]; 
				$id_jenis_obat 		= $get[7]; 
				$min_stok 			= $get[8]; 
				$keterangan			= $get[9]; 
				$created_at 		= $get[10]; 
				$updated_at 		= $get[11]; 
				$id_admin 			= $get[12]; 
				$barcode 			= $get[13]; 

				array_push($data, array(

					'id_barang_m'		=> $id_barang_m, 
					'id_barang'			=> $id_barang, 
					'id_merk_dag'		=> $id_merk_dag, 
					'id_gol_obat'		=> $id_gol_obat, 
					'id_kandungan_obat'	=> $id_kandungan_obat, 
					'dosis'				=> $dosis,
					'sediaan'			=> $sediaan,
					'id_jenis_obat'		=> $id_jenis_obat,
					'min_stok'			=> $min_stok,
					'keterangan'		=> $keterangan,
					'created_at'		=> $created_at,
					'updated_at'		=> $updated_at,
					'id_admin'			=> $id_admin,
					'barcode'			=> $barcode,

				));
			}

			$numrow++; 
		}

		$this->Model_barang_obat->insert_multiple($data);

		redirect('apotek/barang_obat');
	}

}