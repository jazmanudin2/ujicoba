<?php
class Karyawan extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array(
			'Model_karyawan',
			'Model_jabatan',
			'Model_level',
			'Model_provinsi',
			'Model_apotek'
		));
		ceklogin();

	}

	function index($rowno=0){
		// Search text
		$nik 			= "";
		$pencarian  	= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 		= $this->input->post('pencarian');
			$nik 			= $this->input->post('nik');
			
			$data 	= array(

				'pencarian'		=> $pencarian,
				'nik'			=> $nik,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('nik') != NULL){
				$nik = $this->session->userdata('nik');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_karyawan->getRecordCount($nik,$pencarian);

	    // Get records
		$users_record = $this->Model_karyawan->getData($rowno,$rowperpage,$nik,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/karyawan/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['nik'] 				= $nik;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/karyawan/view',$data);
	}

	function get_kota(){
		
		
		$id_provinsi = $this->input->post('id_provinsi');
		$listkota = $this->Model_provinsi->listkota($id_provinsi);
		echo "<option value=''>-- Pilih Kabupaten / Kota -- </option>";
		foreach($listkota->result() as $k){
			
			echo "<option value='$k->id_kota'>$k->nama_kota</option>";
		}
	}
	
	
	function get_kec(){
		
		
		$id_kota = $this->input->post('id_kota');
		$listkec = $this->Model_provinsi->listkec($id_kota);
		echo "<option value=''>-- Pilih Kecamatan -- </option>";
		foreach($listkec->result() as $k){
			
			echo "<option value='$k->id_kecamatan'>$k->nama_kecamatan</option>";
		}
	}

	
	function get_desa(){
		
		$id_kecamatan = $this->input->post('id_kecamatan');
		$listdesa = $this->Model_provinsi->listdesa($id_kecamatan);
		echo "<option value=''>-- Pilih Kelurahan -- </option>";
		foreach($listdesa->result() as $k){
			
			echo "<option value='$k->id_desa'>$k->nama_desa</option>";
		}
	}

	public function delete_checkbox(){

		$nik = $this->input->post('nik');

		$this->db->where_in('nik', explode(",", $nik));
		$this->db->delete('tbl_karyawan');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_karyawan->delete();
		redirect('apotek/karyawan');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data
			$this->Model_karyawan->insert();

		}else{

			$data['listprovinsi'] 	= $this->Model_provinsi->listprovinsi();
			$data['level']		 	= $this->Model_level->view_level();
			$data['jabatan']	 	= $this->Model_jabatan->view_jabatan();
			$data['apotek']	 		= $this->Model_apotek->view_apotek();
			$this->load->view('backend/apotek/karyawan/input',$data);

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_karyawan->update();

		}else{

			$data['listprovinsi'] 	= $this->Model_provinsi->listprovinsi();
			$data['jabatan']	 	= $this->Model_jabatan->view_jabatan();
			$data['apotek']	 		= $this->Model_apotek->view_apotek();
			$data['level']		 	= $this->Model_level->view_level();
			$data['karyawan'] 		= $this->Model_karyawan->get_karyawan()->row_array();
			$this->load->view('backend/apotek/karyawan/edit',$data);

		}
	}

	function upload_image(){

		$config['upload_path']          = './assets/backend/apotek/img/foto_karyawan/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['max_size']             = 100;
		$config['max_width']            = 1024;
		$config['max_height']           = 768;

		$this->load->library('upload', $config);

		$nik 	= $this->input->post('nik');
		$foto 	= $this->upload->data();
		if ($this->upload->do_upload('berkas')){

			$hasil=$this->db->query("UPDATE tbl_karyawan SET foto='$foto' WHERE  nik='$nik' ");
			return $hasil;
		}
	}

	public function detail(){

		$data['karyawan'] 		= $this->Model_karyawan->detail_karyawan()->row_array();
		$this->template->load('template/backend','backend/apotek/karyawan/detail',$data);
	}

}