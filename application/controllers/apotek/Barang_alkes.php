<?php
class Barang_alkes extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array(
			'Model_barang_alkes',
		));
		$this->load->library('upload');
		ceklogin();

	}

	function index($rowno=0){
		// Search text
		$id_barang_m 	= "";
		$pencarian  	= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 		= $this->input->post('pencarian');
			$id_barang_m 	= $this->input->post('id_barang_m');
			
			$data 	= array(

				'pencarian'		=> $pencarian,
				'id_barang_m'	=> $id_barang_m,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_barang_m') != NULL){
				$id_barang_m = $this->session->userdata('id_barang_m');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_barang_alkes->getRecordCount($id_barang_m,$pencarian);

	    // Get records
		$users_record = $this->Model_barang_alkes->getData($rowno,$rowperpage,$id_barang_m,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'apotek/barang_alkes/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_barang_m'] 		= $id_barang_m;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/barang_alkes/view',$data);
	}
	

	public function delete_checkbox(){

		$id_barang_m = $this->input->post('id_barang_m');

		$this->db->where_in('id_barang_m', explode(",", $id_barang_m));
		$this->db->delete('tbl_barang_alkes');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_barang_alkes->delete();
		redirect('apotek/barang_alkes');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data

			$this->Model_barang_alkes->insert();
			redirect('backend/apotek/barang_alkes');
		}else{

			$data['kodeunik'] 		= $this->Model_barang_alkes->code_otomatis();
			$this->load->view('backend/apotek/barang_alkes/input',$data);

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_barang_alkes->update();

		}else{

			$data['barang_alkes'] 		= $this->Model_barang_alkes->get_barang_alkes()->row_array();
			$this->load->view('backend/apotek/barang_alkes/edit',$data);

		}
	}

	public function detail(){

		$data['detail'] 		= $this->Model_barang_alkes->detail_barang()->row_array();
		$data['details'] 		= $this->Model_barang_alkes->detail_tbl_detailbarang();
		$this->template->load('template/backend','backend/apotek/barang_alkes/detail',$data);

	}


	function barcode_alkes(){

		$id_barang_m		= $this->uri->segment(4);

		$this->load->library('zend');
		$this->zend->load('Zend/Barcode'); 
		$barcode			= $this->uri->segment(4);
		$imageResource 		= Zend_Barcode::factory('code128', 'image', array('text'=>$barcode), array())->draw();
		$imageName 			= $barcode.'.jpg';
		$imagePath 			= 'assets/backend/apotek/barcode/barang_alkes/';
		imagejpeg($imageResource, $imagePath.$imageName); 
		$pathBarcode	 	= $imageName; 

		$data = array(

			'barcode'		=> $pathBarcode,

		);

		$this->db->where('id_barang_m',$id_barang_m);
		$this->db->update('tbl_barang_alkes',$data);

		redirect('apotek/barang_alkes');

	}


	public function form(){
		
		$this->template->load('template/backend','backend/apotek/barang_alkes/import');
	}
	

	public function import(){

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$csvreader = PHPExcel_IOFactory::createReader('CSV');
		$loadcsv = $csvreader->load('assets/backend/apotek/csv/barang_alkes/'.$this->filename.'.csv'); 
		$sheet = $loadcsv->getActiveSheet()->getRowIterator();
		
		$data = array();
		
		$numrow = 1;
		foreach($sheet as $row){
			if($numrow > 1){
				$cellIterator = $row->getCellIterator();
				$cellIterator->setIterateOnlyExistingCells(false); 
				$get = array(); 
				foreach ($cellIterator as $cell) {
					array_push($get, $cell->getValue()); 
				}

				$id_barang_m 		= $get[0];
				$id_barang 			= $get[1];
				$jenis_barang 		= $get[2];
				$detail_alkes 		= $get[3];
				$min_stok 			= $get[4];
				$id_admin 			= $get[5]; 
				$keterangan			= $get[6]; 
				$created_at 		= $get[7]; 
				$updated_at 		= $get[8]; 
				$barcode 			= $get[9]; 

				array_push($data, array(

					'id_barang_m'		=> $id_barang_m, 
					'id_barang'			=> $id_barang, 
					'jenis_barang'		=> $jenis_barang, 
					'detail_alkes'		=> $detail_alkes, 
					'min_stok'			=> $min_stok,
					'id_admin'			=> $id_admin,
					'keterangan'		=> $keterangan,
					'created_at'		=> $created_at,
					'updated_at'		=> $updated_at,
					'barcode'			=> $barcode,

				));
			}
			
			$numrow++; 
		}

		$this->Model_barang_alkes->insert_multiple($data);
		
		redirect('apotek/barang_alkes');
	}

}