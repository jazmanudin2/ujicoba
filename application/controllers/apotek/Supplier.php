<?php
class Supplier extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_supplier','Model_jenis_supplier','Model_jenis_produk','Model_sales'));
		ceklogin();
		$this->load->library('upload');
	}

	function index($rowno=0){
		// Search text
		$id_supplier 	= "";
		$pencarian  	= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 		= $this->input->post('pencarian');
			$id_supplier 	= $this->input->post('id_supplier');
			
			$data 	= array(

				'pencarian'		=> $pencarian,
				'id_supplier'	=> $id_supplier,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian 	= $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_supplier') != NULL){
				$id_supplier = $this->session->userdata('id_supplier');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_supplier->getRecordCount($id_supplier,$pencarian);

	    // Get records
		$users_record = $this->Model_supplier->getData($rowno,$rowperpage,$id_supplier,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/supplier/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
		$this->pagination->initialize($config);
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_supplier'] 		= $id_supplier;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/supplier/view',$data);
	}

	public function delete_checkbox(){

		$id_supplier = $this->input->post('id_supplier');

		$this->db->where_in('id_supplier', explode(",", $id_supplier));
		$this->db->delete('tbl_supplier');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_supplier->delete();
		redirect('backend/apotek/supplier');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data

			$this->Model_supplier->insert();
			redirect('backend/apotek/supplier');
		}else{

			$data['kodeunik'] 		= $this->Model_supplier->code_otomatis();
			$data['jenis_sup'] 		= $this->Model_jenis_supplier->view_jenis_supplier();
			$data['jenis_produk']	= $this->Model_jenis_produk->view_jenis_produk();
			$data['sales']			= $this->Model_sales->view_sales();
			$this->load->view('backend/apotek/supplier/input',$data);

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_supplier->update();

		}else{

			$data['supplier'] 		= $this->Model_supplier->get_supplier()->row_array();
			$data['jenis_sup'] 		= $this->Model_jenis_supplier->view_jenis_supplier();
			$data['jenis_produk']	= $this->Model_jenis_produk->view_jenis_produk();
			$data['sales']			= $this->Model_sales->view_sales();
			$this->load->view('backend/apotek/supplier/edit',$data);

		}
	}

	public function detail(){

		$data['detail'] 		= $this->Model_supplier->detail_supplier()->row_array();
		$this->template->load('template/backend','backend/apotek/supplier/detail',$data);

	}

}