<?php
class Jenis_harga_pel extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_jenis_harga_pel'));
		// ceklogin();
	}

	function index($rowno=0){
		// Search text
		$id_jenis_harga_pel 		= "";
		$pencarian  				= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 				= $this->input->post('pencarian');
			$id_jenis_harga_pel 	= $this->input->post('id_jenis_harga_pel');
			
			$data 	= array(

				'pencarian'				=> $pencarian,
				'id_jenis_harga_pel'	=> $id_jenis_harga_pel,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_jenis_harga_pel') != NULL){
				$id_jenis_harga_pel = $this->session->userdata('id_jenis_harga_pel');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_jenis_harga_pel->getRecordCount($id_jenis_harga_pel,$pencarian);

	    // Get records
		$users_record = $this->Model_jenis_harga_pel->getData($rowno,$rowperpage,$id_jenis_harga_pel,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/jenis_harga_pel/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_jenis_harga_pel'] = $id_jenis_harga_pel;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/jenis_harga_pel/view',$data);
	}

	public function delete_checkbox(){

		$id_jenis_harga_pel = $this->input->post('id_jenis_harga_pel');
		$this->db->where_in('id_jenis_harga_pel', explode(",", $id_jenis_harga_pel));
		$this->db->delete('tbl_jenis_harga_pel');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_jenis_harga_pel->delete();
		redirect('apotek/jenis_harga_pel');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data

			$this->Model_jenis_harga_pel->insert();
			redirect('apotek/jenis_harga_pel');
		}else{

			$this->load->view('backend/apotek/jenis_harga_pel/input');

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_jenis_harga_pel->update();

		}else{

			$data['jenis_harga_pel'] 	= $this->Model_jenis_harga_pel->get_jenis_harga_pel()->row_array();
			$this->load->view('backend/apotek/jenis_harga_pel/edit',$data);

		}
	}

}