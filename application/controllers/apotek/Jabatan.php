<?php
class Jabatan extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_jabatan'));
		ceklogin();
	}

	function index($rowno=0){
		// Search text
		$id_jabatan 		= "";
		$pencarian  		= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 		= $this->input->post('pencarian');
			$id_jabatan 	= $this->input->post('id_jabatan');
			
			$data 	= array(

				'pencarian'		=> $pencarian,
				'id_jabatan'	=> $id_jabatan,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_jabatan') != NULL){
				$id_jabatan = $this->session->userdata('id_jabatan');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_jabatan->getRecordCount($id_jabatan,$pencarian);

	    // Get records
		$users_record = $this->Model_jabatan->getData($rowno,$rowperpage,$id_jabatan,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/jabatan/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_jabatan'] 		= $id_jabatan;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/jabatan/view',$data);
	}

	public function delete_checkbox(){

		$id_jabatan = $this->input->post('id_jabatan');
		$this->db->where_in('id_jabatan', explode(",", $id_jabatan));
		$this->db->delete('tbl_jabatan');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_jabatan->delete();
		redirect('apotek/jabatan');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data

			$this->Model_jabatan->insert();
		}else{

			$this->load->view('backend/apotek/jabatan/input');

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_jabatan->update();

		}else{

			$data['jabatan'] 	= $this->Model_jabatan->get_jabatan()->row_array();
			$this->load->view('backend/apotek/jabatan/edit',$data);

		}
	}

}