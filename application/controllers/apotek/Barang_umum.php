<?php
class Barang_umum extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array(
			'Model_barang_umum',
		));
		$this->load->library('upload');
		ceklogin();

	}

	function index($rowno=0){
		// Search text
		$id_barang_m 	= "";
		$pencarian  	= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 		= $this->input->post('pencarian');
			$id_barang_m 	= $this->input->post('id_barang_m');
			
			$data 	= array(

				'pencarian'		=> $pencarian,
				'id_barang_m'	=> $id_barang_m,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_barang_m') != NULL){
				$id_barang_m = $this->session->userdata('id_barang_m');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_barang_umum->getRecordCount($id_barang_m,$pencarian);

	    // Get records
		$users_record = $this->Model_barang_umum->getData($rowno,$rowperpage,$id_barang_m,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/barang_umum/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_barang_m'] 		= $id_barang_m;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/barang_umum/view',$data);
	}
	

	public function delete_checkbox(){

		$id_barang_m = $this->input->post('id_barang_m');

		$this->db->where_in('id_barang_m', explode(",", $id_barang_m));
		$this->db->delete('tbl_barang_umum');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_barang_umum->delete();
		redirect('apotek/barang_umum');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data

			$this->Model_barang_umum->insert();
			
		}else{

			$data['kodeunik'] 		= $this->Model_barang_umum->code_otomatis();
			$this->load->view('backend/apotek/barang_umum/input',$data);

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_barang_umum->update();

		}else{

			$data['barang_umum'] 		= $this->Model_barang_umum->get_barang_umum()->row_array();
			$this->load->view('backend/apotek/barang_umum/edit',$data);

		}
	}

	public function detail(){

		$data['detail'] 		= $this->Model_barang_umum->detail_barang()->row_array();
		$data['details'] 		= $this->Model_barang_umum->detail_tbl_detailbarang();
		$this->template->load('template/backend','backend/apotek/barang_umum/detail',$data);

	}

	function barcode_umum(){

		$id_barang_m		= $this->uri->segment(4);

		$this->load->library('zend');
		$this->zend->load('Zend/Barcode'); 
		$barcode			= $this->uri->segment(4);
		$imageResource 		= Zend_Barcode::factory('code128', 'image', array('text'=>$barcode), array())->draw();
		$imageName 			= $barcode.'.jpg';
		$imagePath 			= 'assets/backend/apotek/barcode/barang_umum/';
		imagejpeg($imageResource, $imagePath.$imageName); 
		$pathBarcode	 	= $imageName; 

		$data = array(

			'barcode'		=> $pathBarcode,

		);

		$this->db->where('id_barang_m',$id_barang_m);
		$this->db->update('tbl_barang_umum',$data);

		redirect('apotek/barang_umum');

	}

	public function form(){
		
		$this->template->load('template/backend','backend/apotek/barang_umum/import');
	}
	

	public function import(){

		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$csvreader = PHPExcel_IOFactory::createReader('CSV');
		$loadcsv = $csvreader->load('assets/backend/apotek/csv/barang_umum/'.$this->filename.'.csv'); 
		$sheet = $loadcsv->getActiveSheet()->getRowIterator();
		
		$data = array();
		
		$numrow = 1;
		foreach($sheet as $row){
			if($numrow > 1){
				$cellIterator = $row->getCellIterator();
				$cellIterator->setIterateOnlyExistingCells(false); 
				$get = array(); 
				foreach ($cellIterator as $cell) {
					array_push($get, $cell->getValue()); 
				}

				$id_barang_m 		= $get[0];
				$id_barang 			= $get[1];
				$nama_barang 		= $get[2];
				$min_stok 			= $get[3];
				$gol_barang 		= $get[4];
				$id_admin 			= $get[5]; 
				$keterangan			= $get[6]; 
				$created_at 		= $get[7]; 
				$updated_at 		= $get[8]; 
				$barcode 			= $get[9]; 

				array_push($data, array(

					'id_barang_m'		=> $id_barang_m, 
					'id_barang'			=> $id_barang, 
					'nama_barang'		=> $nama_barang, 
					'min_stok'			=> $min_stok, 
					'gol_barang'		=> $gol_barang,
					'id_admin'			=> $id_admin,
					'keterangan'		=> $keterangan,
					'created_at'		=> $created_at,
					'updated_at'		=> $updated_at,
					'barcode'			=> $barcode,

				));
			}
			
			$numrow++; 
		}

		$this->Model_barang_umum->insert_multiple($data);
		
		redirect('apotek/barang_umum');
	}


}