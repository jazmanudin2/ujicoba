<?php
class Laporan_barang extends CI_Controller {

	function __construct(){

		parent:: __construct();
		$this->load->Model(array(
			'Model_laporan_barang',
			'Model_pelanggan',
		));
		ceklogin();

	}

	function detail_barang_obat(){

		$data['gol_obat']	= $this->Model_laporan_barang->get_pelanggan();
		$this->template->load('template/backend','backend/apotek/barang_obat/laporan/barang_obat',$data);

	}

	function cetak_barang_obat(){

		$gol_obat  		= $this->input->post('gol_obat');

		if(isset($_POST['export'])){
			// Fungsi header dengan mengirimkan raw data excel
			header("Content-type: application/vnd-ms-excel");

			// Mendefinisikan nama file ekspor "hasil-export.xls"
			header("Content-Disposition: attachment; filename=Laporan Penjualan.xls");
		}

		$data['gol_obat']	= $this->Model_laporan_barang->list_barang_obat($gol_obat)->row_array();
		$data['data']		= $this->Model_laporan_barang->list_barang_obat($gol_obat)->result();
		$this->load->view('backend/apotek/barang_obat/laporan/cetak_barang_obat',$data);

	}

}