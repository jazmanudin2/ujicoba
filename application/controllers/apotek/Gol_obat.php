<?php
class Gol_obat extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_gol_obat','Model_merk_dag'));
		// ceklogin();
	}

	function index($rowno=0){
		// Search text
		$id_gol_obat 		= "";
		$pencarian  		= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 		= $this->input->post('pencarian');
			$id_gol_obat 	= $this->input->post('id_gol_obat');
			
			$data 	= array(

				'pencarian'		=> $pencarian,
				'id_gol_obat'	=> $id_gol_obat,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_gol_obat') != NULL){
				$id_gol_obat = $this->session->userdata('id_gol_obat');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_gol_obat->getRecordCount($id_gol_obat,$pencarian);

	    // Get records
		$users_record = $this->Model_gol_obat->getData($rowno,$rowperpage,$id_gol_obat,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'apotek/gol_obat/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_gol_obat'] 		= $id_gol_obat;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/gol_obat/view',$data);
	}

	public function delete_checkbox(){

		$id_gol_obat = $this->input->post('id_gol_obat');
		$this->db->where_in('id_gol_obat', explode(",", $id_gol_obat));
		$this->db->delete('tbl_gol_obat');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_gol_obat->delete();
		redirect('apotek/gol_obat');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data

			$this->Model_gol_obat->insert();
			redirect('apotek/gol_obat');
		}else{

			$data['merk_dag'] 	= $this->Model_merk_dag->view_merk_dag();
			$this->load->view('backend/apotek/gol_obat/input',$data);

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_gol_obat->update();

		}else{

			$data['merk_dag'] 	= $this->Model_merk_dag->view_merk_dag();
			$data['gol_obat'] 	= $this->Model_gol_obat->get_gol_obat()->row_array();
			$this->load->view('backend/apotek/gol_obat/edit',$data);

		}
	}

}