<?php
class Penjualan extends CI_Controller {

	function __construct(){

		parent:: __construct();
		$this->load->Model(array(
			'Model_penjualan',
			'Model_jenis_pelanggan',
			'Model_barang_obat',
		));
		ceklogin();
		
	}

	function get_pelanggan(){
		
		
		$id_jenis_pelanggan 	= $this->input->post('id_jenis_pelanggan');
		$listpelanggan 	 		= $this->Model_penjualan->get_pelanggan($id_jenis_pelanggan);
		echo "<option value=''>-- Pilih Jenis Pelanggan -- </option>";
		foreach($listpelanggan->result() as $k){
			
			echo "<option value='$k->id_pelanggan'>$k->nama_pelanggan</option>";
		}
	}


	function tampil_barang(){

		$id_barang_m	= $this->input->post('id_barang_m');
		$data 			= $this->Model_penjualan->get_barang($id_barang_m);
		echo json_encode($data);

	}

	function view_barang_obat(){

		$data['barang_obat'] 		= $this->Model_penjualan->view_barang_obat();
		$this->load->view('backend/apotek/penjualan/data_barang_obat',$data);

	}

	function view_barang_umum(){

		$data['barang_obat'] 		= $this->Model_penjualan->view_barang_umum();
		$this->load->view('backend/apotek/penjualan/data_barang_umum',$data);

	}

	function view_barang_alkes(){

		$data['barang_alkes'] 		= $this->Model_penjualan->view_barang_alkes();
		$this->load->view('backend/apotek/penjualan/data_barang_alkes',$data);

	}

	function view_jenis_harga(){

		$data['jenis_harga'] 		= $this->Model_penjualan->view_jenis_harga();
		$this->load->view('backend/apotek/penjualan/data_jenis_harga',$data);

	}

	function input_tmp(){
		
		$this->Model_penjualan->input_tmp();
		
	}

	function hapus_tmp(){

		$dtl			= $this->input->post('id_dtlbarang_temp');
		$id_admin		= $this->session->userdata('id_admin');
		$data 			= $this->db->delete('tbl_detailpenjualan_temp',array('id_admin' => $id_admin,'id_dtlbarang_temp' => $dtl));
		echo json_encode($data);
		
	}

	function hapus_histori_penj(){

		$this->Model_penjualan->delete_histori_penj();

	}	

	function hapus_pembayaran(){

		$this->Model_penjualan->delete_bayar();

	}

	function sum_tmp(){

		$data['sum_tmp'] = $this->Model_penjualan->sum_tmp()->row_array();
		$this->load->view('backend/apotek/penjualan/sum_tmp',$data);

	}

	function cektemp(){

		$id_user 	= $this->session->userdata('id_admin');
		$cek 		= $this->db->get_where('tbl_detailpenjualan_temp',array('id_admin'=>$id_user))->num_rows();
		echo $cek;
		
	}

	function view_temp_barangobat(){

		$data['listmpobat'] = $this->Model_penjualan->view_temp_barangobat();
		$this->load->view('backend/apotek/penjualan/data_temp_barangobat',$data);
		
	}

	function view_temp_barangumum(){

		$data['listmpumum'] = $this->Model_penjualan->view_temp_barangumum();
		$this->load->view('backend/apotek/penjualan/data_temp_barangumum',$data);
		
	}

	function view_temp_barangalkes(){

		$data['listmpalkes'] = $this->Model_penjualan->view_temp_barangalkes();
		$this->load->view('backend/apotek/penjualan/data_temp_barangalkes',$data);
		
	}

	function cekbarang(){

		$id_dtlbarang_temp 	= $this->input->post('id_dtlbarang_temp');
		$cek 				= $this->db->query("SELECT * FROM tbl_detailpenjualan_temp WHERE id_dtlbarang_temp = '$id_dtlbarang_temp' ")->num_rows();
		echo $cek;

	}

	function input(){

		if(isset($_POST['submit'])){

			$this->Model_penjualan->insert();

		}else{
			$data['kodeunik'] 		= $this->Model_penjualan->code_otomatis();
			$data['nobukti'] 		= $this->Model_penjualan->nobukti();
			$data['jenis_pelanggan']= $this->Model_jenis_pelanggan->view_jenis_pelanggan();
			$data['jenis_harga'] 	= $this->Model_penjualan->view_jenis_harga();
			$this->template->load('template/backend','backend/apotek/penjualan/input',$data);

		}
	}
	
	function view_tmp_penjualan(){

		$data['listmp'] = $this->Model_penjualan->view_tmp();
		$this->load->view('backend/apotek/penjualan/data_tmp',$data);
		
	}


	function view_piutang($rowno=0){
		// Search text
		$id_pelanggan 	= "";
		$nama_pelanggan  	= "";
		if($this->input->post('submit') != NULL ){
			$nama_pelanggan 	= $this->input->post('nama_pelanggan');
			$id_pelanggan 	= $this->input->post('id_pelanggan');

			$data 	= array(

				'nama_pelanggan'		 => $nama_pelanggan,
				'id_pelanggan'	 	 => $id_pelanggan,

			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('nama_pelanggan') != NULL){
				$nama_pelanggan = $this->session->userdata('nama_pelanggan');
			}

			if($this->session->userdata('id_pelanggan') != NULL){
				$id_pelanggan = $this->session->userdata('id_pelanggan');
			}

		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}

	    // All records count
		$allcount 	  = $this->Model_penjualan->getRecordCount($id_pelanggan,$nama_pelanggan);

	    // Get records
		$users_record = $this->Model_penjualan->getData($rowno,$rowperpage,$id_pelanggan,$nama_pelanggan);

	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/penjualan/view_piutang';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';

	    // Initialize
		$this->pagination->initialize($config);

		$data['pagination'] 	= $this->pagination->create_links();
		$data['data'] 			= $users_record;
		$data['row'] 			= $rowno;
		$data['id_pelanggan'] 	= $id_pelanggan;
		$data['nama_pelanggan']	= $nama_pelanggan;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/penjualan/view_piutang',$data);
	}


	function view_historipiutang(){

		$data['histori'] = $this->Model_penjualan->get_histori_pelanggan()->row_array();
		$data['sum'] 	 = $this->Model_penjualan->sum_hpiutang()->row_array();
		$data['data'] 	 = $this->Model_penjualan->view_hpiutang();
		$this->template->load('template/backend','backend/apotek/penjualan/view_historipiutang',$data);

	}

	function detailpenjualan(){
		if(isset($_POST['submit'])){

			$this->Model_penjualan->insert_bayar();
		}else{
			$data['nobukti'] 	= $this->Model_penjualan->nobukti();
			$data['penjualan'] 	= $this->Model_penjualan->view_penjualan()->row_array();
			$data['sum'] 		= $this->Model_penjualan->sum_dpenjualan()->row_array();
			$data['rbayar'] 	= $this->Model_penjualan->view_rpembayaran();
			$data['hbayar'] 	= $this->Model_penjualan->view_hbayar();
			$data['postingbyr'] = $this->Model_penjualan->view_posting_pembayaran();
			$data['obat'] 		= $this->Model_penjualan->view_dpembelian_barangobat();
			$data['umum'] 		= $this->Model_penjualan->view_dpembelian_barangumum();
			$data['alkes'] 		= $this->Model_penjualan->view_dpembelian_barangalkes();
		}
		$this->template->load('template/backend','backend/apotek/penjualan/view_detailpenjualan',$data);
	}

	

	function edit_histori_piutang(){
		if(isset($_POST['submit'])){

			$this->Model_penjualan->update_histori_piutang();
		}else{
			$data['histori'] 	= $this->Model_penjualan->get_histori_pembayaran()->row_array();
			$this->load->view('backend/apotek/penjualan/edit_histori_piutang',$data);
		}
	}

	

}