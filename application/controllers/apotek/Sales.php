<?php
class Sales extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_sales','Model_jenis_apt','Model_provinsi'));
		ceklogin();
		$this->load->library('upload');
	}

	function index($rowno=0){
		// Search text
		$id_sales 		= "";
		$pencarian  	= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 	= $this->input->post('pencarian');
			$id_sales 	= $this->input->post('id_sales');
			
			$data 	= array(

				'pencarian'	=> $pencarian,
				'id_sales'	=> $id_sales,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_sales') != NULL){
				$id_sales = $this->session->userdata('id_sales');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_sales->getRecordsalesCount($id_sales,$pencarian);

	    // Get records
		$users_record = $this->Model_sales->getDatasales($rowno,$rowperpage,$id_sales,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/sales/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 	= $this->pagination->create_links();
		$data['data'] 			= $users_record;
		$data['row'] 			= $rowno;
		$data['id_sales'] 		= $id_sales;
		$data['pencarian']		= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/sales/view',$data);
	}

	public function delete_checkbox(){

		$id_sales = $this->input->post('id_sales');

		$this->db->where_in('id_sales', explode(",", $id_sales));
		$this->db->delete('tbl_data_apt');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_sales->delete();
		redirect('backend/apotek/sales');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data

			$this->Model_sales->insert();
			//Notifikasi
			echo $this->session->set_flashdata('msg',
				'<div class="alert alert-success">
				<h4>Successfully</h4>
				<p>Data Berhasil Di Simpan</p>
				</div>');

			redirect('backend/apotek/sales');
		}else{

			$data['listprovinsi'] 	= $this->Model_provinsi->listprovinsi();
			$data['kodeunik'] 		= $this->Model_sales->code_otomatis();
			$data['jenis_apt'] 		= $this->Model_jenis_apt->view_jenis_apt();
			$this->load->view('backend/apotek/sales/input',$data);

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_sales->update();

		}else{

			$data['listprovinsi'] 	= $this->Model_provinsi->listprovinsi();
			$data['jenis_apt'] 		= $this->Model_jenis_apt->view_jenis_apt();
			$data['sales'] 		= $this->Model_sales->get_sales()->row_array();
			$this->load->view('backend/apotek/sales/edit',$data);

		}
	}

	public function detail(){

		$data['dtlapt'] 		= $this->Model_sales->detail_sales()->row_array();
		$this->template->load('template/backend','backend/apotek/sales/detail',$data);

	}

}