<?php
class Jenis_pelanggan extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_jenis_pelanggan'));
		// ceklogin();
	}

	function index($rowno=0){
		// Search text
		$id_jenis_pelanggan 		= "";
		$pencarian  				= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 				= $this->input->post('pencarian');
			$id_jenis_pelanggan 	= $this->input->post('id_jenis_pelanggan');
			
			$data 	= array(

				'pencarian'				=> $pencarian,
				'id_jenis_pelanggan'	=> $id_jenis_pelanggan,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_jenis_pelanggan') != NULL){
				$id_jenis_pelanggan = $this->session->userdata('id_jenis_pelanggan');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_jenis_pelanggan->getRecordCount($id_jenis_pelanggan,$pencarian);

	    // Get records
		$users_record = $this->Model_jenis_pelanggan->getData($rowno,$rowperpage,$id_jenis_pelanggan,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/jenis_pelanggan/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_jenis_pelanggan'] = $id_jenis_pelanggan;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/jenis_pelanggan/view',$data);
	}

	public function delete_checkbox(){

		$id_jenis_pelanggan = $this->input->post('id_jenis_pelanggan');
		$this->db->where_in('id_jenis_pelanggan', explode(",", $id_jenis_pelanggan));
		$this->db->delete('tbl_jenis_pelanggan');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_jenis_pelanggan->delete();
		redirect('apotek/jenis_pelanggan');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data

			$this->Model_jenis_pelanggan->insert();
			redirect('apotek/jenis_pelanggan');
		}else{

			$this->load->view('backend/apotek/jenis_pelanggan/input');

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_jenis_pelanggan->update();

		}else{

			$data['jenis_pelanggan'] 	= $this->Model_jenis_pelanggan->get_jenis_pelanggan()->row_array();
			$this->load->view('backend/apotek/jenis_pelanggan/edit',$data);

		}
	}

}