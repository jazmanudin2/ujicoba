<?php
class Keterangan_harga extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_keterangan_harga'));
		// ceklogin();
	}

	function index($rowno=0){
		// Search text
		$id_keterangan_harga 		= "";
		$pencarian  			= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 			= $this->input->post('pencarian');
			$id_keterangan_harga 	= $this->input->post('id_keterangan_harga');
			
			$data 	= array(

				'pencarian'			=> $pencarian,
				'id_keterangan_harga'	=> $id_keterangan_harga,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_keterangan_harga') != NULL){
				$id_keterangan_harga = $this->session->userdata('id_keterangan_harga');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_keterangan_harga->getRecordJenisSupCount($id_keterangan_harga,$pencarian);

	    // Get records
		$users_record = $this->Model_keterangan_harga->getDataJenisSup($rowno,$rowperpage,$id_keterangan_harga,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/keterangan_harga/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_keterangan_harga'] 	= $id_keterangan_harga;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/keterangan_harga/view',$data);
	}

	public function delete_checkbox(){

		$id_keterangan_harga = $this->input->post('id_keterangan_harga');
		$this->db->where_in('id_keterangan_harga', explode(",", $id_keterangan_harga));
		$this->db->delete('tbl_keterangan_harga');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_keterangan_harga->delete();
		redirect('apotek/keterangan_harga');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data

			$this->Model_keterangan_harga->insert();
			redirect('apotek/keterangan_harga');
		}else{

			$this->load->view('backend/apotek/keterangan_harga/input');

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_keterangan_harga->update();

		}else{

			$data['keterangan_harga'] 	= $this->Model_keterangan_harga->get_keterangan_harga()->row_array();
			$this->load->view('backend/apotek/keterangan_harga/edit',$data);

		}
	}

}