<?php
class Laporan_penjualan extends CI_Controller {

	function __construct(){

		parent:: __construct();
		$this->load->Model(array(
			'Model_laporan_penjualan',
			'Model_pelanggan',
		));
		ceklogin();

	}

	function detail_penjualan(){

		$data['pelanggan']	= $this->Model_laporan_penjualan->get_pelanggan();
		$this->template->load('template/backend','backend/apotek/penjualan/laporan/detail_penjualan',$data);

	}

	function cetak_detail_penjualan(){

		$pelanggan  		= $this->input->post('pelanggan');
		$dari 				= $this->input->post('dari');
		$sampai     		= $this->input->post('sampai');
		$jt					= $this->input->post('jenistransaksi');
		$data['dari']		= $dari;
		$data['sampai']		= $sampai;

		if(isset($_POST['export'])){
			// Fungsi header dengan mengirimkan raw data excel
			header("Content-type: application/vnd-ms-excel");

			// Mendefinisikan nama file ekspor "hasil-export.xls"
			header("Content-Disposition: attachment; filename=Laporan Penjualan.xls");
		}

		$data['pelanggan']	= $this->Model_laporan_penjualan->get_pelanggan($pelanggan)->row_array();
		$data['penjualan']	= $this->Model_laporan_penjualan->list_detail_penjualan($dari,$sampai,$pelanggan,$jt)->result();
		$this->load->view('backend/apotek/penjualan/laporan//cetak_detail_penjualan',$data);

	}

	function pendapatan_penjualan(){

		$this->template->load('template/backend','backend/apotek/penjualan/laporan/pendapatan_penjualan');

	}

	function cetak_pendapatan_penjualan(){

		$pelanggan  		= $this->input->post('pelanggan');
		$dari 				= $this->input->post('dari');
		$sampai     		= $this->input->post('sampai');
		$jt					= $this->input->post('jenistransaksi');
		$data['dari']		= $dari;
		$data['sampai']		= $sampai;

		if(isset($_POST['export'])){
			// Fungsi header dengan mengirimkan raw data excel
			header("Content-type: application/vnd-ms-excel");

			// Mendefinisikan nama file ekspor "hasil-export.xls"
			header("Content-Disposition: attachment; filename=Laporan Penjualan.xls");
		}

		$data['pelanggan']	= $this->Model_laporan_penjualan->get_pelanggan($pelanggan)->row_array();
		$data['penjualan']	= $this->Model_laporan_penjualan->list_pendapatan_penjualan($dari,$sampai)->result();
		$this->load->view('backend/apotek/penjualan/laporan//cetak_pendapatan_penjualan',$data);

	}

}