<?php
class Jenis_produk extends CI_Controller {

	private $filename = "import_data"; 

	function __construct(){

		parent:: __construct();
		$this->load->Model(array('Model_jenis_produk'));
		ceklogin();
	}

	function index($rowno=0){
		// Search text
		$id_jenis_produk 		= "";
		$pencarian  			= "";
		if($this->input->post('submit') != NULL ){
			$pencarian 			= $this->input->post('pencarian');
			$id_jenis_produk 	= $this->input->post('id_jenis_produk');
			
			$data 	= array(

				'pencarian'			=> $pencarian,
				'id_jenis_produk'	=> $id_jenis_produk,
				
			);
			$this->session->set_userdata($data);
		}else{
			if($this->session->userdata('pencarian') != NULL){
				$pencarian = $this->session->userdata('pencarian');
			}

			if($this->session->userdata('id_jenis_produk') != NULL){
				$id_jenis_produk = $this->session->userdata('id_jenis_produk');
			}
		}

	    // Row per page
		$rowperpage = 10;

	    // Row position
		if($rowno != 0){
			$rowno = ($rowno-1) * $rowperpage;
		}
		
	    // All records count
		$allcount 	  = $this->Model_jenis_produk->getRecordJenisSupCount($id_jenis_produk,$pencarian);

	    // Get records
		$users_record = $this->Model_jenis_produk->getDataJenisSup($rowno,$rowperpage,$id_jenis_produk,$pencarian);
		
	    // Pagination Configuration
		$config['base_url'] 		= base_url().'backend/apotek/jenis_produk/index';
		$config['use_page_numbers'] = TRUE;
		$config['total_rows'] 		= $allcount;
		$config['per_page'] 		= $rowperpage;

		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next';
		$config['prev_link']        = 'Prev';
		$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
		$config['full_tag_close']   = '</ul></nav></div>';
		$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
		$config['num_tag_close']    = '</span></li>';
		$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
		$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
		$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
		$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['prev_tagl_close']  = '</span>Next</li>';
		$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
		$config['first_tagl_close'] = '</span></li>';
		$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
		$config['last_tagl_close']  = '</span></li>';
	    // Initialize
		$this->pagination->initialize($config);
		
		$data['pagination'] 		= $this->pagination->create_links();
		$data['data'] 				= $users_record;
		$data['row'] 				= $rowno;
		$data['id_jenis_produk'] 	= $id_jenis_produk;
		$data['pencarian']			= $pencarian;	

	    // Load view
		$this->template->load('template/backend','backend/apotek/jenis_produk/view',$data);
	}

	public function delete_checkbox(){

		$id_jenis_produk = $this->input->post('id_jenis_produk');
		$this->db->where_in('id_jenis_produk', explode(",", $id_jenis_produk));
		$this->db->delete('tbl_jenis_produk');
		echo json_encode();

	}

	public function hapus(){

		$this->Model_jenis_produk->delete();
		redirect('apotek/jenis_produk');

	}

	public function input(){

		if(isset($_POST['submit'])){
			//Insert Data

			$this->Model_jenis_produk->insert();
			redirect('apotek/jenis_produk');
		}else{

			$this->load->view('backend/apotek/jenis_produk/input');

		}
	}

	public function edit(){

		if(isset($_POST['submit'])){

			$this->Model_jenis_produk->update();

		}else{

			$data['jenis_produk'] 	= $this->Model_jenis_produk->get_jenis_produk()->row_array();
			$this->load->view('backend/apotek/jenis_produk/edit',$data);

		}
	}

}