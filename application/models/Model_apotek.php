<?php
class Model_apotek extends CI_Model{


	function view_apotek(){

		$id_admin 	= $this->session->userdata('id_admin');
		$query 		= "SELECT * FROM tbl_data_apt ORDER BY nama_apt ASC";
		return $this->db->query($query);

	}

	public function getData($rowno,$rowperpage,$id_apotek="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_data_apt');
		$this->db->join('tbl_jenis_apt','tbl_data_apt.id_jenis_apt = tbl_jenis_apt.id_jenis_apt');
		$this->db->join('tbl_provinsi','tbl_data_apt.id_provinsi = tbl_provinsi.id_provinsi');
		$this->db->join('tbl_kota','tbl_data_apt.id_kota = tbl_kota.id_kota');
		$this->db->join('tbl_kecamatan','tbl_data_apt.id_kecamatan = tbl_kecamatan.id_kecamatan');
		$this->db->join('tbl_desa','tbl_data_apt.id_desa = tbl_desa.id_desa');

		if($id_apotek != ''){

			$this->db->where('id_apotek', $id_apotek);

		}

		if($pencarian != ''){

			$this->db->like('nama_apt', $pencarian);
			$this->db->or_like('tbl_data_apt.id_jenis_apt', $pencarian);
			$this->db->or_like('tbl_jenis_apt.nama_jenis_apt', $pencarian);
			$this->db->or_like('tbl_data_apt.id_apotek', $pencarian);
			$this->db->or_like('tbl_data_apt.owner_apt', $pencarian);
			$this->db->or_like('tbl_data_apt.penanggungjwb', $pencarian);
			$this->db->or_like('tbl_data_apt.telp_penanggungjwb', $pencarian);
			$this->db->or_like('tbl_data_apt.alamat', $pencarian);
			$this->db->or_like('nama_provinsi', $pencarian);
			$this->db->or_like('nama_kota', $pencarian);
			$this->db->or_like('nama_kecamatan', $pencarian);
			$this->db->or_like('nama_desa', $pencarian);
			$this->db->or_like('tbl_data_apt.telp1', $pencarian);
			$this->db->or_like('tbl_data_apt.telp2', $pencarian);
			$this->db->or_like('tbl_data_apt.email', $pencarian);
			$this->db->or_like('tbl_data_apt.created_at', $pencarian);
			$this->db->or_like('tbl_data_apt.updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_apotek = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_data_apt');
		$this->db->join('tbl_jenis_apt','tbl_data_apt.id_jenis_apt = tbl_jenis_apt.id_jenis_apt');
		$this->db->join('tbl_provinsi','tbl_data_apt.id_provinsi = tbl_provinsi.id_provinsi');
		$this->db->join('tbl_kota','tbl_data_apt.id_kota = tbl_kota.id_kota');
		$this->db->join('tbl_kecamatan','tbl_data_apt.id_kecamatan = tbl_kecamatan.id_kecamatan');
		$this->db->join('tbl_desa','tbl_data_apt.id_desa = tbl_desa.id_desa');
		$this->db->order_by('id_apotek','desc');

		if($id_apotek != ''){
			$this->db->where('id_apotek', $id_apotek);
		}

		if($pencarian != ''){

			$this->db->like('nama_apt', $pencarian);
			$this->db->or_like('tbl_data_apt.id_jenis_apt', $pencarian);
			$this->db->or_like('tbl_data_apt.id_apotek', $pencarian);
			$this->db->or_like('tbl_data_apt.owner_apt', $pencarian);
			$this->db->or_like('tbl_data_apt.penanggungjwb', $pencarian);
			$this->db->or_like('tbl_data_apt.telp_penanggungjwb', $pencarian);
			$this->db->or_like('tbl_data_apt.alamat', $pencarian);
			$this->db->or_like('nama_provinsi', $pencarian);
			$this->db->or_like('nama_kota', $pencarian);
			$this->db->or_like('nama_kecamatan', $pencarian);
			$this->db->or_like('nama_desa', $pencarian);
			$this->db->or_like('tbl_data_apt.telp1', $pencarian);
			$this->db->or_like('tbl_data_apt.telp2', $pencarian);
			$this->db->or_like('tbl_data_apt.email', $pencarian);
			$this->db->or_like('tbl_data_apt.created_at', $pencarian);
			$this->db->or_like('tbl_data_apt.updated_at', $pencarian);

		}

		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}

	function insert(){

		$data = array(

			'id_apotek' 			=> $this->input->post('id_apotek'),
			'id_jenis_apt' 			=> $this->input->post('id_jenis_apt'),
			'nama_apt' 				=> $this->input->post('nama_apt'),
			'owner_apt' 			=> $this->input->post('owner_apt'),
			'penanggungjwb' 		=> $this->input->post('penanggungjwb'),
			'telp_penanggungjwb' 	=> $this->input->post('telp_penanggungjwb'),
			'alamat' 				=> $this->input->post('alamat'),
			'id_provinsi' 			=> $this->input->post('provinsi'),
			'id_kota'		 		=> $this->input->post('kota'),
			'id_kecamatan' 			=> $this->input->post('kecamatan'),
			'id_desa' 				=> $this->input->post('desa'),
			'telp1'		 			=> $this->input->post('telp1'),
			'telp2' 				=> $this->input->post('telp2'),
			'email' 				=> $this->input->post('email'),
			'id_admin'				=> $this->session->userdata('id_admin'),
			'created_at' 			=> date("Y-m-d H:i:s"),

		);

		$this->db->insert('tbl_data_apt',$data);
		redirect('medliz/apotek');
	}

	function delete(){

		$id_apotek	= $this->uri->segment(4);
		$this->db->delete('tbl_data_apt',array('id_apotek' => $id_apotek));

	}

	function code_otomatis(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_data_apt.id_apotek,6) as kode ',false);
		$this->db->where('mid(id_apotek,3,2)',$tahunini);
		$this->db->order_by('id_apotek', 'desc');
		$this->db->limit(6);
		$query 		= $this->db->get('tbl_data_apt');
		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}
		$kodemax 	= str_pad($kode,6,"0",STR_PAD_LEFT);
		$kodejadi  	= "AP".date('y').$kodemax;
		return $kodejadi;

	}


	function get_apotek(){

		$id_apotek 	= $this->input->post('id_apotek');
		$this->db->select('*');
		$this->db->from('tbl_data_apt');
		$this->db->join('tbl_jenis_apt','tbl_data_apt.id_jenis_apt = tbl_jenis_apt.id_jenis_apt');
		$this->db->join('tbl_provinsi','tbl_data_apt.id_provinsi = tbl_provinsi.id_provinsi');
		$this->db->join('tbl_kota','tbl_data_apt.id_kota = tbl_kota.id_kota');
		$this->db->join('tbl_kecamatan','tbl_data_apt.id_kecamatan = tbl_kecamatan.id_kecamatan');
		$this->db->join('tbl_desa','tbl_data_apt.id_desa = tbl_desa.id_desa');
		$this->db->where('id_apotek',$id_apotek);
		$query  = $this->db->get();
		return $query;
	}

	function detail_apotek(){

		$id_apotek	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('tbl_data_apt');
		$this->db->join('tbl_jenis_apt','tbl_data_apt.id_jenis_apt = tbl_jenis_apt.id_jenis_apt');
		$this->db->join('tbl_provinsi','tbl_data_apt.id_provinsi = tbl_provinsi.id_provinsi');
		$this->db->join('tbl_kota','tbl_data_apt.id_kota = tbl_kota.id_kota');
		$this->db->join('tbl_kecamatan','tbl_data_apt.id_kecamatan = tbl_kecamatan.id_kecamatan');
		$this->db->join('tbl_desa','tbl_data_apt.id_desa = tbl_desa.id_desa');
		$this->db->where('id_apotek',$id_apotek);
		$query  = $this->db->get();
		return $query;
	}

	function update(){

		$id_apotek	= $this->input->post('id_apotek');
		$data = array(

			'id_jenis_apt' 			=> $this->input->post('id_jenis_apt'),
			'nama_apt' 				=> $this->input->post('nama_apt'),
			'owner_apt' 			=> $this->input->post('owner_apt'),
			'penanggungjwb' 		=> $this->input->post('penanggungjwb'),
			'telp_penanggungjwb' 	=> $this->input->post('telp_penanggungjwb'),
			'alamat' 				=> $this->input->post('alamat'),
			'id_provinsi' 			=> $this->input->post('id_provinsi'),
			'id_kota'		 		=> $this->input->post('id_kota'),
			'id_kecamatan' 			=> $this->input->post('id_kecamatan'),
			'id_desa' 				=> $this->input->post('id_desa'),
			'telp1'		 			=> $this->input->post('telp1'),
			'telp2' 				=> $this->input->post('telp2'),
			'email' 				=> $this->input->post('email'),
			'updated_at' 			=> date("Y-m-d H:i:s"),
			'id_admin'				=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_apotek',$id_apotek);
		$this->db->update('tbl_data_apt',$data);

		redirect('medliz/apotek');

	}
}