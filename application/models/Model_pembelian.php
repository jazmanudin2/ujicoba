<?php 

class Model_pembelian extends CI_Model{

	function code_otomatis(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_pembelian.no_fak_pemb,9) as kode ',false);
		$this->db->where('mid(no_fak_pemb,3,2)',$tahunini);
		$this->db->order_by('no_fak_pemb', 'desc');
		$this->db->limit(9);
		$query 		= $this->db->get('tbl_pembelian');
		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}
		$kodemax 	= str_pad($kode,9,"0",STR_PAD_LEFT);
		$kodejadi  	= "FK".date('y').$kodemax;
		return $kodejadi;

	}

	function nobukti(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_historibayarhutang.nobukti,9) as kode ',false);
		$this->db->where('left(nobukti,2)',$tahunini);
		$this->db->order_by('nobukti', 'desc');
		$this->db->limit(9);
		$query 		= $this->db->get('tbl_historibayarhutang');
		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}
		$kodemax 	= str_pad($kode,9,"0",STR_PAD_LEFT);
		$kodejadi  	= date('y').$kodemax;
		return $kodejadi;

	}

	public function getData($rowno,$rowperpage,$id_supplier="",$pencarian="") {
		$this->db->select('*');
		$this->db->from('view_hutang');
		$this->db->where('view_hutang.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_supplier','desc');

		if($id_supplier != ''){

			$this->db->where('id_supplier', $id_supplier);

		}

		if($pencarian != ''){

			$this->db->like('id_supplier', $pencarian);
			$this->db->or_like('alamat', $pencarian);
			$this->db->or_like('nama_perusahaan', $pencarian);
			$this->db->or_like('telp_supplier', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_supplier = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('view_hutang');
		$this->db->where('view_hutang.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_supplier','desc');

		if($id_supplier != ''){
			$this->db->where('id_supplier', $id_supplier);
		}

		if($pencarian != ''){
			
			$this->db->like('id_supplier', $pencarian);
			$this->db->or_like('alamat', $pencarian);
			$this->db->or_like('nama_perusahaan', $pencarian);
			$this->db->or_like('telp_supplier', $pencarian);

		}
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}

	function view_hhutang(){

		$id_supplier	= $this->uri->segment(4);
		$query = "SELECT tbl_pembelian.no_fak_pemb,tanggal,tbl_pembelian.id_supplier,total as totalhutang, hb.jmlbayar as totalbayar, total - hb.jmlbayar as sisabayar
		FROM tbl_pembelian
		LEFT JOIN (SELECT no_fak_pemb,SUM(bayar) as jmlbayar FROM tbl_historibayarhutang GROUP BY no_fak_pemb) hb ON (tbl_pembelian.no_fak_pemb = hb.no_fak_pemb)

		INNER JOIN tbl_supplier
		ON tbl_pembelian.id_supplier = tbl_supplier.id_supplier 
		WHERE tbl_pembelian.id_supplier = '$id_supplier'

		GROUP BY tbl_pembelian.no_fak_pemb
		";
		return $this->db->query($query);

	}

	function get_histori_supplier(){

		$id_supplier	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('tbl_supplier');
		$this->db->join('tbl_jenis_supplier','tbl_supplier.id_jenis_supplier = tbl_jenis_supplier.id_jenis_supplier');
		$this->db->join('tbl_jenis_produk','tbl_supplier.id_jenis_produk = tbl_jenis_produk.id_jenis_produk');
		$this->db->join('tbl_sales','tbl_supplier.id_sales = tbl_sales.id_sales');
		$this->db->where('tbl_supplier.id_supplier',$id_supplier);
		$this->db->order_by('id_supplier','desc');
		return $this->db->get();

	}

	function sum_hhutang(){

		$id_supplier	= $this->uri->segment(4);
		$query = "SELECT SUM(total) as totalhutangs
		FROM tbl_pembelian
		LEFT JOIN (SELECT no_fak_pemb,SUM(bayar) as jmlbayar FROM tbl_historibayarhutang GROUP BY no_fak_pemb) hb ON (tbl_pembelian.no_fak_pemb = hb.no_fak_pemb)
		INNER JOIN tbl_supplier
		ON tbl_pembelian.id_supplier = tbl_supplier.id_supplier 
		WHERE tbl_pembelian.id_supplier = '$id_supplier'
		";
		return $this->db->query($query);

	}

	function view_pembelian(){

		$no_fak_pemb	= $this->uri->segment(4);
		$query = "SELECT tbl_supplier.alamat,tbl_supplier.nama_perusahaan,tbl_supplier.telp_supplier,tbl_pembelian.id_supplier,no_fak_pemb,tanggal,subtotal,potongan,total,tbl_sales.id_sales,tbl_sales.nama_sales,ppn,biaya_lain,tbl_pembelian.total,tbl_pembelian.keterangan,tbl_supplier.alamat
		FROM tbl_pembelian
		INNER JOIN tbl_supplier
		ON tbl_pembelian.id_supplier = tbl_supplier.id_supplier
		INNER JOIN tbl_sales
		ON tbl_sales.id_sales = tbl_sales.id_sales
		WHERE tbl_pembelian.no_fak_pemb = '$no_fak_pemb'
		GROUP BY tbl_pembelian.id_supplier
		";
		return $this->db->query($query);

	}

	function view_dpembelian_barangobat(){

		$no_fak_pemb	= $this->uri->segment(4);
		$id_admin		= $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailpembelian.no_fak_pemb,
		tbl_detailpembelian.id_barang_m,
		tbl_detailpembelian.no_batch,
		tbl_detailpembelian.jumlah_perbox,
		tbl_detailpembelian.exp_date,
		tbl_detailpembelian.harga_satuan,
		tbl_detailpembelian.box,
		tbl_detailpembelian.jumlah_satuan,
		tbl_detailpembelian.subtotal,
		tbl_detailpembelian.id_admin,
		tbl_detailbarang.no_batch,
		tbl_detailbarang.stok,
		tbl_detailbarang.exp_date,
		tbl_detailbarang.jumlah_perbox,
		tbl_detailbarang.harga_beli,
		tbl_barang_obat.id_barang,
		tbl_barang_obat.merk_dag,
		tbl_barang_obat.id_gol_obat,
		tbl_barang_obat.id_kandungan_obat,
		tbl_barang_obat.dosis,
		tbl_barang_obat.sediaan,
		tbl_barang_obat.id_jenis_obat,
		tbl_barang_obat.min_stok,
		tbl_barang_obat.keterangan,
		tbl_barang_obat.created_at,
		tbl_barang_obat.updated_at,
		tbl_barang_obat.id_admin
		FROM
		tbl_detailpembelian
		INNER JOIN tbl_detailbarang ON tbl_detailpembelian.id_barang_m = tbl_detailbarang.id_barang_m
		INNER JOIN tbl_barang_obat ON tbl_barang_obat.id_barang_m = tbl_detailbarang.id_barang_m
		WHERE tbl_detailpembelian.no_fak_pemb = '$no_fak_pemb' AND tbl_detailpembelian.id_admin = '$id_admin'
		";
		return $this->db->query($query);


	}

	function view_dpembelian_barangumum(){

		$no_fak_pemb	= $this->uri->segment(4);
		$id_admin		= $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailpembelian.no_fak_pemb,
		tbl_detailpembelian.id_barang_m,
		tbl_detailpembelian.no_batch,
		tbl_detailpembelian.jumlah_perbox,
		tbl_detailpembelian.exp_date,
		tbl_detailpembelian.harga_satuan,
		tbl_detailpembelian.box,
		tbl_detailpembelian.jumlah_satuan,
		tbl_detailpembelian.subtotal,
		tbl_detailpembelian.id_admin,
		tbl_detailbarang.no_batch,
		tbl_detailbarang.stok,
		tbl_detailbarang.exp_date,
		tbl_detailbarang.jumlah_perbox,
		tbl_detailbarang.harga_beli,
		tbl_barang_umum.id_barang,
		tbl_barang_umum.nama_barang,
		tbl_barang_umum.min_stok,
		tbl_barang_umum.gol_barang,
		tbl_barang_umum.id_admin,
		tbl_barang_umum.created_at,
		tbl_barang_umum.updated_at,
		tbl_barang_umum.keterangan
		FROM
		tbl_detailpembelian
		INNER JOIN tbl_detailbarang ON tbl_detailpembelian.id_barang_m = tbl_detailbarang.id_barang_m
		INNER JOIN tbl_barang_umum ON tbl_barang_umum.id_barang_m = tbl_detailbarang.id_barang_m
		WHERE tbl_detailpembelian.no_fak_pemb = '$no_fak_pemb' AND tbl_detailpembelian.id_admin = '$id_admin'
		";
		return $this->db->query($query);

	}

	function view_dpembelian_barangalkes(){

		$no_fak_pemb	= $this->uri->segment(4);
		$id_admin		= $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailpembelian.no_fak_pemb,
		tbl_detailpembelian.id_barang_m,
		tbl_detailpembelian.no_batch,
		tbl_detailpembelian.jumlah_perbox,
		tbl_detailpembelian.exp_date,
		tbl_detailpembelian.harga_satuan,
		tbl_detailpembelian.box,
		tbl_detailpembelian.jumlah_satuan,
		tbl_detailpembelian.subtotal,
		tbl_detailpembelian.id_admin,
		tbl_detailbarang.no_batch,
		tbl_detailbarang.stok,
		tbl_detailbarang.exp_date,
		tbl_detailbarang.jumlah_perbox,
		tbl_detailbarang.harga_beli,
		tbl_barang_alkes.id_barang,
		tbl_barang_alkes.jenis_barang,
		tbl_barang_alkes.detail_alkes,
		tbl_barang_alkes.min_stok,
		tbl_barang_alkes.keterangan
		FROM
		tbl_detailpembelian
		INNER JOIN tbl_detailbarang ON tbl_detailpembelian.id_barang_m = tbl_detailbarang.id_barang_m
		INNER JOIN tbl_barang_alkes ON tbl_barang_alkes.id_barang_m = tbl_detailbarang.id_barang_m
		WHERE tbl_detailpembelian.no_fak_pemb = '$no_fak_pemb' AND tbl_detailpembelian.id_admin = '$id_admin'
		";
		return $this->db->query($query);

	}

	function get_rbayar_pembayaran(){

		$no_fak_pemb	= $this->uri->segment(4);
		return $this->db->query("SELECT * FROM tbl_detailpemb_rencanakredit WHERE no_fak_pemb='$no_fak_pemb' ");

	}

	function view_rpembayaran(){

		$no_fak_pemb	= $this->uri->segment(4);
		$query = "SELECT jatuhtempo,wajibbayar,tbl_detailpemb_rencanakredit.no_fak_pemb,realisasi,tbl_detailpemb_rencanakredit.cicilanke
		FROM tbl_pembelian
		INNER JOIN tbl_detailpemb_rencanakredit
		ON tbl_pembelian.no_fak_pemb = tbl_detailpemb_rencanakredit.no_fak_pemb WHERE tbl_detailpemb_rencanakredit.no_fak_pemb = '$no_fak_pemb'
		";
		return $this->db->query($query);

	}

	function view_hbayar(){

		$no_fak_pemb	= $this->uri->segment(4);
		$query = "SELECT nobukti,bayar,tglbayar,tbl_historibayarhutang.no_fak_pemb,keterangan
		FROM tbl_historibayarhutang
		INNER JOIN tbl_pembelian
		ON tbl_historibayarhutang.no_fak_pemb = tbl_pembelian.no_fak_pemb
		WHERE tbl_historibayarhutang.no_fak_pemb = '$no_fak_pemb'
		ORDER BY nobukti DESC
		";
		return $this->db->query($query);

	}

	function view_posting_pembayaran(){

		$no_fak_pemb	= $this->uri->segment(4);
		$query = "SELECT tbl_detailpemb_rencanakredit.jatuhtempo,tbl_detailpemb_rencanakredit.wajibbayar,tbl_detailpemb_rencanakredit.no_fak_pemb,tbl_detailpemb_rencanakredit.realisasi,SUM(tbl_detailpemb_rencanakredit.wajibbayar) AS totwajibbayar,SUM(tbl_detailpemb_rencanakredit.realisasi) AS totalrealisasi
		FROM tbl_pembelian
		INNER JOIN tbl_detailpemb_rencanakredit
		ON tbl_pembelian.no_fak_pemb = tbl_detailpemb_rencanakredit.no_fak_pemb WHERE tbl_detailpemb_rencanakredit.no_fak_pemb = '$no_fak_pemb'
		ORDER BY tbl_detailpemb_rencanakredit.wajibbayar ASC
		";
		return $this->db->query($query);

	}

	function view_temp_barangobat(){

		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailpembelian_temp.id_barang_m,
		tbl_detailpembelian_temp.jumlah_perbox,
		tbl_detailpembelian_temp.no_batch,
		tbl_detailpembelian_temp.exp_date,
		tbl_detailpembelian_temp.harga_satuan,
		tbl_detailpembelian_temp.box,
		tbl_detailpembelian_temp.jumlah_satuan,
		tbl_detailpembelian_temp.subtotal,
		tbl_detailpembelian_temp.id_admin,
		tbl_detailpembelian_temp.id_barang,
		tbl_barang_obat.id_barang,
		tbl_barang_obat.id_kandungan_obat,
		tbl_barang_obat.dosis,
		tbl_barang_obat.merk_dag,
		tbl_barang_obat.sediaan,
		tbl_gol_obat.nama_gol_obat,
		tbl_barang_obat.min_stok
		FROM
		tbl_barang_obat
		INNER JOIN tbl_detailpembelian_temp ON tbl_detailpembelian_temp.id_barang_m = tbl_barang_obat.id_barang_m
		INNER JOIN tbl_gol_obat ON tbl_barang_obat.id_gol_obat = tbl_gol_obat.id_gol_obat
		WHERE tbl_detailpembelian_temp.id_admin ='$id_admin'
		";
		return $this->db->query($query);
		
	}

	function get_temp_barang_alkes(){

		$id_barang_m 	= $this->input->post('id_barang_m');
		$id_admin 		= $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailpembelian_temp.id_barang_m,
		tbl_detailpembelian_temp.jumlah_perbox,
		tbl_detailpembelian_temp.no_batch,
		tbl_detailpembelian_temp.exp_date,
		tbl_detailpembelian_temp.harga_satuan,
		tbl_detailpembelian_temp.box,
		tbl_detailpembelian_temp.jumlah_satuan,
		tbl_detailpembelian_temp.subtotal,
		tbl_detailpembelian_temp.id_admin,
		tbl_detailpembelian_temp.id_barang,
		tbl_barang_alkes.id_barang,
		tbl_barang_alkes.jenis_barang,
		tbl_barang_alkes.detail_alkes
		FROM
		tbl_detailpembelian_temp
		INNER JOIN tbl_barang_alkes ON tbl_barang_alkes.id_barang_m = tbl_detailpembelian_temp.id_barang_m
		WHERE tbl_detailpembelian_temp.id_admin ='$id_admin' AND tbl_detailpembelian_temp.id_barang_m = '$id_barang_m'
		";
		return $this->db->query($query);
		
	}

	function view_temp_barangumum(){

		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailpembelian_temp.id_barang_m,
		tbl_detailpembelian_temp.jumlah_perbox,
		tbl_detailpembelian_temp.no_batch,
		tbl_detailpembelian_temp.exp_date,
		tbl_detailpembelian_temp.harga_satuan,
		tbl_detailpembelian_temp.box,
		tbl_detailpembelian_temp.jumlah_satuan,
		tbl_detailpembelian_temp.subtotal,
		tbl_detailpembelian_temp.id_admin,
		tbl_detailpembelian_temp.id_barang,
		tbl_barang_umum.id_barang,
		tbl_barang_umum.nama_barang,
		tbl_barang_umum.min_stok,
		tbl_barang_umum.gol_barang
		FROM
		tbl_detailpembelian_temp
		INNER JOIN tbl_barang_umum ON tbl_barang_umum.id_barang_m = tbl_detailpembelian_temp.id_barang_m
		WHERE tbl_detailpembelian_temp.id_admin ='$id_admin'
		";
		return $this->db->query($query);
		
	}

	function view_temp_barangalkes(){

		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailpembelian_temp.id_barang_m,
		tbl_detailpembelian_temp.jumlah_perbox,
		tbl_detailpembelian_temp.no_batch,
		tbl_detailpembelian_temp.exp_date,
		tbl_detailpembelian_temp.harga_satuan,
		tbl_detailpembelian_temp.box,
		tbl_detailpembelian_temp.jumlah_satuan,
		tbl_detailpembelian_temp.subtotal,
		tbl_detailpembelian_temp.id_admin,
		tbl_detailpembelian_temp.id_barang,
		tbl_barang_alkes.id_barang,
		tbl_barang_alkes.jenis_barang,
		tbl_barang_alkes.detail_alkes
		FROM
		tbl_detailpembelian_temp
		INNER JOIN tbl_barang_alkes ON tbl_barang_alkes.id_barang_m = tbl_detailpembelian_temp.id_barang_m
		WHERE tbl_detailpembelian_temp.id_admin ='$id_admin'
		";
		return $this->db->query($query);
		
	}

	function view_barang_obat(){

		$id_admin 	= $this->session->userdata('id_admin');

		$query = "SELECT *
		FROM
		tbl_barang_obat
		INNER JOIN tbl_jenis_obat ON tbl_jenis_obat.id_jenis_obat = tbl_barang_obat.id_jenis_obat
		INNER JOIN tbl_gol_obat ON tbl_gol_obat.id_gol_obat = tbl_barang_obat.id_gol_obat
		WHERE tbl_barang_obat.id_admin = '$id_admin' 
		GROUP BY id_barang
		ORDER BY id_barang ASC";
		
		return $this->db->query($query);

	}

	function view_barang_alkes(){

		$id_admin 	= $this->session->userdata('id_admin');

		$query = "SELECT *
		FROM
		tbl_barang_alkes
		WHERE tbl_barang_alkes.id_admin = '$id_admin' 
		GROUP BY id_barang
		ORDER BY id_barang ASC";
		
		return $this->db->query($query);

	}


	function view_barang_umum(){

		$id_admin 	= $this->session->userdata('id_admin');
		$query = "SELECT *
		FROM
		tbl_barang_umum
		WHERE tbl_barang_umum.id_admin = '$id_admin' 
		GROUP BY id_barang
		ORDER BY id_barang ASC";
		
		return $this->db->query($query);

	}


	function sum_temp(){

		$id_user = $this->session->userdata('id_admin');
		$query = "SELECT SUM(tbl_detailpembelian_temp.subtotal) AS subtotals
		FROM tbl_detailpembelian_temp
		WHERE tbl_detailpembelian_temp.id_admin = '$id_user'
		";
		return $this->db->query($query);
		
	}


	function get_histori_pembayaran(){

		$nobukti 		= $this->input->post('nobukti');
		return $this->db->get_where('tbl_historibayarhutang',array('nobukti'=>$nobukti));

	}

	function view_sp(){

		$id_user = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_surat_pesanan.id_sp,
		tbl_surat_pesanan.tanggal,
		tbl_surat_pesanan.suppliant,
		tbl_surat_pesanan.id_supplier,
		tbl_surat_pesanan.keterangan,
		tbl_surat_pesanan.id_admin,
		tbl_supplier.nama_perusahaan,
		tbl_jenis_supplier.nama_jenis_supplier,
		tbl_jenis_produk.nama_jenis_produk,
		tbl_supplier.alamat,
		tbl_supplier.telp_supplier
		FROM
		tbl_surat_pesanan
		INNER JOIN tbl_supplier ON tbl_surat_pesanan.id_supplier = tbl_supplier.id_supplier
		INNER JOIN tbl_jenis_supplier ON tbl_supplier.id_jenis_supplier = tbl_jenis_supplier.id_jenis_supplier
		INNER JOIN tbl_jenis_produk ON tbl_supplier.id_jenis_produk = tbl_jenis_produk.id_jenis_produk
		WHERE tbl_surat_pesanan.id_admin = '$id_user'
		";
		return $this->db->query($query);

	}

	function input_tmp(){

		$id_barang_m 		= $this->input->post('id_barang_m');
		$jumlah_perbox 		= $this->input->post('jumlah_perbox');
		$id_barang 			= $this->input->post('id_barang');
		$no_batch 			= $this->input->post('no_batch');
		$exp_date 			= $this->input->post('exp_date');
		$harga_satuan 		= $this->input->post('harga_satuan');
		$box 				= $this->input->post('box');
		$jumlah_satuan 		= $this->input->post('jumlah_satuan');
		$subtotal 			= $this->input->post('subtotal');
		$id_admin		 	= $this->session->userdata('id_admin');

		$data = array(
			
			'id_barang_m'	=> $id_barang_m,
			'jumlah_perbox'	=> $jumlah_perbox,
			'id_barang'		=> $id_barang,
			'no_batch'		=> $no_batch,
			'exp_date'		=> $exp_date,
			'harga_satuan'	=> $harga_satuan,
			'box'			=> $box,
			'jumlah_satuan'	=> $jumlah_satuan,
			'subtotal'		=> $subtotal,
			'id_admin'		=> $id_admin,

		);
		$this->db->insert('tbl_detailpembelian_temp',$data);
	}


	function insert(){

		$no_fak_pemb			= $this->input->post('no_fak_pemb');
		$id_sp					= $this->input->post('id_sp');
		$tanggal				= $this->input->post('tanggal');
		$id_barang_m			= $this->input->post('id_barang_m');
		$id_barang 				= $this->input->post('id_barang');
		$total 					= $this->input->post('total2');
		$subtotal4 				= $this->input->post('subtotal4');
		$potongan 				= $this->input->post('potongan');
		$jenisbayar 			= $this->input->post('jenisbayar');
		$id_supplier 			= $this->input->post('id_supplier');
		$id_sales 				= $this->input->post('id_sales');
		$diskon 				= $this->input->post('diskon');
		$ppn 					= $this->input->post('ppn');
		$jatuhtempo 			= $this->input->post('jatuhtempo');
		$biaya_lain 			= $this->input->post('biaya_lain');
		$jenis_bayar 			= $this->input->post('jenis_bayar');
		$keterangan 			= $this->input->post('keterangan');
		$nobukti 				= $this->input->post('nobukti');
		$tglbayar 				= $this->input->post('tglbayar');
		$wajibbayar 			= $this->input->post('wajibbayar');
		$pembayaran 			= $this->input->post('pembayaran');
		$sisa_bayar 			= $this->input->post('sisa_bayar');
		$id_admin 				= $this->session->userdata('id_admin');
		$cicilanper 			= ($this->input->post('sisa_bayar') / $this->input->post('jumlah_cicilan'));
		$jumlah_cicilan 		= $this->input->post('jumlah_cicilan');
		$tgl  					= substr($jatuhtempo,8,2);
		$tahun 					= substr($jatuhtempo,0,4);
		$bulan					= substr($jatuhtempo,5,2);

		$data = array(

			'no_fak_pemb' 		=> $no_fak_pemb,
			'id_sp' 			=> $id_sp,
			'tanggal' 			=> $tanggal,
			'id_supplier' 		=> $id_supplier,
			'id_sales' 			=> $id_sales,
			'subtotal' 			=> $subtotal4,
			'potongan' 			=> $potongan,
			'ppn' 				=> $ppn,
			'biaya_lain' 		=> $biaya_lain,
			'total' 			=> $total,
			'jenis_bayar' 		=> 'DP',
			'id_admin' 			=> $id_admin,
			'keterangan' 		=> $keterangan,

		);

		$this->db->insert('tbl_pembelian',$data);

		// Insert HistoriBayarhutang
		$histori = array(

			'nobukti' 			=> $nobukti,
			'no_fak_pemb' 		=> $no_fak_pemb,
			'tglbayar' 			=> $tanggal,
			'bayar' 			=> $pembayaran,
			'id_admin' 			=> $id_admin,

		);

		$this->db->insert('tbl_historibayarhutang',$histori);

		// Insert DetailRencanaBayarKredit
		$rencanakredit = array(

			'no_fak_pemb' 			=> $no_fak_pemb,
			'jatuhtempo' 			=> $jatuhtempo,
			'wajibbayar' 			=> $total,
			'cicilanke' 			=> 0,
			'realisasi' 			=> $pembayaran,

		);

		$this->db->insert('tbl_detailpemb_rencanakredit',$rencanakredit);


		// for ($i=1; $i<=$jumlah_cicilan; $i++)
		// {
		// 	$tambah 		= mktime(0,0,0,date($bulan),date($tgl),date($tahun));
		// 	$tgl_cicilan	= date("Y-m-d", $tambah);
		// 	$datark 		= array(

		// 		'no_fak_pemb' => $this->input->post('no_fak_pemb'),
		// 		'cicilanke'   => $i,
		// 		'jatuhtempo'  => $tgl_cicilan,
		// 		'wajibbayar'  => 0,
		// 		'realisasi'   => 0,

		// 	);
		// 	$this->db->insert('tbl_detailpemb_rencanakredit',$datark);

		// 	$bulan++;
		// }

		$qcek 		= "SELECT SUM(bayar) as jmlbayar FROM tbl_historibayarhutang WHERE no_fak_pemb ='$no_fak_pemb'";
		$cek 		= $this->db->query($qcek)->row_array();

		$jmlbayar 	= $cek['jmlbayar'];

		$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='0' WHERE no_fak_pemb='$no_fak_pemb'");
		$q 			= $this->db->query("SELECT * FROM tbl_detailpemb_rencanakredit WHERE no_fak_pemb='$no_fak_pemb' ORDER BY cicilanke ASC")->result();

		foreach ($q as $r) {

			if($r->wajibbayar != $r->realisasi  AND $jmlbayar  >= $r->wajibbayar){

				$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='$r->wajibbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_pemb='$r->no_fak_pemb'");

			}else{

				if($jmlbayar > 0){

					$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='$jmlbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_pemb='$r->no_fak_pemb'");

				}

			}

			$jmlbayar = $jmlbayar - $r->wajibbayar;
		}

		// InsertDetailtbl_pembelianTemp
		$inputtmp2 	= $this->db->get('tbl_detailpembelian_temp');

		foreach ($inputtmp2->result() as $d){

			$datatemp2 = array(

				'id_barang_m' 		=> $d->id_barang_m,
				'no_batch'			=> $d->no_batch,
				'stok'				=> $d->jumlah_satuan,
				'exp_date'			=> $d->exp_date,
				'jumlah_perbox' 	=> $d->jumlah_perbox,
				'harga_beli' 		=> $d->harga_satuan,
				'jenis_mutasi' 		=> 'FR',
				'subtotal' 			=> $d->subtotal,
				'tanggal' 			=> $tanggal,
				'no_fak_pemb' 		=> $no_fak_pemb,
				'jumlah_satuan' 	=> $d->jumlah_satuan,
				'id_supplier' 		=> $id_supplier,
				'id_admin' 			=> $id_admin,

			);

			$this->db->insert('tbl_detailbarang',$datatemp2);

		}

		$inputtmp 	= $this->db->get('tbl_detailpembelian_temp');

		foreach ($inputtmp->result() as $d){

			$datatemp = array(

				'no_fak_pemb' 		=> $no_fak_pemb,
				'id_barang_m' 		=> $d->id_barang_m,
				'jumlah_perbox'		=> $d->jumlah_perbox,
				'no_batch'			=> $d->no_batch,
				'exp_date'			=> $d->exp_date,
				'harga_satuan' 		=> $d->harga_satuan,
				'box' 				=> $d->box,
				'jumlah_satuan' 	=> $d->jumlah_satuan,
				'subtotal' 			=> $d->subtotal,
				'id_admin' 			=> $d->id_admin,

			);

			$this->db->insert('tbl_detailpembelian',$datatemp);

		}

		$query = "truncate tbl_detailpembelian_temp";
		$this->db->query($query);
		redirect('apotek/pembelian/input/'.$no_fak_pemb);

	}


	function insert_bayar(){

		$no_fak_pemb		= $this->input->post('no_fak_pemb');
		$nobukti 			= $this->input->post('nobukti');
		$tglbayar 			= $this->input->post('tglbayar');
		$bayar 				= $this->input->post('bayar');
		$admin 				= $this->session->userdata('id_admin');


		$ququ = $this->db->query("INSERT INTO tbl_historibayarhutang (nobukti,no_fak_pemb,tglbayar,bayar,id_admin) VALUES ('$nobukti','$no_fak_pemb','$tglbayar','$bayar','$admin')");
		if($ququ){

			$cek = $this->db->query("SELECT SUM(bayar) as jmlbayar FROM tbl_historibayarhutang WHERE no_fak_pemb ='$no_fak_pemb'")->row_array();

			$jmlbayar = $cek['jmlbayar'];
			$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='0' WHERE  no_fak_pemb='$no_fak_pemb'");
			$q = $this->db->query("SELECT * FROM tbl_detailpemb_rencanakredit WHERE no_fak_pemb ='$no_fak_pemb' ORDER BY cicilanke ASC")->result();
			foreach ($q as $r) {

				if($r->wajibbayar != $r->realisasi  AND $jmlbayar  >= $r->wajibbayar){

					$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='$r->wajibbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_pemb='$r->no_fak_pemb'");

				}else{

					if($jmlbayar>0 AND $r->wajibbayar !=0){

						$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='$jmlbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_pemb='$r->no_fak_pemb'");

					}else{

						$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='0' WHERE cicilanke='$r->cicilanke' AND no_fak_pemb='$r->no_fak_pemb'");

					}

				}

				$jmlbayar = $jmlbayar - $r->wajibbayar;
			}

		}
		redirect('apotek/pembelian/detailpembelian/'.$no_fak_pemb);
	}


	function delete_bayar(){

		$nobukti 			= $this->uri->segment(4);
		$no_fak_pemb 		= $this->uri->segment(5);
		$tglbayar 			= $this->input->post('tglbayar');
		$bayar 				= $this->input->post('bayar');
		$admin 				= $this->session->userdata('id_admin');

		$query = $this->db->query("DELETE FROM tbl_historibayarhutang WHERE nobukti = '$nobukti'");

		if($query){

			$cek = $this->db->query("SELECT SUM(bayar) as jmlbayar FROM tbl_historibayarhutang WHERE no_fak_pemb ='$no_fak_pemb'")->row_array();
			$jmlbayar = $cek['jmlbayar'];
			$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='0' WHERE  no_fak_pemb='$no_fak_pemb'");
			$q = $this->db->query("SELECT * FROM tbl_detailpemb_rencanakredit WHERE no_fak_pemb ='$no_fak_pemb' ORDER BY cicilanke ASC")->result();

			foreach ($q as $r) {

				if($r->wajibbayar != $r->realisasi  AND $jmlbayar  >= $r->wajibbayar){

					$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='$r->wajibbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_pemb='$r->no_fak_pemb'");

				}else{

					if($jmlbayar > 0 AND $r->wajibbayar !=0){

						$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='$jmlbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_pemb='$r->no_fak_pemb'");

					}else{

						$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='0' WHERE cicilanke='$r->cicilanke' AND no_fak_pemb='$r->no_fak_pemb'");

					}

				}

				$jmlbayar = $jmlbayar - $r->wajibbayar;
			}

		}
		redirect('apotek/pembelian/detailpembelian/'.$no_fak_pemb);
		
	}


	function update_histori_hutang(){

		$no_fak_pemb		= $this->input->post('no_fak_pemb');
		$nobukti 			= $this->input->post('nobukti');
		$tglbayar 			= $this->input->post('tglbayar');
		$bayar 				= $this->input->post('bayar');
		$admin 				= $this->session->userdata('id_admin');

		$data 	= $this->db->query("SELECT  * FROM tbl_historibayarhutang WHERE nobukti = '$nobukti'")->row_array();
		
		if($data['ket']=="DP"){
			
			$this->db->query("UPDATE tbl_pembelian SET tanggal = '$tglbayar' WHERE no_fak_pemb = '$no_fak_pemb'");
			
		}
		
		$query  = $this->db->query("UPDATE tbl_historibayarhutang SET tglbayar = '$tglbayar',bayar='$bayar' WHERE nobukti = '$nobukti'");

		if($query){

			$cek = $this->db->query("SELECT SUM(bayar) as jmlbayar FROM tbl_historibayarhutang WHERE no_fak_pemb ='$no_fak_pemb'")->row_array();

			$jmlbayar = $cek['jmlbayar'];
			$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='0' WHERE  no_fak_pemb='$no_fak_pemb'");
			$q = $this->db->query("SELECT * FROM tbl_detailpemb_rencanakredit WHERE no_fak_pemb ='$no_fak_pemb' ORDER BY cicilanke ASC")->result();
			foreach ($q as $r) {

				if($r->wajibbayar != $r->realisasi  AND $jmlbayar  >= $r->wajibbayar){

					$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='$r->wajibbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_pemb='$r->no_fak_pemb'");

				}else{

					if($jmlbayar>0 AND $r->wajibbayar !=0){

						$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='$jmlbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_pemb='$r->no_fak_pemb'");

					}else{

						$this->db->query("UPDATE tbl_detailpemb_rencanakredit SET realisasi='0' WHERE cicilanke='$r->cicilanke' AND no_fak_pemb='$r->no_fak_pemb'");

					}

				}

				$jmlbayar = $jmlbayar - $r->wajibbayar;
			}

		}
		redirect('apotek/pembelian/detailpembelian/'.$no_fak_pemb);
	}


}