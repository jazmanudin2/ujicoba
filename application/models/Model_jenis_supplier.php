<?php
class Model_jenis_supplier extends CI_Model{

	function view_jenis_supplier(){

		$id_admin	= $this->session->userdata('id_admin');
		$query 		= "SELECT * FROM tbl_jenis_supplier WHERE id_admin = '$id_admin' ORDER BY nama_jenis_supplier ASC";
		return $this->db->query($query);

	}

	function insert(){

		$data = array(
			
			'id_jenis_supplier' 		=> $this->input->post('id_jenis_supplier'),
			'nama_jenis_supplier' 		=> $this->input->post('nama_jenis_supplier'),
			'keterangan' 				=> $this->input->post('keterangan'),
			'created_at' 				=> date("Y-m-d H:i:s"),
			'id_admin' 					=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_jenis_supplier',$data);
		
	}

	function delete(){

		$id_jenis_supplier	= $this->uri->segment(4);
		$this->db->delete('tbl_jenis_supplier',array('id_jenis_supplier' => $id_jenis_supplier));

	}

	public function getData($rowno,$rowperpage,$id_jenis_supplier="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_jenis_supplier');
		$this->db->where('tbl_jenis_supplier.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_jenis_supplier','desc');

		if($id_jenis_supplier != ''){

			$this->db->where('id_jenis_supplier', $id_jenis_supplier);

		}

		if($pencarian != ''){

			$this->db->like('nama_jenis_supplier', $pencarian);
			$this->db->or_like('id_jenis_supplier', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_jenis_supplier = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_jenis_supplier');
		$this->db->where('tbl_jenis_supplier.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_jenis_supplier','desc');

		if($id_jenis_supplier != ''){
			$this->db->where('id_jenis_supplier', $id_jenis_supplier);
		}

		if($pencarian != ''){

			$this->db->like('nama_jenis_supplier', $pencarian);
			$this->db->or_like('id_jenis_supplier', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function get_jenis_supplier(){

		$id_jenis_supplier 	= $this->input->post('id_jenis_supplier');
		return $this->db->get_where('tbl_jenis_supplier',array('id_jenis_supplier'=>$id_jenis_supplier));

	}

	function update(){

		$id_jenis_supplier	= $this->input->post('id_jenis_supplier');
		
		$data = array(
			
			'nama_jenis_supplier' 		=> $this->input->post('nama_jenis_supplier'),
			'keterangan' 				=> $this->input->post('keterangan'),
			'updated_at' 				=> date("Y-m-d H:i:s"),
			'id_admin' 					=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_jenis_supplier',$id_jenis_supplier);
		$this->db->update('tbl_jenis_supplier',$data);
		//Notifikasi
		$this->session->set_flashdata('msg',
			'<div class="alert alert-success">
			<h4>Successfully</h4>
			<p>Data Berhasil Di Ubah</p>

			</div>');

		redirect('apotek/jenis_supplier');

	}
}