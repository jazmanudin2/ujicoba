<?php 

class Model_penjualan extends CI_Model{

	public function getData($rowno,$rowperpage,$id_pelanggan="",$pencarian="") {
		$this->db->select('*');
		$this->db->from('view_piutang');
		$this->db->where('view_piutang.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_pelanggan','desc');

		if($id_pelanggan != ''){
			$this->db->where('id_pelanggan', $id_pelanggan);

		}

		if($pencarian != ''){
			$this->db->like('id_pelanggan', $pencarian);
			$this->db->or_like('nama_pelanggan', $pencarian);
			$this->db->or_like('telp_pelanggan', $pencarian);
			$this->db->or_like('alamat', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_pelanggan = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('view_piutang');
		$this->db->where('view_piutang.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_pelanggan','desc');

		if($id_pelanggan != ''){
			$this->db->where('id_pelanggan', $id_pelanggan);
		}

		if($pencarian != ''){
			$this->db->like('id_pelanggan', $pencarian);
			$this->db->or_like('nama_pelanggan', $pencarian);
			$this->db->or_like('telp_pelanggan', $pencarian);
			$this->db->or_like('alamat', $pencarian);

		}
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}

	function get_pelanggan($id_jenis_pelanggan){
		$this->db->where('id_jenis_pelanggan',$id_jenis_pelanggan);
		return $this->db->get('tbl_pelanggan');
	}


	function get_histori_pelanggan(){

		$id_pelanggan	= $this->uri->segment(4);
		return $this->db->get_where('tbl_pelanggan',array('id_pelanggan'=>$id_pelanggan));

	}

	function view_temp_barangobat(){

		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailpenjualan_temp.id_barang_m,
		tbl_detailpenjualan_temp.jumlah,
		tbl_detailpenjualan_temp.harga_jual,
		tbl_detailpenjualan_temp.subtotal,
		tbl_detailpenjualan_temp.id_dtlbarang_temp,
		tbl_detailpenjualan_temp.id_admin,
		tbl_barang_obat.id_barang,
		tbl_barang_obat.id_kandungan_obat,
		tbl_barang_obat.dosis,
		tbl_barang_obat.sediaan,
		tbl_barang_obat.merk_dag,
		tbl_gol_obat.nama_gol_obat,
		tbl_kandungan_obat.nama_kandungan_obat,
		tbl_barang_obat.min_stok
		FROM
		tbl_barang_obat
		INNER JOIN tbl_detailpenjualan_temp ON tbl_detailpenjualan_temp.id_barang_m = tbl_barang_obat.id_barang_m
		INNER JOIN tbl_gol_obat ON tbl_barang_obat.id_gol_obat = tbl_gol_obat.id_gol_obat
		INNER JOIN tbl_kandungan_obat ON tbl_barang_obat.id_kandungan_obat = tbl_kandungan_obat.id_kandungan_obat
		WHERE tbl_detailpenjualan_temp.id_admin ='$id_admin'
		";
		return $this->db->query($query);
		
	}

	function view_temp_barangumum(){

		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailpenjualan_temp.id_barang_m,
		tbl_detailpenjualan_temp.jumlah,
		tbl_detailpenjualan_temp.id_dtlbarang_temp,
		tbl_detailpenjualan_temp.harga_jual,
		tbl_detailpenjualan_temp.subtotal,
		tbl_detailpenjualan_temp.id_admin,
		tbl_barang_umum.id_barang,
		tbl_barang_umum.nama_barang,
		tbl_barang_umum.min_stok,
		tbl_barang_umum.gol_barang
		FROM
		tbl_detailpenjualan_temp
		INNER JOIN tbl_barang_umum ON tbl_barang_umum.id_barang_m = tbl_detailpenjualan_temp.id_barang_m
		WHERE tbl_detailpenjualan_temp.id_admin ='$id_admin'
		";
		return $this->db->query($query);
		
	}

	function view_temp_barangalkes(){

		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailpenjualan_temp.id_barang_m,
		tbl_detailpenjualan_temp.jumlah,
		tbl_detailpenjualan_temp.id_dtlbarang_temp,
		tbl_detailpenjualan_temp.harga_jual,
		tbl_detailpenjualan_temp.subtotal,
		tbl_detailpenjualan_temp.id_admin,
		tbl_barang_alkes.id_barang,
		tbl_barang_alkes.jenis_barang,
		tbl_barang_alkes.detail_alkes
		FROM
		tbl_detailpenjualan_temp
		INNER JOIN tbl_barang_alkes ON tbl_barang_alkes.id_barang_m = tbl_detailpenjualan_temp.id_barang_m
		WHERE tbl_detailpenjualan_temp.id_admin ='$id_admin'
		";
		return $this->db->query($query);
		
	}

	function sum_tmp(){

		$id_user = $this->session->userdata('id_admin');
		$query = "SELECT SUM(tbl_detailpenjualan_temp.subtotal) AS subtotals
		FROM tbl_detailpenjualan_temp
		WHERE tbl_detailpenjualan_temp.id_admin = '$id_user' 
		";
		return $this->db->query($query);
		
	}

	function view_jenis_harga(){

		$id_user = $this->session->userdata('id_admin');
		$id_barang_m = $this->input->post('id_barang_m');
		$query = "SELECT * FROM tbl_jenis_harga
		INNER JOIN tbl_keterangan_harga ON tbl_keterangan_harga.id_keterangan_harga = tbl_jenis_harga.keterangan
		WHERE tbl_jenis_harga.id_admin = '$id_user' AND tbl_jenis_harga.id_barang_m ='$id_barang_m'
		";
		return $this->db->query($query);
		
	}

	function view_barang_obat(){

		$id_user = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_barang_obat.id_barang_m,
		tbl_barang_obat.id_barang,
		tbl_barang_obat.id_gol_obat,
		tbl_barang_obat.id_kandungan_obat,
		tbl_barang_obat.dosis,
		tbl_barang_obat.sediaan,
		tbl_barang_obat.id_jenis_obat,
		tbl_barang_obat.min_stok,
		tbl_barang_obat.keterangan,
		tbl_barang_obat.created_at,
		tbl_barang_obat.updated_at,
		tbl_barang_obat.merk_dag,
		tbl_gol_obat.nama_gol_obat,
		tbl_barang_obat.id_admin,
		tbl_barang_obat.barcode,
		tbl_detailbarang.no_batch,
		tbl_jenis_obat.nama_jenis_obat,
		tbl_kandungan_obat.nama_kandungan_obat,
		tbl_detailbarang.exp_date,
		tbl_detailbarang.jumlah_perbox,
		tbl_detailbarang.harga_beli,
		tbl_detailbarang.jenis_mutasi,
		tbl_detailbarang.stok AS totalstok,
		tbl_detailbarang.id_dtlbarang
		FROM
		tbl_barang_obat
		INNER JOIN tbl_detailbarang ON tbl_barang_obat.id_barang_m = tbl_detailbarang.id_barang_m
		INNER JOIN tbl_jenis_obat ON tbl_barang_obat.id_jenis_obat = tbl_jenis_obat.id_jenis_obat
		INNER JOIN tbl_gol_obat ON tbl_barang_obat.id_gol_obat = tbl_gol_obat.id_gol_obat
		INNER JOIN tbl_kandungan_obat ON tbl_barang_obat.id_kandungan_obat = tbl_kandungan_obat.id_kandungan_obat
		WHERE tbl_barang_obat.id_admin = '$id_user' AND tbl_detailbarang.stok >0
		ORDER BY
		tbl_detailbarang.exp_date ASC
		";
		return $this->db->query($query);
		
	}

	function view_barang_umum(){

		$id_user = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailbarang.id_barang_m,
		tbl_detailbarang.stok,
		tbl_detailbarang.jumlah_perbox,
		tbl_detailbarang.exp_date,
		tbl_detailbarang.harga_beli,
		tbl_barang_umum.id_barang,
		tbl_barang_umum.nama_barang,
		tbl_barang_umum.min_stok,
		tbl_barang_umum.gol_barang,
		tbl_barang_umum.updated_at,
		tbl_barang_umum.keterangan,
		tbl_barang_umum.id_admin,
		tbl_barang_umum.created_at,
		tbl_detailbarang.stok AS totalstok,
		tbl_detailbarang.id_dtlbarang
		FROM
		tbl_detailbarang
		INNER JOIN tbl_barang_umum ON tbl_barang_umum.id_barang_m = tbl_detailbarang.id_barang_m
		WHERE tbl_barang_umum.id_admin = '$id_user' AND tbl_detailbarang.stok >0
		ORDER BY
		tbl_detailbarang.exp_date ASC
		";
		return $this->db->query($query);
		
	}

	function view_barang_alkes(){

		$id_user = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailbarang.id_barang_m,
		tbl_detailbarang.stok,
		tbl_detailbarang.exp_date,
		tbl_detailbarang.jumlah_perbox,
		tbl_detailbarang.harga_beli,
		tbl_barang_alkes.id_barang,
		tbl_barang_alkes.jenis_barang,
		tbl_barang_alkes.detail_alkes,
		tbl_barang_alkes.min_stok,
		tbl_barang_alkes.id_admin,
		tbl_barang_alkes.keterangan,
		tbl_barang_alkes.created_at,
		tbl_barang_alkes.updated_at,
		tbl_detailbarang.stok AS totalstok,
		tbl_detailbarang.id_dtlbarang
		FROM
		tbl_detailbarang
		INNER JOIN tbl_barang_alkes ON tbl_barang_alkes.id_barang_m = tbl_detailbarang.id_barang_m
		WHERE tbl_barang_alkes.id_admin = '$id_user' AND tbl_detailbarang.stok >0
		ORDER BY
		tbl_detailbarang.exp_date ASC
		";
		return $this->db->query($query);
		
	}

	function view_penjualan(){

		$no_fak_penj	= $this->uri->segment(4);
		$query = "SELECT alamat,nama_pelanggan,telp_pelanggan,nama_perusahaan,tbl_penjualan.id_pelanggan,no_fak_penj,tanggal,subtotal,potongan,total,alamat
		FROM tbl_penjualan
		INNER JOIN tbl_pelanggan
		ON tbl_penjualan.id_pelanggan = tbl_pelanggan.id_pelanggan
		WHERE tbl_penjualan.no_fak_penj = '$no_fak_penj'
		GROUP BY tbl_penjualan.id_pelanggan
		";
		return $this->db->query($query);

	}

	
	function view_dpembelian_barangobat(){

		$no_fak_penj	= $this->uri->segment(4);
		$id_admin		= $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailpenjualan.no_fak_penj,
		tbl_detailpenjualan.id_barang_m,
		tbl_detailpenjualan.harga_jual,
		tbl_detailpenjualan.jumlah,
		tbl_detailpenjualan.subtotal,
		tbl_detailpenjualan.id_admin,
		tbl_barang_obat.id_barang,
		tbl_barang_obat.id_kandungan_obat,
		tbl_barang_obat.dosis,
		tbl_barang_obat.sediaan,
		tbl_barang_obat.merk_dag,
		tbl_gol_obat.nama_gol_obat
		FROM
		tbl_detailpenjualan
		INNER JOIN tbl_barang_obat ON tbl_barang_obat.id_barang_m = tbl_detailpenjualan.id_barang_m
		INNER JOIN tbl_gol_obat ON tbl_barang_obat.id_gol_obat = tbl_gol_obat.id_gol_obat
		WHERE tbl_detailpenjualan.no_fak_penj = '$no_fak_penj' 
		";
		return $this->db->query($query);


	}

	function view_dpembelian_barangumum(){

		$no_fak_penj	= $this->uri->segment(4);
		$id_admin		= $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailpenjualan.no_fak_penj,
		tbl_detailpenjualan.id_barang_m,
		tbl_detailpenjualan.harga_jual,
		tbl_detailpenjualan.jumlah,
		tbl_detailpenjualan.subtotal,
		tbl_barang_umum.id_barang,
		tbl_barang_umum.nama_barang,
		tbl_barang_umum.gol_barang,
		tbl_barang_umum.id_admin
		FROM
		tbl_detailpenjualan
		INNER JOIN tbl_barang_umum ON tbl_barang_umum.id_barang_m = tbl_detailpenjualan.id_barang_m 
		WHERE tbl_detailpenjualan.no_fak_penj = '$no_fak_penj' 
		";
		return $this->db->query($query);

	}

	function view_dpembelian_barangalkes(){

		$no_fak_penj	= $this->uri->segment(4);
		$id_admin		= $this->session->userdata('id_admin');
		
		$query = "SELECT
		tbl_detailpenjualan.no_fak_penj,
		tbl_detailpenjualan.id_barang_m,
		tbl_detailpenjualan.harga_jual,
		tbl_detailpenjualan.jumlah,
		tbl_detailpenjualan.subtotal,
		tbl_detailpenjualan.id_admin,
		tbl_barang_alkes.id_barang,
		tbl_barang_alkes.jenis_barang,
		tbl_barang_alkes.min_stok,
		tbl_barang_alkes.detail_alkes
		FROM
		tbl_detailpenjualan
		INNER JOIN tbl_barang_alkes ON tbl_barang_alkes.id_barang_m = tbl_detailpenjualan.id_barang_m
		WHERE tbl_detailpenjualan.no_fak_penj = '$no_fak_penj' 
		";
		return $this->db->query($query);

	}

	function sum_dpenjualan(){

		$no_fak_penj	= $this->uri->segment(4);
		$query = "SELECT SUM(tbl_detailpenjualan.subtotal) AS subtotals
		FROM tbl_detailpenjualan
		INNER JOIN tbl_barang_obat
		ON tbl_detailpenjualan.id_barang_m = tbl_barang_obat.id_barang_m 
		WHERE tbl_detailpenjualan.no_fak_penj = '$no_fak_penj'
		";
		return $this->db->query($query);


	}

	function view_hpiutang(){

		$id_pelanggan	= $this->uri->segment(4);
		$query = "SELECT tbl_penjualan.no_fak_penj,tanggal,tbl_penjualan.id_pelanggan,total as totalpiutang, hb.jmlbayar as totalbayar, total - hb.jmlbayar as sisabayar
		FROM tbl_penjualan
		LEFT JOIN (SELECT no_fak_penj,SUM(bayar) as jmlbayar FROM tbl_historibayarpiutang GROUP BY no_fak_penj) hb ON (tbl_penjualan.no_fak_penj = hb.no_fak_penj)

		INNER JOIN tbl_pelanggan
		ON tbl_penjualan.id_pelanggan = tbl_pelanggan.id_pelanggan 
		WHERE tbl_penjualan.id_pelanggan = '$id_pelanggan'

		GROUP BY tbl_penjualan.no_fak_penj
		";
		return $this->db->query($query);

	}

	function sum_hpiutang(){

		$id_pelanggan	= $this->uri->segment(4);
		$query = "SELECT SUM(total) as totalpiutangs
		FROM tbl_penjualan
		LEFT JOIN (SELECT no_fak_penj,SUM(bayar) as jmlbayar FROM tbl_historibayarpiutang GROUP BY no_fak_penj) hb ON (tbl_penjualan.no_fak_penj = hb.no_fak_penj)
		INNER JOIN tbl_pelanggan
		ON tbl_penjualan.id_pelanggan = tbl_pelanggan.id_pelanggan 
		WHERE tbl_penjualan.id_pelanggan = '$id_pelanggan'
		";
		return $this->db->query($query);

	}

	function view_hbayar(){

		$no_fak_penj	= $this->uri->segment(4);
		$query = "SELECT nobukti,bayar,tglbayar,tbl_historibayarpiutang.no_fak_penj,ket
		FROM tbl_historibayarpiutang
		INNER JOIN tbl_penjualan
		ON tbl_historibayarpiutang.no_fak_penj = tbl_penjualan.no_fak_penj
		WHERE tbl_historibayarpiutang.no_fak_penj = '$no_fak_penj'
		ORDER BY nobukti DESC
		";
		return $this->db->query($query);

	}

	function sum_hbayar(){

		$no_fak_penj	= $this->uri->segment(4);
		$query = "SELECT nobukti,bayar,tglbayar,tbl_historibayarpiutang.no_fak_penj,ket,SUM(bayar) AS sisabyr
		FROM tbl_historibayarpiutang
		INNER JOIN tbl_penjualan
		ON tbl_historibayarpiutang.no_fak_penj = tbl_penjualan.no_fak_penj
		WHERE tbl_historibayarpiutang.no_fak_penj = '$no_fak_penj'
		ORDER BY nobukti DESC
		";
		return $this->db->query($query);

	}

	function no_fak_penj_hbayar(){

		$no_fak_penj	= $this->uri->segment(4);
		$nobukti		= $this->uri->segment(5);
		$query = "SELECT nobukti,bayar,tglbayar,ket,tbl_historibayarpiutang.no_fak_penj,tbl_historibayarpiutang.nobukti
		FROM tbl_historibayarpiutang
		INNER JOIN tbl_penjualan
		ON tbl_historibayarpiutang.no_fak_penj = tbl_penjualan.no_fak_penj
		WHERE tbl_historibayarpiutang.no_fak_penj = '$no_fak_penj' AND tbl_historibayarpiutang.nobukti = '$nobukti'
		ORDER BY nobukti DESC
		";
		return $this->db->query($query);

	}

	function view_rpembayaran(){

		$no_fak_penj	= $this->uri->segment(4);
		$query = "SELECT cicilanke,jatuhtempo,wajibbayar,tbl_detail_rencanakredit.no_fak_penj,realisasi
		FROM tbl_penjualan
		INNER JOIN tbl_detail_rencanakredit
		ON tbl_penjualan.no_fak_penj = tbl_detail_rencanakredit.no_fak_penj WHERE tbl_detail_rencanakredit.no_fak_penj = '$no_fak_penj'
		ORDER BY cicilanke ASC
		";
		return $this->db->query($query);

	}

	function view_posting_pembayaran(){

		$no_fak_penj	= $this->uri->segment(4);
		$query = "SELECT tbl_detail_rencanakredit.cicilanke,tbl_detail_rencanakredit.jatuhtempo,tbl_detail_rencanakredit.wajibbayar,tbl_detail_rencanakredit.no_fak_penj,tbl_detail_rencanakredit.realisasi
		FROM tbl_penjualan
		INNER JOIN tbl_detail_rencanakredit
		ON tbl_penjualan.no_fak_penj = tbl_detail_rencanakredit.no_fak_penj WHERE tbl_detail_rencanakredit.no_fak_penj = '$no_fak_penj'
		ORDER BY tbl_detail_rencanakredit.wajibbayar ASC
		";
		return $this->db->query($query);

	}

	public function getDataPiutang($rowno,$rowperpage,$id_pelanggan="",$nama_pelanggan="") {
		$this->db->select('*');
		$this->db->from('view_piutang');
		$this->db->order_by('nama_pelanggan','desc');

		if($id_pelanggan != ''){
			$this->db->where('id_pelanggan', $id_pelanggan);

		}

		if($nama_pelanggan != ''){
			$this->db->like('nama_pelanggan', $nama_pelanggan);
			$this->db->or_like('alamat_pelanggan', $nama_pelanggan);
			$this->db->or_like('no_hp', $nama_pelanggan);
			$this->db->or_like('id_pelanggan', $nama_pelanggan);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordPiutangCount($id_pelanggan = "" ,$nama_pelanggan="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('view_piutang');

		if($id_pelanggan != ''){
			$this->db->where('id_pelanggan', $id_pelanggan);
		}

		if($nama_pelanggan != ''){
			$this->db->like('nama_pelanggan', $nama_pelanggan);
			$this->db->or_like('alamat_pelanggan', $nama_pelanggan);
			$this->db->or_like('no_hp', $nama_pelanggan);
			$this->db->or_like('id_pelanggan', $nama_pelanggan);

		}

		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}

	// function get_barang($id_barang_m){
	// 	$hsl=$this->db->query("SELECT * FROM barang WHERE id_barang_m='$id_barang_m'");
	// 	if($hsl->num_rows()>0){
	// 		foreach ($hsl->result() as $data) {
	// 			$hasil=array(
	// 				'id_barang_m' 	=> $data->id_barang_m,
	// 				'id_barang' 	=> $data->id_barang,
	// 				'harga_jual' 	=> $data->harga_jual,
	// 				'satuan' 		=> $data->satuan,
	// 			);
	// 		}
	// 	}
	// 	return $hasil;
	// }

	function code_otomatis(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_penjualan.no_fak_penj,9) as kode ',false);
		$this->db->where('mid(no_fak_penj,3,2)',$tahunini);
		$this->db->order_by('no_fak_penj', 'desc');
		$this->db->limit(9);
		$query 		= $this->db->get('tbl_penjualan');
		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}
		$kodemax 	= str_pad($kode,9,"0",STR_PAD_LEFT);
		$kodejadi  	= "TR".date('y').$kodemax;
		return $kodejadi;

	}

	function nobukti(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_historibayarpiutang.nobukti,9) as kode ',false);
		$this->db->where('left(nobukti,2)',$tahunini);
		$this->db->order_by('nobukti', 'desc');
		$this->db->limit(9);
		$query 		= $this->db->get('tbl_historibayarpiutang');
		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}
		$kodemax 	= str_pad($kode,9,"0",STR_PAD_LEFT);
		$kodejadi  	= date('y').$kodemax;
		return $kodejadi;

	}

	function input_tmp(){

		$id_barang_m 		= $this->input->post('id_barang_m');
		$harga_jual 		= $this->input->post('harga_jual');
		$harga_beli 		= $this->input->post('harga_beli');
		$id_barang 			= $this->input->post('id_barang');
		$exp_date 			= $this->input->post('exp_date');
		$jumlah 			= $this->input->post('jumlah');
		$subtotal 			= $this->input->post('subtotal');
		$id_dtlbarang_temp 	= $this->input->post('id_dtlbarang_temp');
		$id_admin 			= $this->session->userdata('id_admin');

		$data = array(
			
			'id_barang_m'	=> $id_barang_m,
			'harga_jual'	=> $harga_jual,
			'harga_beli'	=> $harga_beli,
			'jumlah'		=> $jumlah,
			'subtotal'		=> $subtotal,
			'id_dtlbarang_temp'=> $id_dtlbarang_temp,
			'exp_date'		=> $exp_date,
			'id_admin'		=> $id_admin,
			'id_barang'		=> $id_barang,

		);
		$this->db->insert('tbl_detailpenjualan_temp',$data);

	}


	function delete_bayar(){

		$nobukti 			= $this->uri->segment(4);
		$nofaktur 			= $this->uri->segment(4);
		$tglbayar 			= $this->input->post('tglbayar');
		$bayar 				= $this->input->post('bayar');
		$admin 				= $this->session->userdata('id_admin');

		$query = $this->db->query("DELETE FROM tbl_historibayarpiutang WHERE nobukti = '$nobukti'");

		if($query){

			$cek = $this->db->query("SELECT SUM(bayar) as jmlbayar FROM tbl_historibayarpiutang WHERE no_fak_penj ='$nofaktur'")->row_array();
			$jmlbayar = $cek['jmlbayar'];
			$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='0' WHERE  no_fak_penj='$nofaktur'");
			$q = $this->db->query("SELECT * FROM tbl_detail_rencanakredit WHERE no_fak_penj ='$nofaktur' ORDER BY cicilanke ASC")->result();

			foreach ($q as $r) {

				if($r->wajibbayar != $r->realisasi  AND $jmlbayar  >= $r->wajibbayar){

					$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='$r->wajibbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_penj='$r->no_fak_penj'");

				}else{

					if($jmlbayar > 0 AND $r->wajibbayar !=0){

						$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='$jmlbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_penj='$r->no_fak_penj'");

					}else{

						$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='0' WHERE cicilanke='$r->cicilanke' AND no_fak_penj='$r->no_fak_penj'");

					}


				}

				$jmlbayar = $jmlbayar - $r->wajibbayar;
			}

		}
		redirect('apotek/penjualan/detailpenjualan/'.$nofaktur);
		
	}

	function update_histori_piutang(){

		$nofaktur			= $this->input->post('no_fak_penj');
		$nobukti 			= $this->input->post('nobukti');
		$tglbayar 			= $this->input->post('tglbayar');
		$bayar 				= $this->input->post('bayar');
		$admin 				= $this->session->userdata('id_admin');

		$data 	= $this->db->query("SELECT  * FROM tbl_historibayarpiutang WHERE nobukti = '$nobukti'")->row_array();
		
		if($data['ket']=="DP"){
			
			$this->db->query("UPDATE penjualan SET tanggal = '$tglbayar' WHERE no_fak_penj = '$nofaktur'");
			
		}
		
		$query  = $this->db->query("UPDATE tbl_historibayarpiutang SET tglbayar = '$tglbayar',bayar='$bayar' WHERE nobukti = '$nobukti'");

		if($query){

			$cek = $this->db->query("SELECT SUM(bayar) as jmlbayar FROM tbl_historibayarpiutang WHERE no_fak_penj ='$nofaktur'")->row_array();

			$jmlbayar = $cek['jmlbayar'];
			$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='0' WHERE  no_fak_penj='$nofaktur'");
			$q = $this->db->query("SELECT * FROM tbl_detail_rencanakredit WHERE no_fak_penj ='$nofaktur' ORDER BY cicilanke ASC")->result();
			foreach ($q as $r) {

				if($r->wajibbayar != $r->realisasi  AND $jmlbayar  >= $r->wajibbayar){

					$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='$r->wajibbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_penj='$r->no_fak_penj'");

				}else{

					if($jmlbayar>0 AND $r->wajibbayar !=0){

						$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='$jmlbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_penj='$r->no_fak_penj'");

					}else{

						$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='0' WHERE cicilanke='$r->cicilanke' AND no_fak_penj='$r->no_fak_penj'");

					}


				}

				$jmlbayar = $jmlbayar - $r->wajibbayar;
			}

		}
		redirect('apotek/penjualan/detailpenjualan/'.$nofaktur);
	}

	function insert_bayar(){

		$nofaktur			= $this->input->post('no_fak_penj');
		$nobukti 			= $this->input->post('nobukti');
		$tglbayar 			= $this->input->post('tglbayar');
		$bayar 				= $this->input->post('bayar');
		$admin 				= $this->session->userdata('id_admin');


		$ququ = $this->db->query("INSERT INTO tbl_historibayarpiutang (nobukti,no_fak_penj,tglbayar,bayar,id_admin) VALUES ('$nobukti','$nofaktur','$tglbayar','$bayar','$admin')");

		if($ququ){

			$cek = $this->db->query("SELECT SUM(bayar) as jmlbayar FROM tbl_historibayarpiutang WHERE no_fak_penj ='$nofaktur'")->row_array();

			$jmlbayar = $cek['jmlbayar'];
			$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='0' WHERE  no_fak_penj='$nofaktur'");
			$q = $this->db->query("SELECT * FROM tbl_detail_rencanakredit WHERE no_fak_penj ='$nofaktur' ORDER BY cicilanke ASC")->result();
			foreach ($q as $r) {

				if($r->wajibbayar != $r->realisasi  AND $jmlbayar  >= $r->wajibbayar){

					$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='$r->wajibbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_penj='$r->no_fak_penj'");

				}else{

					if($jmlbayar>0 AND $r->wajibbayar !=0){

						$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='$jmlbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_penj='$r->no_fak_penj'");

					}else{

						$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='0' WHERE cicilanke='$r->cicilanke' AND no_fak_penj='$r->no_fak_penj'");

					}


				}

				$jmlbayar = $jmlbayar - $r->wajibbayar;
			}

		}
		redirect('apotek/penjualan/detailpenjualan/'.$nofaktur);
	}


	function insert(){

		$tglcicilan 	= $this->input->post('jatuhtempo');
		$tgl  			= substr($tglcicilan,8,2);
		$tahun 			= substr($tglcicilan,0,4);
		$bulan			= substr($tglcicilan,5,2);
		$cicilanper 	= ($this->input->post('sisa_bayar') / 1);
		$jumlah_cicilan	= 1;
		$no_fak_penj 	= $this->input->post('no_fak_penj');
		$id_pelanggan	= $this->input->post('id_pelanggan');
		$tanggal		= $this->input->post('tanggal');
		$nobukti		= $this->input->post('nobukti');
		$jatuhtempo		= $this->input->post('jatuhtempo');
		$subtotal		= $this->input->post('subtotal3');
		$pembayaran		= $this->input->post('pembayaran');
		$total			= $this->input->post('total2');
		$ppn			= $this->input->post('ppn')/100*$subtotal;
		$potongan		= $this->input->post('potongan');
		$keterangan		= $this->input->post('keterangan');
		$jenisbayar		= $this->input->post('jenisbayar');
		$id_admin		= $this->session->userdata('id_admin');

		$data = array(

			'no_fak_penj' 		=> $no_fak_penj,
			'tanggal' 			=> $tanggal,
			'id_pelanggan' 		=> $id_pelanggan,
			'subtotal' 			=> $subtotal,
			'ppn' 				=> $ppn,
			'potongan' 			=> $potongan,
			'total'				=> $total,
			'jenisbayar' 		=> $jenisbayar,
			'keterangan' 		=> $keterangan,
			'id_admin' 			=> $id_admin,

		);

		$this->db->insert('tbl_penjualan',$data);

		// Insert tbl_historibayarpiutang
		$histori = array(

			'nobukti' 			=> $nobukti,
			'no_fak_penj' 		=> $no_fak_penj,
			'tglbayar' 			=> $tanggal,
			'bayar' 			=> $pembayaran,
			'ket' 				=> $jenisbayar,
			'id_admin' 			=> $id_admin,

		);

		$this->db->insert('tbl_historibayarpiutang',$histori);

		// Insert DetailRencanaBayarKredit
		$rencanakredit = array(

			'no_fak_penj' 		=> $no_fak_penj,
			'jatuhtempo'		=> $jatuhtempo,
			'wajibbayar'		=> $total,
			'cicilanke'			=> 0,
			'realisasi'			=> $pembayaran,

		);

		$this->db->insert('tbl_detail_rencanakredit',$rencanakredit);

		// for ($i=1; $i<=$jumlah_cicilan; $i++)
		// {
		// 	$tambah 		= mktime(0,0,0,date($bulan),date($tgl),date($tahun));
		// 	$tgl_cicilan	= date("Y-m-d", $tambah);
		// 	$datark 		= array(

		// 		'no_fak_penj' => $this->input->post('no_fak_penj'),
		// 		'cicilanke'   => $i,
		// 		'jatuhtempo'  => $tgl_cicilan,
		// 		'wajibbayar'  => 0,
		// 		'realisasi'   => 0,

		// 	);
		// 	$this->db->insert('tbl_detail_rencanakredit',$datark);

		// 	$bulan++;
		// }

		$qcek 		= "SELECT SUM(bayar) as jmlbayar FROM tbl_historibayarpiutang WHERE no_fak_penj ='$no_fak_penj'";
		$cek 		= $this->db->query($qcek)->row_array();

		$jmlbayar 	= $cek['jmlbayar'];

		$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='0' WHERE no_fak_penj='$no_fak_penj'");
		$q 			= $this->db->query("SELECT * FROM tbl_detail_rencanakredit WHERE no_fak_penj='$no_fak_penj' ORDER BY cicilanke ASC")->result();

		foreach ($q as $r) {

			if($r->wajibbayar != $r->realisasi  AND $jmlbayar  >= $r->wajibbayar){

				$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='$r->wajibbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_penj='$r->no_fak_penj'");

			}else{

				if($jmlbayar > 0){

					$this->db->query("UPDATE tbl_detail_rencanakredit SET realisasi='$jmlbayar' WHERE cicilanke='$r->cicilanke' AND no_fak_penj='$r->no_fak_penj'");

				}

			}

			$jmlbayar = $jmlbayar - $r->wajibbayar;
		}

		// InsertDetailPenjualanTemp
		$inputtmp 	= $this->db->get('tbl_detailpenjualan_temp');

		foreach ($inputtmp->result() as $d){

			$datatmp = array(

				'no_fak_penj' 	=> $no_fak_penj,
				'id_barang_m' 	=> $d->id_barang_m,
				'harga_jual'	=> $d->harga_jual,
				'harga_beli'	=> $d->harga_beli,
				'jumlah' 		=> $d->jumlah,
				'subtotal' 		=> $d->subtotal,
				'id_admin' 		=> $d->id_admin,

			);

			$this->db->insert('tbl_detailpenjualan',$datatmp);

		}

		// InsertDetailPenjualanTemp
		$sisastok 	= $this->db->query("SELECT
			tbl_detailpenjualan_temp.jumlah,
			tbl_detailpenjualan_temp.id_dtlbarang_temp,
			tbl_detailpenjualan_temp.exp_date,
			tbl_detailpenjualan_temp.id_admin,
			tbl_detailpenjualan_temp.id_barang_m,
			tbl_detailbarang.id_dtlbarang,
			tbl_detailbarang.id_barang_m,
			tbl_detailbarang.stok,
			tbl_detailbarang.id_dtlbarang,
			tbl_detailbarang.stok,
			tbl_detailbarang.jumlah_perbox,
			tbl_detailbarang.harga_beli,
			tbl_detailbarang.jenis_mutasi,
			tbl_detailbarang.no_batch
			FROM
			tbl_detailpenjualan_temp
			INNER JOIN tbl_detailbarang ON tbl_detailpenjualan_temp.id_dtlbarang_temp = tbl_detailbarang.id_dtlbarang
			WHERE tbl_detailpenjualan_temp.id_admin = '$id_admin'
			ORDER BY exp_date ASC
			");

		foreach ($sisastok->result() as $d){

			$sisastok = $d->stok - $d->jumlah;
			if($d->stok  > 0){

				$this->db->query("UPDATE tbl_detailbarang SET stok = '$sisastok' WHERE tbl_detailbarang.id_barang_m = '$d->id_barang_m' AND tbl_detailbarang.id_dtlbarang ='$d->id_dtlbarang_temp' ");

			}

		}


		$query = "truncate tbl_detailpenjualan_temp";
		$this->db->query($query);
		//Notifikasi
		$this->session->set_flashdata('msg',
			'<div class="alert alert-success">
			<h4>Successfully</h4>
			<p>Data Berhasil Di Simpan</p>
			</div>');

		redirect('apotek/penjualan/detailpenjualan/'.$no_fak_penj);

	}

	function delete(){

		$kode_penjualan	= $this->uri->segment(4);
		$this->db->delete('penjualan',array('kode_penjualan' => $kode_penjualan));

	}

	function delete_histori_penj(){

		$no_fak_penj	= $this->uri->segment(4);
		$id_pelanggan	= $this->uri->segment(5);
		$this->db->query("DELETE FROM tbl_penjualan WHERE no_fak_penj = '$no_fak_penj'");
		$this->db->query("DELETE FROM tbl_historibayarpiutang WHERE no_fak_penj = '$no_fak_penj'");
		$this->db->query("DELETE FROM tbl_detail_rencanakredit WHERE no_fak_penj = '$no_fak_penj'");
		$this->db->query("DELETE FROM tbl_detailpenjualan WHERE no_fak_penj = '$no_fak_penj'");
		redirect('apotek/penjualan/view_historipiutang/'.$id_pelanggan);

	}

	function get_histori_pembayaran(){

		$nobukti 		= $this->input->post('nobukti');
		return $this->db->get_where('tbl_historibayarpiutang',array('nobukti'=>$nobukti));

	}

	function update(){

		$data = array(

			'no_fak_penj' 		=> $this->input->post('no_fak_penj'),
			'tanggal' 			=> $this->input->post('tanggal'),
			'id_pelanggan' 		=> $this->input->post('id_pelanggan'),
			'subtotal' 			=> $this->input->post('subtotal'),
			'potongan' 			=> $this->input->post('potongan'),
			'total'				=> $this->input->post('total'),
			'jenisbayar' 		=> $this->input->post('jenisbayar'),
			'id_admin' 			=> $this->input->post('id_admin'),

		);

		$kode_penjualan	= $this->uri->segment(4);
		$this->db->where('kode_penjualan',$kode_penjualan);
		$this->db->update('penjualan',$data);
		//Notifikasi
		$this->session->set_flashdata('msg',
			'<div class="alert alert-success">
			<h4>Successfully</h4>
			<p>Data Berhasil Di Ubah</p>

			</div>');
		redirect('apotek/penjualan');

	}
}