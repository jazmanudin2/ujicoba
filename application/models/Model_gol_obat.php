<?php
class Model_gol_obat extends CI_Model{

	function view_gol_obat(){

		$id_admin 	= $this->session->userdata('id_admin');
		$query 		= "SELECT * FROM tbl_gol_obat WHERE id_admin = '$id_admin' ORDER BY nama_gol_obat ASC";
		return $this->db->query($query);

	}

	function insert(){

		$data = array(
			
			'id_gol_obat' 		=> $this->input->post('id_gol_obat'),
			'nama_gol_obat' 	=> $this->input->post('nama_gol_obat'),
			'keterangan' 		=> $this->input->post('keterangan'),
			'created_at' 		=> date("Y-m-d H:i:s"),
			'id_admin' 			=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_gol_obat',$data);
		
	}

	function delete(){

		$id_gol_obat	= $this->uri->segment(4);
		$this->db->delete('tbl_gol_obat',array('id_gol_obat' => $id_gol_obat));

	}

	public function getData($rowno,$rowperpage,$id_gol_obat="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_gol_obat');
		$this->db->where('tbl_gol_obat.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_gol_obat','desc');

		if($id_gol_obat != ''){

			$this->db->where('id_gol_obat', $id_gol_obat);

		}

		if($pencarian != ''){

			$this->db->like('nama_gol_obat', $pencarian);
			$this->db->or_like('id_gol_obat', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_gol_obat = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_gol_obat');
		$this->db->where('tbl_gol_obat.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_gol_obat','desc');

		if($id_gol_obat != ''){
			$this->db->where('id_gol_obat', $id_gol_obat);
		}

		if($pencarian != ''){

			$this->db->like('nama_gol_obat', $pencarian);
			$this->db->or_like('id_gol_obat', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}
		
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function get_gol_obat(){

		$id_gol_obat 	= $this->input->post('id_gol_obat');
		$this->db->select('*');
		$this->db->from('tbl_gol_obat');
		$this->db->where('id_gol_obat',$id_gol_obat);
		$query  = $this->db->get();
		return $query;
	}

	function update(){

		$id_gol_obat	= $this->input->post('id_gol_obat');
		
		$data = array(
			
			'nama_gol_obat' 		=> $this->input->post('nama_gol_obat'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'updated_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_gol_obat',$id_gol_obat);
		$this->db->update('tbl_gol_obat',$data);
		redirect('apotek/gol_obat');

	}

	function listmerkdag(){
		
		return $this->db->get('tbl_merk_dag');
		
	}
	
	function listgol($id){
		
		$query = "SELECT * FROM tbl_gol_obat WHERE id_merk_dag = '$id'";
		return $this->db->query($query);
		
	}

}