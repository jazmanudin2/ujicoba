<?php
class Model_pelanggan extends CI_Model{

	function view_pelanggan(){

		$id_admin	= $this->session->userdata('id_admin');
		$query 		= "SELECT * FROM tbl_pelanggan WHERE tbl_pelanggan.id_admin = '$id_admin' ORDER BY nama_pelanggan ASC";
		return $this->db->query($query);

	}

	public function getData($rowno,$rowperpage,$id_pelanggan="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_pelanggan');
		$this->db->join('tbl_jenis_pelanggan','tbl_pelanggan.id_jenis_pelanggan = tbl_jenis_pelanggan.id_jenis_pelanggan');
		$this->db->join('tbl_jenis_harga','tbl_pelanggan.id_jenis_harga = tbl_jenis_harga.id_jenis_harga');
		$this->db->where('tbl_pelanggan.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_pelanggan','desc');

		if($id_pelanggan != ''){

			$this->db->where('id_pelanggan', $id_pelanggan);

		}

		if($pencarian != ''){

			$this->db->like('nama_perusahaan', $pencarian);
			$this->db->or_like('nama_jenis_pelanggan', $pencarian);
			$this->db->or_like('id_pelanggan', $pencarian);
			$this->db->or_like('nama_jenis_harga', $pencarian);
			$this->db->or_like('nama_perusahaan', $pencarian);
			$this->db->or_like('alamat', $pencarian);
			$this->db->or_like('telp_pelanggan', $pencarian);
			$this->db->or_like('catatan', $pencarian);
			$this->db->or_like('tbl_pelanggan.created_at', $pencarian);
			$this->db->or_like('tbl_pelanggan.updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_pelanggan = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_pelanggan');
		$this->db->join('tbl_jenis_pelanggan','tbl_pelanggan.id_jenis_pelanggan = tbl_jenis_pelanggan.id_jenis_pelanggan');
		$this->db->join('tbl_jenis_harga','tbl_pelanggan.id_jenis_harga = tbl_jenis_harga.id_jenis_harga');
		$this->db->where('tbl_pelanggan.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_pelanggan','desc');

		if($id_pelanggan != ''){
			$this->db->where('id_pelanggan', $id_pelanggan);
		}

		if($pencarian != ''){

			$this->db->like('nama_perusahaan', $pencarian);
			$this->db->or_like('nama_jenis_pelanggan', $pencarian);
			$this->db->or_like('id_pelanggan', $pencarian);
			$this->db->or_like('nama_jenis_harga', $pencarian);
			$this->db->or_like('nama_perusahaan', $pencarian);
			$this->db->or_like('alamat', $pencarian);
			$this->db->or_like('telp_pelanggan', $pencarian);
			$this->db->or_like('catatan', $pencarian);
			$this->db->or_like('tbl_pelanggan.created_at', $pencarian);
			$this->db->or_like('tbl_pelanggan.updated_at', $pencarian);

		}


		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}

	function insert(){

		$data = array(
			'id_pelanggan' 			=> $this->input->post('id_pelanggan'),
			'id_jenis_pelanggan' 	=> $this->input->post('id_jenis_pelanggan'),
			'id_jenis_harga' 		=> $this->input->post('id_jenis_harga'),
			'nama_pelanggan' 		=> $this->input->post('nama_pelanggan'),
			'nama_perusahaan' 		=> $this->input->post('nama_perusahaan'),
			'alamat' 				=> $this->input->post('alamat'),
			'telp_pelanggan' 		=> $this->input->post('telp_pelanggan'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'catatan' 				=> $this->input->post('catatan'),
			'created_at' 			=> date("Y-m-d H:i:s"),
			'id_admin'				=> $this->session->userdata('id_admin'),
		);
		$this->db->insert('tbl_pelanggan',$data);
		redirect('apotek/pelanggan');
	}


	function delete(){

		$id_pelanggan	= $this->uri->segment(4);
		$this->db->delete('tbl_pelanggan',array('id_pelanggan' => $id_pelanggan));

	}

	function code_otomatis(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_pelanggan.id_pelanggan,9) as kode ',false);
		$this->db->where('mid(id_pelanggan,3,2)',$tahunini);
		$this->db->order_by('id_pelanggan', 'desc');
		$this->db->limit(9);
		$query 		= $this->db->get('tbl_pelanggan');
		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}
		$kodemax 	= str_pad($kode,9,"0",STR_PAD_LEFT);
		$kodejadi  	= "PL".date('y').$kodemax;
		return $kodejadi;

	}

	function detail_pelanggan(){

		$id_pelanggan	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('tbl_pelanggan');
		$this->db->join('tbl_jenis_pelanggan','tbl_pelanggan.id_jenis_pelanggan = tbl_jenis_pelanggan.id_jenis_pelanggan');
		$this->db->join('tbl_jenis_harga','tbl_pelanggan.id_jenis_harga = tbl_jenis_harga.id_jenis_harga');
		$this->db->where('id_pelanggan',$id_pelanggan);
		$query  = $this->db->get();
		return $query;
	}

	function update(){

		$id_pelanggan	= $this->input->post('id_pelanggan');
		$data = array(

			'id_pelanggan' 			=> $this->input->post('id_pelanggan'),
			'id_jenis_pelanggan' 	=> $this->input->post('id_jenis_pelanggan'),
			'id_jenis_harga' 		=> $this->input->post('id_jenis_harga'),
			'nama_pelanggan' 		=> $this->input->post('nama_pelanggan'),
			'nama_perusahaan' 		=> $this->input->post('nama_perusahaan'),
			'alamat' 				=> $this->input->post('alamat'),
			'telp_pelanggan' 		=> $this->input->post('telp_pelanggan'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'catatan' 				=> $this->input->post('catatan'),
			'updated_at' 			=> date("Y-m-d H:i:s"),
			'id_admin'				=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_pelanggan',$id_pelanggan);
		$this->db->update('tbl_pelanggan',$data);

		redirect('apotek/pelanggan');

	}

	function get_pelanggan(){

		$id_pelanggan 	= $this->input->post('id_pelanggan');
		$this->db->select('*');
		$this->db->from('tbl_pelanggan');
		$this->db->join('tbl_jenis_pelanggan','tbl_pelanggan.id_jenis_pelanggan = tbl_jenis_pelanggan.id_jenis_pelanggan');
		$this->db->join('tbl_jenis_harga','tbl_pelanggan.id_jenis_harga = tbl_jenis_harga.id_jenis_harga');
		$this->db->where('id_pelanggan',$id_pelanggan);
		$query  = $this->db->get();
		return $query;
	}
}