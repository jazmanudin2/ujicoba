<?php
class Model_saldo extends CI_Model{

	public function getData($rowno,$rowperpage,$id_saldo="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_saldo_apt');
		$this->db->where('tbl_saldo_apt.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('tanggal','desc');
		$this->db->order_by('id_saldo','desc');

		if($id_saldo != ''){

			$this->db->where('id_saldo', $id_saldo);

		}

		if($pencarian != ''){

			$this->db->like('nama', $pencarian);
			$this->db->or_like('id_saldo', $pencarian);
			$this->db->or_like('saldo', $pencarian);
			$this->db->or_like('setor', $pencarian);
			$this->db->or_like('tanggal', $pencarian);
			$this->db->or_like('tarik', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_saldo = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_saldo_apt');
		$this->db->where('tbl_saldo_apt.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('tanggal','desc');
		$this->db->order_by('id_saldo','desc');

		if($id_saldo != ''){
			$this->db->where('id_saldo', $id_saldo);
		}

		if($pencarian != ''){

			$this->db->like('nama', $pencarian);
			$this->db->or_like('id_saldo', $pencarian);
			$this->db->or_like('saldo', $pencarian);
			$this->db->or_like('setor', $pencarian);
			$this->db->or_like('tanggal', $pencarian);
			$this->db->or_like('tarik', $pencarian);

		}

		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}

	function insert(){

		$saldo			= $this->input->post('sisa_saldo');
		$id_saldo		= $this->input->post('id_saldo');
		$data = array(

			'id_saldo' 				=> $this->input->post('id_saldo'),
			'nama' 					=> $this->input->post('nama'),
			'tanggal' 				=> $this->input->post('tanggal'),
			'setor'		 			=> $this->input->post('setor'),
			'tarik'		 			=> $this->input->post('tarik'),
			'keterangan'		 	=> $this->input->post('keterangan'),
			'nama_penginput' 		=> $this->session->userdata('username'),
			'created_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_saldo_apt',$data);

		$this->db->query("UPDATE tbl_saldo_apt SET saldo='$saldo' WHERE id_saldo='$id_saldo'");
		redirect('apotek/saldo');

	}

	function delete(){

		$id_saldo	= $this->uri->segment(4);
		$this->db->delete('tbl_saldo_apt',array('id_saldo' => $id_saldo));

	}

	function code_otomatis(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_saldo_apt.id_saldo,6) as kode ',false);
		$this->db->where('mid(id_saldo,3,2)',$tahunini);
		$this->db->order_by('id_saldo', 'desc');
		$this->db->limit(6);
		$query 		= $this->db->get('tbl_saldo_apt');
		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}
		$kodemax 	= str_pad($kode,6,"0",STR_PAD_LEFT);
		$kodejadi  	= "SD".date('y').$kodemax;
		return $kodejadi;

	}


	function get_saldo(){

		$id_saldo 	= $this->input->post('id_saldo');
		$this->db->select('*');
		$this->db->from('tbl_saldo_apt');
		$this->db->where('id_saldo',$id_saldo);
		$query  = $this->db->get();
		return $query;
	}

	function detail_saldo(){

		$id_saldo	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('tbl_saldo_apt');
		$this->db->where('id_saldo',$id_saldo);
		$query  = $this->db->get();
		return $query;
	}

	function update(){

		$id_saldo	= $this->input->post('id_saldo');
		$data = array(

			'nama' 					=> $this->input->post('nama'),
			'tanggal' 				=> $this->input->post('tanggal'),
			'setor'		 			=> $this->input->post('setor'),
			'tarik'		 			=> $this->input->post('tarik'),
			'keterangan'		 	=> $this->input->post('keterangan'),
			'nama_penginput' 		=> $this->session->userdata('username'),
			'updated_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_saldo',$id_saldo);
		$this->db->update('tbl_saldo_apt',$data);

		redirect('apotek/saldo');

	}
}