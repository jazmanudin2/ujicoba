<?php
class Model_keterangan_harga extends CI_Model{

	function view_keterangan_harga(){

		$id_admin	= $this->session->userdata('id_admin');
		$query 		= "SELECT * FROM tbl_keterangan_harga ORDER BY nama_keterangan_harga ASC";
		return $this->db->query($query);

	}

	function insert(){

		$data = array(
			
			'id_keterangan_harga' 		=> $this->input->post('id_keterangan_harga'),
			'nama_keterangan_harga' 	=> $this->input->post('nama_keterangan_harga'),
			'keterangan' 				=> $this->input->post('keterangan'),
			'created_at' 				=> date("Y-m-d H:i:s"),
			'id_admin' 					=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_keterangan_harga',$data);
		
	}

	function delete(){

		$id_keterangan_harga	= $this->uri->segment(4);
		$this->db->delete('tbl_keterangan_harga',array('id_keterangan_harga' => $id_keterangan_harga));

	}

	public function getDataJenisSup($rowno,$rowperpage,$id_keterangan_harga="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_keterangan_harga');
		$this->db->where('tbl_keterangan_harga.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_keterangan_harga','desc');

		if($id_keterangan_harga != ''){

			$this->db->where('id_keterangan_harga', $id_keterangan_harga);

		}

		if($pencarian != ''){

			$this->db->like('nama_keterangan_harga', $pencarian);
			$this->db->or_like('id_keterangan_harga', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordJenisSupCount($id_keterangan_harga = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_keterangan_harga');
		$this->db->where('tbl_keterangan_harga.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_keterangan_harga','desc');

		if($id_keterangan_harga != ''){
			$this->db->where('id_keterangan_harga', $id_keterangan_harga);
		}

		if($pencarian != ''){

			$this->db->like('nama_keterangan_harga', $pencarian);
			$this->db->or_like('id_keterangan_harga', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function get_keterangan_harga(){

		$id_keterangan_harga 	= $this->input->post('id_keterangan_harga');
		return $this->db->get_where('tbl_keterangan_harga',array('id_keterangan_harga'=>$id_keterangan_harga));

	}

	function update(){

		$id_keterangan_harga	= $this->input->post('id_keterangan_harga');
		
		$data = array(
			
			'nama_keterangan_harga' 	=> $this->input->post('nama_keterangan_harga'),
			'keterangan' 				=> $this->input->post('keterangan'),
			'updated_at' 				=> date("Y-m-d H:i:s"),
			'id_admin' 					=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_keterangan_harga',$id_keterangan_harga);
		$this->db->update('tbl_keterangan_harga',$data);
		redirect('apotek/keterangan_harga');

	}
}