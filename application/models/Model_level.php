<?php
class Model_level extends CI_Model{

	function view_level(){

		$query = "SELECT * FROM tbl_level ORDER BY nama_level ASC";
		return $this->db->query($query);

	}

	function insert(){

		$data = array(
			
			'id_level' 			=> $this->input->post('id_level'),
			'nama_level' 		=> $this->input->post('nama_level'),
			'keterangan' 		=> $this->input->post('keterangan'),
			'created_at' 		=> date("Y-m-d H:i:s"),
			'id_admin' 			=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_level',$data);
		
	}

	function delete(){

		$id_level	= $this->uri->segment(4);
		$this->db->delete('tbl_level',array('id_level' => $id_level));

	}

	public function getData($rowno,$rowperpage,$id_level="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_level');
		$this->db->where('tbl_level.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_level','desc');

		if($id_level != ''){

			$this->db->where('id_level', $id_level);

		}

		if($pencarian != ''){

			$this->db->like('nama_level', $pencarian);
			$this->db->or_like('id_level', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_level = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_level');
		$this->db->where('tbl_level.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_level','desc');

		if($id_level != ''){
			$this->db->where('id_level', $id_level);
		}

		if($pencarian != ''){

			$this->db->like('nama_level', $pencarian);
			$this->db->or_like('id_level', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function get_level(){

		$id_level 	= $this->input->post('id_level');
		return $this->db->get_where('tbl_level',array('id_level'=>$id_level));

	}

	function update(){

		$id_level	= $this->input->post('id_level');
		
		$data = array(
			
			'nama_level' 			=> $this->input->post('nama_level'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'updated_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_level',$id_level);
		$this->db->update('tbl_level',$data);
		redirect('medliz/level');

	}
}