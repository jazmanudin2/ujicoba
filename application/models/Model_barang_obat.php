<?php
class Model_barang_obat extends CI_Model{


	public function getData($rowno,$rowperpage,$id_barang_m="",$id_barang="",$id_gol_obat,$id_kandungan_obat,$merk_dag="") {
		
		$this->db->select('*');
		$this->db->from('tbl_barang_obat');
		$this->db->join('tbl_jenis_obat','tbl_barang_obat.id_jenis_obat = tbl_jenis_obat.id_jenis_obat');
		$this->db->join('tbl_gol_obat','tbl_barang_obat.id_gol_obat = tbl_gol_obat.id_gol_obat');
		$this->db->join('tbl_kandungan_obat','tbl_barang_obat.id_kandungan_obat = tbl_kandungan_obat.id_kandungan_obat');
		$this->db->where('tbl_barang_obat.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('tbl_barang_obat.id_barang_m','desc');

		if($id_barang_m != ''){

			$this->db->where('id_barang_m', $id_barang_m);

		}

		if($merk_dag != ''){

			$this->db->like('merk_dag', $merk_dag);
		}

		if($id_barang != ''){

			$this->db->like('id_barang_m', $id_barang);
			$this->db->or_like('id_barang', $id_barang);
			$this->db->or_like('dosis', $id_barang);
			$this->db->or_like('sediaan', $id_barang);
			$this->db->or_like('tbl_barang_obat.id_jenis_obat', $id_barang);
			$this->db->or_like('min_stok', $id_barang);
		}

		if($id_gol_obat != ''){

			$this->db->like('tbl_barang_obat.id_gol_obat', $id_gol_obat);
		}

		if($id_kandungan_obat != ''){

			$this->db->like('tbl_barang_obat.id_kandungan_obat', $id_kandungan_obat);
		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_barang_m = "" ,$id_barang="",$id_gol_obat,$id_kandungan_obat,$merk_dag="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_barang_obat');
		$this->db->join('tbl_jenis_obat','tbl_barang_obat.id_jenis_obat = tbl_jenis_obat.id_jenis_obat');
		$this->db->join('tbl_gol_obat','tbl_barang_obat.id_gol_obat = tbl_gol_obat.id_gol_obat');
		$this->db->join('tbl_kandungan_obat','tbl_barang_obat.id_kandungan_obat = tbl_kandungan_obat.id_kandungan_obat');
		$this->db->where('tbl_barang_obat.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('tbl_barang_obat.id_barang_m','desc');

		if($id_barang_m != ''){
			$this->db->where('id_barang_m', $id_barang_m);
		}

		if($id_barang != ''){

			$this->db->like('id_barang_m', $id_barang);
			$this->db->or_like('id_barang', $id_barang);
			$this->db->or_like('dosis', $id_barang);
			$this->db->or_like('sediaan', $id_barang);
			$this->db->or_like('tbl_barang_obat.id_jenis_obat', $id_barang);
			$this->db->or_like('min_stok', $id_barang);
		}

		if($merk_dag != ''){

			$this->db->like('merk_dag', $merk_dag);
		}

		if($id_gol_obat != ''){

			$this->db->like('tbl_barang_obat.id_gol_obat', $id_gol_obat);
		}

		if($id_kandungan_obat != ''){

			$this->db->like('tbl_barang_obat.id_kandungan_obat', $id_kandungan_obat);
		}

		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
		
	}


	function get_kandungan_obat($id_gol_obat){
		$this->db->where('id_gol_obat',$id_gol_obat);
		return $this->db->get('tbl_kandungan_obat');
	}

	function insert(){

		$id_barang_m		= $this->input->post('id_barang_m');
		$id_kandungan_obat	= $this->input->post('id_kandungan_obat');
		$id_kandungan_obat 	= implode(", ",$id_kandungan_obat);
		$kandungan_obat 	= $this->input->post('id_kandungan_obat');
		
		
		$this->load->library('zend');
		$this->zend->load('Zend/Barcode'); 
		$barcode 			= $this->input->post('id_barang_m'); 
		$imageResource 		= Zend_Barcode::factory('code128', 'image', array('text'=>$barcode), array())->draw();
		$imageName 			= $barcode.'.jpg';
		$imagePath 			= 'assets/backend/apotek/barcode/barang_obat/';
		imagejpeg($imageResource, $imagePath.$imageName); 
		$pathBarcode	 	= $imageName; 

		$data = array(

			'id_barang_m' 			=> $this->input->post('id_barang_m'),
			'id_barang' 			=> $this->input->post('id_barang'),
			'merk_dag' 				=> $this->input->post('merk_dag'),
			'id_supplier' 			=> $this->input->post('id_supplier'),
			'id_gol_obat' 			=> $this->input->post('id_gol_obat'),
			'id_kandungan_obat' 	=> $kandungan_obat,
			'dosis' 				=> $this->input->post('dosis'),
			'sediaan' 				=> $this->input->post('sediaan'),
			'id_jenis_obat' 		=> $this->input->post('id_jenis_obat'),
			'jumlah_perbox' 		=> $this->input->post('jumlah_perbox'),
			'min_stok' 				=> $this->input->post('min_stok'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'hna' 					=> $this->input->post('hna'),
			'het' 					=> $this->input->post('het'),
			'created_at' 			=> date("Y-m-d H:i:s"),
			'id_admin'				=> $this->session->userdata('id_admin'),
			'barcode'				=> $pathBarcode,

		);

		$nos = 1;
		$no  = 1;
		$h   = 'Harga';
		for ($i=1; $i<=6; $i++)
		{
			$datark 		= array(

				'id_barang_m' => $id_barang_m,
				'nama_profit' => $h .$nos++,
				'profit'  	  => '0',
				'hna'  		  => $this->input->post('hna'),
				'harga_jual'  => '0',
				'keterangan'  => $no++,
				'created_at'  => date("Y-m-d H:i:s"),
				'id_admin'    => $this->session->userdata('id_admin'),

			);
			
			$this->db->insert('tbl_jenis_harga',$datark);

		}

		$this->db->insert('tbl_barang_obat',$data);
		redirect('apotek/barang_obat');
	}

	function delete(){

		$id_barang_m	= $this->uri->segment(4);
		$this->db->delete('tbl_barang_obat',array('id_barang_m' => $id_barang_m));

	}

	function code_otomatis(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_barang_obat.id_barang_m,9) as kode ',false);
		$this->db->where('mid(id_barang_m,3,2)',$tahunini);
		$this->db->order_by('id_barang_m', 'desc');
		$this->db->limit(9);
		$query 		= $this->db->get('tbl_barang_obat');
		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}
		$kodemax 	= str_pad($kode,9,"0",STR_PAD_LEFT);
		$kodejadi  	= "BO".date('y').$kodemax;
		return $kodejadi;

	}


	function jenis_harga(){

		$id_barang_m 	= $this->input->post('id_barang_m');
		$this->db->select('*');
		$this->db->from('tbl_jenis_harga');
		$this->db->join('tbl_barang_obat','tbl_barang_obat.id_barang_m = tbl_jenis_harga.id_barang_m');
		$this->db->join('tbl_keterangan_harga','tbl_jenis_harga.keterangan = tbl_keterangan_harga.id_keterangan_harga');
		$this->db->where('tbl_jenis_harga.id_barang_m',$id_barang_m);
		$this->db->order_by('tbl_jenis_harga.id_barang_m','ASC');
		$query  = $this->db->get();
		return $query;
	}


	function get_jenis_harga(){

		$id_jenis_harga 	= $this->input->post('id_jenis_harga');
		$this->db->select('*');
		$this->db->from('tbl_jenis_harga');
		$this->db->join('tbl_barang_obat','tbl_barang_obat.id_barang_m = tbl_jenis_harga.id_barang_m');
		$this->db->join('tbl_keterangan_harga','tbl_jenis_harga.keterangan = tbl_keterangan_harga.id_keterangan_harga');
		$this->db->where('tbl_jenis_harga.id_jenis_harga',$id_jenis_harga);
		$this->db->order_by('tbl_jenis_harga.id_jenis_harga','ASC');
		$query  = $this->db->get();
		return $query;
	}


	function get_keterangan_harga(){

		$this->db->select('*');
		$this->db->from('tbl_keterangan_harga');
		$this->db->where('tbl_keterangan_harga.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('tbl_keterangan_harga.id_keterangan_harga','ASC');
		$query  = $this->db->get();
		return $query;
	}


	function get_barang(){

		$id_barang_m 	= $this->input->post('id_barang_m');
		$this->db->select('*');
		$this->db->from('tbl_barang_obat');
		$this->db->join('tbl_jenis_obat','tbl_barang_obat.id_jenis_obat = tbl_jenis_obat.id_jenis_obat');
		$this->db->join('tbl_gol_obat','tbl_barang_obat.id_gol_obat = tbl_gol_obat.id_gol_obat');
		$this->db->join('tbl_kandungan_obat','tbl_barang_obat.id_kandungan_obat = tbl_kandungan_obat.id_kandungan_obat');
		$this->db->where('tbl_barang_obat.id_admin',$this->session->userdata('id_admin'));
		$this->db->where('tbl_barang_obat.id_barang_m',$id_barang_m);
		$query  = $this->db->get();
		return $query;
	}


	function get_supplier(){

		$this->db->select('*');
		$this->db->from('tbl_supplier');
		$this->db->where('tbl_supplier.id_admin',$this->session->userdata('id_admin'));
		$query  = $this->db->get();
		return $query;
	}

	function detail_barang(){

		$id_barang_m	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('tbl_barang_obat');
		$this->db->join('tbl_jenis_obat','tbl_barang_obat.id_jenis_obat = tbl_jenis_obat.id_jenis_obat');
		$this->db->join('tbl_supplier','tbl_barang_obat.id_supplier = tbl_supplier.id_supplier');
		$this->db->join('tbl_gol_obat','tbl_barang_obat.id_gol_obat = tbl_gol_obat.id_gol_obat');
		$this->db->join('tbl_kandungan_obat','tbl_barang_obat.id_kandungan_obat = tbl_kandungan_obat.id_kandungan_obat');
		$this->db->where('tbl_barang_obat.id_admin',$this->session->userdata('id_admin'));
		$this->db->where('tbl_barang_obat.id_barang_m',$id_barang_m);
		$query  = $this->db->get();
		return $query;
	}

	function detail_tbl_detailbarang(){

		$id_barang_m	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('tbl_detailbarang');
		$this->db->join('tbl_barang_obat','tbl_barang_obat.id_barang_m = tbl_detailbarang.id_barang_m');
		$this->db->join('tbl_supplier','tbl_detailbarang.id_supplier = tbl_supplier.id_supplier');
		$this->db->where('tbl_barang_obat.id_barang_m',$id_barang_m);
		$this->db->order_by('tbl_detailbarang.stok','DESC');
		$this->db->order_by('tbl_detailbarang.exp_date','ASC');
		$query  = $this->db->get();
		return $query;
	}

	function detail_harga(){

		$id_barang_m	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('tbl_jenis_harga');
		$this->db->join('tbl_barang_obat','tbl_barang_obat.id_barang_m = tbl_jenis_harga.id_barang_m');
		$this->db->join('tbl_keterangan_harga','tbl_jenis_harga.keterangan = tbl_keterangan_harga.id_keterangan_harga');
		$this->db->where('tbl_jenis_harga.id_barang_m',$id_barang_m);
		$this->db->order_by('tbl_jenis_harga.id_jenis_harga','ASC');
		$query  = $this->db->get();
		return $query;
	}

	function update(){

		$id_barang_m		= $this->input->post('id_barang_m');
		$kandungan_obat 	= $this->input->post('id_kandungan_obat');
		
		$data = array(

			'id_barang' 			=> $this->input->post('id_barang'),
			'merk_dag' 				=> $this->input->post('merk_dag'),
			'id_supplier' 			=> $this->input->post('id_supplier'),
			'id_gol_obat' 			=> $this->input->post('id_gol_obat'),
			'id_kandungan_obat' 	=> $kandungan_obat,
			'dosis' 				=> $this->input->post('dosis'),
			'sediaan' 				=> $this->input->post('sediaan'),
			'id_jenis_obat' 		=> $this->input->post('id_jenis_obat'),
			'jumlah_perbox' 		=> $this->input->post('jumlah_perbox'),
			'min_stok' 				=> $this->input->post('min_stok'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'hna' 					=> $this->input->post('hna'),
			'het' 					=> $this->input->post('het'),
			'updated_at' 			=> date("Y-m-d H:i:s"),
			'id_admin'				=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_barang_m',$id_barang_m);
		$this->db->update('tbl_barang_obat',$data);

		redirect('apotek/barang_obat');

	}

	function update_harga(){

		$id_barang_m		= $this->input->post('id_barang_m');
		$id_jenis_harga		= $this->input->post('id_jenis_harga');
		
		$data = array(

			'nama_profit' 	=> $this->input->post('nama_profit'),
			'profit' 		=> $this->input->post('profit'),
			'harga_jual'	=> $this->input->post('harga_jual'),
			'keterangan'	=> $this->input->post('keterangan'),
			'updated_at' 	=> date("Y-m-d H:i:s"),

		);

		$this->db->where('id_jenis_harga',$id_jenis_harga);
		$this->db->update('tbl_jenis_harga',$data);

		redirect('apotek/barang_obat/detail/'.$id_barang_m);

	}

	public function upload_file($filename){

		$config['upload_path'] 		= './assets/backend/apotek/csv/barang_obat/';
		$config['allowed_types'] 	= 'csv';
		$config['max_size']			= '2048';
		$config['overwrite'] 		= true;
		$config['file_name'] 		= $filename;

		$this->upload->initialize($config); 
		if($this->upload->do_upload('file')){ 

			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{

			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}
	
	public function insert_multiple($data){

		$this->db->insert_batch('tbl_barang_obat', $data);
		
	}
}