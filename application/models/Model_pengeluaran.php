<?php
class Model_pengeluaran extends CI_Model{


	public function getData($rowno,$rowperpage,$id_pengeluaran="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_pengeluaran_apt');
		$this->db->where('tbl_pengeluaran_apt.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('tanggal','desc');
		$this->db->order_by('id_pengeluaran','desc');

		if($id_pengeluaran != ''){

			$this->db->where('id_pengeluaran', $id_pengeluaran);

		}

		if($pencarian != ''){

			$this->db->like('jenis_pengeluaran', $pencarian);
			$this->db->or_like('id_pengeluaran', $pencarian);
			$this->db->or_like('saldo', $pencarian);
			$this->db->or_like('setor', $pencarian);
			$this->db->or_like('tanggal', $pencarian);
			$this->db->or_like('tarik', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_pengeluaran="" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_pengeluaran_apt');
		$this->db->where('tbl_pengeluaran_apt.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('tanggal','desc');
		$this->db->order_by('id_pengeluaran','desc');

		if($id_pengeluaran != ''){
			$this->db->where('id_pengeluaran', $id_pengeluaran);
		}

		if($pencarian != ''){

			$this->db->like('jenis_pengeluaran', $pencarian);
			$this->db->or_like('id_pengeluaran', $pencarian);
			$this->db->or_like('saldo', $pencarian);
			$this->db->or_like('setor', $pencarian);
			$this->db->or_like('tanggal', $pencarian);
			$this->db->or_like('tarik', $pencarian);

		}

		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}

	function insert(){

		$id_admin 		= $this->session->userdata('id_admin');
		$saldo			= $this->input->post('sisa_saldo');
		$id_pengeluaran	= $this->input->post('id_pengeluaran');
		$data = array(

			'id_pengeluaran' 		=> $this->input->post('id_pengeluaran'),
			'tanggal' 				=> $this->input->post('tanggal'),
			'jenis_pengeluaran' 	=> $this->input->post('jenis_pengeluaran'),
			'penerima' 				=> $this->input->post('penerima'),
			'biaya' 				=> $this->input->post('biaya'),
			'jumlah' 				=> $this->input->post('jumlah'),
			'total' 				=> $this->input->post('total'),
			'id_saldo' 				=> $this->input->post('id_saldo'),
			'invoice' 				=> $this->input->post('invoice'),
			'keterangan'		 	=> $this->input->post('keterangan'),
			'nama_penginput' 		=> $this->session->userdata('username'),
			'created_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_pengeluaran_apt',$data);

		$data = array(

			'id_saldo' 				=> $this->input->post('id_saldo'),
			'tanggal' 				=> $this->input->post('tanggal'),
			'nama' 					=> 'Apotek',
			'tarik' 				=> $this->input->post('total'),
			'setor' 				=> 0,
			'saldo' 				=> $this->input->post('sisa_saldo'),
			'id_pemasukan_pengeluaran'=> $this->input->post('id_pengeluaran'),
			'keterangan'		 	=> $this->input->post('keterangan'),
			'nama_penginput' 		=> $this->session->userdata('username'),
			'created_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_saldo_apt',$data);
		redirect('apotek/pengeluaran');

	}


	function delete(){

		$id_pengeluaran	= $this->uri->segment(4);
		$this->db->delete('tbl_pengeluaran_apt',array('id_pengeluaran' => $id_pengeluaran));
		$this->db->delete('tbl_saldo_apt',array('id_pemasukan_pengeluaran' => $id_pengeluaran));

	}

	function code_otomatis(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_pengeluaran_apt.id_pengeluaran,6) as kode ',false);
		$this->db->where('mid(id_pengeluaran,3,2)',$tahunini);
		$this->db->order_by('id_pengeluaran', 'desc');
		$this->db->limit(6);
		$query 		= $this->db->get('tbl_pengeluaran_apt');
		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}
		$kodemax 	= str_pad($kode,6,"0",STR_PAD_LEFT);
		$kodejadi  	= "PR".date('y').$kodemax;
		return $kodejadi;

	}


	function get_pengeluaran(){

		$id_pengeluaran 	= $this->input->post('id_pengeluaran');
		$this->db->select('*');
		$this->db->from('tbl_pengeluaran_apt');
		$this->db->where('id_pengeluaran',$id_pengeluaran);
		$query  = $this->db->get();
		return $query;
	}
	function get_karyawan(){

		$id_admin 	= $this->session->userdata('id_admin');
		$this->db->select('*');
		$this->db->from('tbl_karyawan');
		$this->db->where('id_admin',$id_admin);
		$query  = $this->db->get();
		return $query;
	}

	function detail_pengeluaran(){

		$id_pengeluaran	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('tbl_pengeluaran_apt');
		$this->db->where('id_pengeluaran',$id_pengeluaran);
		$query  = $this->db->get();
		return $query;
	}

	function update(){

		$id_pengeluaran	= $this->input->post('id_pengeluaran');
		$data = array(

			'nama' 					=> $this->input->post('nama'),
			'tanggal' 				=> $this->input->post('tanggal'),
			'setor'		 			=> $this->input->post('setor'),
			'tarik'		 			=> $this->input->post('tarik'),
			'keterangan'		 	=> $this->input->post('keterangan'),
			'nama_penginput' 		=> $this->session->userdata('username'),
			'updated_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_pengeluaran',$id_pengeluaran);
		$this->db->update('tbl_pengeluaran_apt',$data);

		redirect('apotek/pengeluaran');

	}
}