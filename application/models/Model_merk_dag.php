<?php
class Model_merk_dag extends CI_Model{

	function view_merk_dag(){

		$id_admin	= $this->session->userdata('id_admin');
		$query 		= "SELECT * FROM tbl_merk_dag WHERE id_admin = '$id_admin' ORDER BY nama_merk_dag ASC";
		return $this->db->query($query);

	}

	function insert(){

		$data = array(
			
			'id_merk_dag' 		=> $this->input->post('id_merk_dag'),
			'nama_merk_dag' 	=> $this->input->post('nama_merk_dag'),
			'keterangan' 		=> $this->input->post('keterangan'),
			'created_at' 		=> date("Y-m-d H:i:s"),
			'id_admin' 			=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_merk_dag',$data);
		
	}

	function delete(){

		$id_merk_dag	= $this->uri->segment(4);
		$this->db->delete('tbl_merk_dag',array('id_merk_dag' => $id_merk_dag));

	}

	public function getData($rowno,$rowperpage,$id_merk_dag="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_merk_dag');
		$this->db->where('tbl_merk_dag.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_merk_dag','desc');

		if($id_merk_dag != ''){

			$this->db->where('id_merk_dag', $id_merk_dag);

		}

		if($pencarian != ''){

			$this->db->like('nama_merk_dag', $pencarian);
			$this->db->or_like('id_merk_dag', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_merk_dag = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_merk_dag');
		$this->db->where('tbl_merk_dag.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_merk_dag','desc');

		if($id_merk_dag != ''){
			$this->db->where('id_merk_dag', $id_merk_dag);
		}

		if($pencarian != ''){

			$this->db->like('nama_merk_dag', $pencarian);
			$this->db->or_like('id_merk_dag', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function get_merk_dag(){

		$id_merk_dag 	= $this->input->post('id_merk_dag');
		return $this->db->get_where('tbl_merk_dag',array('id_merk_dag'=>$id_merk_dag));

	}

	function update(){

		$id_merk_dag	= $this->input->post('id_merk_dag');
		
		$data = array(
			
			'nama_merk_dag' 		=> $this->input->post('nama_merk_dag'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'updated_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_merk_dag',$id_merk_dag);
		$this->db->update('tbl_merk_dag',$data);
		//Notifikasi
		$this->session->set_flashdata('msg',
			'<div class="alert alert-success">
			<h4>Successfully</h4>
			<p>Data Berhasil Di Ubah</p>

			</div>');

		redirect('apotek/merk_dag');

	}
}