<?php 

class Model_m_barang_keluar extends CI_Model{


	public function getData($rowno,$rowperpage,$id_mutasi_gudang="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('view_detail_barang_keluar');
		$this->db->where('view_detail_barang_keluar.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_mutasi_gudang','desc');

		if($id_mutasi_gudang != ''){

			$this->db->where('id_mutasi_gudang', $id_mutasi_gudang);

		}

		if($pencarian != ''){

			$this->db->like('id_mutasi_gudang', $pencarian);
			$this->db->or_like('jenis_mutasi', $pencarian);
			$this->db->or_like('tanggal', $pencarian);
			$this->db->or_like('nama_pengirim', $pencarian);
			$this->db->or_like('nama_penerima', $pencarian);
			$this->db->or_like('asal_barang_keluar', $pencarian);
			$this->db->or_like('barang_dikirim_ke', $pencarian);
			$this->db->or_like('biaya_mutasi', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_mutasi_gudang = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('view_detail_barang_keluar');
		$this->db->where('view_detail_barang_keluar.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_mutasi_gudang','desc');

		if($id_mutasi_gudang != ''){
			$this->db->where('id_mutasi_gudang', $id_mutasi_gudang);
		}

		if($pencarian != ''){

			$this->db->like('id_mutasi_gudang', $pencarian);
			$this->db->or_like('jenis_mutasi', $pencarian);
			$this->db->or_like('tanggal', $pencarian);
			$this->db->or_like('nama_pengirim', $pencarian);
			$this->db->or_like('nama_penerima', $pencarian);
			$this->db->or_like('asal_barang_keluar', $pencarian);
			$this->db->or_like('barang_dikirim_ke', $pencarian);
			$this->db->or_like('biaya_mutasi', $pencarian);

		}

		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function code_otomatis(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_m_barang_keluar.id_mutasi_gudang,9) as kode ',false);
		$this->db->where('mid(id_mutasi_gudang,3,2)',$tahunini);
		$this->db->order_by('id_mutasi_gudang','desc');
		$this->db->limit(9);
		$query 		= $this->db->get('tbl_m_barang_keluar');

		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}

		$kodemax 	= str_pad($kode,9,"0",STR_PAD_LEFT);
		$kodejadi  	= "BK".date('y').$kodemax;
		return $kodejadi;

	}


	function input_tmp(){

		$id_barang_m 		= $this->input->post('id_barang_m');
		$jumlah_perbox 		= $this->input->post('jumlah_perbox');
		$id_barang 			= $this->input->post('id_barang');
		$exp_date 			= $this->input->post('exp_date');
		$harga_satuan 		= $this->input->post('harga_satuan');
		$box 				= $this->input->post('box');
		$jumlah_satuan 		= $this->input->post('jumlah_satuan');
		$id_dtlbarang_temp	= $this->input->post('id_dtlbarang_temp');
		$subtotal 			= $this->input->post('subtotal');
		$jenis_mutasi 		= $this->input->post('jenis_mutasi');
		$id_admin		 	= $this->session->userdata('id_admin');

		$data = array(
			
			'id_barang_m'	=> $id_barang_m,
			'jumlah_perbox'	=> $jumlah_perbox,
			'id_barang'		=> $id_barang,
			'exp_date'		=> $exp_date,
			'harga_satuan'	=> $harga_satuan,
			'box'			=> $box,
			'id_dtlbarang_temp'=> $id_dtlbarang_temp,
			'jumlah_satuan'	=> $jumlah_satuan,
			'subtotal'		=> $subtotal,
			'id_admin'		=> $id_admin,
			'jenis_mutasi'	=> $jenis_mutasi,

		);
		$this->db->insert('tbl_detailmutasi_gudang_temp',$data);
	}

	function view_temp_barang_obat(){

		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailmutasi_gudang_temp.id_barang_m,
		tbl_detailmutasi_gudang_temp.jumlah_perbox,
		tbl_detailmutasi_gudang_temp.exp_date,
		tbl_detailmutasi_gudang_temp.harga_satuan,
		tbl_detailmutasi_gudang_temp.box,
		tbl_detailmutasi_gudang_temp.jumlah_satuan,
		tbl_detailmutasi_gudang_temp.subtotal,
		tbl_detailmutasi_gudang_temp.id_admin,
		tbl_detailmutasi_gudang_temp.id_barang,
		tbl_detailmutasi_gudang_temp.jenis_mutasi,
		tbl_detailmutasi_gudang_temp.id_dtlbarang_temp,
		tbl_barang_obat.id_barang,
		tbl_barang_obat.dosis,
		tbl_barang_obat.id_kandungan_obat,
		tbl_barang_obat.sediaan,
		tbl_barang_obat.merk_dag,
		tbl_gol_obat.nama_gol_obat,
		tbl_barang_obat.min_stok
		FROM
		tbl_barang_obat
		INNER JOIN tbl_detailmutasi_gudang_temp ON tbl_detailmutasi_gudang_temp.id_barang_m = tbl_barang_obat.id_barang_m
		INNER JOIN tbl_gol_obat ON tbl_barang_obat.id_gol_obat = tbl_gol_obat.id_gol_obat
		WHERE tbl_detailmutasi_gudang_temp.id_admin ='$id_admin' AND jenis_mutasi='Barang Keluar'
		";
		return $this->db->query($query);
		
	}

	function view_temp_barang_umum(){

		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailmutasi_gudang_temp.id_barang_m,
		tbl_detailmutasi_gudang_temp.jumlah_perbox,
		tbl_detailmutasi_gudang_temp.id_dtlbarang_temp,
		tbl_detailmutasi_gudang_temp.exp_date,
		tbl_detailmutasi_gudang_temp.harga_satuan,
		tbl_detailmutasi_gudang_temp.box,
		tbl_detailmutasi_gudang_temp.jumlah_satuan,
		tbl_detailmutasi_gudang_temp.subtotal,
		tbl_detailmutasi_gudang_temp.id_admin,
		tbl_detailmutasi_gudang_temp.id_barang,
		tbl_detailmutasi_gudang_temp.jenis_mutasi,
		tbl_barang_umum.id_barang,
		tbl_barang_umum.nama_barang,
		tbl_barang_umum.min_stok,
		tbl_barang_umum.gol_barang
		FROM
		tbl_detailmutasi_gudang_temp
		INNER JOIN tbl_barang_umum ON tbl_barang_umum.id_barang_m = tbl_detailmutasi_gudang_temp.id_barang_m
		WHERE tbl_detailmutasi_gudang_temp.id_admin ='$id_admin' AND jenis_mutasi='Barang Keluar'
		";
		return $this->db->query($query);
		
	}

	function view_temp_barang_alkes(){

		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailmutasi_gudang_temp.id_barang_m,
		tbl_detailmutasi_gudang_temp.jumlah_perbox,
		tbl_detailmutasi_gudang_temp.exp_date,
		tbl_detailmutasi_gudang_temp.harga_satuan,
		tbl_detailmutasi_gudang_temp.id_dtlbarang_temp,
		tbl_detailmutasi_gudang_temp.box,
		tbl_detailmutasi_gudang_temp.jumlah_satuan,
		tbl_detailmutasi_gudang_temp.subtotal,
		tbl_detailmutasi_gudang_temp.id_admin,
		tbl_detailmutasi_gudang_temp.id_barang,
		tbl_detailmutasi_gudang_temp.jenis_mutasi,
		tbl_detailmutasi_gudang_temp.jenis_mutasi,
		tbl_barang_alkes.id_barang,
		tbl_barang_alkes.jenis_barang,
		tbl_barang_alkes.detail_alkes
		FROM
		tbl_detailmutasi_gudang_temp
		INNER JOIN tbl_barang_alkes ON tbl_barang_alkes.id_barang_m = tbl_detailmutasi_gudang_temp.id_barang_m
		WHERE tbl_detailmutasi_gudang_temp.id_admin ='$id_admin' AND jenis_mutasi='Barang Keluar'
		";
		return $this->db->query($query);
		
	}


	function view_mutasi_barang_obat(){

		$id_user = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_barang_obat.id_barang_m,
		tbl_barang_obat.id_barang,
		tbl_barang_obat.merk_dag,
		tbl_barang_obat.id_gol_obat,
		tbl_barang_obat.id_kandungan_obat,
		tbl_barang_obat.dosis,
		tbl_barang_obat.sediaan,
		tbl_barang_obat.id_jenis_obat,
		tbl_barang_obat.min_stok,
		tbl_barang_obat.keterangan,
		tbl_barang_obat.created_at,
		tbl_barang_obat.updated_at,
		tbl_gol_obat.nama_gol_obat,
		tbl_barang_obat.id_admin,
		tbl_barang_obat.barcode,
		tbl_detailbarang.no_batch,
		tbl_detailbarang.id_dtlbarang,
		tbl_detailbarang.exp_date,
		tbl_detailbarang.jumlah_perbox,
		tbl_detailbarang.harga_beli,
		tbl_detailbarang.jenis_mutasi,
		tbl_detailbarang.stok AS totalstok
		FROM
		tbl_barang_obat
		INNER JOIN tbl_detailbarang ON tbl_barang_obat.id_barang_m = tbl_detailbarang.id_barang_m
		INNER JOIN tbl_gol_obat ON tbl_barang_obat.id_gol_obat = tbl_gol_obat.id_gol_obat
		INNER JOIN tbl_merk_dag ON tbl_barang_obat.merk_dag = tbl_merk_dag.merk_dag
		WHERE tbl_barang_obat.id_admin = '$id_user' AND tbl_detailbarang.stok >0
		ORDER BY
		tbl_detailbarang.exp_date ASC
		";
		return $this->db->query($query);
		
	}

	function view_mutasi_barang_umum(){

		$id_user = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailbarang.id_barang_m,
		tbl_detailbarang.stok,
		tbl_detailbarang.jumlah_perbox,
		tbl_detailbarang.exp_date,
		tbl_detailbarang.harga_beli,
		tbl_barang_umum.id_barang,
		tbl_barang_umum.nama_barang,
		tbl_detailbarang.id_dtlbarang,
		tbl_barang_umum.min_stok,
		tbl_barang_umum.gol_barang,
		tbl_barang_umum.updated_at,
		tbl_barang_umum.keterangan,
		tbl_barang_umum.id_admin,
		tbl_barang_umum.created_at,
		tbl_detailbarang.stok AS totalstok
		FROM
		tbl_detailbarang
		INNER JOIN tbl_barang_umum ON tbl_barang_umum.id_barang_m = tbl_detailbarang.id_barang_m
		WHERE tbl_barang_umum.id_admin = '$id_user' AND tbl_detailbarang.stok >0
		ORDER BY
		tbl_detailbarang.exp_date ASC
		";
		return $this->db->query($query);
		
	}

	function view_mutasi_barang_alkes(){

		$id_user = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailbarang.id_barang_m,
		tbl_detailbarang.stok,
		tbl_detailbarang.exp_date,
		tbl_detailbarang.jumlah_perbox,
		tbl_detailbarang.harga_beli,
		tbl_barang_alkes.id_barang,
		tbl_barang_alkes.jenis_barang,
		tbl_detailbarang.id_dtlbarang,
		tbl_barang_alkes.detail_alkes,
		tbl_barang_alkes.min_stok,
		tbl_barang_alkes.id_admin,
		tbl_barang_alkes.keterangan,
		tbl_barang_alkes.created_at,
		tbl_barang_alkes.updated_at,
		tbl_detailbarang.stok AS totalstok
		FROM
		tbl_detailbarang
		INNER JOIN tbl_barang_alkes ON tbl_barang_alkes.id_barang_m = tbl_detailbarang.id_barang_m
		WHERE tbl_barang_alkes.id_admin = '$id_user' AND tbl_detailbarang.stok >0
		ORDER BY
		tbl_detailbarang.exp_date ASC
		";
		return $this->db->query($query);
		
	}

	function detail(){

		$id_mutasi_gudang	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('view_detail_barang_keluar');
		$this->db->where('id_mutasi_gudang',$id_mutasi_gudang);
		$this->db->group_by('id_mutasi_gudang');
		$this->db->order_by('tanggal','ASC');
		$query  = $this->db->get();
		return $query;
	}

	function details(){

		$id_mutasi_gudang	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('view_detail_barang_keluar');
		$this->db->where('id_mutasi_gudang',$id_mutasi_gudang);
		$this->db->order_by('tanggal','ASC');
		$query  = $this->db->get();
		return $query;
	}

	function sum_temp(){

		$id_user = $this->session->userdata('id_admin');
		$query = "SELECT SUM(tbl_detailmutasi_gudang_temp.subtotal) AS subtotals
		FROM tbl_detailmutasi_gudang_temp
		WHERE tbl_detailmutasi_gudang_temp.id_admin = '$id_user' AND jenis_mutasi ='Barang Keluar'
		";
		return $this->db->query($query);
		
	}

	function insert(){

		$id_mutasi_gudang		= $this->input->post('id_mutasi_gudang');
		$tanggal				= $this->input->post('tanggal');
		$id_barang_m			= $this->input->post('id_barang_m');
		$barang_dikirim_ke 		= $this->input->post('barang_dikirim_ke');
		$id_barang 				= $this->input->post('id_barang');
		$total 					= $this->input->post('total');
		$biaya_mutasi 			= $this->input->post('biaya_mutasi');
		$pic 					= $this->input->post('pic');
		$keterangan 			= $this->input->post('keterangan');
		$nama_pengirim 			= $this->input->post('nama_pengirim');
		$nama_penerima 			= $this->input->post('nama_penerima');
		$subtotal 				= $this->input->post('subtotal5');
		$jenis_mutasi 			= $this->input->post('jenis_mutasi');
		$id_admin 				= $this->session->userdata('id_admin');

		$data = array(

			'id_mutasi_gudang' 		=> $id_mutasi_gudang,
			'tanggal' 				=> $tanggal,
			'pic_mg' 				=> $pic,
			'barang_dikirim_ke'		=> $barang_dikirim_ke,
			'nama_pengirim' 		=> $nama_pengirim,
			'nama_penerima' 		=> $nama_penerima,
			'biaya_mutasi' 			=> $biaya_mutasi,
			'jenis_mutasi' 			=> 'Barang Keluar',
			'total' 				=> $total,
			'subtotal' 				=> $subtotal,
			'id_admin' 				=> $id_admin,
			'keterangan' 			=> $keterangan,

		);

		$this->db->insert('tbl_m_barang_keluar',$data);

		$inputtmp 	= $this->db->get('tbl_detailmutasi_gudang_temp');

		foreach ($inputtmp->result() as $d){

			$datatemp = array(

				'id_mutasi_gudang' 	=> $id_mutasi_gudang,
				'id_barang_m' 		=> $d->id_barang_m,
				'jumlah_perbox'		=> $d->jumlah_perbox,
				'harga_satuan' 		=> $d->harga_satuan,
				'jumlah_box' 		=> $d->box,
				'exp_date'			=> $d->exp_date,
				'jumlah_satuan' 	=> $d->jumlah_satuan,
				'subtotal' 			=> $d->subtotal,
				'id_admin' 			=> $d->id_admin,
				'jenis_mutasi' 		=> $d->jenis_mutasi,

			);

			$this->db->insert('tbl_detailmutasi_gudang',$datatemp);

		}

		// InsertDetailPenjualanTemp
		$sisastok 	= $this->db->query("SELECT
			tbl_detailmutasi_gudang_temp.jumlah_satuan,
			tbl_detailmutasi_gudang_temp.id_dtlbarang_temp,
			tbl_detailmutasi_gudang_temp.exp_date,
			tbl_detailmutasi_gudang_temp.id_admin,
			tbl_detailmutasi_gudang_temp.id_barang_m,
			tbl_detailbarang.stok 
			FROM
			tbl_detailmutasi_gudang_temp
			INNER JOIN tbl_detailbarang ON tbl_detailmutasi_gudang_temp.id_dtlbarang_temp = tbl_detailbarang.id_dtlbarang
			WHERE id_admin = '$id_admin' AND tbl_detailmutasi_gudang_temp.jenis_mutasi ='Barang Keluar'
			ORDER BY exp_date ASC
			");

		foreach ($sisastok->result() as $d){

			$sisastok = $d->stok - $d->jumlah_satuan;
			if($d->stok  > 0){

				$this->db->query("UPDATE tbl_detailbarang SET stok = '$sisastok' WHERE id_barang_m = '$d->id_barang_m' AND id_dtlbarang = '$d->id_dtlbarang_temp' ORDER BY tbl_detailbarang.exp_date ASC");

			}

		}

		$query = "truncate tbl_detailmutasi_gudang_temp";
		$this->db->query($query);
		redirect('apotek/mutasi_barang_keluar/detail/'.$id_mutasi_gudang);

	}

}