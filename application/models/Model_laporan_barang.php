<?php

class Model_laporan_barang extends CI_Model{

	function get_pelanggan(){

		$id_admin 	  = $this->session->userdata('id_admin');
		$this->db->where('id_admin',$id_admin);
		return $this->db->get('tbl_gol_obat');
	}


	function list_barang_obat($gol_obat=""){

		if($gol_obat != ""){

			$gol_obat = "AND tbl_barang_obat.id_gol_obat = '".$gol_obat."' ";
		}

		$id_admin = $this->session->userdata('id_admin');

		$query = "SELECT
		tbl_barang_obat.id_barang_m,
		tbl_barang_obat.id_barang,
		tbl_barang_obat.merk_dag,
		tbl_barang_obat.dosis,
		tbl_barang_obat.id_gol_obat,
		tbl_barang_obat.sediaan,
		tbl_barang_obat.min_stok,
		tbl_barang_obat.hna,
		tbl_barang_obat.het,
		tbl_barang_obat.keterangan,
		tbl_barang_obat.created_at,
		tbl_barang_obat.updated_at,
		tbl_barang_obat.id_admin,
		tbl_barang_obat.barcode,
		tbl_supplier.nama_perusahaan,
		tbl_barang_obat.jumlah_perbox,
		dtl.no_batch,
		dtl.stok,
		dtl.jenis_mutasi,
		dtl.harga_beli,
		dtl.exp_date,
		tbl_gol_obat.nama_gol_obat,
		tbl_jenis_obat.nama_jenis_obat,
		tbl_kandungan_obat.id_kandungan_obat,
		tbl_kandungan_obat.nama_kandungan_obat
		FROM
		tbl_barang_obat
		LEFT JOIN (SELECT SUM(stok) AS stok,no_batch,jenis_mutasi,exp_date,harga_beli,id_barang_m FROM tbl_detailbarang  WHERE id_admin = '$id_admin' GROUP BY id_barang_m) dtl ON (tbl_barang_obat.id_barang_m = dtl.id_barang_m)
		INNER JOIN tbl_gol_obat ON tbl_barang_obat.id_gol_obat = tbl_gol_obat.id_gol_obat
		INNER JOIN tbl_jenis_obat ON tbl_barang_obat.id_jenis_obat = tbl_jenis_obat.id_jenis_obat
		INNER JOIN tbl_kandungan_obat ON tbl_barang_obat.id_kandungan_obat = tbl_kandungan_obat.id_kandungan_obat
		INNER JOIN tbl_supplier ON tbl_barang_obat.id_supplier = tbl_supplier.id_supplier
		WHERE tbl_barang_obat.id_admin ='$id_admin' "
		.$gol_obat
		."
		GROUP BY
		tbl_barang_obat.id_barang_m ";
		return $this->db->query($query);

	}

}
