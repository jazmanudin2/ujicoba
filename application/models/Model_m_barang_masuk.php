<?php 

class Model_m_barang_masuk extends CI_Model{


	public function getData($rowno,$rowperpage,$id_mutasi_gudang="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('view_detail_barang_masuk');
		$this->db->where('view_detail_barang_masuk.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_mutasi_gudang','desc');

		if($id_mutasi_gudang != ''){

			$this->db->where('id_mutasi_gudang', $id_mutasi_gudang);

		}

		if($pencarian != ''){

			$this->db->like('id_mutasi_gudang', $pencarian);
			$this->db->or_like('jenis_mutasi', $pencarian);
			$this->db->or_like('tanggal', $pencarian);
			$this->db->or_like('nama_pengirim', $pencarian);
			$this->db->or_like('nama_penerima', $pencarian);
			$this->db->or_like('asal_barang_masuk', $pencarian);
			$this->db->or_like('barang_dikirim_ke', $pencarian);
			$this->db->or_like('biaya_mutasi', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_mutasi_gudang = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('view_detail_barang_masuk');
		$this->db->where('view_detail_barang_masuk.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_mutasi_gudang','desc');

		if($id_mutasi_gudang != ''){
			$this->db->where('id_mutasi_gudang', $id_mutasi_gudang);
		}

		if($pencarian != ''){

			$this->db->like('id_mutasi_gudang', $pencarian);
			$this->db->or_like('jenis_mutasi', $pencarian);
			$this->db->or_like('tanggal', $pencarian);
			$this->db->or_like('nama_pengirim', $pencarian);
			$this->db->or_like('nama_penerima', $pencarian);
			$this->db->or_like('asal_barang_masuk', $pencarian);
			$this->db->or_like('barang_dikirim_ke', $pencarian);
			$this->db->or_like('biaya_mutasi', $pencarian);

		}

		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function code_otomatis(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_m_barang_masuk.id_mutasi_gudang,9) as kode ',false);
		$this->db->where('mid(id_mutasi_gudang,3,2)',$tahunini);
		$this->db->order_by('id_mutasi_gudang', 'desc');
		$this->db->limit(9);
		$query 		= $this->db->get('tbl_m_barang_masuk');

		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}

		$kodemax 	= str_pad($kode,9,"0",STR_PAD_LEFT);
		$kodejadi  	= "BM".date('y').$kodemax;
		return $kodejadi;

	}

	function input_tmp(){

		$id_barang_m 		= $this->input->post('id_barang_m');
		$jumlah_perbox 		= $this->input->post('jumlah_perbox');
		$id_barang 			= $this->input->post('id_barang');
		$exp_date 			= $this->input->post('exp_date');
		$harga_satuan 		= $this->input->post('harga_satuan');
		$box 				= $this->input->post('box');
		$jumlah_satuan 		= $this->input->post('jumlah_satuan');
		$subtotal 			= $this->input->post('subtotal');
		$jenis_mutasi 		= $this->input->post('jenis_mutasi');
		$id_admin		 	= $this->session->userdata('id_admin');

		$data = array(
			
			'id_barang_m'	=> $id_barang_m,
			'jumlah_perbox'	=> $jumlah_perbox,
			'id_barang'		=> $id_barang,
			'exp_date'		=> $exp_date,
			'harga_satuan'	=> $harga_satuan,
			'box'			=> $box,
			'jumlah_satuan'	=> $jumlah_satuan,
			'subtotal'		=> $subtotal,
			'id_admin'		=> $id_admin,
			'jenis_mutasi'	=> $jenis_mutasi,

		);
		$this->db->insert('tbl_detailmutasi_gudang_temp',$data);
	}

	function view_temp_barang_obat(){

		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailmutasi_gudang_temp.id_barang_m,
		tbl_detailmutasi_gudang_temp.jumlah_perbox,
		tbl_detailmutasi_gudang_temp.exp_date,
		tbl_detailmutasi_gudang_temp.harga_satuan,
		tbl_detailmutasi_gudang_temp.box,
		tbl_detailmutasi_gudang_temp.jumlah_satuan,
		tbl_detailmutasi_gudang_temp.subtotal,
		tbl_detailmutasi_gudang_temp.id_admin,
		tbl_detailmutasi_gudang_temp.id_barang,
		tbl_detailmutasi_gudang_temp.jenis_mutasi,
		tbl_barang_obat.id_barang,
		tbl_barang_obat.dosis,
		tbl_barang_obat.id_kandungan_obat,
		tbl_barang_obat.sediaan,
		tbl_barang_obat.merk_dag,
		tbl_gol_obat.nama_gol_obat,
		tbl_barang_obat.min_stok
		FROM
		tbl_barang_obat
		INNER JOIN tbl_detailmutasi_gudang_temp ON tbl_detailmutasi_gudang_temp.id_barang_m = tbl_barang_obat.id_barang_m
		INNER JOIN tbl_gol_obat ON tbl_barang_obat.id_gol_obat = tbl_gol_obat.id_gol_obat
		WHERE tbl_detailmutasi_gudang_temp.id_admin ='$id_admin' AND jenis_mutasi='Barang Masuk'
		";
		return $this->db->query($query);
		
	}

	function view_temp_barangumum(){

		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailmutasi_gudang_temp.id_barang_m,
		tbl_detailmutasi_gudang_temp.jumlah_perbox,
		tbl_detailmutasi_gudang_temp.exp_date,
		tbl_detailmutasi_gudang_temp.harga_satuan,
		tbl_detailmutasi_gudang_temp.box,
		tbl_detailmutasi_gudang_temp.jumlah_satuan,
		tbl_detailmutasi_gudang_temp.subtotal,
		tbl_detailmutasi_gudang_temp.id_admin,
		tbl_detailmutasi_gudang_temp.id_barang,
		tbl_detailmutasi_gudang_temp.jenis_mutasi,
		tbl_barang_umum.id_barang,
		tbl_barang_umum.nama_barang,
		tbl_barang_umum.min_stok,
		tbl_barang_umum.gol_barang
		FROM
		tbl_detailmutasi_gudang_temp
		INNER JOIN tbl_barang_umum ON tbl_barang_umum.id_barang_m = tbl_detailmutasi_gudang_temp.id_barang_m
		WHERE tbl_detailmutasi_gudang_temp.id_admin ='$id_admin' AND jenis_mutasi='Barang Masuk'
		";
		return $this->db->query($query);
		
	}

	function view_temp_barangalkes(){

		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailmutasi_gudang_temp.id_barang_m,
		tbl_detailmutasi_gudang_temp.jumlah_perbox,
		tbl_detailmutasi_gudang_temp.exp_date,
		tbl_detailmutasi_gudang_temp.harga_satuan,
		tbl_detailmutasi_gudang_temp.box,
		tbl_detailmutasi_gudang_temp.jumlah_satuan,
		tbl_detailmutasi_gudang_temp.subtotal,
		tbl_detailmutasi_gudang_temp.id_admin,
		tbl_detailmutasi_gudang_temp.id_barang,
		tbl_detailmutasi_gudang_temp.jenis_mutasi,
		tbl_detailmutasi_gudang_temp.jenis_mutasi,
		tbl_barang_alkes.id_barang,
		tbl_barang_alkes.jenis_barang,
		tbl_barang_alkes.detail_alkes
		FROM
		tbl_detailmutasi_gudang_temp
		INNER JOIN tbl_barang_alkes ON tbl_barang_alkes.id_barang_m = tbl_detailmutasi_gudang_temp.id_barang_m
		WHERE tbl_detailmutasi_gudang_temp.id_admin ='$id_admin' AND jenis_mutasi='Barang Masuk'
		";
		return $this->db->query($query);
		
	}


	function view_barang_obat(){

		$id_admin 	= $this->session->userdata('id_admin');
		$query = "SELECT *
		FROM
		tbl_barang_obat
		INNER JOIN tbl_jenis_obat ON tbl_jenis_obat.id_jenis_obat = tbl_barang_obat.id_jenis_obat
		INNER JOIN tbl_gol_obat ON tbl_gol_obat.id_gol_obat = tbl_barang_obat.id_gol_obat
		WHERE tbl_barang_obat.id_admin = '$id_admin' 
		GROUP BY id_barang";
		
		return $this->db->query($query);

	}

	function view_barang_alkes(){

		$id_admin 	= $this->session->userdata('id_admin');
		$query = "SELECT *
		FROM
		tbl_barang_alkes
		WHERE tbl_barang_alkes.id_admin = '$id_admin' 
		GROUP BY id_barang
		ORDER BY id_barang ASC";
		
		return $this->db->query($query);

	}

	function view_barang_umum(){

		$id_admin 	= $this->session->userdata('id_admin');
		$query = "SELECT *
		FROM
		tbl_barang_umum
		WHERE tbl_barang_umum.id_admin = '$id_admin' 
		GROUP BY id_barang
		ORDER BY id_barang ASC";
		
		return $this->db->query($query);

	}


	function detail(){

		$id_mutasi_gudang	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('view_detail_barang_masuk');
		$this->db->where('id_mutasi_gudang',$id_mutasi_gudang);
		$this->db->group_by('id_mutasi_gudang');
		$this->db->order_by('tanggal','ASC');
		$query  = $this->db->get();
		return $query;
	}

	function details(){

		$id_mutasi_gudang	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('view_detail_barang_masuk');
		$this->db->where('id_mutasi_gudang',$id_mutasi_gudang);
		$this->db->order_by('tanggal','ASC');
		$query  = $this->db->get();
		return $query;
	}

	function sum_temp(){

		$id_user = $this->session->userdata('id_admin');
		$query = "SELECT SUM(tbl_detailmutasi_gudang_temp.subtotal) AS subtotals
		FROM tbl_detailmutasi_gudang_temp
		WHERE tbl_detailmutasi_gudang_temp.id_admin = '$id_user' AND jenis_mutasi ='Barang Masuk'
		";
		return $this->db->query($query);
		
	}

	function insert(){

		$id_mutasi_gudang		= $this->input->post('id_mutasi_gudang');
		$tanggal				= $this->input->post('tanggal');
		$id_barang_m			= $this->input->post('id_barang_m');
		$asal_barang_masuk 		= $this->input->post('asal_barang_masuk');
		$id_barang 				= $this->input->post('id_barang');
		$total 					= $this->input->post('total');
		$biaya_mutasi 			= $this->input->post('biaya_mutasi');
		$pic 					= $this->input->post('pic');
		$keterangan 			= $this->input->post('keterangan');
		$nama_pengirim 			= $this->input->post('nama_pengirim');
		$nama_penerima 			= $this->input->post('nama_penerima');
		$subtotal 				= $this->input->post('subtotal5');
		$jenis_mutasi 			= $this->input->post('jenis_mutasi');
		$id_admin 				= $this->session->userdata('id_admin');

		$data = array(

			'id_mutasi_gudang' 		=> $id_mutasi_gudang,
			'tanggal' 				=> $tanggal,
			'pic_mg' 				=> $pic,
			'asal_barang_masuk'		=> $asal_barang_masuk,
			'nama_pengirim' 		=> $nama_pengirim,
			'nama_penerima' 		=> $nama_penerima,
			'biaya_mutasi' 			=> $biaya_mutasi,
			'jenis_mutasi' 			=> 'Barang Masuk',
			'total' 				=> $total,
			'subtotal' 				=> $subtotal,
			'id_admin' 				=> $id_admin,
			'keterangan' 			=> $keterangan,

		);

		$this->db->insert('tbl_m_barang_masuk',$data);

		$inputtmp 	= $this->db->get('tbl_detailmutasi_gudang_temp');

		foreach ($inputtmp->result() as $d){

			$datatemp = array(

				'id_mutasi_gudang' 	=> $id_mutasi_gudang,
				'id_barang_m' 		=> $d->id_barang_m,
				'jumlah_perbox'		=> $d->jumlah_perbox,
				'harga_satuan' 		=> $d->harga_satuan,
				'jumlah_box' 		=> $d->box,
				'exp_date'			=> $d->exp_date,
				'jumlah_satuan' 	=> $d->jumlah_satuan,
				'subtotal' 			=> $d->subtotal,
				'id_admin' 			=> $d->id_admin,
				'jenis_mutasi' 		=> $d->jenis_mutasi,

			);

			$this->db->insert('tbl_detailmutasi_gudang',$datatemp);

		}


		$mg 	= $this->db->query("SELECT * FROM tbl_m_barang_masuk WHERE id_mutasi_gudang='$id_mutasi_gudang' AND id_admin='$id_admin' ORDER BY jenis_mutasi ASC")->result();

		foreach ($mg as $r) {

			if($r->jenis_mutasi  == 'Barang Masuk' ){

				$inputtmp2 	= $this->db->get('tbl_detailmutasi_gudang_temp');

				foreach ($inputtmp2->result() as $d){

					$datatemp2 = array(

						'id_barang_m' 		=> $d->id_barang_m,
						'no_batch'			=> 0,
						'stok'				=> $d->jumlah_satuan,
						'exp_date'			=> $d->exp_date,
						'jumlah_perbox' 	=> $d->jumlah_perbox,
						'harga_beli' 		=> $d->harga_satuan,
						'jenis_mutasi' 		=> 'Barang Masuk',

					);

					$this->db->insert('tbl_detailbarang',$datatemp2);

				}
			}
		}

		$query = "truncate tbl_detailmutasi_gudang_temp";
		$this->db->query($query);
		redirect('apotek/mutasi_barang_masuk/detail/'.$id_mutasi_gudang);

	}

}