<?php
class Model_sales extends CI_Model{

	function view_sales(){

		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT * FROM tbl_sales WHERE id_admin = '$id_admin' ORDER BY nama_sales ASC";
		return $this->db->query($query);

	}

	public function getDataSales($rowno,$rowperpage,$id_sales="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_sales');
		$this->db->where('tbl_sales.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_sales','desc');

		if($id_sales != ''){

			$this->db->where('id_sales', $id_sales);

		}

		if($pencarian != ''){

			$this->db->like('nama_sales', $pencarian);
			$this->db->or_like('id_sales', $pencarian);
			$this->db->or_like('alamat', $pencarian);
			$this->db->or_like('telp_sales', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordSalesCount($id_sales = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_sales');
		$this->db->where('tbl_sales.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_sales','desc');

		if($id_sales != ''){
			$this->db->where('id_sales', $id_sales);
		}

		if($pencarian != ''){

			$this->db->like('nama_sales', $pencarian);
			$this->db->or_like('id_sales', $pencarian);
			$this->db->or_like('alamat', $pencarian);
			$this->db->or_like('telp_sales', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}

		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}

	function insert(){

		$data = array(

			'id_sales' 				=> $this->input->post('id_sales'),
			'nama_sales' 			=> $this->input->post('nama_sales'),
			'alamat' 				=> $this->input->post('alamat'),
			'telp_sales'		 	=> $this->input->post('telp_sales'),
			'created_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_sales',$data);
		redirect('apotek/sales');
	}

	function delete(){

		$id_sales	= $this->uri->segment(4);
		$this->db->delete('tbl_sales',array('id_sales' => $id_sales));

	}

	function code_otomatis(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_sales.id_sales,6) as kode ',false);
		$this->db->where('mid(id_sales,3,2)',$tahunini);
		$this->db->order_by('id_sales', 'desc');
		$this->db->limit(6);
		$query 		= $this->db->get('tbl_sales');
		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}
		$kodemax 	= str_pad($kode,6,"0",STR_PAD_LEFT);
		$kodejadi  	= "SL".date('y').$kodemax;
		return $kodejadi;

	}


	function get_sales(){

		$id_sales 	= $this->input->post('id_sales');
		$this->db->select('*');
		$this->db->from('tbl_sales');
		$this->db->where('id_sales',$id_sales);
		$query  = $this->db->get();
		return $query;
	}

	function detail_sales(){

		$id_sales	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('tbl_sales');
		$this->db->where('id_sales',$id_sales);
		$query  = $this->db->get();
		return $query;
	}

	function update(){

		$id_sales	= $this->input->post('id_sales');
		$data = array(

			'id_sales' 				=> $this->input->post('id_sales'),
			'nama_sales' 			=> $this->input->post('nama_sales'),
			'alamat' 				=> $this->input->post('alamat'),
			'telp_sales'		 	=> $this->input->post('telp_sales'),
			'updated_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_sales',$id_sales);
		$this->db->update('tbl_sales',$data);

		redirect('apotek/sales');

	}
}