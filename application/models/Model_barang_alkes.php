<?php
class Model_barang_alkes extends CI_Model{

	public function getData($rowno,$rowperpage,$id_barang_m="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_barang_alkes');
		$this->db->where('tbl_barang_alkes.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('tbl_barang_alkes.id_barang_m','desc');

		if($id_barang_m != ''){

			$this->db->where('id_barang_m', $id_barang_m);

		}

		if($pencarian != ''){

			$this->db->like('id_barang_m', $pencarian);
			$this->db->or_like('id_barang', $pencarian);
			$this->db->or_like('jenis_barang', $pencarian);
			$this->db->or_like('min_stok', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_barang_m = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_barang_alkes');
		$this->db->where('tbl_barang_alkes.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('tbl_barang_alkes.id_barang_m','desc');
		if($id_barang_m != ''){
			$this->db->where('id_barang_m', $id_barang_m);
		}


		if($pencarian != ''){

			$this->db->like('id_barang_m', $pencarian);
			$this->db->or_like('id_barang', $pencarian);
			$this->db->or_like('jenis_barang', $pencarian);
			$this->db->or_like('min_stok', $pencarian);
		}


		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}

	function insert(){
		
		$this->load->library('zend');
		$this->zend->load('Zend/Barcode'); 
		$barcode 			= $this->input->post('id_barang_m'); 
		$imageResource 		= Zend_Barcode::factory('code128', 'image', array('text'=>$barcode), array())->draw();
		$imageName 			= $barcode.'.jpg';
		$imagePath 			= 'assets/backend/apotek/barcode/barang_alkes/';
		imagejpeg($imageResource, $imagePath.$imageName); 
		$pathBarcode	 	= $imageName;  
		
		$this->ciqrcode->generate($params); 
		$data = array(

			'id_barang_m' 		=> $this->input->post('id_barang_m'),
			'id_barang' 		=> $this->input->post('id_barang'),
			'jenis_barang' 		=> $this->input->post('jenis_barang'),
			'min_stok' 			=> $this->input->post('min_stok'),
			'detail_alkes' 		=> $this->input->post('detail_alkes'),
			'keterangan' 		=> $this->input->post('keterangan'),
			'created_at' 		=> date("Y-m-d H:i:s"),
			'id_admin'			=> $this->session->userdata('id_admin'),
			'barcode'			=> $pathBarcode,

		);

		$this->db->insert('tbl_barang_alkes',$data);
		redirect('apotek/barang_alkes');
	}

	function delete(){

		$id_barang_m	= $this->uri->segment(4);
		$this->db->delete('tbl_barang_alkes',array('id_barang_m' => $id_barang_m));

	}

	function code_otomatis(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_barang_alkes.id_barang_m,9) as kode ',false);
		$this->db->where('mid(id_barang_m,3,2)',$tahunini);
		$this->db->order_by('id_barang_m', 'desc');
		$this->db->limit(9);
		$query 		= $this->db->get('tbl_barang_alkes');
		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}
		$kodemax 	= str_pad($kode,9,"0",STR_PAD_LEFT);
		$kodejadi  	= "BA".date('y').$kodemax;
		return $kodejadi;

	}


	function get_barang_alkes(){

		$id_barang_m 	= $this->input->post('id_barang_m');
		$this->db->select('*');
		$this->db->from('tbl_barang_alkes');
		$this->db->where('tbl_barang_alkes.id_admin',$this->session->userdata('id_admin'));
		$this->db->where('tbl_barang_alkes.id_barang_m',$id_barang_m);
		$query  = $this->db->get();
		return $query;
	}

	function detail_barang(){

		$id_barang_m	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('tbl_barang_alkes');
		$this->db->where('tbl_barang_alkes.id_admin',$this->session->userdata('id_admin'));
		$this->db->where('tbl_barang_alkes.id_barang_m',$id_barang_m);
		$query  = $this->db->get();
		return $query;
	}

	function detail_tbl_detailbarang(){

		$id_barang_m	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('tbl_detailbarang');
		$this->db->join('tbl_barang_alkes','tbl_barang_alkes.id_barang_m = tbl_detailbarang.id_barang_m');
		$this->db->where('tbl_barang_alkes.id_barang_m',$id_barang_m);
		$this->db->order_by('tbl_detailbarang.exp_date','ASC');
		$query  = $this->db->get();
		return $query;
	}

	function update(){

		$id_barang_m	= $this->input->post('id_barang_m');
		$data = array(

			'id_barang' 			=> $this->input->post('id_barang'),
			'jenis_barang' 			=> $this->input->post('jenis_barang'),
			'min_stok' 				=> $this->input->post('min_stok'),
			'detail_alkes' 			=> $this->input->post('detail_alkes'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'updated_at' 			=> date("Y-m-d H:i:s"),
			'id_admin'				=> $this->session->userdata('id_admin'),

		);
		$this->db->where('id_barang_m',$id_barang_m);
		$this->db->update('tbl_barang_alkes',$data);

		redirect('apotek/barang_alkes');

	}


	public function upload_file($filename){

		$config['upload_path'] 		= './assets/backend/apotek/csv/barang_alkes/';
		$config['allowed_types'] 	= 'csv';
		$config['max_size']			= '2048';
		$config['overwrite'] 		= true;
		$config['file_name'] 		= $filename;

		$this->upload->initialize($config); 
		if($this->upload->do_upload('file')){ 

			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{

			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
	}
	
	public function insert_multiple($data){

		$this->db->insert_batch('tbl_barang_alkes', $data);
		
	}
}