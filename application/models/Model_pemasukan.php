<?php
class Model_pemasukan extends CI_Model{

	public function getData($rowno,$rowperpage,$id_pemasukan="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_pemasukan_apt');
		$this->db->where('tbl_pemasukan_apt.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('tanggal','desc');
		$this->db->order_by('id_pemasukan','desc');

		if($id_pemasukan != ''){

			$this->db->where('id_pemasukan', $id_pemasukan);

		}

		if($pencarian != ''){

			$this->db->like('jenis_pemasukan', $pencarian);
			$this->db->or_like('id_pemasukan', $pencarian);
			$this->db->or_like('tanggal', $pencarian);
			$this->db->or_like('biaya', $pencarian);
			$this->db->or_like('total', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_pemasukan = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_pemasukan_apt');
		$this->db->where('tbl_pemasukan_apt.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('tanggal','desc');
		$this->db->order_by('id_pemasukan','desc');

		if($id_pemasukan != ''){
			$this->db->where('id_pemasukan', $id_pemasukan);
		}

		if($pencarian != ''){

			$this->db->like('jenis_pemasukan', $pencarian);
			$this->db->or_like('id_pemasukan', $pencarian);
			$this->db->or_like('biaya', $pencarian);
			$this->db->or_like('tanggal', $pencarian);
			$this->db->or_like('total', $pencarian);

		}

		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}

	function insert(){

		$id_admin 		= $this->session->userdata('id_admin');
		$saldo			= $this->input->post('sisa_saldo');
		$id_pemasukan	= $this->input->post('id_pemasukan');
		$data = array(

			'id_pemasukan' 			=> $this->input->post('id_pemasukan'),
			'tanggal' 				=> $this->input->post('tanggal'),
			'jenis_pemasukan' 		=> $this->input->post('jenis_pemasukan'),
			'penerima' 				=> $this->input->post('penerima'),
			'biaya' 				=> $this->input->post('biaya'),
			'jumlah' 				=> $this->input->post('jumlah'),
			'total' 				=> $this->input->post('total'),
			'id_saldo' 				=> $this->input->post('id_saldo'),
			'invoice' 				=> $this->input->post('invoice'),
			'keterangan'		 	=> $this->input->post('keterangan'),
			'nama_penginput' 		=> $this->session->userdata('username'),
			'created_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_pemasukan_apt',$data);

		$data = array(

			'id_saldo' 				=> $this->input->post('id_saldo'),
			'tanggal' 				=> $this->input->post('tanggal'),
			'nama' 					=> 'Apotek',
			'setor' 				=> $this->input->post('total'),
			'tarik' 				=> 0,
			'saldo' 				=> $this->input->post('sisa_saldo'),
			'id_pemasukan_pengeluaran'=> $this->input->post('id_pemasukan'),
			'keterangan'		 	=> $this->input->post('keterangan'),
			'nama_penginput' 		=> $this->session->userdata('username'),
			'created_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_saldo_apt',$data);
		redirect('apotek/pemasukan');

	}

	function delete(){

		$id_pemasukan	= $this->uri->segment(4);
		$this->db->delete('tbl_pemasukan_apt',array('id_pemasukan' => $id_pemasukan));
		$this->db->delete('tbl_saldo_apt',array('id_pemasukan_pengeluaran' => $id_pemasukan));

	}

	function code_otomatis(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_pemasukan_apt.id_pemasukan,6) as kode ',false);
		$this->db->where('mid(id_pemasukan,3,2)',$tahunini);
		$this->db->order_by('id_pemasukan', 'desc');
		$this->db->limit(6);
		$query 		= $this->db->get('tbl_pemasukan_apt');
		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}
		$kodemax 	= str_pad($kode,6,"0",STR_PAD_LEFT);
		$kodejadi  	= "AC".date('y').$kodemax;
		return $kodejadi;

	}


	function get_pemasukan(){

		$id_pemasukan 	= $this->input->post('id_pemasukan');
		$this->db->select('*');
		$this->db->from('tbl_pemasukan_apt');
		$this->db->join('tbl_saldo_apt','tbl_saldo_apt.id_pemasukan_pengeluaran = tbl_pemasukan_apt.id_pemasukan');
		$this->db->where('id_pemasukan',$id_pemasukan);
		$query  = $this->db->get();
		return $query;
	}
	function get_karyawan(){

		$id_admin 	= $this->session->userdata('id_admin');
		$this->db->select('*');
		$this->db->from('tbl_karyawan');
		$this->db->where('id_admin',$id_admin);
		$query  = $this->db->get();
		return $query;
	}

	function detail_pemasukan(){

		$id_pemasukan	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('tbl_pemasukan_apt');
		$this->db->where('id_pemasukan',$id_pemasukan);
		$query  = $this->db->get();
		return $query;
	}

	function update(){

		$id_saldo		= $this->input->post('id_saldo');
		$id_pemasukan	= $this->input->post('id_pemasukan');

		$data = array(

			'id_pemasukan' 			=> $this->input->post('id_pemasukan'),
			'tanggal' 				=> $this->input->post('tanggal'),
			'jenis_pemasukan' 		=> $this->input->post('jenis_pemasukan'),
			'penerima' 				=> $this->input->post('penerima'),
			'biaya' 				=> $this->input->post('biaya'),
			'jumlah' 				=> $this->input->post('jumlah'),
			'total' 				=> $this->input->post('total'),
			'id_saldo' 				=> $this->input->post('id_saldo'),
			'invoice' 				=> $this->input->post('invoice'),
			'keterangan'		 	=> $this->input->post('keterangan'),
			'nama_penginput' 		=> $this->session->userdata('username'),
			'created_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_pemasukan',$id_pemasukan);
		$this->db->update('tbl_pemasukan_apt',$data);

		$datas = array(

			'id_saldo' 				=> $this->input->post('id_saldo'),
			'tanggal' 				=> $this->input->post('tanggal'),
			'nama' 					=> 'Apotek',
			'setor' 				=> $this->input->post('total'),
			'tarik' 				=> 0,
			'saldo' 				=> $this->input->post('sisa_saldo'),
			'id_pemasukan_pengeluaran'=> $this->input->post('id_pemasukan'),
			'keterangan'		 	=> $this->input->post('keterangan'),
			'nama_penginput' 		=> $this->session->userdata('username'),
			'created_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_saldo',$id_saldo);
		$this->db->where('id_pemasukan_pengeluaran',$id_pemasukan);
		$this->db->update('tbl_saldo_apt',$datas);

		redirect('apotek/pemasukan');

	}
}