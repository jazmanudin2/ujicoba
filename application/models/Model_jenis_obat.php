<?php
class Model_jenis_obat extends CI_Model{

	function view_jenis_obat(){

		$id_admin	= $this->session->userdata('id_admin');
		$query 		= "SELECT * FROM tbl_jenis_obat WHERE id_admin = '$id_admin' ORDER BY nama_jenis_obat ASC";
		return $this->db->query($query);

	}

	function insert(){

		$data = array(
			
			'id_jenis_obat' 		=> $this->input->post('id_jenis_obat'),
			'nama_jenis_obat' 		=> $this->input->post('nama_jenis_obat'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'created_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_jenis_obat',$data);
		redirect('apotek/jenis_obat');
		
	}

	function delete(){

		$id_jenis_obat	= $this->uri->segment(4);
		$this->db->delete('tbl_jenis_obat',array('id_jenis_obat' => $id_jenis_obat));

	}

	public function getData($rowno,$rowperpage,$id_jenis_obat="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_jenis_obat');
		$this->db->where('tbl_jenis_obat.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_jenis_obat','desc');

		if($id_jenis_obat != ''){

			$this->db->where('id_jenis_obat', $id_jenis_obat);

		}

		if($pencarian != ''){

			$this->db->like('nama_jenis_obat', $pencarian);
			$this->db->or_like('id_jenis_obat', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_jenis_obat = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_jenis_obat');
		$this->db->where('tbl_jenis_obat.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_jenis_obat','desc');

		if($id_jenis_obat != ''){
			$this->db->where('id_jenis_obat', $id_jenis_obat);
		}

		if($pencarian != ''){

			$this->db->like('nama_jenis_obat', $pencarian);
			$this->db->or_like('id_jenis_obat', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function get_jenis_obat(){

		$id_jenis_obat 	= $this->input->post('id_jenis_obat');
		return $this->db->get_where('tbl_jenis_obat',array('id_jenis_obat'=>$id_jenis_obat));

	}

	function update(){

		$id_jenis_obat	= $this->input->post('id_jenis_obat');
		
		$data = array(
			
			'nama_jenis_obat' 			=> $this->input->post('nama_jenis_obat'),
			'keterangan' 				=> $this->input->post('keterangan'),
			'updated_at' 				=> date("Y-m-d H:i:s"),
			'id_admin' 					=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_jenis_obat',$id_jenis_obat);
		$this->db->update('tbl_jenis_obat',$data);
		redirect('apotek/jenis_obat');

	}
}