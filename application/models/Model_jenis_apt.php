<?php
class Model_jenis_apt extends CI_Model{

	function view_jenis_apt(){

		$id_admin 	= $this->session->userdata('id_admin');
		$query 		= "SELECT * FROM tbl_jenis_apt WHERE id_admin = '$id_admin' ORDER BY nama_jenis_apt ASC";
		return $this->db->query($query);

	}

	function insert(){

		$data = array(
			
			'id_jenis_apt' 				=> $this->input->post('id_jenis_apt'),
			'nama_jenis_apt' 			=> $this->input->post('nama_jenis_apt'),
			'keterangan' 				=> $this->input->post('keterangan'),
			'created_at' 				=> date("Y-m-d H:i:s"),
			'id_admin' 					=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_jenis_apt',$data);
		
	}

	function delete(){

		$id_jenis_apt	= $this->uri->segment(4);
		$this->db->delete('tbl_jenis_apt',array('id_jenis_apt' => $id_jenis_apt));

	}

	public function getData($rowno,$rowperpage,$id_jenis_apt="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_jenis_apt');
		$this->db->order_by('id_jenis_apt','desc');
		$this->db->where('id_admin',$this->session->userdata('id_admin'));

		if($id_jenis_apt != ''){

			$this->db->where('id_jenis_apt', $id_jenis_apt);

		}

		if($pencarian != ''){

			$this->db->like('nama_jenis_apt', $pencarian);
			$this->db->or_like('id_jenis_apt', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_jenis_apt = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_jenis_apt');
		$this->db->order_by('id_jenis_apt','desc');
		$this->db->where('id_admin',$this->session->userdata('id_admin'));

		if($id_jenis_apt != ''){
			$this->db->where('id_jenis_apt', $id_jenis_apt);
		}

		if($pencarian != ''){

			$this->db->like('nama_jenis_apt', $pencarian);
			$this->db->or_like('id_jenis_apt', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function get_jenis_apt(){

		$id_jenis_apt 	= $this->input->post('id_jenis_apt');
		return $this->db->get_where('tbl_jenis_apt',array('id_jenis_apt'=>$id_jenis_apt));

	}

	function update(){

		$id_jenis_apt	= $this->input->post('id_jenis_apt');
		
		$data = array(
			
			'nama_jenis_apt' 			=> $this->input->post('nama_jenis_apt'),
			'keterangan' 				=> $this->input->post('keterangan'),
			'updated_at' 				=> date("Y-m-d H:i:s"),
			'id_admin' 					=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_jenis_apt',$id_jenis_apt);
		$this->db->update('tbl_jenis_apt',$data);
		redirect('medliz/jenis_apt');

	}
}