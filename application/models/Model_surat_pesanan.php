<?php 

class Model_surat_pesanan extends CI_Model{

	function code_otomatis(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_surat_pesanan.id_sp,6) as kode ',false);
		$this->db->where('mid(id_sp,3,2)',$tahunini);
		$this->db->order_by('id_sp', 'desc');
		$this->db->limit(6);
		$query 		= $this->db->get('tbl_surat_pesanan');
		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}
		$kodemax 	= str_pad($kode,6,"0",STR_PAD_LEFT);
		$kodejadi  	= "SR".date('y').$kodemax;
		return $kodejadi;

	}

	function input_tmp(){

		$id_barang_m 		= $this->input->post('id_barang_m');
		$jumlah_box 		= $this->input->post('jumlah_box');
		$jumlah_satuan 		= $this->input->post('jumlah_satuan');
		$id_admin		 	= $this->session->userdata('id_admin');

		$data = array(
			
			'id_barang_m'	=> $id_barang_m,
			'jumlah_box'	=> $jumlah_box,
			'jumlah_satuan'	=> $jumlah_satuan,
			'id_admin'		=> $id_admin,

		);
		$this->db->insert('tbl_detailsuratpesanan_temp',$data);
	}

	function view_tmp(){

		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT
		tbl_detailsuratpesanan_temp.jumlah_satuan,
		tbl_detailsuratpesanan_temp.jumlah_box,
		tbl_detailsuratpesanan_temp.id_admin,
		tbl_detailsuratpesanan_temp.id_barang_m,
		tbl_barang_obat.id_barang,
		tbl_merk_dag.nama_merk_dag,
		tbl_gol_obat.nama_gol_obat,
		tbl_barang_obat.id_kandungan_obat
		FROM
		tbl_detailsuratpesanan_temp
		INNER JOIN tbl_barang_obat ON tbl_detailsuratpesanan_temp.id_barang_m = tbl_barang_obat.id_barang_m
		INNER JOIN tbl_merk_dag ON tbl_merk_dag.id_merk_dag = tbl_barang_obat.id_merk_dag
		INNER JOIN tbl_gol_obat ON tbl_gol_obat.id_gol_obat = tbl_barang_obat.id_gol_obat
		WHERE tbl_detailsuratpesanan_temp.id_admin ='$id_admin'
		";
		return $this->db->query($query);
		
	}

	public function getData($rowno,$rowperpage,$id_sp="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('view_detail_sp');
		$this->db->where('view_detail_sp.id_admin',$this->session->userdata('id_admin'));
		$this->db->group_by('id_sp');
		$this->db->order_by('id_sp','desc');

		if($id_sp != ''){

			$this->db->where('id_sp', $id_sp);

		}

		if($pencarian != ''){

			$this->db->like('nama_perusahaan', $pencarian);
			$this->db->or_like('id_sp', $pencarian);
			$this->db->or_like('id_supplier', $pencarian);
			$this->db->or_like('id_barang', $pencarian);
			$this->db->or_like('nama_gol_obat', $pencarian);
			$this->db->or_like('nama_merk_dag', $pencarian);
			$this->db->or_like('nama_perusahaan', $pencarian);
			$this->db->or_like('alamat', $pencarian);
			$this->db->or_like('telp_supplier', $pencarian);

		}


		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_sp = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('view_detail_sp');
		$this->db->where('view_detail_sp.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_sp','desc');

		if($id_sp != ''){
			$this->db->where('id_sp', $id_sp);
		}

		if($pencarian != ''){

			$this->db->like('nama_perusahaan', $pencarian);
			$this->db->or_like('id_sp', $pencarian);
			$this->db->or_like('id_supplier', $pencarian);
			$this->db->or_like('id_barang', $pencarian);
			$this->db->or_like('nama_gol_obat', $pencarian);
			$this->db->or_like('nama_merk_dag', $pencarian);
			$this->db->or_like('nama_perusahaan', $pencarian);
			$this->db->or_like('alamat', $pencarian);
			$this->db->or_like('telp_supplier', $pencarian);

		}

		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function view_barang_obat(){

		$id_admin 	= $this->session->userdata('id_admin');

		$query = "SELECT *
		FROM
		tbl_barang_obat
		INNER JOIN tbl_merk_dag ON tbl_merk_dag.id_merk_dag = tbl_barang_obat.id_merk_dag
		INNER JOIN tbl_jenis_obat ON tbl_jenis_obat.id_jenis_obat = tbl_barang_obat.id_jenis_obat
		INNER JOIN tbl_gol_obat ON tbl_gol_obat.id_gol_obat = tbl_barang_obat.id_gol_obat
		WHERE tbl_barang_obat.id_admin = '$id_admin' 
		GROUP BY id_barang
		ORDER BY id_barang ASC";
		
		return $this->db->query($query);

	}
	function insert(){

		$id_sp					= $this->input->post('id_sp');
		$tanggal				= $this->input->post('tanggal');
		$id_barang_m			= $this->input->post('id_barang_m');
		$id_supplier 			= $this->input->post('id_supplier');
		$keterangan 			= $this->input->post('keterangan');
		$suppliant 				= $this->input->post('suppliant');
		$id_admin 				= $this->session->userdata('id_admin');

		$data = array(

			'id_sp' 			=> $id_sp,
			'tanggal' 			=> $tanggal,
			'id_supplier' 		=> $id_supplier,
			'suppliant' 		=> $suppliant,
			'id_admin' 			=> $id_admin,
			'keterangan' 		=> $keterangan,

		);

		$this->db->insert('tbl_surat_pesanan',$data);

		$inputtmp 	= $this->db->get('tbl_detailsuratpesanan_temp');

		foreach ($inputtmp->result() as $d){

			$datatemp = array(

				'id_sp' 			=> $id_sp,
				'id_barang_m' 		=> $d->id_barang_m,
				'jumlah_box'		=> $d->jumlah_box,
				'jumlah_satuan' 	=> $d->jumlah_satuan,
				'id_admin' 			=> $d->id_admin,

			);

			$this->db->insert('tbl_detailsuratpesanan',$datatemp);

		}

		$query = "truncate tbl_detailsuratpesanan_temp";
		$this->db->query($query);
		redirect('apotek/surat_pesanan/input/');

	}

	function detail_sp(){

		$id_sp	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('view_detail_sp');
		$this->db->where('id_sp',$id_sp);
		$this->db->group_by('id_sp');
		$this->db->order_by('tanggal','ASC');
		$query  = $this->db->get();
		return $query;
	}

	function detail_tbl_detailsp(){

		$id_sp	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('view_detail_sp');
		$this->db->where('id_sp',$id_sp);
		$this->db->order_by('tanggal','ASC');
		$query  = $this->db->get();
		return $query;
	}


}