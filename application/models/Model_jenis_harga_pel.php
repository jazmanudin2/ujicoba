<?php
class Model_jenis_harga_pel extends CI_Model{

	function view_jenis_harga_pel(){

		$id_admin	= $this->session->userdata('id_admin');
		$query 		= "SELECT * FROM tbl_jenis_harga_pel WHERE id_admin = '$id_admin' ORDER BY nama_jenis_harga_pel ASC";
		return $this->db->query($query);

	}

	function insert(){

		$data = array(
			
			'id_jenis_harga_pel' 		=> $this->input->post('id_jenis_harga_pel'),
			'nama_jenis_harga_pel' 		=> $this->input->post('nama_jenis_harga_pel'),
			'keterangan' 				=> $this->input->post('keterangan'),
			'id_admin' 					=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_jenis_harga_pel',$data);
		
	}

	function delete(){

		$id_jenis_harga_pel	= $this->uri->segment(4);
		$this->db->delete('tbl_jenis_harga_pel',array('id_jenis_harga_pel' => $id_jenis_harga_pel));

	}

	public function getData($rowno,$rowperpage,$id_jenis_harga_pel="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_jenis_harga_pel');
		$this->db->where('tbl_jenis_harga_pel.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_jenis_harga_pel','desc');

		if($id_jenis_harga_pel != ''){

			$this->db->where('id_jenis_harga_pel', $id_jenis_harga_pel);

		}

		if($pencarian != ''){

			$this->db->like('nama_jenis_harga_pel', $pencarian);
			$this->db->or_like('id_jenis_harga_pel', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_jenis_harga_pel = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_jenis_harga_pel');
		$this->db->where('tbl_jenis_harga_pel.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_jenis_harga_pel','desc');

		if($id_jenis_harga_pel != ''){
			$this->db->where('id_jenis_harga_pel', $id_jenis_harga_pel);
		}

		if($pencarian != ''){

			$this->db->like('nama_jenis_harga_pel', $pencarian);
			$this->db->or_like('id_jenis_harga_pel', $pencarian);

		}
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function get_jenis_harga_pel(){

		$id_jenis_harga_pel 	= $this->input->post('id_jenis_harga_pel');
		return $this->db->get_where('tbl_jenis_harga_pel',array('id_jenis_harga_pel'=>$id_jenis_harga_pel));

	}

	function update(){

		$id_jenis_harga_pel	= $this->input->post('id_jenis_harga_pel');
		
		$data = array(
			
			'nama_jenis_harga_pel' 		=> $this->input->post('nama_jenis_harga_pel'),
			'keterangan' 				=> $this->input->post('keterangan'),
			'id_admin' 					=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_jenis_harga_pel',$id_jenis_harga_pel);
		$this->db->update('tbl_jenis_harga_pel',$data);
		redirect('apotek/jenis_harga_pel');

	}
}