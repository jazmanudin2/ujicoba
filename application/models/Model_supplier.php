<?php
class Model_supplier extends CI_Model{

	function view_supplier(){
		
		$id_admin = $this->session->userdata('id_admin');
		$query = "SELECT * FROM tbl_supplier WHERE id_admin = '$id_admin' ORDER BY nama_perusahaan ASC";
		return $this->db->query($query);

	}

	public function getData($rowno,$rowperpage,$id_supplier="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_supplier');
		$this->db->join('tbl_jenis_supplier','tbl_supplier.id_jenis_supplier = tbl_jenis_supplier.id_jenis_supplier');
		$this->db->join('tbl_jenis_produk','tbl_supplier.id_jenis_produk = tbl_jenis_produk.id_jenis_produk');
		$this->db->join('tbl_sales','tbl_supplier.id_sales = tbl_sales.id_sales');
		$this->db->where('tbl_supplier.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_supplier','desc');

		if($id_supplier != ''){

			$this->db->where('id_supplier', $id_supplier);

		}

		if($pencarian != ''){

			$this->db->like('nama_perusahaan', $pencarian);
			$this->db->or_like('nama_jenis_supplier', $pencarian);
			$this->db->or_like('id_supplier', $pencarian);
			$this->db->or_like('nama_jenis_produk', $pencarian);
			$this->db->or_like('nama_perusahaan', $pencarian);
			$this->db->or_like('tbl_supplier.alamat', $pencarian);
			$this->db->or_like('telp_supplier', $pencarian);
			$this->db->or_like('nama_sales', $pencarian);
			$this->db->or_like('tbl_supplier.keterangan', $pencarian);
			$this->db->or_like('catatan', $pencarian);
			$this->db->or_like('tbl_supplier.created_at', $pencarian);
			$this->db->or_like('tbl_supplier.updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_supplier = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_supplier');
		$this->db->join('tbl_jenis_supplier','tbl_supplier.id_jenis_supplier = tbl_jenis_supplier.id_jenis_supplier');
		$this->db->join('tbl_jenis_produk','tbl_supplier.id_jenis_produk = tbl_jenis_produk.id_jenis_produk');
		$this->db->join('tbl_sales','tbl_supplier.id_sales = tbl_sales.id_sales');
		$this->db->where('tbl_supplier.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_supplier','desc');

		if($id_supplier != ''){
			$this->db->where('id_supplier', $id_supplier);
		}

		if($pencarian != ''){

			$this->db->like('nama_perusahaan', $pencarian);
			$this->db->or_like('nama_jenis_supplier', $pencarian);
			$this->db->or_like('id_supplier', $pencarian);
			$this->db->or_like('nama_jenis_produk', $pencarian);
			$this->db->or_like('nama_perusahaan', $pencarian);
			$this->db->or_like('tbl_supplier.alamat', $pencarian);
			$this->db->or_like('telp_supplier', $pencarian);
			$this->db->or_like('nama_sales', $pencarian);
			$this->db->or_like('tbl_supplier.keterangan', $pencarian);
			$this->db->or_like('catatan', $pencarian);
			$this->db->or_like('tbl_supplier.created_at', $pencarian);
			$this->db->or_like('tbl_supplier.updated_at', $pencarian);

		}

		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}

	function insert(){

		$data = array(

			'id_supplier' 			=> $this->input->post('id_supplier'),
			'id_jenis_supplier' 	=> $this->input->post('id_jenis_supplier'),
			'id_jenis_produk' 		=> $this->input->post('id_jenis_produk'),
			'id_sales' 				=> $this->input->post('id_sales'),
			'nama_perusahaan' 		=> $this->input->post('nama_perusahaan'),
			'alamat' 				=> $this->input->post('alamat'),
			'telp_supplier' 		=> $this->input->post('telp_supplier'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'catatan' 				=> $this->input->post('catatan'),
			'created_at' 			=> date("Y-m-d H:i:s"),
			'id_admin'				=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_supplier',$data);
		redirect('apotek/supplier');
	}

	function delete(){

		$id_supplier	= $this->uri->segment(4);
		$this->db->delete('tbl_supplier',array('id_supplier' => $id_supplier));

	}

	function code_otomatis(){

		$tahunini	= date('y');
		$this->db->select('Right(tbl_supplier.id_supplier,6) as kode ',false);
		$this->db->where('mid(id_supplier,3,2)',$tahunini);
		$this->db->order_by('id_supplier', 'desc');
		$this->db->limit(6);
		$query 		= $this->db->get('tbl_supplier');
		if($query->num_rows()<>0){
			$data 	= $query->row();
			$kode 	= intval($data->kode)+1;
		}else{
			$kode 	= 1;
		}
		$kodemax 	= str_pad($kode,6,"0",STR_PAD_LEFT);
		$kodejadi  	= "SP".date('y').$kodemax;
		return $kodejadi;

	}


	function get_supplier(){

		$id_supplier 	= $this->input->post('id_supplier');
		$this->db->select('*');
		$this->db->from('tbl_supplier');
		$this->db->join('tbl_jenis_supplier','tbl_supplier.id_jenis_supplier = tbl_jenis_supplier.id_jenis_supplier');
		$this->db->join('tbl_jenis_produk','tbl_supplier.id_jenis_produk = tbl_jenis_produk.id_jenis_produk');
		$this->db->join('tbl_sales','tbl_supplier.id_sales = tbl_sales.id_sales');
		$this->db->where('id_supplier',$id_supplier);
		$query  = $this->db->get();
		return $query;
	}

	function detail_supplier(){

		$id_supplier	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('tbl_supplier');
		$this->db->join('tbl_jenis_supplier','tbl_supplier.id_jenis_supplier = tbl_jenis_supplier.id_jenis_supplier');
		$this->db->join('tbl_jenis_produk','tbl_supplier.id_jenis_produk = tbl_jenis_produk.id_jenis_produk');
		$this->db->join('tbl_sales','tbl_supplier.id_sales = tbl_sales.id_sales');
		$this->db->where('id_supplier',$id_supplier);
		$query  = $this->db->get();
		return $query;
	}

	function update(){

		$id_supplier	= $this->input->post('id_supplier');
		$data = array(

			'id_supplier' 			=> $this->input->post('id_supplier'),
			'id_jenis_supplier' 	=> $this->input->post('id_jenis_supplier'),
			'id_sales' 				=> $this->input->post('id_sales'),
			'id_jenis_produk' 		=> $this->input->post('id_jenis_produk'),
			'nama_perusahaan' 		=> $this->input->post('nama_perusahaan'),
			'alamat' 				=> $this->input->post('alamat'),
			'telp_supplier' 		=> $this->input->post('telp_supplier'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'catatan' 				=> $this->input->post('catatan'),
			'updated_at' 			=> date("Y-m-d H:i:s"),
			'id_admin'				=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_supplier',$id_supplier);
		$this->db->update('tbl_supplier',$data);

		redirect('apotek/supplier');

	}
}