<?php
class Model_kandungan_obat extends CI_Model{

	function view_kandungan_obat(){

		$id_admin 	= $this->session->userdata('id_admin');
		$query 		= "SELECT * FROM tbl_kandungan_obat WHERE id_admin = '$id_admin' ORDER BY nama_kandungan_obat ASC";
		return $this->db->query($query);

	}

	function insert(){

		$data = array(
			
			'id_kandungan_obat' 	=> $this->input->post('id_kandungan_obat'),
			'nama_kandungan_obat' 	=> $this->input->post('nama_kandungan_obat'),
			'id_gol_obat' 			=> $this->input->post('id_gol_obat'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'created_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_kandungan_obat',$data);
		
	}

	function delete(){

		$id_kandungan_obat	= $this->uri->segment(4);
		$this->db->delete('tbl_kandungan_obat',array('id_kandungan_obat' => $id_kandungan_obat));

	}

	public function getData($rowno,$rowperpage,$id_kandungan_obat="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_kandungan_obat');
		$this->db->join('tbl_gol_obat','tbl_gol_obat.id_gol_obat = tbl_kandungan_obat.id_gol_obat');
		$this->db->where('tbl_kandungan_obat.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_kandungan_obat','desc');

		if($id_kandungan_obat != ''){

			$this->db->where('id_kandungan_obat', $id_kandungan_obat);

		}

		if($pencarian != ''){

			$this->db->like('nama_kandungan_obat', $pencarian);
			$this->db->or_like('id_kandungan_obat', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_kandungan_obat = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_kandungan_obat');
		$this->db->join('tbl_gol_obat','tbl_gol_obat.id_gol_obat = tbl_kandungan_obat.id_gol_obat');
		$this->db->where('tbl_kandungan_obat.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_kandungan_obat','desc');

		if($id_kandungan_obat != ''){
			$this->db->where('id_kandungan_obat', $id_kandungan_obat);
		}

		if($pencarian != ''){

			$this->db->like('nama_kandungan_obat', $pencarian);
			$this->db->or_like('id_kandungan_obat', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function get_kandungan_obat(){

		$id_kandungan_obat 	= $this->input->post('id_kandungan_obat');
		$this->db->select('*');
		$this->db->from('tbl_kandungan_obat');
		$this->db->join('tbl_gol_obat','tbl_gol_obat.id_gol_obat = tbl_kandungan_obat.id_gol_obat');
		$this->db->where('id_kandungan_obat',$id_kandungan_obat);
		$query  = $this->db->get();
		return $query;
	}

	function update(){

		$id_kandungan_obat	= $this->input->post('id_kandungan_obat');
		
		$data = array(
			
			'nama_kandungan_obat' 	=> $this->input->post('nama_kandungan_obat'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'updated_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_kandungan_obat',$id_kandungan_obat);
		$this->db->update('tbl_kandungan_obat',$data);
		redirect('apotek/kandungan_obat');

	}

}