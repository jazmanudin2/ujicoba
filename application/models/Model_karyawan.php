<?php
class Model_karyawan extends CI_Model{

	function view_karyawan(){

		$id_admin	= $this->session->userdata('id_admin');
		$query 		= "SELECT * FROM tbl_karyawan WHERE id_admin='$id_admin' ";
		return $this->db->query($query);

	}


	public function getData($rowno,$rowperpage,$nik="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_karyawan');
		$this->db->join('tbl_provinsi','tbl_karyawan.id_provinsi = tbl_provinsi.id_provinsi');
		$this->db->join('tbl_kota','tbl_karyawan.id_kota = tbl_kota.id_kota');
		$this->db->join('tbl_kecamatan','tbl_karyawan.id_kecamatan = tbl_kecamatan.id_kecamatan');
		$this->db->join('tbl_desa','tbl_karyawan.id_desa = tbl_desa.id_desa');
		$this->db->where('tbl_karyawan.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('nik','desc');

		if($nik != ''){

			$this->db->where('nik', $nik);

		}

		if($pencarian != ''){

			$this->db->like('nama_depan', $pencarian);
			$this->db->or_like('nama_belakang', $pencarian);
			$this->db->or_like('nik', $pencarian);
			$this->db->or_like('tempat_lahir', $pencarian);
			$this->db->or_like('jk', $pencarian);
			$this->db->or_like('alamat', $pencarian);
			$this->db->or_like('nama_provinsi', $pencarian);
			$this->db->or_like('nama_kota', $pencarian);
			$this->db->or_like('nama_kecamatan', $pencarian);
			$this->db->or_like('nama_desa', $pencarian);
			$this->db->or_like('pendidikan_terakhir', $pencarian);
			$this->db->or_like('telp', $pencarian);
			$this->db->or_like('no_hp', $pencarian);
			$this->db->or_like('email', $pencarian);
			$this->db->or_like('tgl_lahir', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($nik = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_karyawan');
		$this->db->join('tbl_provinsi','tbl_karyawan.id_provinsi = tbl_provinsi.id_provinsi');
		$this->db->join('tbl_kota','tbl_karyawan.id_kota = tbl_kota.id_kota');
		$this->db->join('tbl_kecamatan','tbl_karyawan.id_kecamatan = tbl_kecamatan.id_kecamatan');
		$this->db->join('tbl_desa','tbl_karyawan.id_desa = tbl_desa.id_desa');
		$this->db->where('tbl_karyawan.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('nik','desc');

		if($nik != ''){
			$this->db->where('nik', $nik);
		}

		if($pencarian != ''){

			$this->db->like('nama_depan', $pencarian);
			$this->db->or_like('nama_belakang', $pencarian);
			$this->db->or_like('nik', $pencarian);
			$this->db->or_like('tempat_lahir', $pencarian);
			$this->db->or_like('tempat_lahir', $pencarian);
			$this->db->or_like('jk', $pencarian);
			$this->db->or_like('telp', $pencarian);
			$this->db->or_like('alamat', $pencarian);
			$this->db->or_like('nama_provinsi', $pencarian);
			$this->db->or_like('nama_kota', $pencarian);
			$this->db->or_like('nama_kecamatan', $pencarian);
			$this->db->or_like('nama_desa', $pencarian);
			$this->db->or_like('pendidikan_terakhir', $pencarian);
			$this->db->or_like('no_hp', $pencarian);
			$this->db->or_like('email', $pencarian);
			$this->db->or_like('tgl_lahir', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}

		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function insert(){
		
		$data = array(

			'nik' 					=> $this->input->post('nik'),
			'id_jabatan' 			=> $this->input->post('id_jabatan'),
			'nama_depan' 			=> $this->input->post('nama_depan'),
			'nama_belakang'			=> $this->input->post('nama_belakang'),
			'jk' 					=> $this->input->post('jk'),
			'pendidikan_terakhir'	=> $this->input->post('pendidikan_terakhir'),
			'tempat_lahir' 			=> $this->input->post('tempat_lahir'),
			'tgl_lahir' 			=> $this->input->post('tgl_lahir'),
			'agama' 				=> $this->input->post('agama'),
			'alamat' 				=> $this->input->post('alamat'),
			'id_provinsi' 			=> $this->input->post('provinsi'),
			'id_kota'		 		=> $this->input->post('kabupaten'),
			'id_kecamatan' 			=> $this->input->post('kecamatan'),
			'id_desa' 				=> $this->input->post('desa'),
			'telp' 					=> $this->input->post('telp'),
			'no_hp' 				=> $this->input->post('no_hp'),
			'email' 				=> $this->input->post('email'),
			'mulai_kerja' 			=> $this->input->post('mulai_kerja'),
			'status' 				=> $this->input->post('status'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'id_admin' 				=> $this->session->userdata('id_admin'),
			'created_at' 			=> date("Y-m-d H:i:s"),

		);

		$this->db->insert('tbl_karyawan',$data);
		redirect('apotek/karyawan');
	}


	function delete(){

		$nik	= $this->uri->segment(4);
		$this->db->delete('tbl_karyawan',array('nik' => $nik));

	}

	function get_karyawan(){

		$nik 	= $this->input->post('nik');
		$this->db->select('*');
		$this->db->from('tbl_karyawan');
		$this->db->join('tbl_provinsi','tbl_karyawan.id_provinsi = tbl_provinsi.id_provinsi');
		$this->db->join('tbl_kota','tbl_karyawan.id_kota = tbl_kota.id_kota');
		$this->db->join('tbl_kecamatan','tbl_karyawan.id_kecamatan = tbl_kecamatan.id_kecamatan');
		$this->db->join('tbl_desa','tbl_karyawan.id_desa = tbl_desa.id_desa');
		$this->db->join('tbl_jabatan','tbl_karyawan.id_jabatan = tbl_jabatan.id_jabatan');
		$this->db->where('tbl_karyawan.id_admin',$this->session->userdata('id_admin'));
		$this->db->where('nik',$nik);
		$query  = $this->db->get();
		return $query;
	}

	function detail_karyawan(){

		$nik	= $this->uri->segment(4);
		$this->db->select('*');
		$this->db->from('tbl_karyawan');
		$this->db->join('tbl_provinsi','tbl_karyawan.id_provinsi = tbl_provinsi.id_provinsi');
		$this->db->join('tbl_jabatan','tbl_karyawan.id_jabatan = tbl_jabatan.id_jabatan');
		$this->db->join('tbl_kota','tbl_karyawan.id_kota = tbl_kota.id_kota');
		$this->db->join('tbl_kecamatan','tbl_karyawan.id_kecamatan = tbl_kecamatan.id_kecamatan');
		$this->db->join('tbl_desa','tbl_karyawan.id_desa = tbl_desa.id_desa');
		$this->db->where('tbl_karyawan.id_admin',$this->session->userdata('id_admin'));
		$this->db->where('nik',$nik);
		$query  = $this->db->get();
		return $query;
	}

	function update(){

		$nik						= $this->input->post('nik');

		$data = array(

			'id_jabatan' 			=> $this->input->post('id_jabatan'),
			'nama_depan' 			=> $this->input->post('nama_depan'),
			'nama_belakang'			=> $this->input->post('nama_belakang'),
			'jk' 					=> $this->input->post('jk'),
			'pendidikan_terakhir'	=> $this->input->post('pendidikan_terakhir'),
			'tempat_lahir' 			=> $this->input->post('tempat_lahir'),
			'tgl_lahir' 			=> $this->input->post('tgl_lahir'),
			'agama' 				=> $this->input->post('agama'),
			'alamat' 				=> $this->input->post('alamat'),
			'id_provinsi' 			=> $this->input->post('id_provinsi'),
			'id_kota'		 		=> $this->input->post('id_kota'),
			'id_kecamatan' 			=> $this->input->post('id_kecamatan'),
			'id_desa' 				=> $this->input->post('id_desa'),
			'telp' 					=> $this->input->post('telp'),
			'no_hp' 				=> $this->input->post('no_hp'),
			'email' 				=> $this->input->post('email'),
			'mulai_kerja' 			=> $this->input->post('mulai_kerja'),
			'status' 				=> $this->input->post('status'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'id_admin' 				=> $this->session->userdata('id_admin'),
			'updated_at' 			=> date("Y-m-d H:i:s"),

		);

		$this->db->where('nik',$nik);
		$this->db->update('tbl_karyawan',$data);

		redirect('apotek/karyawan');

	}

	function upload($foto,$nik){
		
	}
}