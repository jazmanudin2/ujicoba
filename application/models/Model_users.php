<?php
class Model_users extends CI_Model{

	function insert(){

		$data = array(
			
			'id_users' 		=> $this->input->post('id_users'),
			'username' 		=> $this->input->post('username'),
			'password' 		=> $this->input->post('password'),
			'email' 		=> $this->input->post('email'),
			'status' 		=> $this->input->post('status'),
			'id_level' 		=> $this->input->post('id_level'),
			'id_apotek' 	=> $this->input->post('id_apotek'),
			'status' 		=> 'Aktif',
			'created_at' 	=> date("Y-m-d H:i:s"),

		);

		$this->db->insert('tbl_users',$data);
		
	}

	function delete(){

		$id_users	= $this->uri->segment(4);
		$this->db->delete('tbl_users',array('id_users' => $id_users));

	}

	public function getData($rowno,$rowperpage,$id_users="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_users');
		$this->db->join('tbl_level','tbl_users.id_level = tbl_level.nama_level');
		$this->db->join('tbl_data_apt','tbl_users.id_admin = tbl_data_apt.id_apotek');
		$this->db->order_by('id_users','desc');

		if($id_users != ''){

			$this->db->where('id_users', $id_users);

		}

		if($pencarian != ''){

			$this->db->like('username', $pencarian);
			$this->db->or_like('id_users', $pencarian);
			$this->db->or_like('id_level', $pencarian);
			$this->db->or_like('nama_level', $pencarian);
			$this->db->or_like('email', $pencarian);
			$this->db->or_like('status', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_users = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_users');
		$this->db->join('tbl_level','tbl_users.id_level = tbl_level.nama_level');
		$this->db->join('tbl_data_apt','tbl_users.id_admin = tbl_data_apt.id_apotek');
		$this->db->order_by('id_users','desc');

		if($id_users != ''){
			$this->db->where('id_users', $id_users);
		}

		if($pencarian != ''){

			$this->db->like('username', $pencarian);
			$this->db->or_like('id_users', $pencarian);
			$this->db->or_like('id_level', $pencarian);
			$this->db->or_like('email', $pencarian);
			$this->db->or_like('status', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function get_users(){

		$id_users 	= $this->input->post('id_users');
		$this->db->select('*');
		$this->db->from('tbl_users');
		$this->db->join('tbl_level','tbl_users.id_level = tbl_level.id_level');
		$this->db->join('tbl_data_apt','tbl_users.id_admin = tbl_data_apt.id_apotek');
		$this->db->where('id_users',$id_users);
		return $this->db->get();

	}

	function get_level(){

		$id_admin 	= $this->session->userdata('id_admin');
		return $this->db->get_where('tbl_level',array('id_admin'=>$id_admin));

	}

	function get_apotek(){

		$id_admin 	= $this->session->userdata('id_admin');
		return $this->db->get_where('tbl_data_apt',array('id_admin'=>$id_admin));

	}

	function update(){

		$id_users	= $this->input->post('id_users');
		
		$data = array(
			
			'username' 		=> $this->input->post('username'),
			'password' 		=> $this->input->post('password'),
			'email' 		=> $this->input->post('email'),
			'status' 		=> $this->input->post('status'),
			'id_level' 		=> $this->input->post('id_level'),
			'id_apotek' 	=> $this->input->post('id_apotek'),
			'updated_at' 	=> date("Y-m-d H:i:s"),

		);

		$this->db->where('id_users',$id_users);
		$this->db->update('tbl_users',$data);
		redirect('medliz/users');

	}
}