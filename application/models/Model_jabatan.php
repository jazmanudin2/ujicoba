<?php class Model_jabatan extends CI_Model{


	function view_jabatan(){

		$id_admin 	= $this->session->userdata('id_admin');
		$query 		= "SELECT * FROM tbl_jabatan ORDER BY nama_jabatan ASC";
		return $this->db->query($query);

	}

	function insert(){

		$data = array(
			
			'id_jabatan' 		=> $this->input->post('id_jabatan'),
			'nama_jabatan' 		=> $this->input->post('nama_jabatan'),
			'keterangan' 		=> $this->input->post('keterangan'),
			'created_at' 		=> date("Y-m-d H:i:s"),
			'id_admin' 			=> $this->session->userdata('id_admin'),

		);

		$this->db->insert('tbl_jabatan',$data);
		redirect('apotek/jabatan');
		
	}

	function delete(){

		$id_jabatan	= $this->uri->segment(4);
		$this->db->delete('tbl_jabatan',array('id_jabatan' => $id_jabatan));

	}

	public function getData($rowno,$rowperpage,$id_jabatan="",$pencarian="") {
		
		$this->db->select('*');
		$this->db->from('tbl_jabatan');
		$this->db->where('tbl_jabatan.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_jabatan','desc');

		if($id_jabatan != ''){

			$this->db->where('id_jabatan', $id_jabatan);

		}

		if($pencarian != ''){

			$this->db->like('nama_jabatan', $pencarian);
			$this->db->or_like('id_jabatan', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}

		$this->db->limit($rowperpage, $rowno); 
		$query = $this->db->get();
		return $query->result_array();
	}

 	// Select total records
	public function getRecordCount($id_jabatan = "" ,$pencarian="") {

		$this->db->select('count(*) as allcount');
		$this->db->from('tbl_jabatan');
		$this->db->where('tbl_jabatan.id_admin',$this->session->userdata('id_admin'));
		$this->db->order_by('id_jabatan','desc');

		if($id_jabatan != ''){

			$this->db->where('id_jabatan', $id_jabatan);
			
		}

		if($pencarian != ''){

			$this->db->like('nama_jabatan', $pencarian);
			$this->db->or_like('id_jabatan', $pencarian);
			$this->db->or_like('keterangan', $pencarian);
			$this->db->or_like('created_at', $pencarian);
			$this->db->or_like('updated_at', $pencarian);

		}
		$query  = $this->db->get();
		$result = $query->result_array();
		return $result[0]['allcount'];
	}


	function get_jabatan(){

		$id_jabatan 	= $this->input->post('id_jabatan');
		return $this->db->get_where('tbl_jabatan',array('id_jabatan'=>$id_jabatan));

	}

	function update(){

		$id_jabatan	= $this->input->post('id_jabatan');
		
		$data = array(
			
			'nama_jabatan' 			=> $this->input->post('nama_jabatan'),
			'keterangan' 			=> $this->input->post('keterangan'),
			'updated_at' 			=> date("Y-m-d H:i:s"),
			'id_admin' 				=> $this->session->userdata('id_admin'),

		);

		$this->db->where('id_jabatan',$id_jabatan);
		$this->db->update('tbl_jabatan',$data);
		//Notifikasi
		$this->session->set_flashdata('msg',
			'<div class="alert alert-success">
			<h4>Successfully</h4>
			<p>Data Berhasil Di Ubah</p>

			</div>');

		redirect('apotek/jabatan');

	}
}